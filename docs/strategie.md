
# Stratégie Globers

## Diagnostique sur l'état du marché


1. Une part significative (>5%) de la population est prête à payer pour une information de qualité:
    * Succès déjà aujourd'hui d'offres payantes alors qu'elles sont imparfaites


2. Il le ferons, car l'information de qualité en ligne sera de plus en plus derriere des paywall
    * La publicité en ligne ne peut pas financer une information de qualité
        * L'usage d'ad-blockers ne cesse d'augmenter, en particulier sur smartphone
        * RGPD complique le targeting et aggrave le rapport de force entre les media et les plateformes pub
        * L'usage smartphone rend la publicité encore plus pénible (temps de chargement, surface d'affichage limitée)
        * La publicité créé des conflits d'intérêts et des de mauvais incentives (contenu sponsorisé, quantité à tout prix)

3. La meilleur façon de faire payer est par un abonnement
    * C'est ce qui marche pour la musique, les séries, les principaux media en ligne
    * Les autres approches (membership, dons, micro-payments) restent marginales


4. Sur internet, les grands media généralistes ne sont pas pertinants par rapport à une multitude de media spécialistes
    * La spécialisation est clé pour la qualité, certains sujets ne sont pas dans l'ADN du media et sont mal traités (ex: actu nationale pour les journaux locaux, sport pour les média généralistes...)
    * Les medias spécialisés permettent de créer des communautés homogènes autour d'eux
    * Sur le gratuit, les médias généralistes subissent depuis longtemps la concurrence des blogs spécialisés


5. Cependant, pour justifier un abonnement, il faut une quantité importante de contenu
    * Succès des plus grands media généralistes (NYT, LeMonde)
    * Succès des offres très larges (Spotify / Netflix) sur le piratage
    * Nécessité de créer un reflexe d'usage pour améliorer la retention
    * Le coût marginal d'une impression suplémentaire est nul, avoir une multitude de mini-paywall n'est pas compris par les clients


6. Payer pour un media n'est pas une démarche purement consumériste, c'est aussi une façon d'affirmer ses convictions
    * Succès de medias engagés (Mediapart)


## Solution

Une plateform où l'on paye (1, 2) un abonnement (3) pour accéder à une multitude de media (4) depuis une application mobile (5), en choisissant le media que l'on souhaite financer avec son abonnement (6)

## Obstacles à surmonter

* Pour faire payer les lecteurs il faut:
    * Du contenu exclusif, de qualité et en quantité
    * Des noms de media qu'ils connaissent et aiment déjà suivre
    * qu'ils soient exposés à du contenu qu'ils aimeraient lire pour déclancher l'abonnement

* Pour faire venir les media il faut:
    * Etre crédible
    * Prouver que la plateforme permet de générer plus de revenus que seul
    * Inspirer confiance sur le fait qu'ils ne seront pas "commoditisé" en étant mélangés

__En résumé: pour faire venir les lecteurs il faut un maximum de media, pour faire venir les media il faut un maximum de lecteurs__

La grande difficulté consiste à enclancher le cercle vertueux suivant:

Plus de lecteurs -> plus de crédibilité et de valeur pour les media -> plus de media -> plus de contenu -> plus de visibilité et de valeur pour les lecteurs -> plus de lecteurs

Et donc casser la situation initiale:

Pas de lecteurs -> pas de crédibilité, pas de valeurs pour les media -> difficile de convaincre les media -> pas de visibilité ni de valeur pour les lecteurs -> pas le lecteurs

## Principes directeurs pour les surmonter

### 1. Faire venir les media sans lecteurs

1. Apporter une plus-value même sans autre médias sur la plateforme
2. Proposer une approche sans risque, non engageante, et sans coût d'entrée pour un media
3. Cibler le marketing pour gagner en crédibilité et en "brand awareness"
4. Rassurer sur la "non-commoditéisation" du média
5. Isoler et démarcher les types de média le plus réceptifs à la proposition

### 2. Faire venir les lecteurs sans media

1. Proposer du contenu autrement que par le système cible
2. Proposer une plus-value en terme d'expérience utilisateur
3. Susciter une adhésion à l'esprit du projet
4. Isoler et démarcher la clientele la plus réceptive à la proposition

## Actions associées

1. Faire venir les media sans lecteurs
    1. Plus-value même sans autre médias
        * Proposer un service tout intégré sans setup: domaine + CMS + appli mobile + système de paiement + outils analytics + outil de partage + SEO
    2. approche sans risque
        * Scraper directement le contenu depuis un wordpress (zero setup pour le site, juste donner son accord)
        * Fournir un service d'export de son contenu vers wordpress
    3. gagner en crédibilité
        * Content marketing autour du monde des media
        * Podcast où l'on interview les media
        * Essayer de faire parler de soi dans les media lus par les journalistes (arrêt sur image, Le Parisien...)
    4. non-commoditéisation
        * Garder une structure par media dans l'application
        * Possibiliter d'apporter un thème graphique et des catégories sous chaque média
    5. média le plus réceptifs
        * Tester différents secteur 
            * Nouveaux media en ligne
            * Media classiques
            * Media spécialisés
            * Média locaux
            * Bloggers spécialisés
            * Journalistes pour les minorités étrangères
            * Pigistes / journalistes indépendants
            * Etudiants en école de journalisme
        * Tester différents chanels d'acquisition

2. Faire venir les lecteurs sans media
    1. Proposer du contenu
        * Proposer aux journaux et aux blogs de scraper leur contenu gratuit ou une partie "démo" de leur contenu
        * Créer directement du contenu:
            * Recruter des stagiaires
            * Payer des pigistes
            * Ecrire soit-même
            * S'associer avec un journaliste
    2. plus-value expérience utilisateur
        * Contenu disponible offline
        * Curation du contenu extérieur
    3. adhésion à l'esprit du projet
        * Content marketing
        * Créer une communauté sur les réseaux sociaux
    4. clientele la plus réceptive
        * Publicité ciblée sur facebook
            * Lecteurs d'autres journaux payants
            * Lecteurs avec des centres d'intérêts addressés par la plateforme
            * Lecteurs sensibles aux problématiques du journalisme
        * Tester différents chanels d'acquisition
