# Debug nodes app with typescript and nodemon

https://stackoverflow.com/questions/38525404/how-to-debug-typescript-in-intellij-idea?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa

=> don't seem to work, What I use: 
https://blog.jetbrains.com/webstorm/2017/09/debugging-node-js-apps-in-webstorm/

(but it requires to compile before)


Before update of ts-node to 7.0.0 i made ts-node work with command options
--inspect --require ts-node/register --compilerOptions {\"module\":\"commonjs\"}
but it doesn't work anymore (don't know why)

# Install Google cloud sql proxy:

https://cloud.google.com/sql/docs/mysql/instance-access-control#dynamicIP
https://cloud.google.com/sql/docs/mysql/connect-admin-proxy
https://stackoverflow.com/questions/41215667/google-cloud-sql-proxy-couldnt-find-default-credentials

# Run sql proxy

./cloud_sql_proxy -instances=insidoor-182517:us-east1:test-mysql-globers=tcp:3307

# Open shell in docker container

docker run -it globers-front /bin/sh

# Who lock a port

On mac: lsof -i tcp:3000

# Execute unit tests in intellij with gradle to apply plugins (default constructor generation...)

http://mrhaki.blogspot.com/2016/03/gradle-goodness-configure-intellij-idea.html


# With jest issue (data null)

Check react-apollo/test-utils.js L821: where we get response by key 
var responses = this.mockedResponsesByKey[key]


  logoDiv: {
    width: 160,
    height: 100,
    float: 'right'
  },

# check issue db script docker container logs
docker logs globers-db-instance


# Pour extraire tous les emails d'un document


(.+)@(.*)\.(.*)

Pour aller cherche les emails :
Dans VS code, regex [a-zA-Z0-9_+&*-]+(?:\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}
Puis selectionné tout ce qui est trouvé avec Alt+Enter
Puis coller autre part