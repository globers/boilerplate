
USE globers;
SET NAMES utf8;



INSERT INTO media VALUES
(NULL, 'media-uri-medium', 'Medium', 'https://upload.wikimedia.org/wikipedia/commons/b/b1/Medium_logo_Wordmark_Black.svg', 'This is Medium', '', '', 'div.section-content', false),
(NULL, 'media-uri-liberation', 'Libération', 'https://upload.wikimedia.org/wikipedia/commons/0/0a/Lib%C3%A9ration.svg', 'div.article-body', 'C est libé', '', '', false);

SET @articleUri1Media1 = LOAD_FILE('/var/lib/mysql-files/article_1.html');

INSERT INTO article Values
	(NULL, 'article-uri-1-media-1', 'How to Spot a Sketchy Spiritual Guru', 'SUBTITLE', NULL, NULL, @articleUri1Media1, 1, 'https://cdn-images-1.medium.com/max/2000/1*_D0SCRqZy95FwUhBbG687g.jpeg', NULL, false),
	(NULL, 'article-uri-2-media-1', 'Your Online Data Is In Peril. The Blockchain Could Save It', 'SUBTITLE', NULL, NULL, 'content Second blablaba', 1, 'https://cdn-images-1.medium.com/max/2000/1*AC3T6bT4_E5N81lNsc1Piw.png', NULL, false);

INSERT INTO article Values
	(NULL, 'article-uri-1-media-2', 'Macron et le collégien, la blague ratée et le coup de badine', 'SUBTITLE', NULL, NULL, 'content First blablaba', 2, 'http://md1.libe.com/photo/1132658-macron-appel-18-juin-afp.jpg', NULL, false),
	(NULL, 'article-uri-2-media-2', 'Est-ce que 40% de personnes ne touchent pas les aides sociales auxquelles elles ont droit, comme le dit Mélenchon ?', 'SUBTITLE', NULL, NULL, 'content Second blablaba', 2, 'http://md1.libe.com/photo/1110689-000_13c0u5.jpg', NULL, false);
