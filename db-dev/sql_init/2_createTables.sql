
USE globers;
SET NAMES utf8;

CREATE TABLE user (
	id INT NOT NULL AUTO_INCREMENT,
    uri VARCHAR(255) NOT NULL,
	first_name VARCHAR(255) NOT NULL,
	last_name VARCHAR(255) NOT NULL,
	email VARCHAR(255) NOT NULL,
	pswd VARCHAR(255) NOT NULL,
	PRIMARY KEY (id),
    UNIQUE(uri),
	UNIQUE(email)
);

CREATE TABLE media (
	id INT NOT NULL AUTO_INCREMENT,
    uri VARCHAR(255) NOT NULL,
	name VARCHAR(255) NOT NULL,
	logo_url VARCHAR(2046) NOT NULL,
	description TEXT NOT NULL,
	theme_primary_color VARCHAR(60) NOT NULL,
	theme_secondary_color VARCHAR(60) NOT NULL,
	origin_html_content_selector VARCHAR(255),
	unpublished BOOLEAN NOT NULL,
	PRIMARY KEY (id),
    UNIQUE(uri)
);

CREATE TABLE creator (
	id INT NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY(user_id) REFERENCES user(id),
	UNIQUE(user_id)
);

CREATE TABLE media_contributor (
	creator_id INT NOT NULL,
	media_id INT NOT NULL,
	FOREIGN KEY(creator_id) REFERENCES creator(id),
	FOREIGN KEY(media_id) REFERENCES media(id),
	PRIMARY KEY (creator_id, media_id)
);


CREATE TABLE article (
	id INT NOT NULL AUTO_INCREMENT,
    uri VARCHAR(255) NOT NULL,
    title VARCHAR(255) NOT NULL,
	subtitle VARCHAR(2046) NOT NULL,
	origin_url VARCHAR(2046),
	origin_html MEDIUMTEXT,
    content MEDIUMTEXT,
	media_id INT NOT NULL,
	image_url VARCHAR(2046) NOT NULL,
	publication_datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
	unpublished BOOLEAN NOT NULL,
	PRIMARY KEY (id),
    UNIQUE(uri),
	FOREIGN KEY(media_id) REFERENCES media(id)
);


CREATE TABLE article_draft (
	id INT NOT NULL AUTO_INCREMENT,
	uri VARCHAR(255) NOT NULL,
	media_id INT NOT NULL,

	PRIMARY KEY (id),
	UNIQUE(uri),
	FOREIGN KEY(media_id) REFERENCES media(id)
);

CREATE TABLE article_draft_revision (

	id INT NOT NULL AUTO_INCREMENT,
	uri VARCHAR(255) NOT NULL,

	/* fields to be copied to article once published */
    title VARCHAR(255),
	subtitle VARCHAR(2046),
    content MEDIUMTEXT,
	image_url VARCHAR(2046),

	/* fields for edition management */
	article_draft_id INT NOT NULL,
	slatejs_value MEDIUMTEXT,
	saving_datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
	creator_id INT NOT NULL,

	/* constraints */
	PRIMARY KEY (id),
	UNIQUE(uri),
	FOREIGN KEY(article_draft_id) REFERENCES article_draft(id),
	FOREIGN KEY (creator_id) REFERENCES creator(id)
);


CREATE TABLE article_publication (
	id INT NOT NULL AUTO_INCREMENT,
	article_id INT NOT NULL,
	article_draft_id INT NOT NULL,

	PRIMARY KEY (id),
	UNIQUE(article_id),
	UNIQUE(article_draft_id),
	FOREIGN KEY(article_id) REFERENCES article(id),
	FOREIGN KEY(article_draft_id) REFERENCES article_draft(id)
);

CREATE TABLE article_publication_revision (
	id INT NOT NULL AUTO_INCREMENT,
	article_publication_id INT NOT NULL,
	article_draft_revision_id INT NOT NULL,
	publication_datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,

	PRIMARY KEY (id),
	FOREIGN KEY(article_publication_id) REFERENCES article_publication(id),
	FOREIGN KEY(article_draft_revision_id) REFERENCES article_draft_revision(id)
);

CREATE TABLE article_consumer (
	user_id INT NOT NULL,
	article_id INT NOT NULL,
	bookmark BOOLEAN NOT NULL,
	FOREIGN KEY(user_id) REFERENCES user(id),
	FOREIGN KEY(article_id) REFERENCES article(id),
	PRIMARY KEY (user_id, article_id)
);

CREATE TABLE media_consumer (
	user_id INT NOT NULL,
	media_id INT NOT NULL,
	follow BOOLEAN NOT NULL,
	FOREIGN KEY(user_id) REFERENCES user(id),
	FOREIGN KEY(media_id) REFERENCES media(id),
	PRIMARY KEY (user_id, media_id)
);

CREATE TABLE reset_password (
	uri VARCHAR(255) NOT NULL,
	user_id INT NOT NULL,
	reset_datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,

	FOREIGN KEY(user_id) REFERENCES user(id),
	PRIMARY KEY (uri)
);