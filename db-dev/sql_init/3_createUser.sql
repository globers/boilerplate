
USE globers;
SET NAMES utf8;

CREATE USER globers_api_db IDENTIFIED BY 'test-globers-psw';
GRANT ALL ON globers.* TO globers_api_db;
