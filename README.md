


## Environment

1. Make sure you have installed the following dependencies

    * `Git`
    * `Docker`
    * `JDK` 8
    * `Node.js` 9.11 and `npm`

2. Recommended as development environment

    * `ÌntelliJ IDEA` with `Gradle` plugin
    * `Chrome` with plugins `React developer tools` and `Apollo client developer Tool`
    * `MySQLWorkbench`

3. Lint and formatting

    * Add typescript linter: In IntelliJ, `Preference > Languages & Frameworks > Typescript > TSLint`
        * Check `enable`
        * Select `{ROOT_FOLDER}/front/node_modules/tslint` as TSLint package
        * Select `Automatic search`

    * Add Typescript prettier (reformat files on save):
        * If not installed, install `File Watchers` plugin
        * In IntelliJ, `Preferences > Tools > File Watchers`. Add two rules:
            * Prettier .ts:
                * File type: `TypeScript`
                * Scope: `Project Files`
                * Program: `{ROOT_FOLDER}/front/node_modules/.bin/prettier`
                * Arguments: `--write $FilePathRelativeToProjectRoot$`
                * Output paths to refresg: `$FilePathRelativeToProjectRoot$`
            * Prettier .tsx: Same config but with
                * File Type: `Typescript JSX`

## Installation & run

1. Database
    * build db: execute `rebuild_db.sh` from `db-dev`folder
    * run db: execute `run_db.sh`from `db-dev`folder
    * This will start mysql database on `localhost:3306`, you can connect to it with login `root`, password `globers-db`

2. In back folder
    * With command line:
        * Execute `./gradlew bootRun`
    * With IntelliJ:
        * Open back project in IntelliJ
        * In Run/Debug configuration, add gradle task
            * Gradle project: `back`
            * Task: `bootRun`
        * Run created task
    * This will start back server on `localhost:8080`, you can hit graphql API directly in the browser at `localhost:8080/graphiql`
    * __NB: If you don't have `Google cloud SDK` installed, it will raise an gcp authentication error (but it still works)__

3. In front folder
    * Install dependencies with command `npm install`
    * Start server with command `npm run dev`
    * This will start front server on `localhost:3000`


## Before a push

* Check unit tests on back: `./gradlew test`
* CHeck linter in front: `npm run lint`
* Apply typescript prettier (if you don't do it by IntelliJ plugin): `npm run prettier`
* Check e2e tests (you must have firefox installed)
    * Start front and back, run in front `npm run cafe`
    * If it fails, you can see what happens with `npm run cafehead`

## Architecture overview

TODO

## Tests & lint to launch before commit

TODO

## Common tasks

### Add Graphql endpoint

### Modify database scheme (liquibase)

### Add Mutation react HOC (apollo)

### 

