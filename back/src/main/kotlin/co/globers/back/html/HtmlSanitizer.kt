package co.globers.back.html

import org.owasp.html.HtmlPolicyBuilder
import org.owasp.html.PolicyFactory
import org.owasp.html.Sanitizers

object HtmlSanitizer {

    private val imagesWithoutSize = HtmlPolicyBuilder()
            .allowUrlProtocols("http", "https").allowElements("img")
            .allowAttributes("alt", "src").onElements("img")
            .toFactory()

    private val policy: PolicyFactory = Sanitizers.BLOCKS
            .and(Sanitizers.TABLES)
            .and(Sanitizers.FORMATTING)
            .and(imagesWithoutSize)
            .and(Sanitizers.LINKS)

    fun sanitizeHtml(html: String): String {
        return policy.sanitize(html)
    }
}

