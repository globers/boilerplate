package co.globers.back.html

import co.globers.back.html.HtmlSanitizer.sanitizeHtml
import org.jsoup.Jsoup
import org.jsoup.select.Elements



fun removeDuplicatedImages(article: Elements, mainImageUrl: String) {
    val imageUrl = article.select("img[src=$mainImageUrl]")
    imageUrl.remove()  // remove images identical to the main image
}

fun removeSocialButtons(article: Elements) {
    article.select(".fsb-social-bar").remove() // wordpress plugin for social network buttons
}

fun extractContent(originHtml: String, mainImageUrl: String, selector: String) : String {

    val contentElements = Jsoup.parse(originHtml).select(selector)

    removeDuplicatedImages(contentElements, mainImageUrl)
    removeSocialButtons(contentElements)

    return sanitizeHtml(contentElements.outerHtml())
}

