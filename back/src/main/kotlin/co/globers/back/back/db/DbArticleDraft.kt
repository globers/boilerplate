package co.globers.back.back.db

import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "article_draft")
data class DbArticleDraft(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long = 0,
        val uri: String,
        val mediaId: Long
)


@Entity
@Table(name = "article_draft_revision")
data class DbArticleDraftRevision(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long = 0,
        val uri: String,

        val title: String?,
        val subtitle: String?,
        val content: String?,
        val imageUrl: String?,
        val slatejsValue: String?,

        val articleDraftId: Long,
        val savingDatetime: java.sql.Timestamp,
        val creatorId: Long
)

@Repository
interface ArticleDraftRepository : JpaRepository<DbArticleDraft, Long> {

    fun findByMediaId(mediaId: Long): List<DbArticleDraft>
    fun findByUri(uri: String): DbArticleDraft?


    @Query("SELECT d, a, p from DbArticleDraft d " +
            "LEFT JOIN DbArticlePublication p ON d.id = p.articleDraftId " +
            "LEFT JOIN DbArticle a ON a.id = p.articleId " +
            "WHERE a.mediaId = :mediaId")
    fun findDraftAndArticleByMediaId(mediaId: Long): List<Array<Any>>

}

@Repository
interface ArticleDraftRevisionRepository : JpaRepository<DbArticleDraftRevision, Long> {

    fun findByArticleDraftId(articleDraftId: Long, pageable: Pageable)
            : List<DbArticleDraftRevision>

    fun findByUri(uri: String): DbArticleDraftRevision?

}

