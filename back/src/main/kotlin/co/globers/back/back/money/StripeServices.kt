package co.globers.back.back.money

import co.globers.back.back.graphql.Media
import co.globers.back.back.graphql.StripeAccountInput
import co.globers.back.back.graphql.StripeCountry
import co.globers.back.back.graphql.StripeCurrency
import co.globers.back.back.graphql.User
import co.globers.back.back.utils.GlobersConfig
import co.globers.back.back.utils.generateUriByHash
import com.stripe.Stripe
import org.springframework.stereotype.Component
import com.stripe.model.Account
import java.io.File as JavaFile
import com.stripe.model.File as StripeFile
import com.stripe.model.Customer
import com.stripe.model.Plan
import com.stripe.model.Product
import com.stripe.model.Subscription
import com.stripe.net.RequestOptions
import java.sql.Timestamp
import java.time.LocalDateTime

@Component
class StripeServices(globersConfig: GlobersConfig) {

    init {
        Stripe.apiKey = globersConfig.stripe.api_key
    }


    fun buildMediaAccount(stripeAccountInput: StripeAccountInput, media: Media, mediaOwner: User): String {

        val params = mapOf(
                "type" to "custom",
                "country" to stripeAccountInput.country.name,
                "email" to mediaOwner.email,
                "account_token" to stripeAccountInput.accountTokenId,
                "business_name" to "${media.name} - Globers",
                "business_url" to "https://globers.co/m/${media.uri}",
                "default_currency" to stripeAccountInput.defaultCurrency.name,
                "external_account" to stripeAccountInput.externalAccountTokenId,
                "product_description" to "'${media.name}', online media on Globers publication platform, paid by a monthly subscription",
                "metadata" to mapOf("mediaUri" to media.uri, "userUri" to mediaOwner.uri)
        )

        val stripeAccount = Account.create(params)

        return stripeAccount.id
    }

    fun buildMediaCustomer(client: User, userSourceTokenId: String, media: Media, stripeMediaAccountId: String): String {

        // must be 3-12 uppercase letters or numbers.
        val invoicePrefix = generateUriByHash("${client.uri}/${media.uri}")
                .toUpperCase()
                .substring(0, 12)

        val params = mapOf(
                "source" to userSourceTokenId,
                "email" to client.email,
                "description" to "${client.firstName} ${client.lastName} for '${media.name}'",
                "invoice_prefix" to invoicePrefix,
                "metadata" to mapOf("mediaUri" to media.uri, "userUri" to client.uri)
        )

        val customer = Customer.create(params, getMediaAccountRequestOptions(stripeMediaAccountId))
        return customer.id
    }

    fun buildMediaProduct(media: Media, stripeMediaAccountId: String): String {

        // displayed on your customer’s credit card statement. This may be up between 5 and 22 characters
        val statementDescriptor = "Globers|${media.uri}".take(22)

        val params = mapOf(
                "name" to "subscription to '${media.name}'",
                "type" to "service",
                "metadata" to mapOf("mediaUri" to media.uri),
                "statement_descriptor" to statementDescriptor
        )

        val product = Product.create(params, getMediaAccountRequestOptions(stripeMediaAccountId))
        return product.id
    }

    fun buildMediaPlan(media: Media, stripeMediaAccountId: String, stripeMediaProductId: String, mediaSubscriptionUri: String, plan: PricingPlan): String {

        val params = mapOf(
                "currency" to plan.currency.name.toLowerCase(), // money expect lowercase on this field
                "interval" to "month",
                "product" to stripeMediaProductId,
                "amount" to plan.amountByMonthInCents,
                "metadata" to mapOf("mediaUri" to media.uri, "mediaSubscriptionUri" to mediaSubscriptionUri),
                "nickname" to "standard-month-${plan.currency}-${plan.priceInCurrency}"
        )

        val stripePlan = Plan.create(params, getMediaAccountRequestOptions(stripeMediaAccountId))
        return stripePlan.id
    }


    fun buildCustomerSubscriptionToPlan(
            client: User, media: Media,
            stripeCustomerId: String, stripeMediaAccountId: String, stripePlanId: String,
            globersFeeInPercent: Double,
            mediaStripeAccountCountry: StripeCountry): String {

        val timestampFirstNextMonth = computeTimestampFirstDayNextMonth()

        val vatOnFee = getVatAsRatio(mediaStripeAccountCountry)
        val applicationFeePercent = globersFeeInPercent * (1 + vatOnFee) // we manage only journalists as individuals (exemption basis of VAT), they must pay VAT
        val vatOnSubscriptionPercent = 0 // we manage only individuals in "exemption basis of VAT" (franchise de base de TVA) => no VAT

        val params = mapOf(
                "customer" to stripeCustomerId,
                "application_fee_percent" to applicationFeePercent,
                "billing" to "charge_automatically",
                "billing_cycle_anchor" to timestampFirstNextMonth,
                "items" to mapOf("0" to mapOf("plan" to stripePlanId)),
                "metadata" to mapOf(
                        "mediaUri" to media.uri,
                        "userUri" to client.uri,
                        "mediaStripeAccountCountry" to mediaStripeAccountCountry.name,
                        "vatOnFee" to vatOnFee
                ),
                "prorate" to false,
                "tax_percent" to vatOnSubscriptionPercent
        )

        val subscription = Subscription.create(params, getMediaAccountRequestOptions(stripeMediaAccountId))
        return subscription.id
    }


    private fun computeTimestampFirstDayNextMonth(): Long {
        var startDate = LocalDateTime.now()
        if (startDate.dayOfMonth != 1) {
            startDate = startDate.plusMonths(1).withDayOfMonth(1).withHour(12)
        }

        return Timestamp.valueOf(startDate).toInstant().epochSecond
    }


    private fun getMediaAccountRequestOptions(stripeMediaAccountId: String) = RequestOptions.builder()
            .setStripeAccount(stripeMediaAccountId)
            .build()


    private fun getVatAsRatio(country: StripeCountry): Double {
        return when (country) {
            StripeCountry.FR -> 0.2
        }
    }



}

data class PricingPlan(val currency: StripeCurrency, val amountByMonthInCents: Int, val priceInCurrency: Double)
