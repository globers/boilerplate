package co.globers.back.back.utils

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@Component
@ConfigurationProperties("globers")
class GlobersConfig {

    lateinit var jwtsignature: String
    lateinit var app_url: String

    val cloudinary = Cloudinary()
    val sendGrid = SendGrid()
    val stripe = Stripe()

    @Suppress("PropertyName", "VariableNaming")
    class Cloudinary {

        lateinit var cloud_name: String
        lateinit var api_key: String
        lateinit var api_secret: String
        lateinit var environment: String
    }

    @Suppress("PropertyName", "VariableNaming")
    class SendGrid {
        lateinit var api_key: String
        lateinit var sender: String
    }

    @Suppress("PropertyName", "VariableNaming")
    class Stripe {
        lateinit var api_key: String
    }
}
