package co.globers.back.back.db

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table


@Entity
@Table(name = "creator")
data class DbCreator(

        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long = 0,
        val userId: Long
)

@Repository
interface CreatorRepository : JpaRepository<DbCreator, Long> {

    fun findByUserId(userId: Long): DbCreator?

    @Query("SELECT c from DbCreator c " +
            "JOIN DbUser u on c.userId = u.id " +
            "WHERE u.uri = :userUri")
    fun findByUserUri(userUri: String): DbCreator

}