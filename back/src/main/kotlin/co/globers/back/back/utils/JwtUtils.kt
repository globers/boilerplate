package co.globers.back.back.utils

import graphql.GraphQLException
import graphql.schema.DataFetchingEnvironment
import graphql.servlet.GraphQLContext
import io.jsonwebtoken.JwtException
import io.jsonwebtoken.JwtParser
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.security.Keys
import org.springframework.stereotype.Component
import java.security.Key
import java.time.Instant
import java.util.*

@Component
class JwtUtils(private val globersConfig: GlobersConfig) {

    fun buildUserToken(userUri: String): String {

        val key: Key = Keys.hmacShaKeyFor(globersConfig.jwtsignature.toByteArray(Charsets.US_ASCII))

        val now = Date.from(Instant.now())
        return Jwts.builder()
                .setIssuer("api.globers.co")
                .setAudience("app.globers.co")
                .setSubject(userUri)
                .claim("role", "user")
                .setIssuedAt(now)
                .setNotBefore(now)
                .signWith(key)
                .compact()
    }

    private fun getUserUriIfTokenValid(token: String): String {

        val key: Key = Keys.hmacShaKeyFor(globersConfig.jwtsignature.toByteArray(Charsets.US_ASCII))
        val jwtParser: JwtParser = Jwts.parser().setSigningKey(key)

        val jws = jwtParser.parseClaimsJws(token)
        if (jws.body["role"] == "user") return jws.body.subject
        else throw JwtException("invalid user JWT token")
    }

    fun tryGetUserUri(jwtToken: String?, env: DataFetchingEnvironment) : String? {

        if (jwtToken != null) return getUserUriIfTokenValid(jwtToken)

        val authorizationHeader = env.getContext<GraphQLContext>().httpServletRequest.get().getHeader("Authorization")

        return if ( authorizationHeader == null || authorizationHeader == "")  null else {
            val token = authorizationHeader.replace("Bearer ", "")
            getUserUriIfTokenValid(token)
        }
    }

    fun getUserUri(jwtToken: String?, env: DataFetchingEnvironment) : String {
        return tryGetUserUri(jwtToken, env) ?: throw GraphQLException("GraphQL request requires authenticated user")
    }
}
