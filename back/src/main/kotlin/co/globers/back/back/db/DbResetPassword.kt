package co.globers.back.back.db

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.sql.Timestamp
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "reset_password")
data class DbResetPassword(
        @Id
        val uri: String,
        val userId: Long,
        val resetDatetime: Timestamp
)


@Repository
interface ResetPasswordRepository : JpaRepository<DbResetPassword, String> {

}
