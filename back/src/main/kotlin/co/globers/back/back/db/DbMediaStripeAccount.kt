package co.globers.back.back.db

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table


@Entity
@Table(name = "media_stripe_account")
data class DbMediaStripeAccount(

        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long = 0,
        val mediaId: Long,
        val ownerId: Long,
        val stripeAccountId: String,
        val country: String
)

@Repository
interface MediaStripeAccountRepository : JpaRepository<DbMediaStripeAccount, Long> {

    fun existsByMediaId(mediaId: Long): Boolean
    fun findByMediaId(mediaId: Long): DbMediaStripeAccount

}