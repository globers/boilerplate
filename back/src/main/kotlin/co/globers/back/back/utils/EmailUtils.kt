package co.globers.back.back.utils

import co.globers.back.back.graphql.Media
import co.globers.back.back.graphql.User
import com.sendgrid.Content
import com.sendgrid.Email
import com.sendgrid.Mail
import com.sendgrid.SendGrid
import org.springframework.stereotype.Component
import com.sendgrid.Method
import com.sendgrid.Request
import org.slf4j.LoggerFactory
import java.io.IOException


@Component
class EmailUtils(private val globersConfig: GlobersConfig) {

    private val logger = LoggerFactory.getLogger(EmailUtils::class.java)


    fun sendResetPasswordEmail(emailTo: String, firstName: String, generatedUri: String): Boolean{

        val subject = "Globers: password reset"
        val link = "${globersConfig.app_url}/resetPwd?uri=$generatedUri"

        val body = """

Hi $firstName,
we got a request to reset your Globers password.
Follow this link to reset your password:

    $link

This link is only valid for 1 hour.
If you did not request a password reset, please ignore this email or contact support@globers.co if you have questions.

Thanks,
The Globers team

https://globers.co
"""

        return sendEmail(emailTo, subject, body)
    }


    fun sendAccountCreatedEmail(userEmail: String, userFirstName: String) : Boolean {

        val subject = "Bienvenue sur Globers"

        val body = """

Bonjour $userFirstName, bienvenue sur Globers.co !

Tu veux créer ton média et gagner de l’argent avec tes abonnés ?
Tu veux soutenir le journalisme indépendant ?

Tu es au bon endroit !

Si tu as des questions ou des remarques, nous sommes à ta dispo par mail ou sur twitter (@HelloGlobers)

A bientôt sur https://globers.co

"""
        return sendEmail(userEmail, subject, body)
    }

    fun sendMediaCreatedEmail(userEmail: String, mediaName: String, mediaUri: String) : Boolean {

        val subject = "'$mediaName' créé sur Globers"

        val body = """

Félicitation pour la création de ton média

	$mediaName

Il est déjà en ligne à l’adresse suivante :

	https://globers.co/m/$mediaUri

Si tu as des questions ou remarques, la team Globers est a ton service à ce mail ou sur notre chaine slack: https://globers-creators.slack.com

Pour générer tes premiers abonnés, voici quelques conseils :

1. Pour être payé

    Assure-toi d’avoir bien connecté tes coordonnés bancaires à ton média. C’est de cette façon que tu percevras l’argent de tes abonnés.

2. Pour attirer des lecteurs

    Adresse-toi à tes futurs lecteurs en diffusant tes articles à ton réseau. Profite de ton audience sur facebook, twitter, instagram pour faire parler de ton média. Tu peux également poster tes articles sur des groupes et pages facebook en rapport avec la thématique que tu abordes. Sur twitter, utilises les hashtags et identifie des personnes susceptibles d’être intéressées par l’article en question.

3. Pour convertir tes lecteurs en abonnés

    Pour convaincre tes lecteurs de s’abonner à ton média, penses à allier spécificité et régularité. Plus ton angle sera original et se démarquera du contenu gratuit, et plus tes abonnés prennent l’habitude de te lire, plus il sera facile de générer des abonnements.


Bonne chance dans cette nouvelle aventure professionnelle.

A très vite!

La team Globers

hello@globers.co

        """

        return sendEmail(userEmail, subject, body)
    }







    private fun sendEmail(emailTo: String, subject: String, body: String): Boolean {

        val sendgridConfig = globersConfig.sendGrid
        val sendgrid = SendGrid(sendgridConfig.api_key)
        val from = Email(sendgridConfig.sender, "Globers")
        val to = Email(emailTo)
        val content = Content("text/plain", body)

        val mail = Mail(from, subject, to, content)

        val request = Request()
        try {
            request.method = Method.POST
            request.endpoint = "mail/send"
            request.body = mail.build()
            val response = sendgrid.api(request)

            val statusCode = response.statusCode
            return statusCode >= 200 || statusCode <= 299
        } catch (ex: IOException) {
            logger.error("error in sendEmail to $emailTo with subject $subject")
            logger.error(ex.toString())
            return false
        }
    }



}

