package co.globers.back.back.graphql.queryresolvers

import co.globers.back.back.db.ArticleDraftRepository
import co.globers.back.back.db.ArticleDraftRevisionRepository
import co.globers.back.back.db.ArticlePublicationRepository
import co.globers.back.back.db.ArticlePublicationRevisionRepository
import co.globers.back.back.db.ArticleRepository
import co.globers.back.back.db.DbArticleDraft
import co.globers.back.back.db.IDbUserPublicProfile
import co.globers.back.back.db.UserRepository
import co.globers.back.back.db.UserSubscriptionRepository
import co.globers.back.back.graphql.ContributedArticle
import co.globers.back.back.graphql.ContributedMedia
import co.globers.back.back.graphql.PublishedArticle
import co.globers.back.back.graphql.Subscriber
import co.globers.back.back.graphql.UserPublicProfile
import co.globers.back.back.utils.toGraphQl
import com.coxautodev.graphql.tools.GraphQLResolver
import org.springframework.stereotype.Component

@Component
class ContributedMediaResolver(
        private val userRepository: UserRepository,
        private val userSubscriptionRepository: UserSubscriptionRepository,
        private val articleDraftRepository: ArticleDraftRepository,
        private val articleDraftRevisionRepository: ArticleDraftRevisionRepository,
        private val publicationRepository: ArticlePublicationRepository,
        private val publicationRevisionRepository: ArticlePublicationRevisionRepository,
        private val articleRepository: ArticleRepository
) : GraphQLResolver<ContributedMedia> {

    fun contributedArticles(contributedMedia: ContributedMedia): List<ContributedArticle> {

        val drafts = articleDraftRepository.findByMediaId(contributedMedia.media.id)

        return drafts.map { draftToContributedArticle(it) }
    }

    fun contributedArticle(contributedMedia: ContributedMedia, articleDraftUri: String): ContributedArticle? {

        val draft = articleDraftRepository.findByUri(articleDraftUri) ?: return null
        if (draft.mediaId != contributedMedia.media.id) {
            return null
        }
        return draftToContributedArticle(draft)
    }

    fun subscribers(contributedMedia: ContributedMedia): List<Subscriber>{

        val dbSubs = userSubscriptionRepository.findUserSubscriptions(contributedMedia.media.id)
        val userIds = dbSubs.map { it.userId }
        val users = userRepository.findAllById(userIds).map { it.id to toGraphQl(it as IDbUserPublicProfile) }.toMap()
        return dbSubs.map { Subscriber.build(users.getValue(it.userId), toGraphQl(it)) }
    }

    private fun draftToContributedArticle(draft: DbArticleDraft): ContributedArticle {
        val pub = publicationRepository.findByArticleDraftId(draft.id)
        val publishedArticle = if (pub == null) {
            null
        } else {

            val article = articleRepository.getOne(pub.articleId)

            if (article.unpublished) {
                null
            } else {

                val pubRevision = publicationRevisionRepository.findFirstByArticlePublicationIdOrderByPublicationDatetimeDesc(pub.id)
                val revision = articleDraftRevisionRepository.getOne(pubRevision.articleDraftRevisionId)

                PublishedArticle.build(toGraphQl(article), toGraphQl(revision))
            }
        }
        return ContributedArticle.build(toGraphQl(draft), publishedArticle)
    }


}