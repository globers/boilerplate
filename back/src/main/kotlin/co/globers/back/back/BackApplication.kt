package co.globers.back.back

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication


@SpringBootApplication
class BackApplication

fun main(args: Array<String>) {
    @Suppress("SpreadOperator")
    runApplication<BackApplication>(*args)
}
