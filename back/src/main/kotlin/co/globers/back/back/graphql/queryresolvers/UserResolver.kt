package co.globers.back.back.graphql.queryresolvers

import co.globers.back.back.db.CreatorRepository
import co.globers.back.back.db.MediaConsumerRepository
import co.globers.back.back.db.MediaRepository
import co.globers.back.back.db.MediaSubscriptionPlanRepository
import co.globers.back.back.db.UserSubscriptionRepository
import co.globers.back.back.graphql.Creator
import co.globers.back.back.graphql.User
import co.globers.back.back.graphql.UserMedia
import co.globers.back.back.graphql.UserSubscription
import co.globers.back.back.utils.toGraphQl
import com.coxautodev.graphql.tools.GraphQLResolver
import org.springframework.stereotype.Component

@Component
class UserResolver(
        private val userSubscriptionRepository: UserSubscriptionRepository,
        private val mediaSubscriptionPlanRepository: MediaSubscriptionPlanRepository,
        private val mediaRepository: MediaRepository,
        private val mediaConsumerRepository: MediaConsumerRepository,
        private val creatorRepository: CreatorRepository) : GraphQLResolver<User> {

    @Suppress("unused")
    fun userMedia(user: User, mediaUri: String): UserMedia? {

        val dbMedia = mediaRepository.findByUriAndUnpublishedFalse(mediaUri) ?:return null
        val media = toGraphQl(dbMedia)

        val followed = mediaConsumerRepository.findByUserUriMediaUri(user.uri, mediaUri)?.follow ?: false

        return UserMedia.build(user.uri, media, followed)
    }

    @Suppress("unused")
    fun userMedias(user: User): List<UserMedia> {
        val dbUserMediaList = mediaConsumerRepository.findByUserUri(user.uri)
        return mediaRepository.findByUnpublishedFalse()
                .map { m ->
                    UserMedia.build(user.uri,
                            toGraphQl(m),
                            dbUserMediaList.any { m.id == it.mediaConsumerId.mediaId && it.follow })
                }
    }

    @Suppress("unused")
    fun creator(user: User): Creator? {
        val dbCreator = creatorRepository.findByUserId(user.id)
        return if (dbCreator != null) Creator.build(dbCreator.id, user.uri) else null
    }

    @Suppress("unused")
    fun subscriptions(user: User): List<UserSubscription> {
        val dbUserSubscriptions = userSubscriptionRepository.findByUserId(user.id)
        return dbUserSubscriptions.map { toGraphQl(it) }

    }

}