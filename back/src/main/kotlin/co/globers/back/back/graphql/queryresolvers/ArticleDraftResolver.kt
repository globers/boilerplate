package co.globers.back.back.graphql.queryresolvers

import co.globers.back.back.db.ArticleDraftRevisionRepository
import co.globers.back.back.graphql.ArticleDraft
import co.globers.back.back.graphql.ArticleDraftRevision
import co.globers.back.back.utils.toGraphQl
import com.coxautodev.graphql.tools.GraphQLResolver
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Component

@Component
class ArticleDraftResolver(
        private val articleDraftRevisionRepository: ArticleDraftRevisionRepository
) : GraphQLResolver<ArticleDraft> {
    fun revisions(articleDraft: ArticleDraft, nbMostRecent: Int): List<ArticleDraftRevision> {

        val dbRevisions = articleDraftRevisionRepository
                .findByArticleDraftId(articleDraft.id, PageRequest.of(0, nbMostRecent, Sort.Direction.DESC, "id"))

        return dbRevisions.map { toGraphQl(it) }
    }

    fun revision(articleDraft: ArticleDraft, articleDraftRevisionUri: String): ArticleDraftRevision? {

        val revision = articleDraftRevisionRepository.findByUri(articleDraftRevisionUri) ?: return null

        if (articleDraft.id != revision.articleDraftId) {
            return null
        }
        return toGraphQl(revision)
    }
}