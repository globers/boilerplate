package co.globers.back.back.db

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.io.Serializable
import javax.persistence.Embeddable
import javax.persistence.EmbeddedId
import javax.persistence.Entity
import javax.persistence.Table


@Embeddable
data class ArticleConsumerId (val userId: Long, val articleId: Long) : Serializable

@Entity
@Table(name = "article_consumer")
 data class DbArticleConsumer(

        @EmbeddedId
        val articleConsumerId: ArticleConsumerId,
        val bookmark: Boolean
)

@Repository
interface ArticleConsumerRepository : JpaRepository<DbArticleConsumer, ArticleConsumerId> {

}
