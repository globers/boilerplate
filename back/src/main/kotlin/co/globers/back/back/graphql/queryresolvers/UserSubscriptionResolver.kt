package co.globers.back.back.graphql.queryresolvers

import co.globers.back.back.db.MediaSubscriptionPlanRepository
import co.globers.back.back.graphql.SubscriptionPlan
import co.globers.back.back.graphql.UserSubscription
import co.globers.back.back.utils.toGraphQl
import com.coxautodev.graphql.tools.GraphQLResolver
import org.springframework.stereotype.Component

@Component
class UserSubscriptionResolver(private val mediaSubscriptionPlanRepository: MediaSubscriptionPlanRepository) : GraphQLResolver<UserSubscription> {

    @Suppress("unused")
    fun plan(userSubscription: UserSubscription) : SubscriptionPlan {
        return toGraphQl(mediaSubscriptionPlanRepository.getOne(userSubscription.mediaSubscriptionPlanId))
    }

}