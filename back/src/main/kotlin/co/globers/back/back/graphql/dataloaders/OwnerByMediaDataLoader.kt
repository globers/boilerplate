package co.globers.back.back.graphql.dataloaders

import co.globers.back.back.db.DbUserPublicProfile
import co.globers.back.back.db.UserRepository
import co.globers.back.back.graphql.UserPublicProfile
import co.globers.back.back.utils.toGraphQl
import org.dataloader.BatchLoader
import org.dataloader.DataLoader
import org.springframework.core.task.SyncTaskExecutor
import org.springframework.stereotype.Component
import java.util.concurrent.CompletableFuture
import java.util.concurrent.CompletionStage
import java.util.function.Supplier


@Component
class OwnerByMediaBatchLoader(
        private val userRepository: UserRepository
) : BatchLoader<Long, UserPublicProfile> {

    override fun load(mediaIds: List<Long>): CompletionStage<List<UserPublicProfile>> {

        val supplier: Supplier<List<UserPublicProfile>> = Supplier {
            val profiles = userRepository.findOwners(mediaIds).map { it.mediaId to it }.toMap()
            mediaIds.map {
                toGraphQl(
                        profiles.getOrDefault(
                                it,
                                // for media without writer (possible for test data, no sql constraint to prevent this)
                                DbUserPublicProfile(-1, "uri-unknown", "", "", null, it)
                        )
                )
            }
        }

        return CompletableFuture.supplyAsync<List<UserPublicProfile>>(
                supplier,
                SyncTaskExecutor())
    }
}


class OwnerByMediaLoader(ownerByMediaBatchLoader: OwnerByMediaBatchLoader)
    : DataLoader<Long, UserPublicProfile>(ownerByMediaBatchLoader)
