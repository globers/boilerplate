package co.globers.back.back.graphql.dataloaders

import graphql.schema.DataFetchingEnvironment
import graphql.servlet.GraphQLContext
import org.dataloader.DataLoader

fun <D, K, V> getDataLoader(env: DataFetchingEnvironment, loaderClassName: String): D where D : DataLoader<K, V> {

    val graphQLContext = env.getContext<GraphQLContext>()
    val dataLoaderRegistryOpt = graphQLContext.dataLoaderRegistry
    val dataLoaderRegistry = dataLoaderRegistryOpt.get()
    @Suppress("UNCHECKED_CAST")
    return dataLoaderRegistry.getDataLoader<K, V>(loaderClassName) as D
}
