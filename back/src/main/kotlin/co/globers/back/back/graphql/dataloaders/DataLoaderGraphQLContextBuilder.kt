package co.globers.back.back.graphql.dataloaders

import graphql.servlet.GraphQLContext
import graphql.servlet.GraphQLContextBuilder
import org.dataloader.DataLoaderRegistry
import org.springframework.stereotype.Component
import javax.servlet.http.HttpServletRequest
import javax.websocket.server.HandshakeRequest


@Component
class DataLoaderGraphQLContextBuilder(
        private val userArticlesByUserMediaBatchLoader: UserArticlesByUserMediaBatchLoader,
        private val articlesByMediaBatchLoader: ArticlesByMediaBatchLoader,
        private val ownerByMediaBatchLoader: OwnerByMediaBatchLoader,
        private val nbArticlesByMediaLoader: NbArticlesByMediaBatchLoader
) : GraphQLContextBuilder {

    private fun configureContext(context: GraphQLContext) : GraphQLContext{
        context.setDataLoaderRegistry(DataLoaderRegistry()
                .register(
                        UserArticlesByUserMediaDataLoader::class.simpleName,
                        UserArticlesByUserMediaDataLoader(userArticlesByUserMediaBatchLoader))
                .register(
                        ArticlesByMediaDataLoader::class.simpleName,
                        ArticlesByMediaDataLoader(articlesByMediaBatchLoader))
                .register(
                        OwnerByMediaLoader::class.simpleName,
                        OwnerByMediaLoader(ownerByMediaBatchLoader))
                .register(
                        NbArticlesByMediaLoader::class.simpleName,
                        NbArticlesByMediaLoader(nbArticlesByMediaLoader)))
        return context
    }

    override fun build(httpServletRequest: HttpServletRequest): GraphQLContext {
        val context = GraphQLContext(httpServletRequest)
        return configureContext(context)
    }

    override fun build(handshakeRequest: HandshakeRequest): GraphQLContext {
        val context = GraphQLContext()
        return configureContext(context)
    }

    override fun build(): GraphQLContext {
        val context = GraphQLContext()
        return configureContext(context)
    }
}
