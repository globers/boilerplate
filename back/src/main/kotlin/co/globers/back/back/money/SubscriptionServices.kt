package co.globers.back.back.money


import co.globers.back.back.db.DbMediaContractTerms
import co.globers.back.back.db.DbMediaStripeAccount
import co.globers.back.back.db.DbMediaStripeCustomer
import co.globers.back.back.db.DbMediaStripePlan
import co.globers.back.back.db.DbMediaStripeProduct
import co.globers.back.back.db.DbMediaStripeSubscription
import co.globers.back.back.db.DbMediaSubscriptionPlan
import co.globers.back.back.db.DbUserSubscription
import co.globers.back.back.db.MediaContractTermsRepository
import co.globers.back.back.db.MediaRepository
import co.globers.back.back.db.MediaStripeAccountRepository
import co.globers.back.back.db.MediaStripeCustomerRepository
import co.globers.back.back.db.MediaStripePlanRepository
import co.globers.back.back.db.MediaStripeProductRepository
import co.globers.back.back.db.MediaStripeSubscriptionRepository
import co.globers.back.back.db.MediaSubscriptionPlanRepository
import co.globers.back.back.db.UserSubscriptionRepository
import co.globers.back.back.graphql.Media
import co.globers.back.back.graphql.StripeAccountInput
import co.globers.back.back.graphql.StripeCountry
import co.globers.back.back.graphql.StripeCurrency
import co.globers.back.back.graphql.StripeUserSubscriptionInput
import co.globers.back.back.graphql.User
import co.globers.back.back.utils.generateUriByClean
import co.globers.back.back.utils.generateUriByCleanAndHash
import co.globers.back.back.utils.toGraphQl
import com.stripe.exception.StripeException
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import java.time.Instant

@Component
class SubscriptionServices(
        private val stripeServices: StripeServices,
        private val mediaRepository: MediaRepository,
        private val mediaStripeAccountRepository: MediaStripeAccountRepository,
        private val mediaStripeProductRepository: MediaStripeProductRepository,
        private val mediaStripePlanRepository: MediaStripePlanRepository,
        private val mediaSubscriptionPlanRepository: MediaSubscriptionPlanRepository,
        private val mediaContractTermsRepository: MediaContractTermsRepository,
        private val mediaStripeCustomerRepository: MediaStripeCustomerRepository,
        private val mediaStripeSubscriptionRepository: MediaStripeSubscriptionRepository,
        private val userSubscriptionRepository: UserSubscriptionRepository
) {

    @Throws(StripeException::class)
    @Transactional
    fun buildMedia(media: Media, mediaOwner: User, stripeAccountInput: StripeAccountInput) {

        val stripeMediaAccountId = stripeServices.buildMediaAccount(stripeAccountInput, media, mediaOwner)
        val stripeMediaProductId = stripeServices.buildMediaProduct(media, stripeMediaAccountId)

        val dbStripeAccount = mediaStripeAccountRepository.save(DbMediaStripeAccount(
                mediaId = media.id, stripeAccountId = stripeMediaAccountId, country = stripeAccountInput.country.name, ownerId = mediaOwner.id))
        val dbStripeProduct = mediaStripeProductRepository.save(DbMediaStripeProduct(
                stripeProductId = stripeMediaProductId, mediaStripeAccountInternalId = dbStripeAccount.id))

        mediaContractTermsRepository.save(DbMediaContractTerms(mediaId = media.id, globersFeePercent = 25.0))

        StripeCountry.values().forEach {

            val plan = getPricingPlan(it)
            val mediaSubscriptionUri = generateUriByCleanAndHash("${plan.currency}-${plan.priceInCurrency}-${media.uri}", Instant.now().toString())
            val stripePlanId = stripeServices.buildMediaPlan(media, stripeMediaAccountId, stripeMediaProductId, mediaSubscriptionUri, plan)
            val dbMediaStripePlan = mediaStripePlanRepository.save(
                    DbMediaStripePlan(stripePlanId = stripePlanId, mediaStripeProductInternalId = dbStripeProduct.id))

            mediaSubscriptionPlanRepository.save(
                    DbMediaSubscriptionPlan(uri = mediaSubscriptionUri, mediaId = media.id, country = it.name, currency = plan.currency.name, price = plan.priceInCurrency,
                            mediaStripePlanInternalId = dbMediaStripePlan.id))
        }


    }

    @Throws(StripeException::class)
    @Transactional
    fun buildSubscription(mediaSubscriptionUri: String, client: User, stripeSubscriptionInput: StripeUserSubscriptionInput) {

        // 1) get data about media account, plan, contract
        val dbPlan = mediaSubscriptionPlanRepository.findByUri(mediaSubscriptionUri)

        val stripeAccount = mediaStripeAccountRepository.findByMediaId(dbPlan.mediaId)
        val dbContract = mediaContractTermsRepository.findByMediaId(dbPlan.mediaId)
        val stripePlan = mediaStripePlanRepository.getOne(dbPlan.mediaStripePlanInternalId)
        val dbMedia = mediaRepository.getOne(dbPlan.mediaId)
        val media = toGraphQl(dbMedia)

        // 2) create stripe customer & subscription
        val stripeCustomerId = stripeServices.buildMediaCustomer(client, stripeSubscriptionInput.userSourceTokenId, media, stripeAccount.stripeAccountId)
        val stripeSubscriptionId = stripeServices.buildCustomerSubscriptionToPlan(
                client, media, stripeCustomerId, stripeAccount.stripeAccountId, stripePlan.stripePlanId, dbContract.globersFeePercent,
                StripeCountry.valueOf(stripeAccount.country)
        )


        val userSubscriptionUri = generateUriByClean("${client.uri}-$mediaSubscriptionUri")

        // 3)  save created stripe customer & subscription & user subscription
        val dbMediaStripeCustomer = mediaStripeCustomerRepository.save(
                DbMediaStripeCustomer(
                        stripeCustomerId = stripeCustomerId, mediaStripeAccountInternalId = stripeAccount.id)
        )

        val dbStripeSubscription = mediaStripeSubscriptionRepository.save(DbMediaStripeSubscription(
                stripeSubscriptionId = stripeSubscriptionId, mediaStripePlanInternalId = stripePlan.id,
                mediaStripeCustomerInternalId = dbMediaStripeCustomer.id))

        userSubscriptionRepository.save(DbUserSubscription(
                uri = userSubscriptionUri,
                userId = client.id, mediaStripeSubscriptionInternalId = dbStripeSubscription.id,
                mediaSubscriptionPlanId = stripePlan.id, startSubscriptionDate = Instant.now()))
    }


    private fun getPricingPlan(country: StripeCountry): PricingPlan {
        return when (country) {
            StripeCountry.FR -> PricingPlan(StripeCurrency.EUR, 799, 7.99)
        }
    }

}
