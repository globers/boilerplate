package co.globers.back.back.graphql.dataloaders

import co.globers.back.back.db.ArticleRepository
import co.globers.back.back.graphql.UserArticle
import co.globers.back.back.graphql.UserMedia
import co.globers.back.back.utils.toGraphQl
import org.dataloader.BatchLoader
import org.dataloader.DataLoader
import org.springframework.core.task.SyncTaskExecutor
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Component
import java.util.concurrent.CompletableFuture
import java.util.concurrent.CompletionStage
import java.util.function.Supplier


data class UserArticlesByUserMediaKey(val userMedia: UserMedia, val nbMostRecent: Int)

@Component
class UserArticlesByUserMediaBatchLoader(
        private val articleRepository: ArticleRepository) : BatchLoader<UserArticlesByUserMediaKey, List<UserArticle>> {

    // NB: Contract is not respected (n most recent by media is replaced by n*nbMedia most recent)
    // Solution is to make a loop where we check which media is not fulfilled and we redo
    // another request with those medias
    override fun load(keys: List<UserArticlesByUserMediaKey>): CompletionStage<List<List<UserArticle>>> {

        val supplier: Supplier<List<List<UserArticle>>> = Supplier {

            val userUri = keys[0].userMedia.userUri
            val mediaIds = keys.map { it.userMedia.media.id }
            val nbArticlesHeuristic = keys.sumBy { it.nbMostRecent }

            val bookmarked = articleRepository
                    .findBookmarkedByUser(userUri, mediaIds)
                    .groupBy { it.mediaId }

            val mostRecent = articleRepository
                    .findMostRecentPublishedFromMediaList(mediaIds, PageRequest.of(0, nbArticlesHeuristic))
                    .groupBy { it.mediaId }

            keys.map { (userMedia) ->

                val bookmarkedThisMedia = bookmarked
                        .getOrDefault(userMedia.media.id, emptyList()).toSet()

                val articles =
                        bookmarkedThisMedia.union(mostRecent.getOrDefault(userMedia.media.id, emptyList()))

                articles.map { article ->
                    UserArticle.build(
                            userUri,
                            toGraphQl(article),
                            bookmarkedThisMedia.contains(article))
                }
            }
        }

        return CompletableFuture.supplyAsync<List<List<UserArticle>>>(
                supplier,
                SyncTaskExecutor())
    }
}


class UserArticlesByUserMediaDataLoader(userArticlesByUserMediaBatchLoader: UserArticlesByUserMediaBatchLoader)
    : DataLoader<UserArticlesByUserMediaKey, List<UserArticle>>(userArticlesByUserMediaBatchLoader)
