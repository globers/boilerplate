package co.globers.back.back.db

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.io.Serializable
import javax.persistence.Embeddable
import javax.persistence.EmbeddedId
import javax.persistence.Entity
import javax.persistence.Table


@Embeddable
data class MediaContributorId (val creatorId: Long, val mediaId: Long) : Serializable

@Entity
@Table(name = "media_Contributor")
 data class DbMediaContributor(

        @EmbeddedId
        val mediaContributorId: MediaContributorId
)

@Repository
interface MediaContributorRepository : JpaRepository<DbMediaContributor, MediaContributorId> {

    fun findByMediaContributorIdCreatorId(creatorId: Long) : List<DbMediaContributor>


    @Query("SELECT mc from DbMediaContributor mc " +
            "JOIN DbCreator c ON mc.mediaContributorId.creatorId = c.id " +
            "JOIN DbUser u ON c.userId = u.id " +
            "JOIN DbMedia m ON mc.mediaContributorId.mediaId = m.id " +
            "WHERE u.uri= :userUri AND m.uri = :mediaUri")
    fun findByUserUriMediaUri(userUri: String, mediaUri: String): DbMediaContributor?

    @Query("SELECT mc from DbMediaContributor mc " +
            "JOIN DbCreator c ON mc.mediaContributorId.creatorId = c.id " +
            "JOIN DbUser u ON c.userId = u.id " +
            "JOIN DbMedia m ON mc.mediaContributorId.mediaId = m.id " +
            "WHERE u.uri= :userUri AND m.id = :mediaId")
    fun findByUserUriMediaId(userUri: String, mediaId: Long): DbMediaContributor?
}
