package co.globers.back.back.graphql

import java.time.Instant

@Suppress("EmptyDefaultConstructor")
sealed class UserOrAnonymous()

object Anonymous : UserOrAnonymous()

data class User(val id: Long, val uri: String, val firstName: String, val lastName: String, val email: String, val profilePictureUrl: String?) : UserOrAnonymous()

data class UserPublicProfile private constructor(val uri: String, val firstName: String, val lastName: String, val profilePictureUrl: String?) {

    companion object {
        fun build(userUri: String, firstName: String, lastName: String, profilePictureUrl: String?): UserPublicProfile {
            // userPublicProfile has exact same uri as user because it's a "view" on user
            // is user is modified, userPublicProfile should be modified too in apollo cache
            return UserPublicProfile("UserPublicProfile-$userUri", firstName, lastName, profilePictureUrl)
        }
    }
}


// ---------------------

data class Article(val uri: String, val title: String, val subtitle: String, val imageUrl: String, val unpublished: Boolean, val firstPublication: GraphqlDate,
                   val lastPublication: GraphqlDate)


data class UserArticle private constructor(val uri: String, val article: Article, val bookmarked: Boolean) {

    companion object {
        fun build(userUri: String, article: Article, bookmarked: Boolean): UserArticle {
            return UserArticle("userArticle-$userUri-${article.uri}", article, bookmarked)
        }
    }
}

// --------------------

data class Media(val id: Long, val uri: String, val name: String, val logoUrl: String, val description: String,
                 val themePrimaryColor: String, val themeSecondaryColor: String, val originHtmlContentSelector: String?, val unpublished: Boolean)

data class UserMedia private constructor(val uri: String, val userUri: String, val media: Media, val followed: Boolean) {

    companion object {
        fun build(userUri: String, media: Media, followed: Boolean): UserMedia {
            return UserMedia("userMedia-$userUri-${media.uri}", userUri, media, followed)
        }
    }
}


// -----------------

enum class LoginFailureReason {
    UNKNOWN_USER,
    INVALID_PASSWORD
}

data class LoginFailure(val failureReason: LoginFailureReason) : LoginReply()
data class LoginSuccess(val user: User, val jwtToken: String) : LoginReply()

@Suppress("EmptyDefaultConstructor")
sealed class LoginReply()

// -----------------

enum class SaveUserFailureReason {
    EMAIL_ALREADY_EXISTS,
    INVALID_EMAIL,
    PASSWORD_TOO_SHORT,
    EMPTY_FIRSTNAME,
    EMPTY_LASTNAME,
    WRONG_PREVIOUS_PASSWORD,
    INVALID_PROFILE_PICTURE
}

@Suppress("EmptyDefaultConstructor")
sealed class SaveUserReply()

data class SaveUserFailure(val failureReason: SaveUserFailureReason) : SaveUserReply()
data class SaveUserSuccess(val user: User, val jwtToken: String) : SaveUserReply()

// -----------------

enum class SaveMediaFailureReason {
    NAME_ALREADY_EXISTS,
    EMPTY_NAME,
    EMPTY_DESCRIPTION,
    EMPTY_LOGO_IMAGE,
    INVALID_LOGO_IMAGE
}

data class SaveMediaFailure(val failureReason: SaveMediaFailureReason) : SaveMediaReply()
data class SaveMediaSuccess(val contributedMedia: ContributedMedia) : SaveMediaReply()

@Suppress("EmptyDefaultConstructor")
sealed class SaveMediaReply()

// ------- Creator -------

data class Creator private constructor(
        val id: Long,
        val uri: String
) {
    companion object {
        fun build(id: Long, userUri: String): Creator {
            return Creator(id, "creator-$userUri")
        }
    }
}

data class ContributedMedia private constructor(
        val uri: String,
        val media: Media,
        val stripeAccountConnected: Boolean
) {
    // TODO Supprimer creatorUri argument, useless and not consistent, contributedMedia is not associated to a specific creator if there is several creators
    companion object {
        fun build(creatorUri: String, media: Media, stripeAccountConnected: Boolean): ContributedMedia {
            return ContributedMedia("contributedMedia-$creatorUri-${media.uri}", media, stripeAccountConnected)
        }
    }
}

data class ArticleDraft(
        val id: Long,
        val uri: String
)


data class GraphqlDate private constructor(
        val nbSecFrom1970: Long
) {
    companion object {
        fun build(instant: Instant): GraphqlDate {
            return GraphqlDate(instant.epochSecond)
        }

        fun build(sqlTimestamp: java.sql.Timestamp): GraphqlDate {
            return build(sqlTimestamp.toInstant())
        }
    }
}


data class GraphqlDateInput private constructor(
        val nbSecFrom1970: Long
) {
    fun toInstant() = Instant.ofEpochSecond(nbSecFrom1970)
}


data class ArticleDraftRevision(
        val uri: String,
        val title: String?,
        val subtitle: String?,
        val content: String?,
        val imageUrl: String?,
        val slatejsValue: String?,
        val savingDate: GraphqlDate
)


data class ContributedArticle private constructor(
        val uri: String,
        val draft: ArticleDraft,
        val publishedArticle: PublishedArticle?
) {
    companion object {
        fun build(draft: ArticleDraft, publishedArticle: PublishedArticle?): ContributedArticle {
            return ContributedArticle("contributedArticle-${draft.uri}", draft, publishedArticle)
        }
    }
}

data class PublishedArticle private constructor(
        val uri: String,
        val article: Article,
        val revision: ArticleDraftRevision
) {
    companion object {
        fun build(article: Article, revision: ArticleDraftRevision): PublishedArticle {
            return PublishedArticle("publishedArticle-${article.uri}", article, revision)
        }
    }
}


data class SaveArticleDraftRevisionFailure(val failureReason: SaveArticleDraftRevisionFailureReason) : SaveArticleDraftRevisionReply()
data class SaveArticleDraftRevisionSuccess(val revision: ArticleDraftRevision) : SaveArticleDraftRevisionReply()

@Suppress("EmptyDefaultConstructor")
sealed class SaveArticleDraftRevisionReply()

enum class SaveArticleDraftRevisionFailureReason {
    INVALID_ARTICLE_IMAGE
}


// ----------------


@Suppress("EmptyDefaultConstructor")
sealed class PublishArticleReply {}

data class PublishArticleFailure(val failureReason: PublishArticleFailureReason
) : PublishArticleReply()

enum class PublishArticleFailureReason {
    EMPTY_TITLE,
    EMPTY_SUBTITLE,
    EMPTY_CONTENT,
    EMPTY_IMAGE
}

data class PublishArticleSuccess(val article: Article
) : PublishArticleReply()


// -----------------

@Suppress("unused", "ClassName", "ClassNaming")
data class _Useless(
        val anonymous: Anonymous?,
        val user: User?,
        val saveUserSuccess: SaveUserSuccess?,
        val saveUserFailure: SaveUserFailure?,
        val loginSuccess: LoginSuccess?,
        val loginFailure: LoginFailure?,
        val saveMediaSuccess: SaveMediaSuccess?,
        val saveMediaFailure: SaveMediaFailure?,

        val saveArticleDraftRevisionSuccess: SaveArticleDraftRevisionSuccess?,
        val saveArticleDraftRevisionFailure: SaveArticleDraftRevisionFailure?,

        val publishArticleSuccess: PublishArticleSuccess?,
        val publishArticleFailure: PublishArticleFailure?,

        val resetPasswordSuccess: ResetPasswordSuccess?,
        val resetPasswordFailure: ResetPasswordFailure?,

        val uploadImageForDraftSuccess: UploadImageForDraftSuccess?,
        val uploadImageForDraftFailure: UploadImageForDraftFailure?,

        val saveStripeAccountSuccess: SaveStripeAccountSuccess?,
        val saveStripeAccountFailure: SaveStripeAccountFailure?,

        val subscribeToMediaSuccess: SubscribeToMediaSuccess?,
        val subscribeToMediaFailure: SubscribeToMediaFailure?
)

// ---------------------

data class GraphqlFile(val file: String, val fileFormat: FileFormat)

enum class FileFormat {
    BASE_64,
    URL
}

// --------------- RESET PASSWORD ---------


enum class SendResetPasswordEmailReply {
    SUCCESS,
    UNKNOWN_USER,
    EMAIL_DELIVERY_FAILURE
}

@Suppress("EmptyDefaultConstructor")
sealed class ResetPasswordReply {}


data class ResetPasswordSuccess(
        val jwtToken: String
) : ResetPasswordReply()


data class ResetPasswordFailure(
        val failureReason: ResetPasswordFailureReason
) : ResetPasswordReply()

enum class ResetPasswordFailureReason {
    PASSWORD_TOO_SHORT,
    RESET_PASSWORD_URI_EXPIRED,
    INVALID_RESET_PASSWORD_URI
}

// -------------------------


@Suppress("EmptyDefaultConstructor")
sealed class UploadImageForDraftReply {}


data class UploadImageForDraftSuccess(
        val imageUrl: String
) : UploadImageForDraftReply()


data class UploadImageForDraftFailure(
        val failureReason: UploadImageForDraftFailureReason
) : UploadImageForDraftReply()

enum class UploadImageForDraftFailureReason {
    INVALID_IMAGE_FILE,
    INVALID_DRAFT_URI
}


// ----------------------

// --------------------- Stripe account -------------------


enum class StripeCountry {
    FR
}

//  ISO currency code
enum class StripeCurrency {
    EUR
}

data class StripeAccountInput(
        val country: StripeCountry,
        val defaultCurrency: StripeCurrency,
        val accountTokenId: String, // stripe account data: legal entity, tos
        val externalAccountTokenId: String // bank account token
)

@Suppress("EmptyDefaultConstructor")
sealed class SaveStripeAccountReply {}

data class SaveStripeAccountSuccess(val ok: Boolean?) : SaveStripeAccountReply()

data class SaveStripeAccountFailure(
        val failureReason: SaveStripeAccountFailureReason,
        val stripeErrorMessage: String? // Error message if failureReason == STRIPE_ERROR
) : SaveStripeAccountReply()

enum class SaveStripeAccountFailureReason {
    STRIPE_ERROR
}


data class SubscriptionPlan(
        val uri: String,
        val country: StripeCountry,
        val currency: StripeCurrency,
        val price: Double
)


// ------------------------End Stripe account ------------------------

// ----------------------- Media subscription -------------------------


data class StripeUserSubscriptionInput(val userSourceTokenId: String)

@Suppress("EmptyDefaultConstructor")
sealed class SubscribeToMediaReply {}

data class SubscribeToMediaSuccess(val ok: Boolean?) : SubscribeToMediaReply()

data class SubscribeToMediaFailure(
        val failureReason: SubscribeToMediaFailureReason,
        val stripeErrorMessage: String? // Error message if failureReason == STRIPE_ERROR
) : SubscribeToMediaReply()

enum class SubscribeToMediaFailureReason {
    ALREADY_SUBSCRIBED,
    STRIPE_ERROR
}

data class UserSubscription(
        val uri: String,
        val mediaSubscriptionPlanId: Long,
        val startSubscriptionDate: GraphqlDate

)

data class Subscriber private constructor(
        val uri: String,
        val profile: UserPublicProfile,
        val subscription: UserSubscription
){

    companion object {
        fun build(profile: UserPublicProfile, subscription: UserSubscription): Subscriber {
            return Subscriber("subscriber-${subscription.uri}", profile, subscription)
        }
    }

}


// ------------------------ End media subscription

// ---------- Admin

data class Admin(val uri: String)