package co.globers.back.back.utils

import co.globers.back.back.db.DbArticle
import co.globers.back.back.db.DbArticleDraft
import co.globers.back.back.db.DbArticleDraftRevision
import co.globers.back.back.db.DbMedia
import co.globers.back.back.db.DbMediaSubscriptionPlan
import co.globers.back.back.db.DbUser
import co.globers.back.back.db.DbUserPublicProfile
import co.globers.back.back.db.DbUserSubscription
import co.globers.back.back.db.IDbArticleWithoutContent
import co.globers.back.back.db.IDbUserPublicProfile
import co.globers.back.back.graphql.Article
import co.globers.back.back.graphql.ArticleDraft
import co.globers.back.back.graphql.ArticleDraftRevision
import co.globers.back.back.graphql.GraphqlDate
import co.globers.back.back.graphql.Media
import co.globers.back.back.graphql.StripeCountry
import co.globers.back.back.graphql.StripeCurrency
import co.globers.back.back.graphql.SubscriptionPlan
import co.globers.back.back.graphql.User
import co.globers.back.back.graphql.UserPublicProfile
import co.globers.back.back.graphql.UserSubscription
import co.globers.back.html.extractContent

fun getContent(article: DbArticle, selector: String?): String {
    return if (article.originHtml != null && selector != null) {
        extractContent(article.originHtml, article.imageUrl, selector)
    } else if (article.content != null)
        article.content
    else {
        ""
    }
}

fun toGraphQl(dbArticle: IDbArticleWithoutContent): Article {
    return Article(dbArticle.uri, dbArticle.title, dbArticle.subtitle, dbArticle.imageUrl, dbArticle.unpublished,
            GraphqlDate.build(dbArticle.firstPublication), GraphqlDate.build(dbArticle.lastPublication))
}

fun toGraphQl(dbUserPublicProfile: IDbUserPublicProfile) : UserPublicProfile {
    return UserPublicProfile.build(dbUserPublicProfile.uri, dbUserPublicProfile.firstName, dbUserPublicProfile.lastName, dbUserPublicProfile.profilePictureUrl)
}

fun toGraphQl(dbMedia: DbMedia): Media {
    return Media(dbMedia.id, dbMedia.uri, dbMedia.name, dbMedia.logoUrl, dbMedia.description,
            dbMedia.themePrimaryColor, dbMedia.themeSecondaryColor, dbMedia.originHtmlContentSelector, dbMedia.unpublished)
}

fun toGraphQl(dbUser: DbUser): User {
    return User(dbUser.id, dbUser.uri, dbUser.firstName, dbUser.lastName, dbUser.email, dbUser.profilePictureUrl)
}

fun toGraphQl(dbDraft: DbArticleDraft): ArticleDraft {
    return ArticleDraft(dbDraft.id, dbDraft.uri)
}

fun toGraphQl(dbRevision: DbArticleDraftRevision): ArticleDraftRevision {
    return ArticleDraftRevision(dbRevision.uri, dbRevision.title, dbRevision.subtitle, dbRevision.content,
            dbRevision.imageUrl, dbRevision.slatejsValue, GraphqlDate.build(dbRevision.savingDatetime))
}

fun toGraphQl(dbMediaSubscriptionPlan: DbMediaSubscriptionPlan): SubscriptionPlan {
    return SubscriptionPlan(
            dbMediaSubscriptionPlan.uri,
            StripeCountry.valueOf(dbMediaSubscriptionPlan.country),
            StripeCurrency.valueOf(dbMediaSubscriptionPlan.currency),
            dbMediaSubscriptionPlan.price)
}

fun toGraphQl(dbUserSubscription: DbUserSubscription) : UserSubscription {
    return UserSubscription(dbUserSubscription.uri, dbUserSubscription.mediaSubscriptionPlanId, GraphqlDate.build(dbUserSubscription.startSubscriptionDate))
}
