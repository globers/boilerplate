package co.globers.back.back.graphql.queryresolvers

import co.globers.back.back.db.ArticleRepository
import co.globers.back.back.graphql.UserArticle
import co.globers.back.back.graphql.UserMedia
import co.globers.back.back.graphql.dataloaders.UserArticlesByUserMediaDataLoader
import co.globers.back.back.graphql.dataloaders.UserArticlesByUserMediaKey
import co.globers.back.back.graphql.dataloaders.getDataLoader
import co.globers.back.back.utils.toGraphQl
import com.coxautodev.graphql.tools.GraphQLResolver
import graphql.schema.DataFetchingEnvironment
import org.springframework.stereotype.Component
import java.util.concurrent.CompletableFuture

@Component
class UserMediaResolver(private val articleRepository: ArticleRepository) : GraphQLResolver<UserMedia> {

    @Suppress("unused")
    fun userArticles(userMedia: UserMedia, nbMostRecent: Int, env: DataFetchingEnvironment)
            : CompletableFuture<List<UserArticle>> {

        val loader: UserArticlesByUserMediaDataLoader =
                getDataLoader(env, UserArticlesByUserMediaDataLoader::class.simpleName as String)
        return loader.load(UserArticlesByUserMediaKey(userMedia, nbMostRecent))
    }

    @Suppress("unused")
    fun userArticle(userMedia: UserMedia, articleUri: String): UserArticle? {
        val article = articleRepository.findByUriAndUnpublishedFalse(articleUri)
        // TODO: bookmarked=false is probably a bug!
        return if(article != null) UserArticle.build(userMedia.userUri, toGraphQl(article), false) else null
    }

}