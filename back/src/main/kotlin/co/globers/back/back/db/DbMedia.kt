package co.globers.back.back.db

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "media")
data class DbMedia(

        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long = 0,
        val uri: String,
        val name: String,
        val logoUrl: String,
        val description: String,
        val themePrimaryColor: String,
        val themeSecondaryColor: String,
        val originHtmlContentSelector: String?,
        val unpublished: Boolean
)


@Repository
interface MediaRepository : JpaRepository<DbMedia, Long> {

    fun findByUriAndUnpublishedFalse(uri: String): DbMedia?
    fun findByUri(uri: String): DbMedia?
    fun existsByUri(uri: String): Boolean

    fun findByUnpublishedFalse(): List<DbMedia>

}
