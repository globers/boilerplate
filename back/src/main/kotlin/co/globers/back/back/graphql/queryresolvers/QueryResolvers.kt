package co.globers.back.back.graphql.queryresolvers

import co.globers.back.back.db.UserRepository
import co.globers.back.back.graphql.Admin
import co.globers.back.back.graphql.Anonymous
import co.globers.back.back.graphql.UserOrAnonymous
import co.globers.back.back.graphql._Useless
import co.globers.back.back.utils.JwtUtils
import co.globers.back.back.utils.toGraphQl
import com.coxautodev.graphql.tools.GraphQLQueryResolver
import graphql.GraphQLException
import graphql.schema.DataFetchingEnvironment
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.stereotype.Component


@Component
class QueryResolver(
        private val userRepository: UserRepository,
        private val jwtUtils: JwtUtils) : GraphQLQueryResolver {

    @Suppress("unused")
    fun user(jwtToken: String?, env: DataFetchingEnvironment): UserOrAnonymous {

        val userUri = jwtUtils.tryGetUserUri(jwtToken, env) ?: return Anonymous

        val dbUser = try {
            userRepository.findByUri(userUri)
        } catch (ex: EmptyResultDataAccessException) {
            return Anonymous
        }
        return toGraphQl(dbUser)
    }


    fun admin(jwtToken: String?, env: DataFetchingEnvironment): Admin {
        val userUri = jwtUtils.tryGetUserUri(jwtToken, env)!!

        val user = userRepository.findByUri(userUri)
        if (user.privilege != "admin"){
            throw GraphQLException("user ${userUri} has no admin privilege")
        }
        return Admin("admin-$userUri")
    }


    @Suppress("unused", "FunctionName")
    fun _useless(): _Useless = _Useless(null, null, null, null,
            null, null, null, null, null,
            null, null, null,
            null, null, null, null,
            null, null, null, null)

}


