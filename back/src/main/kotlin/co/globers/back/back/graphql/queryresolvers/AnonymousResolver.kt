package co.globers.back.back.graphql.queryresolvers

import co.globers.back.back.db.MediaRepository
import co.globers.back.back.graphql.Anonymous
import co.globers.back.back.graphql.Media
import co.globers.back.back.utils.toGraphQl
import com.coxautodev.graphql.tools.GraphQLResolver
import org.springframework.stereotype.Component

@Component
class AnonymousResolver(private val mediaRepository: MediaRepository) : GraphQLResolver<Anonymous> {

    @Suppress("unused")
    fun media(anonymous: Anonymous, mediaUri: String): Media? {
        val dbMedia = mediaRepository.findByUriAndUnpublishedFalse(mediaUri)
        return if (dbMedia != null) toGraphQl(dbMedia) else null
    }

    @Suppress("unused")
    fun medias(anonymous: Anonymous): List<Media> {
        return mediaRepository.findByUnpublishedFalse()
                .map { toGraphQl(it) }

    }
}