package co.globers.back.back.db

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.time.Instant
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table


@Entity
@Table(name = "user_subscription")
data class DbUserSubscription(

        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long = 0,
        val uri: String,
        val userId: Long,
        val mediaStripeSubscriptionInternalId: Long,
        val mediaSubscriptionPlanId: Long,
        val startSubscriptionDate: Instant
)

@Repository
interface UserSubscriptionRepository : JpaRepository<DbUserSubscription, Long> {

        fun findByUserId(userId: Long): List<DbUserSubscription>

        @Query("""
        SELECT us
        FROM DbUserSubscription us
        JOIN DbMediaSubscriptionPlan msp ON us.mediaSubscriptionPlanId = msp.id
        WHERE msp.mediaId = :mediaId
    """)
        fun findUserSubscriptions(mediaId: Long): List<DbUserSubscription>
}