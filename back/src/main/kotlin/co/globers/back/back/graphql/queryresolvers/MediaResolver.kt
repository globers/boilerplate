package co.globers.back.back.graphql.queryresolvers

import co.globers.back.back.db.ArticleRepository
import co.globers.back.back.db.MediaSubscriptionPlanRepository
import co.globers.back.back.graphql.Article
import co.globers.back.back.graphql.GraphqlDateInput
import co.globers.back.back.graphql.Media
import co.globers.back.back.graphql.StripeCountry
import co.globers.back.back.graphql.StripeCurrency
import co.globers.back.back.graphql.SubscriptionPlan
import co.globers.back.back.graphql.UserPublicProfile
import co.globers.back.back.graphql.dataloaders.ArticlesByMediaDataLoader
import co.globers.back.back.graphql.dataloaders.ArticlesByMediaKey
import co.globers.back.back.graphql.dataloaders.NbArticleByMediaKey
import co.globers.back.back.graphql.dataloaders.NbArticlesByMediaLoader
import co.globers.back.back.graphql.dataloaders.OwnerByMediaLoader
import co.globers.back.back.graphql.dataloaders.getDataLoader
import co.globers.back.back.utils.toGraphQl
import com.coxautodev.graphql.tools.GraphQLResolver
import graphql.schema.DataFetchingEnvironment
import org.springframework.stereotype.Component
import java.util.concurrent.CompletableFuture

@Component
class MediaResolver(
        private val articleRepository: ArticleRepository,
        private val subscriptionPlanRepository: MediaSubscriptionPlanRepository) : GraphQLResolver<Media> {

    @Suppress("unused")
    fun article(media: Media, uri: String): Article? {

        val article = articleRepository.findByUriAndUnpublishedFalse(uri)
        return if (article != null) toGraphQl(article) else null
    }

    @Suppress("unused")
    fun articles(media: Media, nbMostRecent: Int, env: DataFetchingEnvironment): CompletableFuture<List<Article>> {
        val loader: ArticlesByMediaDataLoader =
                getDataLoader(env, ArticlesByMediaDataLoader::class.simpleName as String)

        return loader.load(ArticlesByMediaKey(media, nbMostRecent))
    }

    @Suppress("unused")
    fun owner(media: Media, env: DataFetchingEnvironment): CompletableFuture<UserPublicProfile> {

        val loader: OwnerByMediaLoader = getDataLoader(env, OwnerByMediaLoader::class.simpleName as String)
        return loader.load(media.id)
    }

    @Suppress("unused")
    fun nbArticles(media: Media, since: GraphqlDateInput, env: DataFetchingEnvironment): CompletableFuture<Int> {

        val loader: NbArticlesByMediaLoader = getDataLoader(env, NbArticlesByMediaLoader::class.simpleName as String)
        return loader.load(NbArticleByMediaKey(media.id, since.toInstant()))
    }

    @Suppress("unused")
    fun subscriptionPlans(media: Media): List<SubscriptionPlan> {
        return subscriptionPlanRepository
                .findByMediaId(media.id)
                .map { toGraphQl(it) }
    }
}