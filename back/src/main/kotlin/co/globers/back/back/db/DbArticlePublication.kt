package co.globers.back.back.db

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.sql.Timestamp
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table


@Entity
@Table(name = "article_publication")
data class DbArticlePublication(

        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long = 0,

        val articleId: Long,
        val articleDraftId: Long
)

@Entity
@Table(name = "article_publication_revision")
data class DbArticlePublicationRevision(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long = 0,

        val articlePublicationId: Long,
        val articleDraftRevisionId: Long,
        val publicationDatetime: Timestamp
)

@Repository
interface ArticlePublicationRepository : JpaRepository<DbArticlePublication, Long> {

    fun findByArticleDraftId(articleDraftId: Long): DbArticlePublication?

}

@Repository
interface ArticlePublicationRevisionRepository : JpaRepository<DbArticlePublicationRevision, Long> {
    fun findByArticleDraftRevisionId(articleDraftRevisionId: Long): DbArticlePublicationRevision?

    fun findFirstByArticlePublicationIdOrderByPublicationDatetimeDesc(articlePublicationId: Long): DbArticlePublicationRevision



}