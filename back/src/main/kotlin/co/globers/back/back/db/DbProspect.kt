package co.globers.back.back.db

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table


@Entity
@Table(name = "prospect")
data class DbProspect(

        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long = 0,
        val email: String,
        val origin: String,
        val comment: String?
)

@Repository
interface ProspectRepository : JpaRepository<DbProspect, Long> {

}