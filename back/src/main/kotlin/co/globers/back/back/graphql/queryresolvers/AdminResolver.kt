package co.globers.back.back.graphql.queryresolvers

import co.globers.back.back.db.UserRepository
import co.globers.back.back.graphql.Admin
import co.globers.back.back.graphql.User
import co.globers.back.back.utils.toGraphQl
import com.coxautodev.graphql.tools.GraphQLResolver
import org.springframework.stereotype.Component

@Component
class AdminResolver(
        private val userRepository: UserRepository) : GraphQLResolver<Admin> {

    @Suppress("unused")
    fun users(admin: Admin): List<User> {
        return userRepository.findAll().map { toGraphQl(it) }
    }

}