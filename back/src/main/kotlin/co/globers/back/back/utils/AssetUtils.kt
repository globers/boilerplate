package co.globers.back.back.utils

import co.globers.back.back.graphql.FileFormat
import co.globers.back.back.graphql.GraphqlFile
import com.cloudinary.Cloudinary
import org.apache.commons.codec.binary.Base64
import org.springframework.stereotype.Component


@Component
class AssetUtils(private val globersConfig: GlobersConfig) {

    private val cloudinaryService = Cloudinary(
            mapOf(
                    "cloud_name" to globersConfig.cloudinary.cloud_name,
                    "api_key" to globersConfig.cloudinary.api_key,
                    "api_secret" to globersConfig.cloudinary.api_secret)
    )

    private val cloudinaryUrlPrefix = "https://res.cloudinary.com/globers/image/upload"

    fun uploadMediaLogo(logoImage: GraphqlFile, mediaUri: String): String {

        val publicId = "${globersConfig.cloudinary.environment}/medias/$mediaUri/logo"
        return uploadAndGetUrl(publicId, logoImage)
    }


    private fun uploadAndGetUrl(publicId: String, image: GraphqlFile): String {
        val options = mapOf(
                "public_id" to publicId
        )

        val uploadResult = when (image.fileFormat) {
            FileFormat.BASE_64 -> {
                // The file received if of the form "data:image/[FILE_TYPE];base64,[BINARY_FILE_CONTENT_AS_BASE64]"
                val imageWithoutHeader = image.file.split(",")[1]
                val imageByteArray = Base64.decodeBase64(imageWithoutHeader.toByteArray())
                cloudinaryService.uploader().upload(imageByteArray, options)
            }
            FileFormat.URL -> cloudinaryService.uploader().upload(image.file, options)
        }

        return uploadResult["secure_url"] as String
    }


    fun uploadArticleMainImage(imageBaseUrl: String, mediaUri: String, articleUri: String): String {

        val publicId = "${globersConfig.cloudinary.environment}/medias/$mediaUri/articles/$articleUri/mainImage"
        val options = mapOf(
                "public_id" to publicId

        )
        val uploadResult = cloudinaryService.uploader().upload(imageBaseUrl, options)
        return uploadResult["secure_url"] as String
    }


    fun uploadArticleDraftImage(image: GraphqlFile, mediaUri: String, draftUri: String): String {

        val publicIdPrefix = "${globersConfig.cloudinary.environment}/medias/$mediaUri/drafts/$draftUri"

        // Image already uploaded for this draft
        if (image.fileFormat == FileFormat.URL && image.file.contains(Regex("^$cloudinaryUrlPrefix/v[0-9]+/$publicIdPrefix"))) {
            return image.file
        }

        val imageUri = generateUriByHash("${image.fileFormat}/${image.file}")

        val publicId = "$publicIdPrefix/$imageUri"
        return uploadAndGetUrl(publicId, image)
    }

    fun uploadProfilePicture(profilePicture: GraphqlFile, userUri: String): String {
        val publicId = "${globersConfig.cloudinary.environment}/users/$userUri/profilePicture"
        return uploadAndGetUrl(publicId, profilePicture)
    }


}
