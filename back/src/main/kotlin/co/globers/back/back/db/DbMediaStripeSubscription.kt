package co.globers.back.back.db

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table


@Entity
@Table(name = "media_stripe_subscription")
data class DbMediaStripeSubscription(

        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long = 0,

        val stripeSubscriptionId: String,
        val mediaStripeCustomerInternalId: Long,
        val mediaStripePlanInternalId: Long
)

@Repository
interface MediaStripeSubscriptionRepository : JpaRepository<DbMediaStripeSubscription, Long> {


}