package co.globers.back.back.utils

import java.util.regex.Pattern


class EmailValidator {

    companion object {

        private const val emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$"

        private val pat = Pattern.compile(emailRegex)

        fun isValid(email: String): Boolean {
            return pat.matcher(email).matches()
        }
    }
}
