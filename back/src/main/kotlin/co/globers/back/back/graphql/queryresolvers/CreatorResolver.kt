package co.globers.back.back.graphql.queryresolvers

import co.globers.back.back.db.MediaContributorId
import co.globers.back.back.db.MediaContributorRepository
import co.globers.back.back.db.MediaRepository
import co.globers.back.back.db.MediaStripeAccountRepository
import co.globers.back.back.graphql.ContributedMedia
import co.globers.back.back.graphql.Creator
import co.globers.back.back.utils.toGraphQl
import com.coxautodev.graphql.tools.GraphQLResolver
import org.springframework.stereotype.Component

@Component
class CreatorResolver(
        private val mediaRepository: MediaRepository,
        private val mediaContributorRepository: MediaContributorRepository,
        private val mediaStripeAccountRepository: MediaStripeAccountRepository
) : GraphQLResolver<Creator> {


    fun contributedMedia(creator: Creator, mediaUri: String): ContributedMedia? {

        val dbMedia = mediaRepository.findByUri(mediaUri) ?: return null
        val dbMediaContributor = mediaContributorRepository.findById(MediaContributorId(creator.id, dbMedia.id))

        val stripeAccountConnected = mediaStripeAccountRepository.existsByMediaId(dbMedia.id)

        return if (dbMediaContributor.isPresent) {
            ContributedMedia.build(creator.uri, toGraphQl(dbMedia), stripeAccountConnected)
        } else {
            null
        }
    }

    fun contributedMedias(creator: Creator): List<ContributedMedia> {
        val mediaContributors = mediaContributorRepository.findByMediaContributorIdCreatorId(creator.id)
        val medias = mediaRepository.findAllById(mediaContributors.map { it.mediaContributorId.mediaId })

        return medias.map { ContributedMedia.build(creator.uri, toGraphQl(it), mediaStripeAccountRepository.existsByMediaId(it.id)) }
    }
}