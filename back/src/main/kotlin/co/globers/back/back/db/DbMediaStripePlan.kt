package co.globers.back.back.db

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table


@Entity
@Table(name = "media_stripe_plan")
data class DbMediaStripePlan(

        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long = 0,
        val stripePlanId: String,
        val mediaStripeProductInternalId: Long
)

@Repository
interface MediaStripePlanRepository : JpaRepository<DbMediaStripePlan, Long> {

}