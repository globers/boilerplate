package co.globers.back.back.db

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table


@Entity
@Table(name = "media_contract_terms")
data class DbMediaContractTerms(

        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long = 0,
        val mediaId: Long,
        val globersFeePercent: Double
)

@Repository
interface MediaContractTermsRepository : JpaRepository<DbMediaContractTerms, Long> {
    fun findByMediaId(mediaId: Long): DbMediaContractTerms
}