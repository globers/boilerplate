package co.globers.back.back.db

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table


@Entity
@Table(name = "media_subscription_plan")
data class DbMediaSubscriptionPlan(

        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long = 0,
        val uri: String,
        val mediaId: Long,
        val country: String,
        val currency: String,
        val price: Double,
        val mediaStripePlanInternalId: Long
)

@Repository
interface MediaSubscriptionPlanRepository : JpaRepository<DbMediaSubscriptionPlan, Long> {

    fun findByMediaId(mediaId: Long): List<DbMediaSubscriptionPlan>
    fun findByUri(uri: String): DbMediaSubscriptionPlan
}