package co.globers.back.back.graphql.dataloaders

import co.globers.back.back.db.ArticleRepository
import co.globers.back.back.graphql.Article
import co.globers.back.back.graphql.Media
import co.globers.back.back.utils.toGraphQl
import org.dataloader.BatchLoader
import org.dataloader.DataLoader
import org.springframework.core.task.SyncTaskExecutor
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Component
import java.util.concurrent.CompletableFuture
import java.util.concurrent.CompletionStage
import java.util.function.Supplier


data class ArticlesByMediaKey(val media: Media, val nbMostRecent: Int)

@Component
class ArticlesByMediaBatchLoader(
        private val articleRepository: ArticleRepository) : BatchLoader<ArticlesByMediaKey, List<Article>> {

    // NB: Contract is not respected (n most recent by media is replaced by n*nbMedia most recent)
    // Solution is to make a loop where we check which media is not fulfilled and we redo
    // another request with those medias
    override fun load(keys: List<ArticlesByMediaKey>): CompletionStage<List<List<Article>>> {

        val supplier: Supplier<List<List<Article>>> = Supplier {
            val mediaIds = keys.map { it.media.id }
            val nbArticlesHeuristic = keys.sumBy { it.nbMostRecent }

            val mostRecent = articleRepository
                    .findMostRecentPublishedFromMediaList(mediaIds, PageRequest.of(0, nbArticlesHeuristic))
                    .groupBy { it.mediaId }

            keys.map { (media) ->
                val articles = mostRecent.getOrDefault(media.id, emptyList())
                articles.map { article ->
                    toGraphQl(article)
                }
            }
        }

        return CompletableFuture.supplyAsync<List<List<Article>>>(
                supplier,
                SyncTaskExecutor())
    }
}


class ArticlesByMediaDataLoader(articlesByMediaBatchLoader: ArticlesByMediaBatchLoader)
    : DataLoader<ArticlesByMediaKey, List<Article>>(articlesByMediaBatchLoader)
