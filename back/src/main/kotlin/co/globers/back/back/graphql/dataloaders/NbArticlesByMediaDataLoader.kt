package co.globers.back.back.graphql.dataloaders

import co.globers.back.back.db.ArticleRepository
import org.dataloader.BatchLoader
import org.dataloader.DataLoader
import org.springframework.core.task.SyncTaskExecutor
import org.springframework.stereotype.Component
import java.time.Instant
import java.util.concurrent.CompletableFuture
import java.util.concurrent.CompletionStage
import java.util.function.Supplier

data class NbArticleByMediaKey(val mediaId: Long, val since: Instant)


@Component
class NbArticlesByMediaBatchLoader(
        private val articleRepository: ArticleRepository
) : BatchLoader<NbArticleByMediaKey, Int> {

    override fun load(keys: List<NbArticleByMediaKey>): CompletionStage<List<Int>> {

        val supplier: Supplier<List<Int>> = Supplier {

            val keyToNbArticles = mutableMapOf<NbArticleByMediaKey, Int>()
            val groups = keys.groupBy { it.since }

            for ((date, listKeys) in groups) {
                val countsByMediaId = articleRepository.countNbArticleByMediaId(listKeys.map { it.mediaId }, date)

                val keyToNbThisGroup = countsByMediaId.map { NbArticleByMediaKey(it[0] as Long, date) to (it[1] as Long).toInt() }
                keyToNbArticles.putAll(keyToNbThisGroup)
            }
            keys.map { keyToNbArticles.getOrDefault(it, 0) }
        }

        return CompletableFuture.supplyAsync<List<Int>>(
                supplier,
                SyncTaskExecutor())
    }
}


class NbArticlesByMediaLoader(nbArticlesByMediaBatchLoader: NbArticlesByMediaBatchLoader)
    : DataLoader<NbArticleByMediaKey, Int>(nbArticlesByMediaBatchLoader)
