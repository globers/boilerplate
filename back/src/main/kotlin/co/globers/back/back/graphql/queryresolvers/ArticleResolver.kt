package co.globers.back.back.graphql.queryresolvers

import co.globers.back.back.db.ArticleRepository
import co.globers.back.back.db.MediaRepository
import co.globers.back.back.graphql.Article
import co.globers.back.back.utils.getContent
import com.coxautodev.graphql.tools.GraphQLResolver
import org.springframework.stereotype.Component

@Component
class ArticleResolver(
        private val articleRepository: ArticleRepository,
        private val mediaRepository: MediaRepository) : GraphQLResolver<Article> {


    @Suppress("unused")
    fun content(article: Article): String {
        val dbArticle = articleRepository.findByUri(article.uri)
        val dbMedia = mediaRepository.findById(dbArticle.mediaId).get()
        return getContent(dbArticle, dbMedia.originHtmlContentSelector)
    }
}