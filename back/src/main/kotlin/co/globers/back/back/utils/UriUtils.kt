package co.globers.back.back.utils

import org.apache.commons.lang3.StringUtils

// 12 exa = 48 bits; proba collision is 1e-11 with 100 entries. cf. "birthday paradoxe"
private const val NB_HEXA_CHAR_DISAMBIGUATION = 12
// 32 hexa = 128 bits, proba of collision is 0 (<< 1e-16 %) even with 100 billions entries
private const val NB_HEXA_CHAR_UNICITY = 32
// too long uri are bad for SEO
private const val MAX_URI_SIZE = 80

fun generateUriByCleanAndHash(strToClean: String, strToHashToEnsureUniqueness: String) : String {
    return generateUriByClean(strToClean)+ "-" +
            generateUriByHash(strToClean +
                    strToHashToEnsureUniqueness).substring(0, NB_HEXA_CHAR_DISAMBIGUATION)
}

fun generateUriByClean(strToClean: String) : String {

    return StringUtils.stripAccents(strToClean) //replace accentuated chars by equivalent
            .toLowerCase()
            .replace(Regex("[ ']+"), "-") // replace separators by "-"
            .replace(Regex("[^a-z0-9-]"), "") //remove all non standards characters
            .replace(Regex("[-]+"),"-") //remove duplicated "-"
            .trim('-') //remove trailing "-"
            .take(MAX_URI_SIZE)
}


fun generateUriByHash(strToHashToEnsureUniqueness: String) : String {
    return org.apache.commons.codec.digest.DigestUtils.sha256Hex(strToHashToEnsureUniqueness)
            .substring(0, NB_HEXA_CHAR_UNICITY)
}
