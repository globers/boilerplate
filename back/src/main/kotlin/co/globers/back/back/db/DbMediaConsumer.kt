package co.globers.back.back.db

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.io.Serializable
import javax.persistence.Embeddable
import javax.persistence.EmbeddedId
import javax.persistence.Entity
import javax.persistence.Table


@Embeddable
data class MediaConsumerId (val userId: Long, val mediaId: Long) : Serializable

@Entity
@Table(name = "media_consumer")
 data class DbMediaConsumer(

        @EmbeddedId
        val mediaConsumerId: MediaConsumerId,
        val follow: Boolean
)

@Repository
interface MediaConsumerRepository : JpaRepository<DbMediaConsumer, MediaConsumerId> {


    @Query("SELECT mc from DbMediaConsumer mc " +
            "JOIN DbUser u ON mc.mediaConsumerId.userId = u.id " +
            "WHERE u.uri= :userUri")
    fun findByUserUri(userUri: String): List<DbMediaConsumer>


    @Query("SELECT mc from DbMediaConsumer mc " +
            "JOIN DbUser u ON mc.mediaConsumerId.userId = u.id " +
            "JOIN DbMedia m ON mc.mediaConsumerId.mediaId = m.id " +
            "WHERE u.uri= :userUri AND m.uri = :mediaUri")
    fun findByUserUriMediaUri(userUri: String, mediaUri: String): DbMediaConsumer?

}
