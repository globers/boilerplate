package co.globers.back.back.graphql

import co.globers.back.back.db.ArticleConsumerId
import co.globers.back.back.db.ArticleConsumerRepository
import co.globers.back.back.db.ArticleDraftRepository
import co.globers.back.back.db.ArticleDraftRevisionRepository
import co.globers.back.back.db.ArticlePublicationRepository
import co.globers.back.back.db.ArticlePublicationRevisionRepository
import co.globers.back.back.db.ArticleRepository
import co.globers.back.back.db.CreatorRepository
import co.globers.back.back.db.DbArticleConsumer
import co.globers.back.back.db.DbArticle
import co.globers.back.back.db.DbArticleDraft
import co.globers.back.back.db.DbArticleDraftRevision
import co.globers.back.back.db.DbArticlePublication
import co.globers.back.back.db.DbArticlePublicationRevision
import co.globers.back.back.db.DbCreator
import co.globers.back.back.db.DbMedia
import co.globers.back.back.db.DbMediaConsumer
import co.globers.back.back.db.DbMediaContributor
import co.globers.back.back.db.DbMediaStripeAccount
import co.globers.back.back.db.DbProspect
import co.globers.back.back.db.DbResetPassword
import co.globers.back.back.db.DbUser
import co.globers.back.back.db.MediaConsumerId
import co.globers.back.back.db.MediaConsumerRepository
import co.globers.back.back.db.MediaContributorId
import co.globers.back.back.db.MediaContributorRepository
import co.globers.back.back.db.MediaRepository
import co.globers.back.back.db.MediaStripeAccountRepository
import co.globers.back.back.db.ProspectRepository
import co.globers.back.back.db.ResetPasswordRepository
import co.globers.back.back.db.UserRepository
import co.globers.back.back.db.UserSubscriptionRepository
import co.globers.back.back.utils.AssetUtils
import co.globers.back.back.utils.EmailUtils
import co.globers.back.back.utils.EmailValidator
import co.globers.back.back.utils.JwtUtils
import co.globers.back.back.utils.generateUriByClean
import co.globers.back.back.utils.generateUriByCleanAndHash
import co.globers.back.back.utils.generateUriByHash
import co.globers.back.back.utils.toGraphQl
import co.globers.back.scraper.scrapArticle
import co.globers.back.back.money.SubscriptionServices
import com.coxautodev.graphql.tools.GraphQLMutationResolver
import com.stripe.exception.StripeException
import com.sun.org.apache.bcel.internal.generic.LOR
import graphql.GraphQLException
import graphql.schema.DataFetchingEnvironment
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import java.sql.Timestamp
import java.time.Instant
import java.util.*
import org.slf4j.LoggerFactory;

data class ArticleInput(val title: String, val subtitle: String, val content: String, val imageUrl: String)
data class MediaInput(
        val name: String,
        val logoImage: GraphqlFile,
        val description: String,
        val themePrimaryColor: String,
        val themeSecondaryColor: String,
        val originHtmlContentSelector: String?,
        val unpublished: Boolean)

data class UserInput(val firstName: String, val lastName: String, val profilePicture: GraphqlFile?)
data class LoginInput(val email: String, val plainTextPassword: String)


data class ArticleDraftRevisionInput(
        val title: String?,
        val subtitle: String?,
        val content: String?,
        val image: GraphqlFile?,
        val slatejsValue: String?
)


@Component
class MutationResolver(
        private val mediaRepository: MediaRepository,
        private val articleRepository: ArticleRepository,
        private val mediaConsumerRepository: MediaConsumerRepository,
        private val mediaContributorRepository: MediaContributorRepository,
        private val userRepository: UserRepository,
        private val articleConsumerRepository: ArticleConsumerRepository,
        private val creatorRepository: CreatorRepository,
        private val articleDraftRepository: ArticleDraftRepository,
        private val articleDraftRevisionRepository: ArticleDraftRevisionRepository,
        private val articlePublicationRepository: ArticlePublicationRepository,
        private val articlePublicationRevisionRepository: ArticlePublicationRevisionRepository,
        private val resetPasswordRepository: ResetPasswordRepository,
        private val mediaStripeAccountRepository: MediaStripeAccountRepository,
        private val userSubscriptionRepository: UserSubscriptionRepository,
        private val prospectRepository: ProspectRepository,
        private val jwtUtils: JwtUtils,
        private val assetUtils: AssetUtils,
        private val emailUtils: EmailUtils,
        private val subscriptionServices: SubscriptionServices)
    : GraphQLMutationResolver {

    private val logger = LoggerFactory.getLogger(MutationResolver::class.java)

    @Transactional
    fun createArticleFromUrl(
            mediaUri: String, articleUrl: String, jwtToken: String?, env: DataFetchingEnvironment): Article {
        checkIsMediaContributor(mediaUri, jwtToken, env)

        val article = scrapArticle(articleUrl)
        val publicationDate = Instant.now()

        val uri = generateUriByCleanAndHash(article.title, mediaUri)
        val imageUrl = assetUtils.uploadArticleMainImage(article.imageUrl, mediaUri, uri)
        val dbMedia = mediaRepository.findByUri(mediaUri)
                ?: throw GraphQLException("createArticleFromUrl failed, media '$mediaUri' not found")

        val dbArticle = articleRepository.save(
                DbArticle(
                        uri = uri,
                        title = article.title,
                        subtitle = "", // subtitle not managed for scrapped article
                        originUrl = articleUrl,
                        originHtml = article.originHtml,
                        content = null,
                        mediaId = dbMedia.id,
                        imageUrl = imageUrl,
                        unpublished = false,
                        firstPublication = publicationDate,
                        lastPublication = publicationDate))

        return toGraphQl(dbArticle)
    }

    private fun checkIsMediaContributor(mediaUri: String, jwtToken: String?, env: DataFetchingEnvironment) {
        val userUri = jwtUtils.getUserUri(jwtToken, env)
        if (mediaContributorRepository.findByUserUriMediaUri(userUri, mediaUri) == null)
            throw GraphQLException("user '$userUri' is not contributor of media '$mediaUri'")
    }

    private fun checkIsMediaContributor(dbArticle: DbArticle, jwtToken: String?, env: DataFetchingEnvironment) {
        val userUri = jwtUtils.getUserUri(jwtToken, env)
        if (mediaContributorRepository.findByUserUriMediaId(userUri, dbArticle.mediaId) == null)
            throw GraphQLException("user '$userUri' is not contributor of article '${dbArticle.uri}'")
    }


    @Transactional
    fun createMedia(media: MediaInput, jwtToken: String?, env: DataFetchingEnvironment): SaveMediaReply {
        val reply = saveMedia(jwtToken, env, media, null, null, false)

        if (reply is SaveMediaSuccess) {
            val userUri = jwtUtils.getUserUri(jwtToken, env)
            val user = userRepository.findByUri(userUri)
            emailUtils.sendMediaCreatedEmail(user.email, reply.contributedMedia.media.name, reply.contributedMedia.media.uri)
        }

        return reply
    }

    private fun saveMedia(jwtToken: String?, env: DataFetchingEnvironment, media: MediaInput, mediaId: Long?, mediaUri: String?, stripeAccountConnected: Boolean): SaveMediaReply {
        val userUri = jwtUtils.getUserUri(jwtToken, env)
        val userId = userRepository.findByUri(userUri).id

        val dbCreator = creatorRepository.findByUserId(userId) ?: creatorRepository.save(DbCreator(userId = userId))
        val creator = Creator.build(dbCreator.id, userUri)

        if (media.name.isBlank()) return SaveMediaFailure(SaveMediaFailureReason.EMPTY_NAME)
        if (media.description.isBlank()) return SaveMediaFailure(SaveMediaFailureReason.EMPTY_DESCRIPTION)
        if (media.logoImage.file.isBlank())
            return SaveMediaFailure(SaveMediaFailureReason.EMPTY_LOGO_IMAGE)

        var uri = mediaUri
        if (uri == null) {
            uri = generateUriByClean(media.name)
            if (mediaRepository.existsByUri(uri)) return SaveMediaFailure(SaveMediaFailureReason.NAME_ALREADY_EXISTS)
        }

        val logoUrl: String
        try {
            logoUrl = assetUtils.uploadMediaLogo(media.logoImage, uri)
        } catch (e: Exception) {
            return SaveMediaFailure(SaveMediaFailureReason.INVALID_LOGO_IMAGE)
        }

        val dbMedia = mediaRepository.save(
                DbMedia(
                        id = mediaId ?: 0,
                        uri = uri,
                        name = media.name,
                        logoUrl = logoUrl,
                        description = media.description,
                        themePrimaryColor = media.themePrimaryColor,
                        themeSecondaryColor = media.themeSecondaryColor,
                        originHtmlContentSelector = media.originHtmlContentSelector,
                        unpublished = media.unpublished)
        )

        mediaContributorRepository.save(DbMediaContributor(MediaContributorId(creatorId = creator.id, mediaId = dbMedia.id)))

        return SaveMediaSuccess(ContributedMedia.build(creator.uri, toGraphQl(dbMedia), stripeAccountConnected))
    }


    @Transactional
    fun updateMedia(mediaUri: String, media: MediaInput, jwtToken: String?, env: DataFetchingEnvironment): SaveMediaReply {
        logger.info("graphql/mutation/updateMedia($mediaUri)")

        checkIsMediaContributor(mediaUri, jwtToken, env)
        val dbMedia = mediaRepository.findByUri(mediaUri)
                ?: throw GraphQLException("updateMedia failed, media '$mediaUri' not found")

        val stripeAccountConnected = mediaStripeAccountRepository.existsByMediaId(dbMedia.id)
        return saveMedia(jwtToken, env, media, dbMedia.id, dbMedia.uri, stripeAccountConnected)
    }


    @Transactional
    fun createArticle(
            mediaUri: String, article: ArticleInput, jwtToken: String?, env: DataFetchingEnvironment): Article {
        checkIsMediaContributor(mediaUri, jwtToken, env)

        val uri = generateUriByCleanAndHash(article.title, mediaUri)
        val imageUrl = assetUtils.uploadArticleMainImage(article.imageUrl, mediaUri, uri)
        val media = mediaRepository.findByUri(mediaUri)
                ?: throw GraphQLException("createArticle failed, media '$mediaUri' not found")

        val now = Instant.now()
        val dbArticle = articleRepository.save(
                DbArticle(
                        uri = uri,
                        title = article.title,
                        subtitle = article.subtitle,
                        originUrl = null,
                        originHtml = null,
                        content = article.content,
                        mediaId = media.id,
                        imageUrl = imageUrl,
                        unpublished = false,
                        firstPublication = now,
                        lastPublication = now))
        return toGraphQl(dbArticle)
    }

    private val minPasswordSize = 6


    @Transactional
    fun createUser(user: UserInput, login: LoginInput): SaveUserReply {
        val result = saveUser(user, login, null)
        if (result is SaveUserSuccess) {
            emailUtils.sendAccountCreatedEmail(login.email, user.firstName)
        }
        return result
    }

    private fun saveUser(user: UserInput, login: LoginInput?, previousDbUser: DbUser?): SaveUserReply {

        if (login == null && previousDbUser == null) {
            throw GraphQLException("BUG: login cannot be null if previousDbUser is")
        }

        val id: Long
        val uri: String
        val email: String
        val pswd: String

        // we don't change id / uri if user already exists
        if (previousDbUser != null) {
            id = previousDbUser.id
            uri = previousDbUser.uri
        } else {
            id = 0
            uri = generateUriByHash(login!!.email)
        }

        // we change email / password only if login is given
        if (login != null) {
            if (!EmailValidator.isValid(login.email)) return SaveUserFailure(SaveUserFailureReason.INVALID_EMAIL)
            if (login.plainTextPassword.length < minPasswordSize) return SaveUserFailure(SaveUserFailureReason.PASSWORD_TOO_SHORT)
            if (userRepository.findByEmailIgnoreCase(login.email) != null) return SaveUserFailure(SaveUserFailureReason.EMAIL_ALREADY_EXISTS)

            email = login.email
            pswd = BCryptPasswordEncoder().encode(login.plainTextPassword)
        } else {
            email = previousDbUser!!.email
            pswd = previousDbUser.pswd
        }

        if (user.firstName.isBlank()) return SaveUserFailure(SaveUserFailureReason.EMPTY_FIRSTNAME)
        if (user.lastName.isBlank()) return SaveUserFailure(SaveUserFailureReason.EMPTY_LASTNAME)


        val profilePictureUrl: String? = if (user.profilePicture == null) {
            null
        } else {
            try {
                assetUtils.uploadProfilePicture(user.profilePicture, uri)
            } catch (e: Exception) {
                return SaveUserFailure(SaveUserFailureReason.INVALID_PROFILE_PICTURE)
            }
        }

        val dbUser = userRepository.save(DbUser(
                id = id,
                uri = uri,
                firstName = user.firstName,
                lastName = user.lastName,
                email = email,
                pswd = pswd,
                profilePictureUrl = profilePictureUrl

        ))

        return SaveUserSuccess(
                toGraphQl(dbUser),
                jwtUtils.buildUserToken(dbUser.uri))
    }

    @Transactional
    fun updateUser(user: UserInput, jwtToken: String?, env: DataFetchingEnvironment): SaveUserReply {

        val userUri = jwtUtils.getUserUri(jwtToken, env)
        val dbUser = userRepository.findByUri(userUri)

        return saveUser(user, null, dbUser)
    }


    @Transactional
    fun loginUser(login: LoginInput): LoginReply {
        logger.info("graphql/mutation/loginUser(${login.email})")
        val dbUser = userRepository.findByEmailIgnoreCase(login.email)
                ?: return LoginFailure(LoginFailureReason.UNKNOWN_USER)

        return if (BCryptPasswordEncoder().matches(login.plainTextPassword, dbUser.pswd)) {
            LoginSuccess(
                    toGraphQl(dbUser),
                    jwtUtils.buildUserToken(dbUser.uri))
        } else {
            LoginFailure(LoginFailureReason.INVALID_PASSWORD)
        }
    }


    @Transactional
    fun followMedia(mediaUri: String, follow: Boolean, jwtToken: String?, env: DataFetchingEnvironment): UserMedia {
        val userUri = jwtUtils.getUserUri(jwtToken, env)

        val dbUser = userRepository.findByUri(userUri)
        val dbMedia = mediaRepository.findByUri(mediaUri)
                ?: throw GraphQLException("followMedia failed, media '$mediaUri' not found")

        mediaConsumerRepository.save(DbMediaConsumer(MediaConsumerId(dbUser.id, dbMedia.id), follow))
        return UserMedia.build(userUri, toGraphQl(dbMedia), follow)
    }

    @Transactional
    fun bookmarkArticle(articleUri: String, bookmark: Boolean, jwtToken: String?, env: DataFetchingEnvironment)
            : UserArticle {

        val userUri = jwtUtils.getUserUri(jwtToken, env)

        val dbUser = userRepository.findByUri(userUri)
        val dbArticle = articleRepository.findByUri(articleUri)

        val dbArticleConsumer =
                articleConsumerRepository.save(DbArticleConsumer(ArticleConsumerId(dbUser.id, dbArticle.id), bookmark))

        return UserArticle.build(
                userUri, toGraphQl(dbArticle), dbArticleConsumer.bookmark)
    }

    @Transactional
    fun createArticleDraft(mediaUri: String, jwtToken: String?, env: DataFetchingEnvironment): ArticleDraft {

        val userUri = jwtUtils.getUserUri(jwtToken, env)

        checkIsMediaContributor(mediaUri, jwtToken, env)

        val dbMedia = mediaRepository.findByUri(mediaUri)
                ?: throw GraphQLException("createArticleDraft failed, media '$mediaUri' not found")
        val dbDraft = articleDraftRepository.save(DbArticleDraft(uri = generateUriByHash("$userUri/$mediaUri/${Instant.now()}"), mediaId = dbMedia.id))

        return toGraphQl(dbDraft)
    }

    @Transactional
    fun createArticleDraftRevision(draftUri: String, revision: ArticleDraftRevisionInput, jwtToken: String?, env: DataFetchingEnvironment)
            : SaveArticleDraftRevisionReply {

        val dbDraft = articleDraftRepository.findByUri(draftUri)
                ?: throw GraphQLException("createArticleDraftRevision failed, draft '$draftUri' not found")
        return createOrUpdateArticleDraftRevision(dbDraft, null, revision, jwtToken, env)
    }

    @Transactional
    fun updateArticleDraftRevision(draftRevisionUri: String, revision: ArticleDraftRevisionInput, jwtToken: String?, env: DataFetchingEnvironment)
            : SaveArticleDraftRevisionReply {

        val dbDraftRevision = articleDraftRevisionRepository.findByUri(draftRevisionUri)
                ?: throw GraphQLException("updateArticleDraftRevision failed, revision '$draftRevisionUri' not found")

        val dbDraft = articleDraftRepository.getOne(dbDraftRevision.articleDraftId)

        return createOrUpdateArticleDraftRevision(dbDraft, dbDraftRevision, revision, jwtToken, env)
    }

    private fun createOrUpdateArticleDraftRevision(dbDraft: DbArticleDraft, previousRevision: DbArticleDraftRevision?, revision: ArticleDraftRevisionInput, jwtToken: String?, env: DataFetchingEnvironment)
            : SaveArticleDraftRevisionReply {

        checkIsDraftContributor(dbDraft, jwtToken, env)

        val userUri = jwtUtils.getUserUri(jwtToken, env)
        val dbCreator = creatorRepository.findByUserUri(userUri)
        val mediaUri = mediaRepository.getOne(dbDraft.mediaId).uri

        val imageUrl = if (revision.image != null) {
            try {
                assetUtils.uploadArticleDraftImage(revision.image, mediaUri, dbDraft.uri)
            } catch (e: Exception) {
                return SaveArticleDraftRevisionFailure(SaveArticleDraftRevisionFailureReason.INVALID_ARTICLE_IMAGE)
            }
        } else {
            null
        }
        val dbRev = toDbArticleDraftRevision(dbDraft, dbCreator, revision, imageUrl, previousRevision)
        val savedDbRev = articleDraftRevisionRepository.save(dbRev)

        return SaveArticleDraftRevisionSuccess(toGraphQl(savedDbRev))
    }


    private fun checkIsDraftContributor(draft: DbArticleDraft, jwtToken: String?, env: DataFetchingEnvironment) {
        val userUri = jwtUtils.getUserUri(jwtToken, env)
        if (mediaContributorRepository.findByUserUriMediaId(userUri, draft.mediaId) == null)
            throw GraphQLException("user '$userUri' is not contributor of article draft '${draft.uri}'")
    }


    private fun toDbArticleDraftRevision(articleDraft: DbArticleDraft, creator: DbCreator, revision: ArticleDraftRevisionInput, imageUrl: String?, previousRevision: DbArticleDraftRevision?): DbArticleDraftRevision {
        val savingDate = Instant.now()
        return DbArticleDraftRevision(
                id = previousRevision?.id ?: 0,
                uri = previousRevision?.uri ?: generateUriByHash("$articleDraft.uri/$savingDate"),
                title = revision.title,
                subtitle = revision.subtitle,
                content = revision.content,
                imageUrl = imageUrl,
                articleDraftId = articleDraft.id,
                slatejsValue = revision.slatejsValue,
                creatorId = creator.id,
                savingDatetime = Timestamp.from(savingDate))
    }

    @Transactional
    fun publishArticleFromDraft(articleDraftRevisionUri: String, jwtToken: String?, env: DataFetchingEnvironment): PublishArticleReply {

        val userUri = jwtUtils.getUserUri(jwtToken, env)
        val revision = articleDraftRevisionRepository.findByUri(articleDraftRevisionUri)
                ?: throw GraphQLException("publishArticleFromDraft failed, revision '$articleDraftRevisionUri' not found")
        val creator = creatorRepository.findByUserUri(userUri)

        // 1) Check right
        if (creator.id != revision.creatorId) {
            throw GraphQLException("publishArticleFromDraft failed, user '$userUri' is not the creator of draft '$articleDraftRevisionUri'")
        }

        // 2) Check revision is ok to be published
        val failureReason = getPublicationFailureReason(revision)
        if (failureReason != null) {
            return PublishArticleFailure(failureReason)
        }

        val title = revision.title!!
        val subtitle = revision.subtitle!!
        val imageUrl = revision.imageUrl!!
        val content = revision.content!!

        var publication = articlePublicationRepository.findByArticleDraftId(revision.articleDraftId)

        val draft = articleDraftRepository.getOne(revision.articleDraftId)

        val now = Instant.now()

        val (uri, firstPublication) = if (publication != null) {
            val previousArticle = articleRepository.getOne(publication.articleId)
            Pair(previousArticle.uri, previousArticle.firstPublication)
        } else {
            val media = mediaRepository.getOne(draft.mediaId)
            Pair(generateUriByCleanAndHash(title, subtitle + media.uri), now)
        }

        // 2) Save article (or update if it has already been published)
        val dbArticle = articleRepository.save(
                DbArticle(
                        id = publication?.articleId ?: 0,
                        uri = uri,
                        title = title,
                        subtitle = subtitle,
                        originUrl = null,
                        originHtml = null,
                        content = content,
                        mediaId = draft.mediaId,
                        imageUrl = imageUrl,
                        unpublished = false,
                        firstPublication = firstPublication,
                        lastPublication = now))

        // 3) create publication if it didn't exist
        if (publication == null) {
            publication = articlePublicationRepository.save(DbArticlePublication(articleId = dbArticle.id, articleDraftId = draft.id))
        }

        // 4) Add publication revision
        articlePublicationRevisionRepository.save(DbArticlePublicationRevision(
                articlePublicationId = publication.id,
                articleDraftRevisionId = revision.id,
                publicationDatetime = Timestamp.from(now)))

        return PublishArticleSuccess(toGraphQl(dbArticle))
    }

    private fun getPublicationFailureReason(revision: DbArticleDraftRevision): PublishArticleFailureReason? {
        return when {
            revision.title.isNullOrBlank() -> PublishArticleFailureReason.EMPTY_TITLE
            revision.subtitle.isNullOrBlank() -> PublishArticleFailureReason.EMPTY_SUBTITLE
            revision.content.isNullOrBlank() -> PublishArticleFailureReason.EMPTY_CONTENT
            revision.imageUrl.isNullOrBlank() -> PublishArticleFailureReason.EMPTY_IMAGE
            else -> null
        }
    }

    @Transactional
    fun sendResetPasswordEmail(email: String): SendResetPasswordEmailReply {
        val user = userRepository.findByEmailIgnoreCase(email) ?: return SendResetPasswordEmailReply.UNKNOWN_USER
        val uri = UUID.randomUUID().toString().replace("-", "") // remove '-' to reduce length

        resetPasswordRepository.save(DbResetPassword(uri, user.id, Timestamp.from(Instant.now())))
        val sendEmailOk = emailUtils.sendResetPasswordEmail(email, user.firstName, uri)
        return if (sendEmailOk) SendResetPasswordEmailReply.SUCCESS else SendResetPasswordEmailReply.EMAIL_DELIVERY_FAILURE
    }

    @Transactional
    fun resetPassword(plainTextNewPassword: String, resetPasswordUri: String): ResetPasswordReply {
        val resetPasswordNullable = resetPasswordRepository.findById(resetPasswordUri)

        if (!resetPasswordNullable.isPresent) return ResetPasswordFailure(ResetPasswordFailureReason.INVALID_RESET_PASSWORD_URI)

        val resetPassword = resetPasswordNullable.get()

        val resetInstant = resetPassword.resetDatetime.toInstant()
        if (Instant.now() > resetInstant.plusSeconds(3600)) {
            return ResetPasswordFailure(ResetPasswordFailureReason.RESET_PASSWORD_URI_EXPIRED)
        }

        if (plainTextNewPassword.length < minPasswordSize) {
            return ResetPasswordFailure(ResetPasswordFailureReason.PASSWORD_TOO_SHORT)
        }

        val dbUser = userRepository.getOne(resetPassword.userId)
        val newHashedPwd = BCryptPasswordEncoder().encode(plainTextNewPassword)
        val modifiedUser = DbUser(dbUser.id, dbUser.uri, dbUser.firstName, dbUser.lastName, dbUser.email, newHashedPwd, dbUser.profilePictureUrl)
        userRepository.save(modifiedUser)

        val newToken = jwtUtils.buildUserToken(dbUser.uri)

        return ResetPasswordSuccess(newToken)
    }


    @Transactional
    fun unpublishArticle(articleUri: String, jwtToken: String?, env: DataFetchingEnvironment): Boolean {

        val dbArticle = articleRepository.findByUri(articleUri)
        checkIsMediaContributor(dbArticle, jwtToken, env)

        articleRepository.save(DbArticle(
                dbArticle.id,
                dbArticle.uri,
                dbArticle.title,
                dbArticle.subtitle,
                dbArticle.originUrl,
                dbArticle.originHtml,
                dbArticle.content,
                dbArticle.mediaId,
                dbArticle.imageUrl,
                true,
                dbArticle.firstPublication,
                dbArticle.lastPublication))
        return true
    }

    @Transactional
    fun uploadImageForDraft(draftUri: String, imageFile: GraphqlFile, jwtToken: String?, env: DataFetchingEnvironment): UploadImageForDraftReply {

        val draft = articleDraftRepository.findByUri(draftUri)
                ?: return UploadImageForDraftFailure(UploadImageForDraftFailureReason.INVALID_DRAFT_URI)

        checkIsDraftContributor(draft, jwtToken, env)
        val media = mediaRepository.getOne(draft.mediaId)

        val imageUrl = try {
            assetUtils.uploadArticleDraftImage(imageFile, media.uri, draft.uri)
        } catch (e: Exception) {
            return UploadImageForDraftFailure(UploadImageForDraftFailureReason.INVALID_IMAGE_FILE)
        }
        return UploadImageForDraftSuccess(imageUrl)

    }

    // TODO Check there is no account yet
    // Transactional is specified by subcriptionServices to rollabck inside try-catch in this function
    fun createStripeAccount(mediaUri: String, account: StripeAccountInput, jwtToken: String?, env: DataFetchingEnvironment): SaveStripeAccountReply {
        logger.info("graphql/mutation/createStripeAccount($mediaUri)/start")

        checkIsMediaContributor(mediaUri, jwtToken, env)
        val userUri = jwtUtils.getUserUri(jwtToken, env)
        val dbUser = userRepository.findByUri(userUri)
        val dbMedia = mediaRepository.findByUri(mediaUri)!!
        val user = toGraphQl(dbUser)
        val media = toGraphQl(dbMedia)

        try {
            subscriptionServices.buildMedia(media, user, account)
        } catch (ex: StripeException) {
            logger.error("graphql/mutation/createStripeAccount($mediaUri)/StripeException/${ex}")
            return SaveStripeAccountFailure(SaveStripeAccountFailureReason.STRIPE_ERROR, ex.message)
        }


        logger.info("graphql/mutation/createStripeAccount($mediaUri)/success")
        return SaveStripeAccountSuccess(null)
    }


    fun subscribeToMedia(mediaSubscriptionPlanUri: String, subscriptionInput: StripeUserSubscriptionInput, jwtToken: String?, env: DataFetchingEnvironment): SubscribeToMediaReply {

        val userUri = jwtUtils.getUserUri(jwtToken, env)

        logger.info("graphql/mutation/subscribeToMedia($mediaSubscriptionPlanUri,$userUri)/start")

        val dbUser = userRepository.findByUri(userUri)
        val user = toGraphQl(dbUser)
        val subs = userSubscriptionRepository.findByUserId(user.id)
        if (subs.isNotEmpty()) {
            return SubscribeToMediaFailure(SubscribeToMediaFailureReason.ALREADY_SUBSCRIBED, null)
        }

        try {
            subscriptionServices.buildSubscription(mediaSubscriptionPlanUri, user, subscriptionInput)
        } catch (ex: StripeException) {
            logger.error("graphql/mutation/subscribeToMedia($mediaSubscriptionPlanUri,$userUri)/StripeException/${ex}")
            return SubscribeToMediaFailure(SubscribeToMediaFailureReason.STRIPE_ERROR, ex.message)
        }


        logger.info("graphql/mutation/subscribeToMedia($mediaSubscriptionPlanUri,$userUri)/success")
        return SubscribeToMediaSuccess(null)
    }

    fun createProspect(email: String, origin: String): Boolean {
        prospectRepository.save(DbProspect(email = email, origin = origin, comment = null))
        return true
    }

}


