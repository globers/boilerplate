package co.globers.back.back.db

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table


interface IDbUserPublicProfile {
    val uri: String
    val firstName: String
    val lastName: String
    val profilePictureUrl: String?
}

@Entity
@Table(name = "user")
data class DbUser(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long = 0,
        override val uri: String,
        override val firstName: String,
        override val lastName: String,
        val email: String,
        val pswd: String,
        override val profilePictureUrl: String?,
        val privilege: String? = null
) : IDbUserPublicProfile

data class DbUserPublicProfile(
        val id: Long = 0,
        override val uri: String,
        override val firstName: String,
        override val lastName: String,
        override val profilePictureUrl: String?,
        val mediaId: Long
) : IDbUserPublicProfile

private const val newDbUserPublicProfileString = "new co.globers.back.back.db.DbUserPublicProfile(" +
        "u.id, u.uri, u.firstName, u.lastName, u.profilePictureUrl, mc.mediaContributorId.mediaId)"

@Repository
interface UserRepository : JpaRepository<DbUser, Long> {

    fun findByUri(uri: String): DbUser
    fun findByEmailIgnoreCase(email: String): DbUser?


    @Query("""
        SELECT $newDbUserPublicProfileString
        FROM DbUser u
        JOIN DbCreator c ON c.userId = u.id
        JOIN DbMediaContributor mc ON mc.mediaContributorId.creatorId = c.id
        WHERE mc.mediaContributorId.mediaId IN :mediaIds
    """)
    fun findOwners(mediaIds: List<Long>): List<DbUserPublicProfile>



}
