package co.globers.back.back.db

import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.time.Instant
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

interface IDbArticleWithoutContent {
    val id: Long
    val uri: String
    val title: String
    val subtitle: String
    val originUrl: String?
    val mediaId: Long
    val imageUrl: String
    val unpublished: Boolean
    val firstPublication: Instant
    val lastPublication: Instant
}

@Entity
@Table(name = "article")
data class DbArticle(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        override val id: Long = 0,
        override val uri: String,
        override val title: String,
        override val subtitle: String,
        override val originUrl: String?,
        val originHtml: String?,
        val content: String?,
        override val mediaId: Long,
        override val imageUrl: String,
        override val unpublished: Boolean,
        override val firstPublication: Instant,
        override val lastPublication: Instant
) : IDbArticleWithoutContent



data class DbArticleWithoutContentProjection(
        override val id: Long,
        override val uri: String,
        override val title: String,
        override val subtitle: String,
        override val originUrl: String?,
        override val mediaId: Long,
        override val imageUrl: String,
        override val unpublished: Boolean,
        override val firstPublication: Instant,
        override val lastPublication: Instant
) : IDbArticleWithoutContent


private const val newDbArticleWithoutContentProjectionString = "new co.globers.back.back.db.DbArticleWithoutContentProjection(" +
"a.id, a.uri, a.title, a.subtitle, a.originUrl, a.mediaId, a.imageUrl, a.unpublished, a.firstPublication, a.lastPublication)"

@Repository
interface ArticleRepository : JpaRepository<DbArticle, Long> {


    @Query("SELECT " + newDbArticleWithoutContentProjectionString +
            " from DbArticle a " +
            "WHERE a.mediaId IN :mediaIds AND a.unpublished = false " +
            "ORDER BY a.firstPublication DESC")
    fun findMostRecentPublishedFromMediaList(mediaIds: List<Long>, pageable: Pageable): List<DbArticleWithoutContentProjection>

    fun findByUri(uri: String): DbArticle
    fun findByUriAndUnpublishedFalse(uri: String): DbArticle?

    @Query("SELECT " + newDbArticleWithoutContentProjectionString +
            " from DbArticle a " +
            "JOIN DbArticleConsumer ac on ac.articleConsumerId.articleId = a.id " +
            "JOIN DbUser u on ac.articleConsumerId.userId = u.id " +
            "WHERE u.uri = :userUri AND a.mediaId IN :mediaIds AND ac.bookmark = true AND a.unpublished = false ")
    fun findBookmarkedByUser(userUri: String, mediaIds: List<Long>): List<DbArticleWithoutContentProjection>


    @Query("""
        SELECT a.mediaId, count(a)
        FROM DbArticle a
        WHERE firstPublication > :minFirstPublication
        AND a.mediaId IN :mediaIds
        GROUP BY a.mediaId
    """)
    fun countNbArticleByMediaId(mediaIds: List<Long>, minFirstPublication: Instant): List<Array<Any>>

}
