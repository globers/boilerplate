package co.globers.back.scraper

import org.jsoup.Jsoup

data class ScrappedArticle(
        val title: String,
        val originHtml: String,
        val imageUrl: String
)


fun scrapArticle(url: String): ScrappedArticle {

    val connection = Jsoup.connect(url)
    val article = connection.get()
    val rawTitle = article.select("meta[property=og:title]")?.attr("content") ?: article.title()
    val siteName = article.select("meta[property=og:site_name]")?.attr("content")
    val cleanedTitle =
            if (siteName != null)
                rawTitle.removeSuffix(siteName).removePrefix(siteName).trim(' ', '|', '-', '/', '•')
            else
                rawTitle

    val imageUrl = article.select("meta[property=og:image]")?.attr("content") ?: ""
    val originHtml = article.outerHtml()
    return ScrappedArticle(cleanedTitle, originHtml, imageUrl)
}
