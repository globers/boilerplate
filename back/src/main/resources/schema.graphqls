

type GraphqlDate {
    nbSecFrom1970: Int!
}

input GraphqlDateInput {
    nbSecFrom1970: Int!
}


type SubscriptionPlan {
    uri: String!
    country: StripeCountry!
    currency: StripeCurrency!
    price: Float!
}

type Media {
    uri: ID!
    name: String!
    logoUrl: String!
    description: String!
    themePrimaryColor: String!
    themeSecondaryColor: String!
    originHtmlContentSelector: String
    owner: UserPublicProfile! # TODO: Replace by an array UserPublicProfile which are the media contributors
    unpublished: Boolean!
    articles(nbMostRecent: Int!): [Article!]!
    article(uri: ID!): Article
    nbArticles(since: GraphqlDateInput!): Int!
    subscriptionPlans: [SubscriptionPlan!]!
}


enum FileFormat {
    BASE_64
    URL
}

input GraphqlFile {
    fileFormat: FileFormat!
    file: String!
}

input MediaInput {
    name: String!
    name: String!
    logoImage: GraphqlFile!
    description: String!
    themePrimaryColor: String!
    themeSecondaryColor: String!
    originHtmlContentSelector: String
    unpublished: Boolean!
}


type Article {
    uri: ID!
    title: String!
    subtitle: String!
    content: String!
    imageUrl: String!
    unpublished: Boolean!
    firstPublication: GraphqlDate!
    lastPublication: GraphqlDate!
}


type UserPublicProfile {
    uri: ID!
    firstName: String!
    lastName: String!
    profilePictureUrl: String
}


type UserSubscription{
    uri: String!
    plan: SubscriptionPlan!
    startSubscriptionDate: GraphqlDate!
}

type User {
    uri: ID!
    firstName: String!
    lastName: String!
    email: String!
    profilePictureUrl: String


    userMedia(mediaUri: ID!): UserMedia
    userMedias: [UserMedia!]!

    creator: Creator

    subscriptions: [UserSubscription!]!
}

type Creator {
    uri: ID!
    contributedMedia(mediaUri: ID!): ContributedMedia
    contributedMedias: [ContributedMedia!]!
}

type Anonymous {
    media(mediaUri: ID!): Media
    medias: [Media!]!
}

union UserOrAnonymous = User | Anonymous

input UserInput {
    firstName: String!
    lastName: String!
    profilePicture: GraphqlFile
}

# -------------------

input LoginInput {
    email: String!
    plainTextPassword: String!
}


type LoginSuccess {
    user: User!
    jwtToken: String!
}

type LoginFailure {
    failureReason: LoginFailureReason!
}

enum LoginFailureReason {
    UNKNOWN_USER
    INVALID_PASSWORD
}

union LoginReply = LoginSuccess | LoginFailure

# -------------------

type SaveUserFailure {
    failureReason: SaveUserFailureReason!
}

enum SaveUserFailureReason {
    EMAIL_ALREADY_EXISTS
    INVALID_EMAIL
    PASSWORD_TOO_SHORT
    EMPTY_FIRSTNAME
    EMPTY_LASTNAME
    WRONG_PREVIOUS_PASSWORD
    INVALID_PROFILE_PICTURE
}

type SaveUserSuccess {
    user: User!
    jwtToken: String!
}

union SaveUserReply = SaveUserSuccess | SaveUserFailure

# -------------------

type UserMedia {
    uri: ID!
    media: Media!
    followed: Boolean!

    userArticles(nbMostRecent: Int!): [UserArticle!]!
    userArticle(articleUri: ID!): UserArticle
}

type UserArticle {
    uri: ID!
    article: Article!
    bookmarked: Boolean!
}

# -------------------

type SaveMediaSuccess {
    contributedMedia: ContributedMedia!
}

enum SaveMediaFailureReason {
    NAME_ALREADY_EXISTS
    EMPTY_NAME
    EMPTY_DESCRIPTION
    EMPTY_LOGO_IMAGE
    INVALID_LOGO_IMAGE
}

type SaveMediaFailure {
    failureReason: SaveMediaFailureReason!
}

union SaveMediaReply = SaveMediaSuccess | SaveMediaFailure

type Subscriber {
    uri: ID!
    profile: UserPublicProfile
    subscription: UserSubscription
}


type ContributedMedia {
    uri: ID!
    media: Media!
    stripeAccountConnected: Boolean!
    subscribers: [Subscriber!]!

    contributedArticles: [ContributedArticle!]!
    contributedArticle(articleDraftUri: ID!): ContributedArticle
}

type ContributedArticle {
    uri: ID!
    draft: ArticleDraft!
    publishedArticle: PublishedArticle
}

type PublishedArticle {
    uri: ID!
    article: Article!
    revision: ArticleDraftRevision!
}

# ----------

input ArticleDraftRevisionInput {
    title: String
    subtitle: String
    content: String
    image: GraphqlFile
    slatejsValue: String
}

type ArticleDraft {
    uri: ID!
    revisions(nbMostRecent: Int!): [ArticleDraftRevision!]!
    revision(articleDraftRevisionUri: ID!): ArticleDraftRevision
}

type ArticleDraftRevision {
    uri: ID!
    title: String
    subtitle: String
    content: String
    imageUrl: String
    slatejsValue: String
    savingDate: GraphqlDate!
}

union SaveArticleDraftRevisionReply = SaveArticleDraftRevisionSuccess | SaveArticleDraftRevisionFailure


type SaveArticleDraftRevisionSuccess {
    revision: ArticleDraftRevision!
}

type SaveArticleDraftRevisionFailure {
    failureReason: SaveArticleDraftRevisionFailureReason!
}

enum SaveArticleDraftRevisionFailureReason {
    INVALID_ARTICLE_IMAGE
}

# -------------------

union PublishArticleReply = PublishArticleSuccess | PublishArticleFailure

type PublishArticleFailure {
    failureReason: PublishArticleFailureReason!
}

enum PublishArticleFailureReason {
    EMPTY_TITLE
    EMPTY_SUBTITLE
    EMPTY_CONTENT
    EMPTY_IMAGE
}

type PublishArticleSuccess {
    article: Article!
}

# ------------------- RESET PASSWORD -----------------

enum SendResetPasswordEmailReply {
    SUCCESS,
    UNKNOWN_USER,
    EMAIL_DELIVERY_FAILURE
}

union ResetPasswordReply = ResetPasswordSuccess | ResetPasswordFailure

type ResetPasswordSuccess {
    jwtToken: String!
}

type ResetPasswordFailure {
    failureReason: ResetPasswordFailureReason
}

enum ResetPasswordFailureReason{
    PASSWORD_TOO_SHORT
    RESET_PASSWORD_URI_EXPIRED
    INVALID_RESET_PASSWORD_URI
}

# -------------------


union UploadImageForDraftReply = UploadImageForDraftSuccess | UploadImageForDraftFailure

type UploadImageForDraftSuccess {
    imageUrl: String!
}

type UploadImageForDraftFailure {
    failureReason: UploadImageForDraftFailureReason!
}

enum UploadImageForDraftFailureReason {
    INVALID_IMAGE_FILE
    INVALID_DRAFT_URI
}

# --------------------- Stripe account -------------------


enum StripeCountry {
    FR
}

enum StripeCurrency {
    EUR
}


input StripeAccountInput {
    country: StripeCountry!
    defaultCurrency: StripeCurrency!
    accountTokenId: String! # stripe account data: legal entity, tos
    externalAccountTokenId: String! # bank account token
}

union SaveStripeAccountReply = SaveStripeAccountSuccess | SaveStripeAccountFailure

type SaveStripeAccountSuccess {
    ok: Boolean
}

type SaveStripeAccountFailure {
    failureReason: SaveStripeAccountFailureReason!
    stripeErrorMessage: String # Error message if failureReason == STRIPE_ERROR
}

enum SaveStripeAccountFailureReason {
    STRIPE_ERROR
}

# ------------------------End Stripe account ------------------------

# ----------------------- Media Subscription ------------------------

input StripeUserSubscriptionInput {
    userSourceTokenId: String!
}

union SubscribeToMediaReply = SubscribeToMediaSuccess | SubscribeToMediaFailure

type SubscribeToMediaSuccess {
    ok: Boolean
}

type SubscribeToMediaFailure {
    failureReason: SubscribeToMediaFailureReason!
    stripeErrorMessage: String # Error message if failureReason == STRIPE_ERROR
}

enum SubscribeToMediaFailureReason {
    ALREADY_SUBSCRIBED
    STRIPE_ERROR
}

# ----------------------- End subscription ---------------------------


# ------------- Admin



type Admin {
    uri: ID!
    users: [User!]!
}


# ------- End Amdin



# The Root Query for the application
type Query {
    # return null in user is not authenticated
    user(jwtToken: String): UserOrAnonymous!
    admin(jwtToken: String): Admin

    # useless, just so java graphql know types when only used in Unions
    _useless : _Useless!
}


type Mutation {

    createArticleDraft(mediaUri: ID!, jwtToken: String): ArticleDraft! # Return a reply as others
    createArticleDraftRevision(draftUri: ID!, revision: ArticleDraftRevisionInput!, jwtToken: String)
        : SaveArticleDraftRevisionReply!

    updateArticleDraftRevision(draftRevisionUri: ID!, revision: ArticleDraftRevisionInput!, jwtToken: String)
    : SaveArticleDraftRevisionReply!

    uploadImageForDraft(draftUri: ID!, imageFile: GraphqlFile!, jwtToken: String): UploadImageForDraftReply!

    # unused: should be removed ?
    createArticleFromUrl(mediaUri: ID!,  articleUrl: String!, jwtToken: String): Article!

    publishArticleFromDraft(articleDraftRevisionUri: ID!, jwtToken: String): PublishArticleReply!

    # Return proper reply (instead of boolean)?
    unpublishArticle(articleUri: ID!, jwtToken: String): Boolean

    createMedia(media: MediaInput!, jwtToken: String): SaveMediaReply!
    updateMedia(mediaUri: ID!, media: MediaInput!, jwtToken: String): SaveMediaReply!

    createStripeAccount(mediaUri: ID!, account: StripeAccountInput!, jwtToken: String): SaveStripeAccountReply!
    subscribeToMedia(mediaSubscriptionPlanUri: ID!, subscriptionInput: StripeUserSubscriptionInput!, jwtToken: String): SubscribeToMediaReply!

    createUser(user: UserInput!, login: LoginInput!): SaveUserReply!
    # TODO: add optional login and previousPassword parameter to make possible email / password update
    # But email update requires more work: check previous password, update stripe info, verify email validity
    updateUser(user: UserInput!, jwtToken: String): SaveUserReply!
    loginUser(login: LoginInput!): LoginReply!
    sendResetPasswordEmail(email: String!): SendResetPasswordEmailReply!
    resetPassword(plainTextNewPassword: String!, resetPasswordUri: String!): ResetPasswordReply!

    followMedia(mediaUri: ID!, follow: Boolean!, jwtToken: String): UserMedia!
    bookmarkArticle(articleUri: ID!, bookmark: Boolean!, jwtToken: String): UserArticle!

    createProspect(email: String!, origin: String!): Boolean
}


type _Useless {
    anonymous : Anonymous
    user: User
    saveUserSuccess: SaveUserSuccess
    saveUserFailure: SaveUserFailure
    loginSuccess: LoginSuccess
    loginFailure: LoginFailure
    saveMediaSuccess: SaveMediaSuccess
    saveMediaFailure: SaveMediaFailure

    saveArticleDraftRevisionSuccess: SaveArticleDraftRevisionSuccess
    saveArticleDraftRevisionFailure: SaveArticleDraftRevisionFailure

    publishArticleSuccess: PublishArticleSuccess
    publishArticleFailure: PublishArticleFailure

    resetPasswordSuccess: ResetPasswordSuccess
    resetPasswordFailure: ResetPasswordFailure

    uploadImageForDraftSuccess: UploadImageForDraftSuccess
    uploadImageForDraftFailure: UploadImageForDraftFailure

    saveStripeAccountSuccess: SaveStripeAccountSuccess
    saveStripeAccountFailure: SaveStripeAccountFailure

    subscribeToMediaSuccess: SubscribeToMediaSuccess
    subscribeToMediaFailure: SubscribeToMediaFailure

}

schema {
    query: Query
    mutation: Mutation
}
