package co.globers.back.back

import co.globers.back.back.db.DbMedia
import co.globers.back.back.db.MediaRepository
import junit.framework.TestCase
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@ActiveProfiles("test")
@AutoConfigureTestDatabase //use embedded database H2
// Line under replaced for issue: https://github.com/graphql-java-kickstart/graphql-spring-boot/issues/113
// it appeared when I updated graphql libs from 4.XX to 5.XX
//@SpringBootTest //Load all spring boot context
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class BackApplicationTests {

    @Autowired
    private lateinit var mediaRepository: MediaRepository

    @Test
    fun myFirstTest() {

        mediaRepository.save(DbMedia(uri="uri", name="name", logoUrl = "", description = "", themePrimaryColor = "", themeSecondaryColor = "", originHtmlContentSelector = null, unpublished = false))

        val list = mediaRepository.findAll()
        TestCase.assertEquals(1, list.size)
    }

}
