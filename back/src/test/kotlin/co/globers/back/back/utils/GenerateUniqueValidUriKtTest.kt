package co.globers.back.back.utils

import junit.framework.TestCase
import org.junit.Test

class GenerateUniqueValidUriTest {

    @Test
    fun generateValidUriTest() {

        TestCase.assertEquals("space-et-d-accent-eeee-aaa-u", generateUriByClean("space-- : - et d  '  accent   éèêë aäâ ù"))
        TestCase.assertEquals("upper", generateUriByClean("UPPER"))
        TestCase.assertEquals(
                "too-long-iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii",
                generateUriByClean("too long iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii"))
        TestCase.assertEquals("invalid-chars", generateUriByClean("invalid chars &~#{([-"))
    }
}