package co.globers.back.back

import co.globers.back.back.db.DbMedia
import co.globers.back.back.db.MediaRepository
import junit.framework.TestCase
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@ActiveProfiles("test")
@DataJpaTest //don't load full spring context, only repository and entities
class DumbUnitTest {

    @Autowired
    private lateinit var mediaRepository: MediaRepository

    @Test
    fun myFirstTest() {

        mediaRepository.save(DbMedia(uri="uri", name="name", logoUrl = "", description = "", themePrimaryColor = "", themeSecondaryColor = "", originHtmlContentSelector = null, unpublished = false))

        val list = mediaRepository.findAll()
        TestCase.assertEquals(1, list.size)
    }

}