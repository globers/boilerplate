/* eslint-disable no-underscore-dangle */

import { SheetsRegistry } from 'jss'
import {
  createMuiTheme,
  createGenerateClassName,
  Theme
} from '@material-ui/core/styles'

// copy of https://github.com/mui-org/material-ui/blob/master/examples/nextjs/src/getPageContext.js

// A theme with custom primary and secondary color.
// It's optional.
const theme: Theme = createMuiTheme({
  typography: {
    useNextVariants: true,
    fontSize: 19,
    fontFamily: '"Cormorant Garamond", serif'
  },
  palette: {
    primary: {
      main: '#01515C'
    },
    background: {
      default: 'rgba(250,250,250,1)',
      paper: 'rgba(250,250,250,1)'
    }
  }
})

function createPageContext() {
  return {
    theme,
    // This is needed in order to deduplicate the injection of CSS in the page.
    sheetsManager: new Map(),
    // This is needed in order to inject the critical CSS.
    sheetsRegistry: new SheetsRegistry(),
    // The standard class name generator.
    generateClassName: createGenerateClassName()
  }
}

export default function getPageContext() {
  // Make sure to create a new context for every server-side request so that data
  // isn't shared between connections (which would be bad).
  // @ts-ignore
  if (!process.browser) {
    return createPageContext()
  }

  const globalAny = global as any

  // Reuse context on the client-side.
  if (!globalAny.__INIT_MATERIAL_UI__) {
    globalAny.__INIT_MATERIAL_UI__ = createPageContext()
  }

  return globalAny.__INIT_MATERIAL_UI__
}
