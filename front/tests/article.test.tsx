// Deactivated for now due to
// https://github.com/apollographql/react-apollo/issues/2282

//
// import React, { ReactElement } from 'react'
// import TestRenderer, { ReactTestRenderer } from 'react-test-renderer'
// import { MockedProvider } from 'react-apollo/test-utils'
// import { Article, query } from '../pages/article'
//
// export const AsyncTestRenderer = (
//   elements: ReactElement<any>
// ): Promise<ReactTestRenderer> => {
//   /** Render, then allow the event loop to be flushed before returning */
//   const renderer: ReactTestRenderer = TestRenderer.create(elements)
//
//   // Sleep 1 ms then return renderer, this is because render is called once synchronously with "loading",
//   // then another time loaded
//   return new Promise<ReactTestRenderer>(resolve => {
//     setTimeout(() => resolve(renderer), 1)
//   })
// }
//
// describe('Article page', () => {
//   // Graphql query will be executed asynchronously, so we make a test "async" so it wait all "await" calls before exit
//   it('renders correctly', async () => {
//     const mocks = [
//       {
//         request: {
//           query,
//           variables: { mediaUri: 'mediaUriTest', articleUri: 'articleUriTest' }
//         },
//         result: {
//           data: {
//             user: {
//               media: {
//                 uri: 'mediaUri',
//                 name: 'name',
//                 logoUrl: '',
//                 article: {
//                   uri: 'articleUri',
//                   title: 'title',
//                   imageUrl: '',
//                   content: 'content'
//                 }
//               }
//             }
//           }
//         }
//       }
//     ]
//
//     const routerProp = {
//       query: { mediaUri: 'mediaUriTest', articleUri: 'articleUriTest' }
//     }
//     const component = await AsyncTestRenderer(
//       <MockedProvider mocks={mocks} addTypename={false}>
//         <Article router={routerProp} />
//       </MockedProvider>
//     )
//
//     const json = component.toJSON()
//     expect(json).toMatchSnapshot()
//   })
// })
