import { Selector } from 'testcafe' // first import testcafe selectors

// DOCKER_HOST_NAME to override when running testcafe from docker
// where host is not localhost but "docker"
const dockerHostName = process.env.DOCKER_HOST_NAME || 'localhost'
fixture('Article click test').page(`http://${dockerHostName}:3000/`) // declare the fixture // specify the start page

test('My first test', async t => {
  await t

    .click('#logo-media-media-uri-medium')
    .click('#email-popup-cancel-button')

    // The two lines above are to solve issue with the email popup in landing page
    // With testcafe, popup is displayed when media is first click
    // so 1) we click 2) we close popup 3) below: we click again on link

    .click('#logo-media-media-uri-medium')
    .click('#link-article-article-uri-2-media-1')
    .expect(Selector('#title').innerText)
    .eql('Your Online Data Is In Peril. The Blockchain Could Save It')
})
