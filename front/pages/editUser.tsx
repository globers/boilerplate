import * as React from 'react'
import withRoot from '../materialui/withRoot'
import withData from '../graphql/withData'
import { withRouter, WithRouterProps } from 'next/router'

import gql from 'graphql-tag'
import { Query } from 'react-apollo'
import LoadingProgress from '../components/utils/loadingProgress'
import UpdateUserBehavior from '../components/account/UpdateUserBehavior'
import Layout from '../components/MyLayout'
import AppBar from '../components/AppBar'
import Router from 'next/router'

interface IUser {
  __typename: 'User'
  uri: string
  firstName: string
  lastName: string
  profilePictureUrl: string | null
}

interface IData {
  user: IUser
}

const query = gql`
  query {
    user {
      ... on User {
        uri
        firstName
        lastName
        profilePictureUrl
      }
    }
  }
`

class EditArticle extends React.Component<WithRouterProps<{}>> {
  public render() {
    // @ts-ignore
    if (!process.browser) {
      return null
    }

    return (
      <Query<IData, {}> query={query}>
        {({ loading, error, data }) => {
          if (loading) {
            return <LoadingProgress />
          }

          if (error) {
            // tslint:disable-next-line no-console
            console.log(error)
          }
          if (error || !data) {
            return <div>Unknown Error</div>
          }

          if (!data.user) {
            // tslint:disable-next-line no-console
            console.log('editArticle: NO USER ON DATA!, data:')
            // tslint:disable-next-line no-console
            console.log(data)
            return <LoadingProgress /> // This is a hack to try to make it work. cf: https://github.com/apollographql/react-apollo/issues/1314
          }

          return (
            <Layout
              pageTitle="Globers - Ecrivez, vous êtes payé"
              appBar={
                <AppBar
                  isHome={false}
                  userFirstName={data.user.firstName}
                  media={null}
                />
              }
            >
              <UpdateUserBehavior
                initialFirstName={data.user.firstName}
                initialLastName={data.user.lastName}
                initialProfilePictureUrl={data.user.profilePictureUrl}
                onAccountSaved={this.onAccountSaved}
              />
            </Layout>
          )
        }}
      </Query>
    )
  }

  private onAccountSaved = async () => {
    Router.back()
  }
}

export default withRoot(withData(withRouter(EditArticle)))
