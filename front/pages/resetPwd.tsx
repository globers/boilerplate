import * as React from 'react'
import withRoot from '../materialui/withRoot'
import withData from '../graphql/withData'
import { withRouter, WithRouterProps } from 'next/router'
import ResetPasswordForm from '../components/account/ResetPasswordForm'

interface IQuery {
  uri: string
}

interface IProps extends WithRouterProps<IQuery> {}

const ResetPasswordPage = (props: IProps) => {
  return <ResetPasswordForm resetPasswordUri={props.router.query!!.uri} />
}

export default withRoot(
  withData(withRouter((props: IProps) => ResetPasswordPage(props)))
)
