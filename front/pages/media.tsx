import gql from 'graphql-tag'
import * as React from 'react'
import { Query } from 'react-apollo'
import Layout from '../components/MyLayout'
import withData from '../graphql/withData'
import withRoot from '../materialui/withRoot'
import { withRouter, WithRouterProps } from 'next/router'
import LoadingProgress from '../components/utils/loadingProgress'
import AppBar from '../components/AppBar'
import ArticleList from '../components/article/ArticleList'
import MediaAdminAppBar from '../components/media/MediaAdminAppBar'
import ErrorPage from '../components/ErrorPage'
import { IGraphqlDate } from '../graphql/schema/GraphqlDate'
import Head from 'next/head'
import { getMediaUrl, getSubscribeToMediaUrl } from '../services/urlService'
import LinkButton from '../components/utils/LinkButton'
import MediaFooter from '../components/media/MediaFooter'

interface IMedia {
  uri: string
  name: string
  logoUrl: string
  articles: IArticle[]
  description: string
  subscriptionPlans: ISubscriptionPlan[]
  themePrimaryColor: string

  owner: IUserPublicProfile
}

interface ISubscriptionPlan {
  uri: string
}

interface IUserPublicProfile {
  uri: string
  firstName: string
  lastName: string
}

interface IArticle {
  uri: string
  title: string
  subtitle: string
  imageUrl: string
  firstPublication: IGraphqlDate
}

interface IUserArticle {
  article: IArticle
  bookmarked: boolean
}

interface IUserMedia {
  media: IMedia
  followed: boolean
  userArticles: IUserArticle[]
}

interface IContributedMedia {
  uri: string
  media: { uri: string }
}

interface IUserSubscription {
  uri: string
}

interface ICreator {
  uri: string
  contributedMedia: IContributedMedia | null
}

interface IAnonymous {
  __typename: 'Anonymous'
  media: IMedia | null
}

interface IUser {
  __typename: 'User'
  userMedia: IUserMedia | null
  firstName: string
  creator: ICreator | null
  subscriptions: IUserSubscription[]
}

type UserOrAnonymous = IUser | IAnonymous

interface IData {
  user: UserOrAnonymous
}

export const query = gql`
  fragment mediaFieldsForMedia on Media {
    uri
    name
    logoUrl
    subscriptionPlans {
      uri
    }
    unpublished
    description
    themePrimaryColor
    owner {
      uri
      firstName
      lastName
    }
  }

  fragment articleFieldsForMedia on Article {
    uri
    title
    subtitle
    imageUrl
    firstPublication {
      nbSecFrom1970
    }
  }

  query($mediaUri: ID!) {
    user {
      ... on User {
        uri
        firstName
        subscriptions {
          uri
        }
        userMedia(mediaUri: $mediaUri) {
          media {
            ...mediaFieldsForMedia
          }
          userArticles(nbMostRecent: 100) {
            article {
              ...articleFieldsForMedia
            }
            uri
            bookmarked
          }
          uri
          followed
        }
        creator {
          uri
          contributedMedia(mediaUri: $mediaUri) {
            uri
            media {
              uri
            }
          }
        }
      }
      ... on Anonymous {
        media(mediaUri: $mediaUri) {
          ...mediaFieldsForMedia
          articles(nbMostRecent: 100) {
            ...articleFieldsForMedia
          }
        }
      }
    }
  }
`

interface IQuery {
  mediaUri: string
}

class MediaQuery extends Query<IData, {}> {}

interface IProps extends WithRouterProps<IQuery> {}

function Media(props: IProps) {
  return (
    <MediaQuery
      query={query}
      variables={{
        mediaUri: props.router.query!.mediaUri
      }}
    >
      {({ loading, error, data }) => {
        if (loading) {
          return <LoadingProgress />
        }

        if (error) {
          // tslint:disable-next-line no-console
          console.log(error)
        }
        if (error || !data) {
          return <div>Unknown Error</div>
        }

        let userMedia: IUserMedia
        let isMediaAdmin = false
        // let userMedia: IUserMedia
        switch (data.user.__typename) {
          case 'User':
            const userMediaOrNull = data.user.userMedia
            if (!userMediaOrNull) {
              return <ErrorPage statusCode={404} />
            }
            userMedia = userMediaOrNull

            isMediaAdmin =
              data.user.creator !== null &&
              data.user.creator.contributedMedia !== null
            break
          case 'Anonymous':
            if (!data.user.media) {
              return <ErrorPage statusCode={404} />
            }
            userMedia = {
              media: data.user.media,
              followed: false,
              userArticles: data.user.media.articles.map(a => ({
                article: a,
                bookmarked: false
              }))
            }
            break
          default:
            throw new Error('Invalid type')
        }

        const isLoggedIn = data.user.__typename === 'User'

        let subscribeToMediaUrl = null
        if (userMedia.media.subscriptionPlans.length === 1) {
          subscribeToMediaUrl = getSubscribeToMediaUrl(userMedia.media.uri)
        }

        const isUserSubscribed =
          isLoggedIn && (data.user as IUser).subscriptions.length >= 1

        return (
          <div>
            <Layout
              pageTitle={`${userMedia.media.name} - Globers`}
              appBar={
                isMediaAdmin ? (
                  <MediaAdminAppBar
                    media={userMedia.media}
                    userFirstName={(data.user as IUser).firstName}
                    isAdminView={false}
                  />
                ) : (
                  <AppBar
                    isHome={false}
                    logoUrl={null}
                    media={userMedia.media}
                    userFirstName={
                      isLoggedIn ? (data.user as IUser).firstName : null
                    }
                  >
                    {subscribeToMediaUrl && (
                      <LinkButton
                        url={subscribeToMediaUrl}
                        variant={'contained'}
                        color={'primary'}
                        themePrimaryColor={userMedia.media.themePrimaryColor}
                      >
                        Subscribe
                      </LinkButton>
                    )}
                  </AppBar>
                )
              }
            >
              <Head>
                <meta
                  property="og:title"
                  content={`${userMedia.media.name} - Globers`}
                />
                <meta
                  property="og:description"
                  content={userMedia.media.description}
                />
                <meta property="og:image" content={userMedia.media.logoUrl} />
                <meta
                  property="og:url"
                  content={getMediaUrl(userMedia.media.uri).as}
                />
                <meta name="twitter:card" content="summary_large_image" />
              </Head>
              <ArticleList userMedia={userMedia} isLoggedIn={isLoggedIn} />
              <MediaFooter
                media={userMedia.media}
                isUserAlreadySubscribed={isUserSubscribed}
              />
            </Layout>
          </div>
        )
      }}
    </MediaQuery>
  )
}
export default withRoot(withData(withRouter((props: IProps) => Media(props))))
