import gql from 'graphql-tag'
import * as React from 'react'
import { Query } from 'react-apollo'
import withData from '../graphql/withData'
import withRoot from '../materialui/withRoot'
import { withRouter, WithRouterProps } from 'next/router'
import LoadingProgress from '../components/utils/loadingProgress'
import { IGraphqlDate } from '../graphql/schema/GraphqlDate'
import MediaAdminPage from '../components/media/MediaAdminPage'
import ErrorPage from '../components/ErrorPage'

interface IArticle {
  uri: string
  title: string
  imageUrl: string
  firstPublication: IGraphqlDate
  lastPublication: IGraphqlDate
}

interface IPublishedArticle {
  uri: string
  article: IArticle
  revision: { uri: string }
}

interface IArticleDraft {
  uri: string
  revisions: IArticleDraftRevision[]
}

interface IArticleDraftRevision {
  uri: string
  title: string
  subtitle: string
  content: string
  imageUrl: string
  savingDate: IGraphqlDate
}

interface IContributedArticle {
  uri: string
  draft: IArticleDraft
  publishedArticle: IPublishedArticle | null
}

interface IMedia {
  uri: string
  logoUrl: string
  name: string
  unpublished: boolean
  description: string
  themePrimaryColor: string
}

interface IContributedMedia {
  uri: string
  media: IMedia
  stripeAccountConnected: boolean
  contributedArticles: IContributedArticle[]
  subscribers: ISubscriber[]
}

interface IUserSubscription {
  uri: string
  startSubscriptionDate: IGraphqlDate
}

interface ISubscriber {
  uri: string
  profile: IUserPublicProfile
  subscription: IUserSubscription
}

interface IUserPublicProfile {
  uri: string
  firstName: string
  lastName: string
}

interface ICreator {
  uri: string
  contributedMedia: IContributedMedia | null
}

interface IUser {
  uri: string
  firstName: string
  creator: ICreator
}

interface IData {
  user: IUser
  firstName: string
  lastName: string
  profilePictureUrl: string | null
}

class MediaAdminQuery extends Query<IData, {}> {}

export const query = gql`
  query($mediaUri: ID!) {
    user {
      ... on User {
        uri
        firstName
        lastName
        profilePictureUrl
        creator {
          uri
          contributedMedia(mediaUri: $mediaUri) {
            uri
            stripeAccountConnected
            subscribers {
              uri
              profile {
                uri
                firstName
                lastName
              }
              subscription {
                uri
                startSubscriptionDate {
                  nbSecFrom1970
                }
              }
            }
            media {
              uri
              logoUrl
              name
              unpublished
              description
              themePrimaryColor
            }
            contributedArticles {
              uri
              draft {
                uri
                revisions(nbMostRecent: 1) {
                  uri
                  title
                  subtitle
                  imageUrl
                  savingDate {
                    nbSecFrom1970
                  }
                }
              }
              publishedArticle {
                uri
                article {
                  uri
                  title
                  imageUrl
                  firstPublication {
                    nbSecFrom1970
                  }
                  lastPublication {
                    nbSecFrom1970
                  }
                }
                revision {
                  uri
                }
              }
            }
          }
        }
      }
    }
  }
`

interface IQuery {
  mediaUri: string
}

interface IProps extends WithRouterProps<IQuery> {}

const MediaAdmin = (props: IProps) => (
  <MediaAdminQuery
    query={query}
    variables={{
      mediaUri: props.router.query!.mediaUri
    }}
    fetchPolicy={'cache-and-network'}
  >
    {({ loading, error, data }) => {
      if (loading) {
        return <LoadingProgress />
      }

      if (error) {
        // tslint:disable-next-line no-console
        console.log(error)
      }
      if (error || !data) {
        return <div>Unknown Error</div>
      }

      if (!data.user || !data.user.creator) {
        // tslint:disable-next-line no-console
        console.log('mediaAdmin: NO USER ON DATA!, data:')
        // tslint:disable-next-line no-console
        console.log(data)
        return <LoadingProgress /> // This is a hack to try to make it work. cf: https://github.com/apollographql/react-apollo/issues/1314
      }

      const contributedMedia = data.user.creator.contributedMedia

      if (!contributedMedia) {
        return <ErrorPage statusCode={404} />
      }

      return (
        <MediaAdminPage contributedMedia={contributedMedia} user={data.user} />
      )
    }}
  </MediaAdminQuery>
)

export default withRoot(
  withData(withRouter((props: IProps) => MediaAdmin(props)))
)
