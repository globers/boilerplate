import gql from 'graphql-tag'
import * as React from 'react'
import { Query } from 'react-apollo'
import Layout from '../components/MyLayout'
import withData from '../graphql/withData'
import withRoot from '../materialui/withRoot'
import LoadingProgress from '../components/utils/loadingProgress'
import { withRouter } from 'next/router'
import AppBar from '../components/AppBar'
import { TodayMinus, toGraphqlDate } from '../services/dateServices'
import LandingPage from '../components/landing/LandingPage'
import Head from 'next/head'
import { globersLogoUrl } from '../services/imageService'

interface IMedia {
  uri: string
  name: string
  logoUrl: string
  description: string
  themePrimaryColor: string
  owner: IUserPublicProfile
  unpublished: boolean
  nbArticles: number
}

interface IUserPublicProfile {
  uri: string
  firstName: string
  lastName: string
  profilePictureUrl: string | null
}

interface IUserMedia {
  media: IMedia
  uri: string
  followed: boolean
}

interface IContributedMedia {
  uri: string
  media: IMedia
}

interface IAnonymous {
  __typename: 'Anonymous'
  medias: IMedia[]
}

interface ICreator {
  contributedMedias: IContributedMedia[]
}

interface IUser {
  __typename: 'User'
  uri: string
  firstName: string
  userMedias: IUserMedia[]
  creator: ICreator | null
}

type UserOrAnonymous = IUser | IAnonymous

interface IData {
  user: UserOrAnonymous
}

const query = gql`
  fragment mediaFieldsForIndex on Media {
    uri
    name
    description
    logoUrl
    unpublished
    themePrimaryColor
    owner {
      uri
      firstName
      lastName
      profilePictureUrl
    }
    nbArticles(since: $nbArticlesSince)
  }

  query($nbArticlesSince: GraphqlDateInput!) {
    user {
      ... on User {
        uri
        firstName
        userMedias {
          media {
            ...mediaFieldsForIndex
          }
          uri
          followed
        }
        creator {
          uri
          contributedMedias {
            uri
            media {
              ...mediaFieldsForIndex
            }
          }
        }
      }
      ... on Anonymous {
        medias {
          ...mediaFieldsForIndex
        }
      }
    }
  }
`

class IndexQuery extends Query<IData, {}> {}

const Index = () => {
  return (
    <IndexQuery
      query={query}
      variables={{
        nbArticlesSince: toGraphqlDate(TodayMinus(60))
      }}
    >
      {({ loading, error, data }) => {
        if (loading) {
          return <LoadingProgress />
        }
        if (error) {
          // tslint:disable-next-line no-console
          console.log(error)
        }
        if (error || !data) {
          return <div>Unknown Error</div>
        }

        let userMedias: IUserMedia[]
        let contribMedias: IContributedMedia[]

        switch (data.user.__typename) {
          case 'User':
            contribMedias = data.user.creator
              ? data.user.creator.contributedMedias
              : []
            userMedias = data.user.userMedias.filter(
              u => !contribMedias.find(c => c.media.uri === u.media.uri)
            )
            break
          case 'Anonymous':
            userMedias = data.user.medias.map(m => ({
              media: m,
              followed: false,
              uri: `userMedia-${m.uri}`
            }))
            contribMedias = []
            break
          default:
            throw new Error('Invalid type')
        }

        const isLoggedIn = data.user.__typename === 'User'

        return (
          <Layout
            pageTitle="Globers - Ecrivez, vous êtes payé"
            appBar={
              <AppBar
                isHome={true}
                userFirstName={
                  isLoggedIn ? (data.user as IUser).firstName : null
                }
                media={null}
              />
            }
          >
            <Head>
              <meta
                property="og:title"
                content="Globers - Ecrivez, vous êtes payé"
              />
              <meta
                property="og:description"
                content="Globers te permet de créer ton journal en ligne et d'être rémunéré par tes abonnés."
              />
              <meta property="og:image" content={globersLogoUrl} />
              <meta property="og:url" content="https://globers.co" />
              <meta name="twitter:card" content="summary_large_image" />
            </Head>
            <LandingPage
              contributedMedias={contribMedias}
              userMedias={userMedias}
              isLoggedIn={isLoggedIn}
            />
          </Layout>
        )
      }}
    </IndexQuery>
  )
}

export default withRoot(withData(withRouter(() => Index())))
