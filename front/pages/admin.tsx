import gql from 'graphql-tag'
import * as React from 'react'
import { Query } from 'react-apollo'
import withData from '../graphql/withData'
import withRoot from '../materialui/withRoot'
import LoadingProgress from '../components/utils/loadingProgress'
import { withRouter } from 'next/router'
import { TodayMinus, toGraphqlDate } from '../services/dateServices'
import AdminPage, { IAdmin } from '../components/AdminPage'

interface IData {
  admin: IAdmin
}

// to keep consistent with IAdmin structure in AdminPage
const query = gql`
  query($nbArticlesSince: GraphqlDateInput!) {
    admin {
      uri
      users {
        uri
        firstName
        lastName
        email
        creator {
          uri
          contributedMedias {
            uri
            stripeAccountConnected
            media {
              uri
              name
              unpublished
              nbArticles(since: $nbArticlesSince)
            }
          }
        }
      }
    }
  }
`

class AdminQuery extends Query<IData, {}> {}

const Admin = () => {
  return (
    <AdminQuery
      query={query}
      variables={{
        nbArticlesSince: toGraphqlDate(TodayMinus(60))
      }}
    >
      {({ loading, error, data }) => {
        if (loading) {
          return <LoadingProgress />
        }
        if (error) {
          // tslint:disable-next-line no-console
          console.log(error)
        }
        if (error || !data) {
          return <div>Unknown Error</div>
        }

        return <AdminPage admin={data.admin} />
      }}
    </AdminQuery>
  )
}

export default withRoot(withData(withRouter(() => Admin())))
