import * as React from 'react'
import withRoot from '../materialui/withRoot'
import withData from '../graphql/withData'
import { withRouter, WithRouterProps } from 'next/router'
import gql from 'graphql-tag'
import LoadingProgress from '../components/utils/loadingProgress'
import { Query } from 'react-apollo'
import CreateMediaStripeAccount from '../components/media/CreateMediaStripeAccount'
import Head from 'next/head'
import StripeWrapper from '../components/utils/StripeWrapper'

interface IUser {
  __typename: 'User'
  firstName: string
  lastName: string
  uri: string
  creator: {
    uri: string
    contributedMedia: {
      uri: string
      media: {
        uri: string
        name: string
      }
    }
  }
}

interface IData {
  user: IUser
}

const query = gql`
  query($mediaUri: ID!) {
    user {
      ... on User {
        firstName
        lastName
        uri
        creator {
          uri
          contributedMedia(mediaUri: $mediaUri) {
            uri
            media {
              uri
              name
            }
          }
        }
      }
    }
  }
`

interface IVariables {
  mediaUri: string
}

class ConnectBankAccountToMediaQuery extends Query<IData, IVariables> {}

interface IQuery {
  mediaUri: string
}

interface IProps extends WithRouterProps<IQuery> {}

const ConnectBankAccountToMedia = (props: IProps) => {
  // @ts-ignore
  if (!process.browser) {
    return null
  }

  return (
    <ConnectBankAccountToMediaQuery
      query={query}
      variables={{
        mediaUri: props.router.query!.mediaUri
      }}
    >
      {({ loading, error, data }) => {
        if (loading) {
          return <LoadingProgress />
        }
        if (error) {
          // tslint:disable-next-line no-console
          console.log(error)
        }
        if (error || !data) {
          return <div>Unknown Error</div>
        }

        switch (data.user.__typename) {
          case 'User':
            return (
              <>
                <Head>
                  <title>Connect Bank Account</title>
                </Head>
                <StripeWrapper>
                  <CreateMediaStripeAccount
                    mediaUri={data.user.creator.contributedMedia.media.uri}
                    loginFirstName={data.user.firstName}
                    loginLastName={data.user.lastName}
                  />
                </StripeWrapper>
              </>
            )
          default:
            throw new Error('Invalid type')
        }
      }}
    </ConnectBankAccountToMediaQuery>
  )
}

export default withRoot(
  withData(withRouter((props: IProps) => ConnectBankAccountToMedia(props)))
)
