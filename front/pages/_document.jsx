import React from 'react'
import Document, { Head, Main, NextScript } from 'next/document'
import JssProvider from 'react-jss/lib/JssProvider'
import flush from 'styled-jsx/server'
import getPageContext from '../materialui/getPageContext'
import { isProductionEnv } from '../services/environment'

class MyDocument extends Document {

  //cf. https://www.garymeehan.ie/blog/google-analytics-nextjs-and-prismic
  static setGoogleTags() {
    return {
      __html: `
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', 'UA-109526034-1');
      `
    };
  }

  static setStackdriver(){
    return {
      __html:  `window.addEventListener('DOMContentLoaded', function() {
        var errorHandler = new StackdriverErrorReporter()
        errorHandler.start({
          key: 'AIzaSyA04MRjBQplRhv_BHtbqcvjdH5Ui6uT6pE',
          projectId: 'insidoor-182517'
        })
      })
       `
    }
  }

  static setEmbedly(){
    return {
      __html:  `(function(w, d){
   var id='embedly-platform', n = 'script';
   if (!d.getElementById(id)){
     w.embedly = w.embedly || function() {(w.embedly.q = w.embedly.q || []).push(arguments);};
     var e = d.createElement(n); e.id = id; e.async=1;
     e.src = ('https:' === document.location.protocol ? 'https' : 'http') + '://cdn.embedly.com/widgets/platform.js';
     var s = d.getElementsByTagName(n)[0];
     s.parentNode.insertBefore(e, s);
   }
  })(window, document);`
    }
  }

  render() {
    const { pageContext } = this.props

    return (
      <html lang="en" dir="ltr">
      <Head>

        { isProductionEnv && (
          <>
          <script async src="https://www.googletagmanager.com/gtag/js?id=UA-109526034-1"></script>
          <script dangerouslySetInnerHTML={MyDocument.setGoogleTags()} />
            <script defer src="https://cdn.jsdelivr.net/npm/stackdriver-errors-js@0.5.0/dist/stackdriver-errors-concat.min.js"></script>
            <script type="text/javascript"  dangerouslySetInnerHTML={MyDocument.setStackdriver()}/>
          </>
          )
        }
        <script type="text/javascript"  dangerouslySetInnerHTML={MyDocument.setEmbedly()}/>

        <meta charSet="utf-8"/>
        {/* Use minimum-scale=1 to enable GPU rasterization */}
        <meta
          name="viewport"
          content={
            'user-scalable=0, initial-scale=1, ' +
            'minimum-scale=1, width=device-width, height=device-height'
          }
        />
        {/* PWA primary color */}
        <meta name="theme-color" content={pageContext.theme.palette.primary.main}/>
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500"
        />
        <link href="https://fonts.googleapis.com/css?family=Cormorant+Garamond:300,400,500" rel="stylesheet"/>

        <link rel="apple-touch-icon" sizes="57x57" href="https://res.cloudinary.com/globers/image/upload/prod/app/favicon/apple-icon-57x57.png"/>
        <link rel="apple-touch-icon" sizes="60x60" href="https://res.cloudinary.com/globers/image/upload/prod/app/favicon/apple-icon-60x60.png"/>
        <link rel="apple-touch-icon" sizes="72x72" href="https://res.cloudinary.com/globers/image/upload/prod/app/favicon/apple-icon-72x72.png"/>
        <link rel="apple-touch-icon" sizes="76x76" href="https://res.cloudinary.com/globers/image/upload/prod/app/favicon/apple-icon-76x76.png"/>
        <link rel="apple-touch-icon" sizes="114x114" href="https://res.cloudinary.com/globers/image/upload/prod/app/favicon/apple-icon-114x114.png"/>
        <link rel="apple-touch-icon" sizes="120x120" href="https://res.cloudinary.com/globers/image/upload/prod/app/favicon/apple-icon-120x120.png"/>
        <link rel="apple-touch-icon" sizes="144x144" href="https://res.cloudinary.com/globers/image/upload/prod/app/favicon/apple-icon-144x144.png"/>
        <link rel="apple-touch-icon" sizes="152x152" href="https://res.cloudinary.com/globers/image/upload/prod/app/favicon/apple-icon-152x152.png"/>
        <link rel="apple-touch-icon" sizes="180x180" href="https://res.cloudinary.com/globers/image/upload/prod/app/favicon/apple-icon-180x180.png"/>
        <link rel="icon" type="image/png" sizes="192x192" href="https://res.cloudinary.com/globers/image/upload/prod/app/favicon/android-icon-192x192.png"/>
        <link rel="icon" type="image/png" sizes="32x32" href="https://res.cloudinary.com/globers/image/upload/prod/app/favicon/favicon-32x32.png"/>
        <link rel="icon" type="image/png" sizes="96x96" href="https://res.cloudinary.com/globers/image/upload/prod/app/favicon/favicon-96x96.png"/>
        <link rel="icon" type="image/png" sizes="16x16" href="https://res.cloudinary.com/globers/image/upload/prod/app/favicon/favicon-16x16.png"/>
        <link rel="manifest" href="https://res.cloudinary.com/globers/raw/upload/prod/app/favicon/manifest.json"/>
        <meta name="msapplication-TileColor" content="#ffffff"/>
        <meta name="msapplication-TileImage" content="https://res.cloudinary.com/globers/image/upload/prod/app/favicon/ms-icon-144x144.png"/>
        <meta name="theme-color" content="#ffffff"/>
      </Head>
      <body>
      <Main/>
      <NextScript/>
      </body>
      </html>
    )
  }
}

MyDocument.getInitialProps = ctx => {
  // Resolution order
  //
  // On the server:
  // 1. page.getInitialProps
  // 2. document.getInitialProps
  // 3. page.render
  // 4. document.render
  //
  // On the server with error:
  // 2. document.getInitialProps
  // 3. page.render
  // 4. document.render
  //
  // On the client
  // 1. page.getInitialProps
  // 3. page.render

  // Get the context of the page to collected side effects.
  const pageContext = getPageContext()
  const page = ctx.renderPage(Component => props => (
    <JssProvider
      registry={pageContext.sheetsRegistry}
      generateClassName={pageContext.generateClassName}
    >
      <Component pageContext={pageContext} {...props} />
    </JssProvider>
  ))

  return {
    ...page,
    pageContext,
    styles: (
      <React.Fragment>
        <style
          id="jss-server-side"
          // eslint-disable-next-line react/no-danger
          dangerouslySetInnerHTML={{ __html: pageContext.sheetsRegistry.toString() }}
        />
        {flush() || null}
      </React.Fragment>
    )
  }
}

export default MyDocument