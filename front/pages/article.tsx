// @ts-ignore
import Layout from '../components/MyLayout'
import withData from '../graphql/withData'
import { Query } from 'react-apollo'
import gql from 'graphql-tag'
import * as React from 'react'
import { withRouter, WithRouterProps } from 'next/router'
import withRoot from '../materialui/withRoot'
import ArticleContent from '../components/article/ArticleContent'
import LoadingProgress from '../components/utils/loadingProgress'
import AppBar from '../components/AppBar'
import ErrorPage from '../components/ErrorPage'
import { IGraphqlDate } from '../graphql/schema/GraphqlDate'
import Head from 'next/head'
import { getArticleUrl, getSubscribeToMediaUrl } from '../services/urlService'
import LinkButton from '../components/utils/LinkButton'

interface IMedia {
  uri: string
  name: string
  logoUrl: string
  themePrimaryColor: string

  description: string
  logoImageUrl: string
  unpublished: boolean
  owner: IUserPublicProfile

  article: IArticle | null
  subscriptionPlans: ISubscriptionPlan[]
}

interface ISubscriptionPlan {
  uri: string
}

interface IUserPublicProfile {
  uri: string
  firstName: string
  lastName: string
}

interface IArticle {
  uri: string
  title: string
  subtitle: string
  imageUrl: string
  content: string
  firstPublication: IGraphqlDate
}

interface IUserMedia {
  media: IMedia
}

interface IAnonymous {
  __typename: 'Anonymous'
  media: IMedia | null
}

interface IUserSubscription {
  uri: string
}

interface IContributedMedia {
  uri: string
  media: { uri: string }
}

interface ICreator {
  uri: string
  contributedMedia: IContributedMedia | null
}

interface IUser {
  __typename: 'User'
  userMedia: IUserMedia | null
  firstName: string
  subscriptions: IUserSubscription[]
  creator: ICreator | null
}

type UserOrAnonymous = IUser | IAnonymous

interface IData {
  user: UserOrAnonymous
}

const query = gql`
  fragment mediaFieldsForArticle on Media {
    uri
    name
    logoUrl
    unpublished
    description
    themePrimaryColor
    owner {
      uri
      firstName
      lastName
    }
    subscriptionPlans {
      uri
    }
    article(uri: $articleUri) {
      uri
      title
      subtitle
      imageUrl
      content
      firstPublication {
        nbSecFrom1970
      }
    }
  }

  query($mediaUri: ID!, $articleUri: ID!) {
    user {
      ... on User {
        uri
        subscriptions {
          uri
        }
        userMedia(mediaUri: $mediaUri) {
          media {
            ...mediaFieldsForArticle
          }
          uri
        }
        firstName
        creator {
          uri
          contributedMedia(mediaUri: $mediaUri) {
            uri
            media {
              uri
            }
          }
        }
      }
      ... on Anonymous {
        media(mediaUri: $mediaUri) {
          ...mediaFieldsForArticle
        }
      }
    }
  }
`

interface IVariables {
  mediaUri: string
  articleUri: string
}
class ArticleQuery extends Query<IData, IVariables> {}

interface IQuery {
  mediaUri: string
  articleUri: string
}

interface IArticleProps extends WithRouterProps<IQuery> {}

export const Article = (articleProps: IArticleProps) => (
  <ArticleQuery
    query={query}
    variables={{
      mediaUri: articleProps.router.query!.mediaUri,
      articleUri: articleProps.router.query!.articleUri
    }}
  >
    {({ loading, error, data }) => {
      if (loading) {
        return <LoadingProgress />
      }
      if (error) {
        // tslint:disable-next-line no-console
        console.log(error)
      }
      if (error || !data) {
        return <div>Unknown Error</div>
      }

      const isLoggedIn = data.user.__typename === 'User'

      let media: IMedia
      if (isLoggedIn && (data.user as IUser).userMedia) {
        media = ((data.user as IUser).userMedia as IUserMedia).media
      } else if (data.user.__typename === 'Anonymous' && data.user.media) {
        media = data.user.media
      } else {
        return <ErrorPage statusCode={404} />
      }

      if (!media.article) {
        return <ErrorPage statusCode={404} />
      }

      let isMediaAdmin = false
      if (isLoggedIn) {
        const user: IUser = data.user as IUser
        if (user.creator) {
          isMediaAdmin = user.creator.contributedMedia !== null
        }
      }

      let subscribeToMediaUrl = null
      if (media.subscriptionPlans.length === 1) {
        subscribeToMediaUrl = getSubscribeToMediaUrl(media.uri)
      }

      const isUserSubscribed =
        isLoggedIn && (data.user as IUser).subscriptions.length >= 1

      return (
        <Layout
          pageTitle={`${media.article.title} - ${media.name} - Globers`}
          appBar={
            <AppBar
              isHome={false}
              logoUrl={null}
              media={media}
              userFirstName={isLoggedIn ? (data.user as IUser).firstName : null}
            >
              {subscribeToMediaUrl &&
                !isUserSubscribed && (
                  <LinkButton
                    url={subscribeToMediaUrl}
                    variant={'contained'}
                    color={'primary'}
                    themePrimaryColor={media.themePrimaryColor}
                  >
                    Subscribe
                  </LinkButton>
                )}
            </AppBar>
          }
        >
          <Head>
            <meta
              property="og:title"
              content={`${media.article.title} - ${media.name} - Globers`}
            />
            <meta property="og:description" content={media.article.subtitle} />
            <meta property="og:image" content={media.article.imageUrl} />
            <meta
              property="og:url"
              content={getArticleUrl(media.uri, media.article.uri).as}
            />
            <meta name="twitter:card" content="summary_large_image" />
          </Head>
          <ArticleContent
            article={media.article}
            media={media}
            isMediaAdmin={isMediaAdmin}
            isUserSubscribed={isUserSubscribed}
            mediaCanBeSubscribed={subscribeToMediaUrl !== null}
          />
        </Layout>
      )
    }}
  </ArticleQuery>
)

export default withRoot(
  withData(withRouter((props: IArticleProps) => Article(props)))
)
