import gql from 'graphql-tag'
import * as React from 'react'
import { Query } from 'react-apollo'
import Layout from '../components/MyLayout'
import withData from '../graphql/withData'
import withRoot from '../materialui/withRoot'
import LoadingProgress from '../components/utils/loadingProgress'
import { withRouter } from 'next/router'
import AppBar from '../components/AppBar'
import { TodayMinus, toGraphqlDate } from '../services/dateServices'
import AllMediasFlex from '../components/media/AllMediasFlex'

interface IMedia {
  uri: string
  name: string
  logoUrl: string
  description: string
  owner: IUserPublicProfile
  unpublished: boolean
  nbArticles: number
  themePrimaryColor: string
}

interface IUserPublicProfile {
  uri: string
  firstName: string
  lastName: string
  profilePictureUrl: string | null
}

interface IUserMedia {
  media: IMedia
  uri: string
  followed: boolean
}

interface IAnonymous {
  __typename: 'Anonymous'
  medias: IMedia[]
}

interface IUser {
  __typename: 'User'
  uri: string
  firstName: string
  userMedias: IUserMedia[]
}

type UserOrAnonymous = IUser | IAnonymous

interface IData {
  user: UserOrAnonymous
}

const query = gql`
  fragment mediaFieldsForIndex on Media {
    uri
    name
    description
    logoUrl
    unpublished
    themePrimaryColor
    owner {
      uri
      firstName
      lastName
      profilePictureUrl
    }
    nbArticles(since: $nbArticlesSince)
  }

  query($nbArticlesSince: GraphqlDateInput!) {
    user {
      ... on User {
        uri
        firstName
        userMedias {
          media {
            ...mediaFieldsForIndex
          }
          uri
          followed
        }
      }
      ... on Anonymous {
        medias {
          ...mediaFieldsForIndex
        }
      }
    }
  }
`

class AllMediasQuery extends Query<IData, {}> {}

const AllMedias = () => {
  return (
    <AllMediasQuery
      query={query}
      variables={{
        nbArticlesSince: toGraphqlDate(TodayMinus(60))
      }}
    >
      {({ loading, error, data }) => {
        if (loading) {
          return <LoadingProgress />
        }
        if (error) {
          // tslint:disable-next-line no-console
          console.log(error)
        }
        if (error || !data) {
          return <div>Unknown Error</div>
        }

        let userMedias: IUserMedia[]

        switch (data.user.__typename) {
          case 'User':
            userMedias = data.user.userMedias
            break
          case 'Anonymous':
            userMedias = data.user.medias.map(m => ({
              media: m,
              followed: false,
              uri: `userMedia-${m.uri}`
            }))
            break
          default:
            throw new Error('Invalid type')
        }

        const isLoggedIn = data.user.__typename === 'User'

        return (
          <Layout
            pageTitle="Globers - Ecrivez, vous êtes payé"
            appBar={
              <AppBar
                isHome={true}
                userFirstName={
                  isLoggedIn ? (data.user as IUser).firstName : null
                }
                media={null}
              />
            }
          >
            <AllMediasFlex userMedias={userMedias} />
          </Layout>
        )
      }}
    </AllMediasQuery>
  )
}

export default withRoot(withData(withRouter(() => AllMedias())))
