import App, { Container } from 'next/app'
import React from 'react'
import Router, { withRouter } from 'next/router'


function trackPageView(url) {
  try {
    window.gtag('config', 'UA-109526034-1', {
      page_location: url
    });
  } catch (error) {
    // TODO Add some log when we manage client error logging
    // silences the error in dev mode
    // and/or if gtag fails to load
  }
}

class MyApp extends App {



  // https://www.garymeehan.ie/blog/google-analytics-nextjs-and-prismic
  componentDidMount() {
    Router.onRouteChangeComplete = url => {
      trackPageView(url);
    };

    // added to the example code, else initial load rendered from server was not taken into account
    trackPageView(this.props.router.asPath)
  }


  render() {
    const { Component, pageProps } = this.props
    return (
      <Container>
      <Component {...pageProps} />
    </Container>
  )
  }
}

export default withRouter(MyApp)