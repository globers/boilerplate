import React from 'react'
import withRoot from '../materialui/withRoot'
import { withRouter, WithRouterProps } from 'next/router'
import withData from '../graphql/withData'
import ErrorPage from '../components/ErrorPage'

interface IProps extends WithRouterProps<{}> {
  statusCode: number
}

class Error extends React.Component<IProps> {
  public static async getInitialProps({ res, err }: any) {
    const statusCode = res ? res.statusCode : err ? err.statusCode : null
    return { statusCode }
  }

  constructor(props: IProps) {
    super(props)
  }

  public render() {
    return <ErrorPage statusCode={this.props.statusCode} />
  }
}

export default withRoot(withData(withRouter(Error)))
