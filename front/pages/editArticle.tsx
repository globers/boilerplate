import * as React from 'react'
import withRoot from '../materialui/withRoot'
import withData from '../graphql/withData'
import { withRouter, WithRouterProps } from 'next/router'

import gql from 'graphql-tag'
import { Query } from 'react-apollo'
import LoadingProgress from '../components/utils/loadingProgress'
import EditorPage from '../components/editor/EditorPage'
import { IGraphqlDate } from '../graphql/schema/GraphqlDate'
import ErrorPage from '../components/ErrorPage'
import { toGraphqlDate } from '../services/dateServices'

interface IMedia {
  uri: string
  name: string
  logoUrl: string
  themePrimaryColor: string
}

interface IArticleDraftRevision {
  uri: string | null
  title: string | null
  subtitle: string | null
  imageUrl: string | null
  slatejsValue: string | null
  savingDate: IGraphqlDate
}

interface IArticleDraft {
  uri: string
  revisions: IArticleDraftRevision[]
}

interface IContributedMedia {
  uri: string
  media: IMedia
  contributedArticle: IContributedArticle | null
}

interface IContributedArticle {
  uri: string
  draft: IArticleDraft
  publishedArticle: { article: { uri: string } } | null
}

interface ICreator {
  uri: string
  contributedMedia: IContributedMedia | null
}

interface IUser {
  __typename: 'User'
  firstName: string
  creator: ICreator
}

interface IData {
  user: IUser
}

const query = gql`
  query($mediaUri: ID!, $articleDraftUri: ID!) {
    user {
      ... on User {
        uri
        firstName
        creator {
          uri
          contributedMedia(mediaUri: $mediaUri) {
            uri
            media {
              uri
              name
              logoUrl
              themePrimaryColor
            }
            contributedArticle(articleDraftUri: $articleDraftUri) {
              uri
              draft {
                uri
                revisions(nbMostRecent: 1) {
                  uri
                  title
                  subtitle
                  imageUrl
                  slatejsValue
                  savingDate {
                    nbSecFrom1970
                  }
                }
              }
              publishedArticle {
                uri
                article {
                  uri
                }
              }
            }
          }
        }
      }
    }
  }
`

interface IQuery {
  mediaUri: string
  articleDraftUri: string
}

class EditArticle extends React.Component<WithRouterProps<IQuery>> {
  public render() {
    // @ts-ignore
    if (!process.browser) {
      return null
    }

    const { mediaUri, articleDraftUri } = this.props.router.query!

    return (
      <Query<IData, {}>
        query={query}
        variables={{
          mediaUri,
          articleDraftUri
        }}
        fetchPolicy={'cache-and-network'}
      >
        {({ loading, error, data }) => {
          if (loading) {
            return <LoadingProgress />
          }

          if (error) {
            // tslint:disable-next-line no-console
            console.log(error)
          }
          if (error || !data) {
            return <div>Unknown Error</div>
          }

          if (!data.user) {
            // tslint:disable-next-line no-console
            console.log('editArticle: NO USER ON DATA!, data:')
            // tslint:disable-next-line no-console
            console.log(data)
            return <LoadingProgress /> // This is a hack to try to make it work. cf: https://github.com/apollographql/react-apollo/issues/1314
          }

          const contribMedia = data.user.creator.contributedMedia

          if (!contribMedia) {
            return <ErrorPage statusCode={404} />
          }

          const contribArticle = contribMedia.contributedArticle

          if (!contribArticle) {
            return <ErrorPage statusCode={404} />
          }

          const revisions = contribArticle.draft.revisions
          let revision: IArticleDraftRevision
          if (revisions.length === 1) {
            revision = revisions[0]
          } else {
            revision = {
              uri: null,
              imageUrl: null,
              slatejsValue: null,
              title: null,
              subtitle: null,
              savingDate: toGraphqlDate(new Date())
            }
          }
          const media = contribMedia.media

          return (
            <EditorPage
              articleDraftUri={articleDraftUri}
              userFirstName={data.user.firstName}
              media={media}
              initialArticleImageUrl={revision.imageUrl}
              initialSlatejsAsJsonStr={revision.slatejsValue}
              initialSubtitle={revision.subtitle}
              initialTitle={revision.title}
              initialLastSavedRevisionUri={revision.uri}
              initialArticleUriIfPublished={
                contribArticle.publishedArticle
                  ? contribArticle.publishedArticle.article.uri
                  : null
              }
            />
          )
        }}
      </Query>
    )
  }
}

export default withRoot(withData(withRouter(EditArticle)))
