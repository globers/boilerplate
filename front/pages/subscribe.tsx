import * as React from 'react'
import withRoot from '../materialui/withRoot'
import withData from '../graphql/withData'
import { withRouter, WithRouterProps } from 'next/router'
import gql from 'graphql-tag'
import LoadingProgress from '../components/utils/loadingProgress'
import { Query } from 'react-apollo'
import Head from 'next/head'
import StripeWrapper from '../components/utils/StripeWrapper'
import SubscribeToMediaBehavior from '../components/account/SubscribeToMediaBehavior'
import { StripeCurrency } from '../graphql/schema/StripeCurrency'
import { StripeCountry } from '../graphql/schema/StripeCountry'
import ErrorPage from '../components/ErrorPage'
import LoginOrCreateAccountModals, {
  ModalStatus
} from '../components/account/loginOrCreateAccountModals'
import { isBrowser } from '../services/environment'

interface IUser {
  __typename: 'User'
  firstName: string
  lastName: string
  uri: string
  userMedia: IUserMedia
}

interface IAnonymous {
  __typename: 'Anonymous'
}

interface IUserMedia {
  uri: string
  media: IMedia
}

interface ISubscriptionPlan {
  uri: string
  price: number
  currency: StripeCurrency
  country: StripeCountry
}

interface IMedia {
  uri: string
  name: string
  subscriptionPlans: ISubscriptionPlan[]
}

interface IData {
  user: IUser | IAnonymous
}

const query = gql`
  query($mediaUri: ID!) {
    user {
      ... on User {
        firstName
        lastName
        uri
        userMedia(mediaUri: $mediaUri) {
          uri
          media {
            uri
            name
            subscriptionPlans {
              uri
              price
              currency
              country
            }
          }
        }
      }
    }
  }
`

interface IVariables {
  mediaUri: string
}

class SubscribeQuery extends Query<IData, IVariables> {}

interface IQuery {
  mediaUri: string
}

interface IProps extends WithRouterProps<IQuery> {}

class Subscribe extends React.Component<IProps> {
  public render() {
    if (!isBrowser()) {
      return null
    }
    return (
      <SubscribeQuery
        query={query}
        variables={{
          mediaUri: this.props.router.query!.mediaUri
        }}
      >
        {({ loading, error, data }) => {
          if (loading) {
            return <LoadingProgress />
          }
          if (error) {
            // tslint:disable-next-line no-console
            console.log(error)
          }
          if (error || !data) {
            return <div>Unknown Error</div>
          }

          switch (data.user.__typename) {
            case 'User':
              const plans = data.user.userMedia.media.subscriptionPlans
              if (plans.length !== 1) {
                return <ErrorPage statusCode={404} />
              }
              const plan = plans[0]

              return (
                <>
                  <Head>
                    <title>Subscribe to media</title>
                  </Head>
                  <StripeWrapper>
                    <SubscribeToMediaBehavior
                      mediaUri={data.user.userMedia.media.uri}
                      mediaName={data.user.userMedia.media.name}
                      subscriptionPlan={plan}
                    />
                  </StripeWrapper>
                </>
              )
            case 'Anonymous':
              return (
                <LoginOrCreateAccountModals
                  initialModalStatus={ModalStatus.CreateAccount}
                  closable={false}
                />
              )
            default:
              throw new Error('Invalid type')
          }
        }}
      </SubscribeQuery>
    )
  }
}

export default withRoot(withData(withRouter(Subscribe)))
