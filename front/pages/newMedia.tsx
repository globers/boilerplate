import { CreateMedia } from '../components/media/CreateMedia'
import * as React from 'react'
import withRoot from '../materialui/withRoot'
import withData from '../graphql/withData'
import { withRouter } from 'next/router'
import gql from 'graphql-tag'
import LoadingProgress from '../components/utils/loadingProgress'
import { Query } from 'react-apollo'
import LoginOrCreateAccountModals, {
  ModalStatus
} from '../components/account/loginOrCreateAccountModals'

interface IAnonymous {
  __typename: 'Anonymous'
}

interface IUser {
  __typename: 'User'
  firstName: string
}

type UserOrAnonymous = IUser | IAnonymous

interface IData {
  user: UserOrAnonymous
}

const query = gql`
  query {
    user {
      ... on User {
        firstName
        uri
      }
    }
  }
`

class NewMediaQuery extends Query<IData, {}> {}

const NewMedia = () => {
  return (
    <NewMediaQuery query={query}>
      {({ loading, error, data }) => {
        if (loading) {
          return <LoadingProgress />
        }
        if (error) {
          // tslint:disable-next-line no-console
          console.log(error)
        }
        if (error || !data) {
          return <div>Unknown Error</div>
        }

        switch (data.user.__typename) {
          case 'User':
            return <CreateMedia />
          case 'Anonymous':
            return (
              <LoginOrCreateAccountModals
                initialModalStatus={ModalStatus.CreateAccount}
                closable={false}
              />
            )
          default:
            throw new Error('Invalid type')
        }
      }}
    </NewMediaQuery>
  )
}

export default withRoot(withData(withRouter(() => NewMedia())))
