import * as React from 'react'
import withRoot from '../materialui/withRoot'
import withData from '../graphql/withData'
import { withRouter } from 'next/router'
import SendResetPasswordEmailForm from '../components/account/SendResetPasswordEmailForm'

const SendResetPasswordEmailPage = () => {
  return <SendResetPasswordEmailForm />
}

export default withRoot(
  withData(withRouter(() => SendResetPasswordEmailPage()))
)
