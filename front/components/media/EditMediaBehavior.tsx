import * as React from 'react'
import EditMediaForm from './EditMediaForm'
import { ImageFile, toGraphqlFile } from '../utils/imageDropZoneField'
import { IGraphqlFile } from '../../graphql/schema/GraphqlFile'
import { SaveMediaFailureReason } from './SaveMediaReply'

export interface IMediaInput {
  name: string
  logoImage: IGraphqlFile
  description: string
  themePrimaryColor: string
  themeSecondaryColor: string
  originHtmlContentSelector: string
  unpublished: boolean
}

interface IProps {
  initialMedia: IMedia | null
  saveFailureReason: SaveMediaFailureReason | null
  onSave: (mediaInput: IMediaInput) => void
  waitForSubmitReply: boolean
  stripeAccountConnected: boolean
}

interface IMedia {
  uri: string
  name: string
  logoUrl: string
  description: string
  themePrimaryColor: string
  unpublished: boolean
}

interface IState {
  name: string | null
  description: string | null
  logoImage: ImageFile | null
  isPublished: boolean
  themePrimaryColor: string

  nullNameError: boolean
  nullDescriptionError: boolean
  nullImageError: boolean
}

export default class EditMediaBehavior extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)

    if (props.initialMedia) {
      const media = props.initialMedia
      this.state = {
        name: media.name,
        description: media.description,
        logoImage: { type: 'IFileAsUrl', fileUrl: media.logoUrl },
        isPublished: !media.unpublished,
        nullNameError: false,
        nullDescriptionError: false,
        nullImageError: false,
        themePrimaryColor: media.themePrimaryColor
      }
    } else {
      this.state = {
        name: null,
        description: null,
        logoImage: null,
        themePrimaryColor: '',
        isPublished: true,
        nullNameError: false,
        nullDescriptionError: false,
        nullImageError: false
      }
    }
  }

  public render() {
    return (
      <EditMediaForm
        nameAlreadyExistsError={
          this.props.saveFailureReason ===
          SaveMediaFailureReason.NAME_ALREADY_EXISTS
        }
        emptyNameError={this.state.nullNameError}
        emptyDescriptionError={this.state.nullDescriptionError}
        emptyLogoImageError={this.state.nullImageError}
        invalidLogoImageError={
          this.props.saveFailureReason ===
          SaveMediaFailureReason.INVALID_LOGO_IMAGE
        }
        description={this.state.description}
        isEdit={this.props.initialMedia != null}
        logoImage={this.state.logoImage}
        name={this.state.name}
        themePrimaryColor={this.state.themePrimaryColor}
        onClickSubmit={this.onClickSubmit}
        onDescriptionChanged={this.onDescriptionChanged}
        onLogoImageChanged={this.onLogoChanged}
        onNameChanged={this.onNamedChanged}
        onThemePrimaryColorChanged={this.onThemePrimaryColorChanged}
        displayIsPublished={this.props.initialMedia != null}
        isPublished={this.state.isPublished}
        mediaUri={this.props.initialMedia ? this.props.initialMedia.uri : null}
        onClickSwitchPublished={this.onClickSwitchPublished}
        waitForSubmitReply={this.props.waitForSubmitReply}
        stripeAccountConnected={this.props.stripeAccountConnected}
      />
    )
  }

  private onClickSubmit = async () => {
    const nullName = !this.state.name
    const nullDescription = !this.state.description
    const nullImage = !this.state.logoImage

    this.setState({
      nullNameError: nullName,
      nullDescriptionError: nullDescription,
      nullImageError: nullImage
    })

    if (!nullName && !nullDescription && !nullImage) {
      const mediaInput: IMediaInput = {
        description: this.state.description!!,
        name: this.state.name!!,
        logoImage: await toGraphqlFile(this.state.logoImage!!),
        originHtmlContentSelector: '',
        themePrimaryColor: this.state.themePrimaryColor,
        themeSecondaryColor: '',
        unpublished: !this.state.isPublished
      }

      this.props.onSave(mediaInput)
    }
  }

  private onDescriptionChanged = (description: string | null) =>
    this.setState({ description })
  private onLogoChanged = (logoImage: ImageFile | null) =>
    this.setState({ logoImage })
  private onNamedChanged = (name: string | null) => this.setState({ name })

  private onThemePrimaryColorChanged = (color: string) =>
    this.setState({ themePrimaryColor: color })

  private onClickSwitchPublished = () =>
    this.setState({ isPublished: !this.state.isPublished })
}
