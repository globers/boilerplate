import React from 'react'
import { createStyles, withStyles, WithStyles } from '@material-ui/core'
import Drawer from '@material-ui/core/Drawer/Drawer'
import List from '@material-ui/core/List/List'
import ListItem from '@material-ui/core/ListItem/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon/ListItemIcon'
import Divider from '@material-ui/core/Divider/Divider'
import SendIcon from '@material-ui/icons/Send'
import CreateIcon from '@material-ui/icons/Create'
import PeopleIcon from '@material-ui/icons/People'
import PersonIcon from '@material-ui/icons/Person'
import SettingsIcon from '@material-ui/icons/Settings'
import ListItemText from '@material-ui/core/ListItemText/ListItemText'

const styles = () =>
  createStyles({
    drawer: {
      width: 250,
      flexShrink: 0
    },
    drawerPaper: {
      width: 250,
      paddingTop: 64 // Same padding as in Layout to take AppBar size in account
    }
  })

export enum MenuItem {
  Drafts,
  Published,
  Subscribers,
  Settings,
  UserProfile
}

interface IProps extends WithStyles<typeof styles> {
  selectedItem: MenuItem
  onItemClick: (item: MenuItem) => void
}

class MediaAdminMenu extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props)
  }

  public render() {
    const { classes, selectedItem } = this.props
    return (
      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper
        }}
      >
        <List>
          <ListItem
            button={true}
            selected={selectedItem === MenuItem.Drafts}
            onClick={this.onItemClick(MenuItem.Drafts)}
          >
            <ListItemIcon>
              <CreateIcon />
            </ListItemIcon>
            <ListItemText primary={'Drafts'} />
          </ListItem>

          <ListItem
            button={true}
            selected={selectedItem === MenuItem.Published}
            onClick={this.onItemClick(MenuItem.Published)}
          >
            <ListItemIcon>
              <SendIcon />
            </ListItemIcon>
            <ListItemText primary={'Published'} />
          </ListItem>
          <Divider />
          <ListItem
            button={true}
            selected={selectedItem === MenuItem.Subscribers}
            onClick={this.onItemClick(MenuItem.Subscribers)}
          >
            <ListItemIcon>
              <PeopleIcon />
            </ListItemIcon>
            <ListItemText primary={'Subscribers'} />
          </ListItem>

          <ListItem
            button={true}
            selected={selectedItem === MenuItem.Settings}
            onClick={this.onItemClick(MenuItem.Settings)}
          >
            <ListItemIcon>
              <SettingsIcon />
            </ListItemIcon>
            <ListItemText primary={'Media Settings'} />
          </ListItem>

          <ListItem
            button={true}
            selected={selectedItem === MenuItem.UserProfile}
            onClick={this.onItemClick(MenuItem.UserProfile)}
          >
            <ListItemIcon>
              <PersonIcon />
            </ListItemIcon>
            <ListItemText primary={'User Profile'} />
          </ListItem>
        </List>
      </Drawer>
    )
  }

  private onItemClick = (item: MenuItem) => () => this.props.onItemClick(item)
}

export default withStyles(styles)(MediaAdminMenu)
