import React from 'react'
import { StyleRules, withStyles } from '@material-ui/core/styles'
import { WithStyles } from '@material-ui/core'
import MediaIcon from './MediaIcon'
import Typography from '@material-ui/core/Typography/Typography'
import Button from '@material-ui/core/Button/Button'
import { Theme } from '@material-ui/core/es'
import Link from 'next/link'
import { getMediaAdminUrl, getMediaUrl } from '../../services/urlService'

const styles = (theme: Theme): StyleRules => ({
  root: {
    margin: '12px',
    padding: '8px',
    width: '320px',
    height: '350px',
    overflowY: 'auto',
    borderStyle: 'solid',
    borderWidth: '1px',
    borderColor: '#D3D3D3',
    backgroundColor: theme.palette.background.paper
  },
  logoAndHeader: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  logo: {
    maxWidth: '100%',
    width: '100%',
    height: '150px',
    maxHeight: '150px',
    margin: '8px auto'
  },
  header: {
    margin: '8px'
  },
  link: {
    textDecoration: 'none'
  },
  title: {},
  owner: {},
  ownerTypo: {
    color: theme.palette.primary.main,
    opacity: 0.8
  },
  followButton: {
    margin: '4px'
  },
  description: {
    padding: '8px'
  },
  descriptionTypo: {},
  recentArticlesButtons: {},
  notPublishedLabel: {
    color: 'red'
  }
})

interface IMedia {
  uri: string
  name: string
  description: string
  logoImageUrl: string
  unpublished: boolean
  owner: IUserPublicProfile
}

interface IUserPublicProfile {
  uri: string
  firstName: string
  lastName: string
}

interface IProps extends WithStyles<typeof styles> {
  media: IMedia
  isContributor: boolean
}

function MediaSnippet(props: IProps) {
  const { classes, media, isContributor } = props

  const mediaUrl = isContributor
    ? getMediaAdminUrl(media.uri)
    : getMediaUrl(media.uri)

  return (
    <div className={classes.root}>
      {isContributor &&
        media.unpublished && (
          <Typography variant="h6" className={classes.notPublishedLabel}>
            Not Published
          </Typography>
        )}
      <div className={classes.logoAndHeader}>
        <div className={classes.logo}>
          <MediaIcon media={media} isMediaAdmin={isContributor} />
        </div>
        <div className={classes.header}>
          <Link as={mediaUrl.as} href={mediaUrl.href}>
            <a className={classes.link}>
              <div className={classes.title}>
                <Typography variant="subtitle1">{media.name}</Typography>
              </div>
            </a>
          </Link>
          <div className={classes.owner}>
            <Typography variant="subtitle2" className={classes.ownerTypo}>
              <em>
                By<b> {media.owner.firstName}</b>
              </em>
            </Typography>
          </div>
        </div>
        {isContributor && (
          <div className={classes.followButton}>
            <Link href={mediaUrl.href} as={mediaUrl.as}>
              <a className={classes.link}>
                <Button size="small" variant="outlined" color="primary">
                  Manage media
                </Button>
              </a>
            </Link>
          </div>
        )}
      </div>
      <Link as={mediaUrl.as} href={mediaUrl.href}>
        <a className={classes.link}>
          <div className={classes.description}>
            <Typography variant="subtitle2" className={classes.descriptionTypo}>
              {media.description}
            </Typography>
          </div>
        </a>
      </Link>
    </div>
  )
}

export default withStyles(styles)(MediaSnippet)
