import * as React from 'react'
import gql from 'graphql-tag'
import { Mutation } from 'react-apollo'
import EditMediaBehavior, { IMediaInput } from './EditMediaBehavior'
import { SaveMediaFailureReason, SaveMediaReply } from './SaveMediaReply'

const UpdateMediaMut = gql`
  mutation UpdateMedia($mediaUri: ID!, $media: MediaInput!) {
    updateMedia(mediaUri: $mediaUri, media: $media) {
      ... on SaveMediaFailure {
        failureReason
      }
      ... on SaveMediaSuccess {
        contributedMedia {
          stripeAccountConnected
          uri
          media {
            uri
            description
            logoUrl
            name
            owner {
              firstName
              lastName
              uri
            }
            themePrimaryColor
          }
        }
      }
    }
  }
`

interface IMutationReply {
  updateMedia: SaveMediaReply
}

interface IState {
  saveFailureReason: SaveMediaFailureReason | null
  waitForUpdateMediaReply: boolean
}

interface IContributedMedia {
  uri: string
  media: IMediaFromQuery
  stripeAccountConnected: boolean
}

interface IMediaFromQuery {
  uri: string
  name: string
  description: string
  logoUrl: string
  unpublished: boolean
  themePrimaryColor: string
}

interface IProps {
  contributedMedia: IContributedMedia
}

export class UpdateMedia extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    this.state = {
      saveFailureReason: null,
      waitForUpdateMediaReply: false
    }
  }

  public render() {
    return (
      <Mutation mutation={UpdateMediaMut} onCompleted={this.onMutationResult}>
        {mutation => {
          const callMutation = (mediaInput: IMediaInput) => {
            this.setState({ waitForUpdateMediaReply: true })
            mutation({
              variables: {
                mediaUri: this.props.contributedMedia.media.uri,
                media: mediaInput
              }
            })
          }

          return (
            <EditMediaBehavior
              stripeAccountConnected={
                this.props.contributedMedia.stripeAccountConnected
              }
              initialMedia={this.props.contributedMedia.media}
              saveFailureReason={this.state.saveFailureReason}
              onSave={callMutation}
              waitForSubmitReply={this.state.waitForUpdateMediaReply}
            />
          )
        }}
      </Mutation>
    )
  }

  private onMutationResult = (result: IMutationReply) => {
    this.setState({ waitForUpdateMediaReply: false })
    switch (result.updateMedia.__typename) {
      case 'SaveMediaFailure':
        this.setState({ saveFailureReason: result.updateMedia.failureReason })
        break
      case 'SaveMediaSuccess':
        this.setState({ saveFailureReason: null })
        break
    }
  }
}
