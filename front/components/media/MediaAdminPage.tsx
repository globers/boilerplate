import React from 'react'
import { withStyles, WithStyles } from '@material-ui/core'
import Layout from '../MyLayout'
import MediaAdminAppBar from './MediaAdminAppBar'
import { IGraphqlDate } from '../../graphql/schema/GraphqlDate'
import { StyleRules } from '@material-ui/core/styles'
import MediaAdminMenu, { MenuItem } from './MediaAdminMenu'
import DraftsItem from './mediaAdminPagItems/DraftsItem'
import PublishedItem from './mediaAdminPagItems/PublishedItem'
import SettingsItem from './mediaAdminPagItems/SettingsItem'
import SubscribersItem from './mediaAdminPagItems/SubscribersItem'
import UserProfileItem from './mediaAdminPagItems/UserProfileItem'

const styles: StyleRules = {
  root: {
    display: 'flex'
  },
  header: {
    paddingTop: '50px',
    paddingBottom: '80px',
    width: '100%'
  },
  headerTitle: {
    paddingLeft: '32px',
    float: 'left'
  },
  contentWrapper: {
    maxWidth: 700,
    margin: '0 auto', // center horizontally
    flexGrow: 1
  },
  newArticleDiv: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: '100px'
  },
  createDraftButton: {
    float: 'right',
    marginTop: '7px',
    marginBottom: '7px'
  },
  mainContent: {
    flexGrow: 1,
    maxWidth: '100%'
  }
}

interface IArticle {
  uri: string
  title: string
  imageUrl: string
  firstPublication: IGraphqlDate
  lastPublication: IGraphqlDate
}

interface IPublishedArticle {
  uri: string
  article: IArticle
  revision: { uri: string }
}

interface IArticleDraft {
  uri: string
  revisions: IArticleDraftRevision[]
}

interface IArticleDraftRevision {
  uri: string
  title: string
  subtitle: string
  content: string
  imageUrl: string
  savingDate: IGraphqlDate
}

interface IContributedArticle {
  uri: string
  draft: IArticleDraft
  publishedArticle: IPublishedArticle | null
}

interface IMedia {
  uri: string
  logoUrl: string
  name: string
  description: string
  themePrimaryColor: string
}

interface IUser {
  uri: string
  firstName: string
  lastName: string
  profilePictureUrl: string | null
}

interface IProps extends WithStyles<typeof styles> {
  user: IUser
  contributedMedia: IContributedMedia
}

interface IContributedMedia {
  uri: string
  media: IMedia
  stripeAccountConnected: boolean
  contributedArticles: IContributedArticle[]
  subscribers: ISubscriber[]
}

interface IUserSubscription {
  uri: string
  startSubscriptionDate: IGraphqlDate
}

interface ISubscriber {
  uri: string
  profile: IUserPublicProfile
  subscription: IUserSubscription
}

interface IUserPublicProfile {
  uri: string
  firstName: string
  lastName: string
}

interface IState {
  selectedItem: MenuItem
}

class MediaAdminPage extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    this.state = { selectedItem: MenuItem.Drafts }
  }

  public render() {
    const { classes, user, contributedMedia } = this.props

    const media = contributedMedia.media

    return (
      <Layout
        pageTitle={`${media.name} - Admin - Globers`}
        appBar={
          <MediaAdminAppBar
            media={media}
            userFirstName={user.firstName}
            isAdminView={true}
          />
        }
      >
        <div className={classes.root}>
          <MediaAdminMenu
            onItemClick={this.onItemClick}
            selectedItem={this.state.selectedItem}
          />

          <div className={classes.mainContent}>
            {this.getSelectedItemPage()}
          </div>
        </div>
      </Layout>
    )
  }

  private onItemClick = (item: MenuItem) => {
    this.setState({ selectedItem: item })
  }

  private getSelectedItemPage = () => {
    const { contributedMedia } = this.props
    const media = contributedMedia.media

    switch (this.state.selectedItem) {
      case MenuItem.Drafts:
        const notPublished = contributedMedia.contributedArticles.filter(
          c => c.publishedArticle === null
        )
        return <DraftsItem notPublishedArticles={notPublished} media={media} />
      case MenuItem.Published:
        const published = contributedMedia.contributedArticles.filter(
          c => c.publishedArticle !== null
        )
        return <PublishedItem publishedArticles={published} media={media} />
      case MenuItem.Settings:
        return <SettingsItem contributedMedia={contributedMedia} />
      case MenuItem.Subscribers:
        return (
          <SubscribersItem
            mediaUri={media.uri}
            subscribers={contributedMedia.subscribers}
            bankAccountConnected={contributedMedia.stripeAccountConnected}
          />
        )
      case MenuItem.UserProfile:
        return <UserProfileItem user={this.props.user} />
    }
  }
}

export default withStyles(styles)(MediaAdminPage)
