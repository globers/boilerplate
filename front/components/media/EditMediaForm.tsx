import * as React from 'react'
import {
  createStyles,
  TextField,
  WithStyles,
  WithTheme
} from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button/Button'
import ImageDropZoneField, { ImageFile } from '../utils/imageDropZoneField'
import FormControlLabel from '@material-ui/core/FormControlLabel/FormControlLabel'
import Switch from '@material-ui/core/Switch/Switch'
import LoadingProgress from '../utils/loadingProgress'
import Link from 'next/link'
// @ts-ignore
import { ChromePicker } from 'react-color'
import { getConnectBankAccountToMediaUrl } from '../../services/urlService'
import Typography from '@material-ui/core/Typography/Typography'
import ExpansionPanel from '@material-ui/core/ExpansionPanel/ExpansionPanel'
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary/ExpansionPanelSummary'
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails/ExpansionPanelDetails'
import { ExpandMore } from '@material-ui/icons'
import { isDark } from '../../services/styleServices'
import withTheme from '@material-ui/core/styles/withTheme'

const styles = () =>
  createStyles({
    saveButton: {
      display: 'block',
      margin: '20px auto'
    },
    connectAccountButton: {},
    textField: {},
    listFields: {
      margin: '20px',
      listStyleType: 'none',
      maxWidth: 600,
      width: '100%',
      '@global': {
        li: {
          marginTop: 16,
          marginBottom: 16
        }
      }
    },
    logoDropZoneOutside: {
      width: '400px',
      height: '133px',
      maxWidth: '400px',
      maxHeight: '133px',
      display: 'block',
      position: 'relative',
      '@media (max-width: 420px)': {
        width: 300,
        height: 100,
        maxWidth: 300,
        maxHeight: 100
      }
    },
    errorLogoMessage: {
      color: 'red'
    },
    link: {
      textDecoration: 'none'
    },
    BarColorPanel: {
      maxWidth: 280
    },
    resetMediaColorButton: {
      display: 'block',
      margin: '0px auto 8px auto'
    }
  })

interface IProps extends WithStyles<typeof styles>, WithTheme {
  onClickSubmit: () => void
  onNameChanged: (name: string | null) => void
  onDescriptionChanged: (description: string | null) => void
  onLogoImageChanged: (image: ImageFile | null) => void
  onThemePrimaryColorChanged: (colorAsHex: string) => void
  onClickSwitchPublished: () => void
  isEdit: boolean
  waitForSubmitReply: boolean

  nameAlreadyExistsError: boolean
  emptyNameError: boolean
  emptyDescriptionError: boolean
  emptyLogoImageError: boolean
  invalidLogoImageError: boolean

  mediaUri: string | null
  name: string | null
  description: string | null
  logoImage: ImageFile | null
  isPublished: boolean
  themePrimaryColor: string

  displayIsPublished: boolean
  stripeAccountConnected: boolean
}

class EditMediaForm extends React.PureComponent<IProps> {
  public render() {
    const { classes, isEdit, mediaUri } = this.props
    let bankUrl = null
    if (isEdit) {
      // mediaUri should not be null in edit mode
      bankUrl = getConnectBankAccountToMediaUrl(mediaUri as string)
    }

    return (
      <ul className={classes.listFields}>
        <li>
          <TextField
            className={classes.textField}
            id="name-input"
            label="Media name"
            margin="normal"
            onChange={this.onChangeName}
            fullWidth={true}
            error={
              this.props.emptyNameError || this.props.nameAlreadyExistsError
            }
            value={this.props.name ? this.props.name : ''}
            helperText={
              this.props.emptyNameError
                ? 'Name must not be empty'
                : this.props.nameAlreadyExistsError
                  ? 'Media name already exists'
                  : null
            }
          />
        </li>
        <li>
          <TextField
            className={classes.textField}
            id="description-input"
            label="Short description"
            margin="normal"
            onChange={this.onChangeDescription}
            fullWidth={true}
            multiline={true}
            rowsMax="4"
            error={this.props.emptyDescriptionError}
            value={this.props.description ? this.props.description : ''}
            helperText={
              this.props.emptyDescriptionError
                ? 'Description must not be empty'
                : null
            }
          />
        </li>
        <li>
          <div className={classes.logoDropZoneOutside}>
            <ImageDropZoneField
              onImageFileDropped={this.props.onLogoImageChanged}
              imageFile={this.props.logoImage}
              messageInDropZone="Media logo, it should be wider than it is tall"
              displayLoading={false}
            />
          </div>
          {this.props.invalidLogoImageError && (
            <div className={classes.errorLogoMessage}>
              Cannot read image file
            </div>
          )}
          {this.props.emptyLogoImageError && (
            <div className={classes.errorLogoMessage}>
              Logo image must not be empty
            </div>
          )}
        </li>
        {this.props.isEdit && (
          <li>
            <ExpansionPanel className={classes.BarColorPanel}>
              <ExpansionPanelSummary
                style={{ backgroundColor: this.props.themePrimaryColor }}
                expandIcon={<ExpandMore />}
              >
                <Typography
                  style={{
                    color: isDark(this.props.themePrimaryColor)
                      ? 'white'
                      : 'black'
                  }}
                  variant={'body1'}
                >
                  Navigation bar color
                </Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <div>
                  <Button
                    variant="outlined"
                    onClick={this.onMediaPrimaryColorReset}
                    className={classes.resetMediaColorButton}
                    size={'small'}
                  >
                    Reset color
                  </Button>
                  <ChromePicker
                    color={
                      this.props.themePrimaryColor
                        ? this.props.themePrimaryColor
                        : this.props.theme.palette.background.default
                    }
                    onChangeComplete={this.onMediaPrimaryColorChange}
                    disableAlpha={true}
                  />
                </div>
              </ExpansionPanelDetails>
            </ExpansionPanel>
          </li>
        )}
        {this.props.displayIsPublished && (
          <li>
            <FormControlLabel
              control={
                <Switch
                  checked={this.props.isPublished}
                  onChange={this.props.onClickSwitchPublished}
                  value="checkedB"
                  color="primary"
                />
              }
              label="Published"
            />
          </li>
        )}
        {this.props.isEdit &&
          bankUrl && (
            <li>
              {this.props.stripeAccountConnected ? (
                <>
                  <Button
                    variant="outlined"
                    color="primary"
                    disabled={true}
                    className={classes.connectAccountButton}
                    fullWidth={false}
                  >
                    Bank account connected
                  </Button>
                  <div>
                    To modify your bank account, please contact
                    support@globers.co
                  </div>
                </>
              ) : (
                <Link href={bankUrl.href} as={bankUrl.as}>
                  <a className={classes.link}>
                    <Button
                      variant="outlined"
                      color="primary"
                      className={classes.connectAccountButton}
                      fullWidth={false}
                    >
                      Connect Bank account
                    </Button>
                  </a>
                </Link>
              )}
            </li>
          )}
        <li>
          <Button
            variant="contained"
            color="primary"
            className={classes.saveButton}
            fullWidth={false}
            onClick={this.props.onClickSubmit}
            disabled={this.props.waitForSubmitReply}
          >
            {this.props.waitForSubmitReply && <LoadingProgress />}
            {this.props.isEdit ? 'Save' : 'Create media'}
          </Button>
        </li>
      </ul>
    )
  }

  private onChangeName = (e: any) => this.props.onNameChanged(e.target.value)

  private onMediaPrimaryColorChange = (color: { hex: string }) => {
    this.props.onThemePrimaryColorChanged(color.hex)
  }

  private onMediaPrimaryColorReset = () => {
    this.props.onThemePrimaryColorChanged('')
  }

  private onChangeDescription = (e: any) =>
    this.props.onDescriptionChanged(e.target.value)
}

export default withStyles(styles)(withTheme()(EditMediaForm))
