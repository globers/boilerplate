import React from 'react'
import { withStyles, WithStyles } from '@material-ui/core'
import { StyleRules } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button/Button'
import Typography from '@material-ui/core/Typography/Typography'
import Link from 'next/link'
import { backgroundIndexImageUrl } from '../../services/imageService'
import MediaSnippetFlex from '../landing/MediaSnippetFlex'

interface IMedia {
  uri: string
  name: string
  logoUrl: string
  description: string
  unpublished: boolean
  owner: IUserPublicProfile
  nbArticles: number
}

interface IUserPublicProfile {
  uri: string
  firstName: string
  lastName: string
}

interface IUserMedia {
  uri: string
  media: IMedia
  followed: boolean
}

interface IContributedMedia {
  uri: string
  media: IMedia
}

const styles = (): StyleRules => ({
  root: {
    margin: '0 auto' // center horizontally
  },
  mediaList: {
    maxWidth: 700,
    margin: '0 auto' // center horizontally
  },
  link: {
    textDecoration: 'none'
  },
  paperCreateMedia: {
    marginTop: '32px',
    marginBottom: '16px',
    height: '200px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  nbMedias: {
    padding: '80px 20px 40px 20px',
    textAlign: 'center',
    color: 'white'
  },
  createMediaButton: {
    display: 'flex',
    textAlign: 'center',
    margin: '20px auto 60px auto'
  },
  callToAction: {
    backgroundImage: `url("${backgroundIndexImageUrl}")`,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    paddingTop: '100px',
    paddingBottom: '100px'
  }
})

interface IProps extends WithStyles<typeof styles> {
  userMedias: IUserMedia[]
  contributedMedias: IContributedMedia[]
}

class MediaSnippetList extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props)
  }

  public render() {
    const { classes, userMedias, contributedMedias } = this.props

    const nbMedias = contributedMedias.length + userMedias.length

    return (
      <div className={classes.root}>
        <div className={classes.callToAction}>
          <Typography variant="h3" className={classes.nbMedias}>
            <b>{'Join the ' + nbMedias + ' medias already on Globers'}</b>
          </Typography>
          <Link href="\newMedia">
            <a className={classes.link}>
              <Button
                size="large"
                variant="contained"
                color="primary"
                className={classes.createMediaButton}
              >
                <b>Create My Media</b>
              </Button>
            </a>
          </Link>
        </div>
        <MediaSnippetFlex
          userMedias={userMedias}
          contributedMedias={contributedMedias}
        />
      </div>
    )
  }
}

export default withStyles(styles)(MediaSnippetList)
