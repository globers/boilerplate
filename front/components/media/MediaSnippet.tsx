import React from 'react'
import { StyleRules, Theme } from '@material-ui/core/styles'
import { WithStyles, withStyles } from '@material-ui/core'
import Link from 'next/link'
import { getMediaAdminUrl, getMediaUrl } from '../../services/urlService'
import { getFormatedLogoUrl } from '../../services/imageService'
import Typography from '@material-ui/core/Typography/Typography'
import Avatar from '@material-ui/core/Avatar/Avatar'
import Button from '@material-ui/core/Button/Button'

const styles = (theme: Theme): StyleRules => ({
  root: {
    width: '320px',
    maxWidth: '320px',
    height: '450px',
    borderStyle: 'solid',
    borderRadius: '8px',
    borderWidth: '1px',
    borderColor: '#D3D3D3',
    backgroundColor: theme.palette.background.paper
  },
  header: {
    borderRadius: '8px 8px 0px 0px',
    height: 150,
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10
  },
  mediaLogo: {
    objectFit: 'contain',
    maxWidth: '100%',
    maxHeight: '100%',
    borderRadius: 8
  },
  content: {
    padding: '16px 16px 0px 16px',
    position: 'relative',
    bottom: 30,
    height: '220px',
    maxHeight: '220px',
    overflowY: 'auto'
  },
  profileOrAvatarContainer: {
    bottom: 30,
    position: 'relative',
    height: 100,
    paddingLeft: 16,
    paddingRight: 16,
    display: 'flex'
  },
  profileImage: {
    objectFit: 'cover',
    borderRadius: '50%',
    height: 100,
    width: 100,
    borderWidth: 3,
    backgroundColor: theme.palette.background.paper,
    borderColor: theme.palette.background.paper,
    borderStyle: 'solid'
  },
  avatar: {
    backgroundColor: theme.palette.primary.main,
    height: 100,
    width: 100,
    borderWidth: 3,
    borderColor: theme.palette.background.paper,
    borderStyle: 'solid'
  },
  link: {
    textDecoration: 'none'
  },
  ownerTypo: {
    color: theme.palette.primary.main,
    opacity: 0.9
  },
  description: {
    marginTop: '8px'
  },
  button: {
    marginLeft: 'auto',
    marginBottom: '8px',
    alignSelf: 'flex-end'
  }
})

interface IMedia {
  uri: string
  name: string
  description: string
  logoUrl: string
  unpublished: boolean
  themePrimaryColor: string
  owner: IUserPublicProfile
}

interface IUserPublicProfile {
  uri: string
  firstName: string
  lastName: string
  profilePictureUrl: string | null
}

interface IProps extends WithStyles<typeof styles> {
  media: IMedia
  isContributor: boolean
}

function MediaSnippet(props: IProps) {
  const { classes, media, isContributor } = props

  const mediaUrl = isContributor
    ? getMediaAdminUrl(media.uri)
    : getMediaUrl(media.uri)

  const headerStyle = media.themePrimaryColor
    ? { backgroundColor: media.themePrimaryColor }
    : {}

  return (
    <div className={classes.root}>
      <Link as={mediaUrl.as} href={mediaUrl.href}>
        <a className={classes.link}>
          <div className={classes.header} style={headerStyle}>
            <img
              src={getFormatedLogoUrl(media.logoUrl)}
              className={classes.mediaLogo}
              alt={media.name}
              id={`logo-media-${media.uri}`}
            />
          </div>
        </a>
      </Link>
      <div className={classes.profileOrAvatarContainer}>
        {media.owner.profilePictureUrl ? (
          <img
            src={getFormatedLogoUrl(media.owner.profilePictureUrl)}
            className={classes.profileImage}
            alt={media.name}
            id={`profile-${media.owner.uri}`}
          />
        ) : (
          <Avatar className={classes.avatar}>
            {media.owner.firstName.charAt(0).toUpperCase() +
              media.owner.lastName.charAt(0).toUpperCase()}
          </Avatar>
        )}

        {isContributor && (
          <div className={classes.button}>
            <Link href={mediaUrl.href} as={mediaUrl.as}>
              <a className={classes.link}>
                <Button size="small" variant="outlined" color="primary">
                  Manage media
                </Button>
              </a>
            </Link>
          </div>
        )}
      </div>
      <div className={classes.content}>
        <Link as={mediaUrl.as} href={mediaUrl.href}>
          <a className={classes.link}>
            <div className={classes.title}>
              <Typography variant="subtitle1">{media.name}</Typography>
            </div>
          </a>
        </Link>
        <Typography variant="subtitle2" className={classes.ownerTypo}>
          <em>{`By ${media.owner.firstName} ${media.owner.lastName}`}</em>
        </Typography>
        <Typography className={classes.description} variant="body2">
          {media.description}
        </Typography>
      </div>
    </div>
  )
}

export default withStyles(styles)(MediaSnippet)
