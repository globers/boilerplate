import React from 'react'
import { createStyles, withStyles, WithStyles } from '@material-ui/core'
import { IGraphqlDate, toDate } from '../../../graphql/schema/GraphqlDate'
import Table from '@material-ui/core/Table/Table'
import TableHead from '@material-ui/core/TableHead/TableHead'
import TableCell from '@material-ui/core/TableCell/TableCell'
import TableRow from '@material-ui/core/TableRow/TableRow'
import TableBody from '@material-ui/core/TableBody/TableBody'
import { formatDate } from '../../../services/dateServices'
import Link from 'next/link'
import Button from '@material-ui/core/Button/Button'
import { getConnectBankAccountToMediaUrl } from '../../../services/urlService'
import Typography from '@material-ui/core/Typography/Typography'

const styles = () =>
  createStyles({
    link: {
      textDecoration: 'none'
    },
    connectAccountButton: {
      display: 'block',
      margin: '20px auto'
    },
    noBankAccountMessage: {
      display: 'block',
      margin: '20px auto',
      padding: 20,
      maxWidth: 600,
      width: '100%'
    }
  })

interface IUserSubscription {
  uri: string
  startSubscriptionDate: IGraphqlDate
}

interface ISubscriber {
  uri: string
  profile: IUserPublicProfile
  subscription: IUserSubscription
}

interface IUserPublicProfile {
  uri: string
  firstName: string
  lastName: string
}

interface IProps extends WithStyles<typeof styles> {
  subscribers: ISubscriber[]
  bankAccountConnected: boolean
  mediaUri: string
}

class SubscribersItem extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props)
  }

  public render() {
    const { subscribers, bankAccountConnected, mediaUri, classes } = this.props

    if (!bankAccountConnected) {
      const bankUrl = getConnectBankAccountToMediaUrl(mediaUri)
      return (
        <>
          <div className={classes.noBankAccountMessage}>
            <Typography variant="subtitle1">
              Bank account not connected. Connect your bank account to start
              acquire subscribers
            </Typography>
          </div>
          <Link href={bankUrl.href} as={bankUrl.as}>
            <a className={classes.link}>
              <Button
                variant="contained"
                color="primary"
                className={classes.connectAccountButton}
                fullWidth={false}
              >
                Connect Bank account
              </Button>
            </a>
          </Link>
        </>
      )
    }

    return (
      <div>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>First name</TableCell>
              <TableCell>Last name</TableCell>
              <TableCell>Subscription date</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {subscribers.length === 0 && (
              <TableRow>
                <TableCell>No subscriber yet</TableCell>
                <TableCell />
                <TableCell />
              </TableRow>
            )}

            {subscribers.map((sub: ISubscriber) => (
              <TableRow key={sub.uri}>
                <TableCell>{sub.profile.firstName}</TableCell>
                <TableCell>{sub.profile.lastName}</TableCell>
                <TableCell>
                  {formatDate(toDate(sub.subscription.startSubscriptionDate))}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </div>
    )
  }
}

export default withStyles(styles)(SubscribersItem)
