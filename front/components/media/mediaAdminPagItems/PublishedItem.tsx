import React from 'react'
import { withStyles, WithStyles } from '@material-ui/core'
import DraftList from '../../article/DraftList'
import { IGraphqlDate } from '../../../graphql/schema/GraphqlDate'
import { StyleRules } from '@material-ui/core/styles'

const styles: StyleRules = {
  contentWrapper: {
    maxWidth: 700,
    margin: '0 auto' // center horizontally
  }
}

interface IArticle {
  uri: string
  title: string
  imageUrl: string
  firstPublication: IGraphqlDate
  lastPublication: IGraphqlDate
}

interface IPublishedArticle {
  uri: string
  article: IArticle
  revision: { uri: string }
}

interface IContributedArticle {
  uri: string
  publishedArticle: IPublishedArticle | null
}

interface IMedia {
  uri: string
  logoUrl: string
  name: string
}

interface IProps extends WithStyles<typeof styles> {
  media: IMedia
  publishedArticles: IContributedArticle[]
}

class DraftsItem extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props)
  }

  public render() {
    const { classes, media, publishedArticles } = this.props
    return (
      <div className={classes.contentWrapper}>
        <DraftList
          mediaUri={media.uri}
          contributedArticles={publishedArticles}
          displayCreateDraftButton={false}
        />
      </div>
    )
  }
}

export default withStyles(styles)(DraftsItem)
