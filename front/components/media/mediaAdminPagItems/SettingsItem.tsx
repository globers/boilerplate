import React from 'react'
import { createStyles, withStyles } from '@material-ui/core'
import { UpdateMedia } from '../UpdateMedia'

const styles = () => createStyles({})

interface IContributedMedia {
  uri: string
  media: IMediaFromQuery
  stripeAccountConnected: boolean
}

interface IMediaFromQuery {
  uri: string
  name: string
  description: string
  logoUrl: string
  unpublished: boolean
  themePrimaryColor: string
}

interface IProps {
  contributedMedia: IContributedMedia
}
class SettingsItem extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props)
  }

  public render() {
    return <UpdateMedia contributedMedia={this.props.contributedMedia} />
  }
}

export default withStyles(styles)(SettingsItem)
