import React from 'react'
import { withStyles, WithStyles } from '@material-ui/core'
import DraftList from '../../article/DraftList'
import { IGraphqlDate } from '../../../graphql/schema/GraphqlDate'
import CreateArticleDraft from '../../mutation/CreateArticleDraft'
import Button from '@material-ui/core/Button/Button'
import Router from 'next/router'
import { StyleRules } from '@material-ui/core/styles'
import LoadingProgress from '../../utils/loadingProgress'

const styles: StyleRules = {
  header: {
    paddingTop: '50px',
    paddingBottom: '80px',
    width: '100%'
  },
  contentWrapper: {
    maxWidth: 700,
    margin: '0 auto', // center horizontally
    flexGrow: 1
  },
  newArticleDiv: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: '100px'
  },
  createDraftButton: {
    float: 'right',
    marginTop: '7px',
    marginBottom: '7px'
  }
}

interface IArticleDraft {
  uri: string
  revisions: IArticleDraftRevision[]
}

interface IArticleDraftRevision {
  uri: string
  title: string
  subtitle: string
  content: string
  imageUrl: string
  savingDate: IGraphqlDate
}

interface IContributedArticle {
  uri: string
  draft: IArticleDraft
}

interface IMedia {
  uri: string
  logoUrl: string
  name: string
}

interface IProps extends WithStyles<typeof styles> {
  media: IMedia
  notPublishedArticles: IContributedArticle[]
}

interface IState {
  waitingForDraftCreation: boolean
}

class DraftsItem extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    this.state = { waitingForDraftCreation: false }
  }

  public render() {
    const { classes, media, notPublishedArticles } = this.props
    return (
      <div className={classes.contentWrapper}>
        <div className={classes.header}>
          <CreateArticleDraft
            mediaUri={media.uri}
            onDraftCreated={this.onDraftCreated}
          >
            {createDraft => (
              <Button
                variant="contained"
                color="primary"
                size="small"
                onClick={this.onClickCreateDraft(createDraft)}
                className={classes.createDraftButton}
                disabled={this.state.waitingForDraftCreation}
              >
                {this.state.waitingForDraftCreation ? (
                  <>
                    <LoadingProgress />New article
                  </>
                ) : (
                  'New article'
                )}
              </Button>
            )}
          </CreateArticleDraft>
        </div>
        <DraftList
          mediaUri={media.uri}
          contributedArticles={notPublishedArticles}
          displayCreateDraftButton={true}
        />
      </div>
    )
  }

  private onDraftCreated = (mediaUri: string, articleDraftUri: string) => {
    this.setState({ waitingForDraftCreation: false })
    Router.push({
      pathname: '/editArticle',
      query: {
        mediaUri,
        articleDraftUri
      }
    })
  }

  private onClickCreateDraft = (createDraft: () => void) => () => {
    this.setState({ waitingForDraftCreation: true })
    createDraft()
  }
}

export default withStyles(styles)(DraftsItem)
