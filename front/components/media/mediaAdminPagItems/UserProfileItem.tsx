import React from 'react'
import { createStyles, withStyles, WithStyles } from '@material-ui/core'
import UpdateUserBehavior from '../../account/UpdateUserBehavior'

const styles = () =>
  createStyles({
    root: {
      width: 10
    }
  })

interface IUser {
  uri: string
  firstName: string
  lastName: string
  profilePictureUrl: string | null
}

interface IProps extends WithStyles<typeof styles> {
  user: IUser
}

class UserProfileItem extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props)
  }

  public render() {
    const { user } = this.props
    return (
      <UpdateUserBehavior
        initialFirstName={user.firstName}
        initialLastName={user.lastName}
        initialProfilePictureUrl={user.profilePictureUrl}
        onAccountSaved={noOp}
      />
    )
  }
}

// tslint:disable-next-line
function noOp() {}

export default withStyles(styles)(UserProfileItem)
