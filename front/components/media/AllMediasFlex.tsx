import React from 'react'
import { withStyles, WithStyles } from '@material-ui/core'
import MediaSnippet from '../media/MediaSnippet'
import { StyleRules } from '@material-ui/core/styles'
import LandingPageSection from '../landing/LandingSection'

interface IMedia {
  uri: string
  name: string
  logoUrl: string
  description: string
  unpublished: boolean
  owner: IUserPublicProfile
  nbArticles: number
  themePrimaryColor: string
}

interface IUserPublicProfile {
  uri: string
  firstName: string
  lastName: string
  profilePictureUrl: string | null
}

interface IUserMedia {
  uri: string
  media: IMedia
  followed: boolean
}

const styles = (): StyleRules => ({
  mediaList: {
    maxWidth: 1200,
    display: 'flex',
    flexWrap: 'wrap',
    margin: '0 auto', // center horizontally,
    alignItems: 'center',
    justifyContent: 'center'
  },
  mediaSnippetWrapper: {
    margin: 12
  }
})

interface IProps extends WithStyles<typeof styles> {
  userMedias: IUserMedia[]
}

class AllMediasFlex extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props)
  }

  public render() {
    const { classes, userMedias } = this.props

    return (
      <LandingPageSection
        background={'light'}
        title={`Les ${userMedias.length} médias de Globers`}
      >
        <div className={classes.mediaList}>
          {userMedias
            .sort(
              (a: IUserMedia, b: IUserMedia) =>
                b.media.nbArticles - a.media.nbArticles
            )
            .map(u => (
              <div className={classes.mediaSnippetWrapper}>
                <MediaSnippet
                  key={u.uri}
                  media={u.media}
                  isContributor={false}
                />
              </div>
            ))}
        </div>
      </LandingPageSection>
    )
  }
}

export default withStyles(styles)(AllMediasFlex)
