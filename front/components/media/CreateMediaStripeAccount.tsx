import React from 'react'
import EditMediaStripeAccountForm, {
  IStripeInput
} from './EditMediaStripeAccountForm'
import { injectStripe, ReactStripeElements } from 'react-stripe-elements'
import { IStripeAccountInputForToken } from '../../services/stripeServices'
import CreateStripeAccount, {
  ISaveStripeAccountFailure,
  IStripeAccountInput,
  SaveStripeAccountReason
} from '../mutation/CreateStripeAccount'
import { Dialog } from '@material-ui/core'
import DialogTitle from '@material-ui/core/DialogTitle/DialogTitle'
import DialogActions from '@material-ui/core/DialogActions/DialogActions'
import Button from '@material-ui/core/Button/Button'
import { getMediaAdminUrl } from '../../services/urlService'
import Router from 'next/router'
import DialogContentText from '@material-ui/core/DialogContentText/DialogContentText'
import DialogContent from '@material-ui/core/DialogContent/DialogContent'

enum StateForm {
  errorMessage,
  accountCreatedMessage,
  waitForSubmitReply,
  input
}

interface IState {
  stateForm: StateForm
  errorMessage: string | null // not null if errorMessage == errorMessage
}

interface IProps {
  mediaUri: string
  loginFirstName: string
  loginLastName: string
}

// NB: parent should include StripeProvider / Elements components
// https://github.com/stripe/react-stripe-elements#setting-up-your-payment-form-injectstripe
class CreateMediaStripeAccount extends React.Component<
  IProps & ReactStripeElements.InjectedStripeProps,
  IState
> {
  constructor(props: IProps) {
    super(props)
    this.state = {
      errorMessage: null,
      stateForm: StateForm.input
    }
  }

  public render() {
    const { loginFirstName, loginLastName, mediaUri } = this.props

    return (
      <>
        <CreateStripeAccount
          mediaUri={mediaUri}
          onAccountCreated={this.onAccountCreated}
          onAccountCreationFailed={this.onAccountCreationFailed}
        >
          {(createStripeAccount: (account: IStripeAccountInput) => void) => (
            <EditMediaStripeAccountForm
              initFirstName={loginFirstName}
              initLastName={loginLastName}
              onSubmit={this.onSubmit(createStripeAccount)}
              waitForSubmitReply={
                this.state.stateForm === StateForm.waitForSubmitReply
              }
            />
          )}
        </CreateStripeAccount>
        {this.state.stateForm === StateForm.errorMessage && <div>ERROR</div>}

        <Dialog
          open={this.state.stateForm === StateForm.errorMessage}
          onClose={this.onAccountErrorMessageClose}
          aria-labelledby="alert-dialog-error"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-error">
            Bank account connection failed
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              {this.state.errorMessage}
              <br />
              <br />
              <b>
                If you need help to solve this problem, please contact
                support@globers.co
              </b>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button
              onClick={this.onAccountErrorMessageClose}
              color="primary"
              autoFocus={true}
            >
              Ok
            </Button>
          </DialogActions>
        </Dialog>

        <Dialog
          open={this.state.stateForm === StateForm.accountCreatedMessage}
          onClose={this.onAccountCreatedMessageClose}
          aria-labelledby="alert-dialog-created"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-created">
            Bank account connected!
          </DialogTitle>
          <DialogActions>
            <Button
              onClick={this.onAccountCreatedMessageClose}
              color="primary"
              autoFocus={true}
            >
              Ok
            </Button>
          </DialogActions>
        </Dialog>
      </>
    )
  }

  private onAccountErrorMessageClose = () => {
    this.setState({
      stateForm: StateForm.input,
      errorMessage: null
    })
  }

  private onAccountCreatedMessageClose = () => {
    const mediaUrl = getMediaAdminUrl(this.props.mediaUri)
    Router.push(mediaUrl.href, mediaUrl.as)
  }

  private onAccountCreated = () => {
    this.setState({
      stateForm: StateForm.accountCreatedMessage,
      errorMessage: null
    })
  }

  private onAccountCreationFailed = (failure: ISaveStripeAccountFailure) => {
    switch (failure.failureReason) {
      case SaveStripeAccountReason.STRIPE_ERROR:
        const error = failure.stripeErrorMessage as string
        this.setState({
          stateForm: StateForm.errorMessage,
          errorMessage: error
        })
        break
    }
  }

  private onSubmit = (
    createStripeAccount: (account: IStripeAccountInput) => void
  ) => async (stripeInput: IStripeInput) => {
    this.setState({
      stateForm: StateForm.waitForSubmitReply,
      errorMessage: null
    })

    const account: IStripeAccountInputForToken = {
      legal_entity: {
        address: {
          city: stripeInput.addressCity,
          line1: stripeInput.addressLine1,
          line2: stripeInput.addressLine2,
          postal_code: stripeInput.addressPostalCode
        },
        dob: {
          day: stripeInput.dobDay,
          month: stripeInput.dobMonth,
          year: stripeInput.dobYear
        },
        first_name: stripeInput.firstName,
        last_name: stripeInput.lastName,
        type: stripeInput.type
      },
      tos_shown_and_accepted: stripeInput.stripeTosAccepted
    }

    let accountToken
    try {
      // @ts-ignore (seems to be a bug in typescript types of stripe ?
      // I follow https://stripe.com/docs/connect/account-tokens
      accountToken = await this.props.stripe!.createToken('account', account)

      if (accountToken.error) {
        this.setErrorFromStripeToken(accountToken.error)
        return
      }
    } catch (error) {
      this.setErrorFromStripeToken(error)
      return
    }

    let externalAccountToken
    // On react stripe lib, stripe will look for needed elements itself (here IbanElement)
    // so no need to specify it in createToken call

    try {
      externalAccountToken = await this.props.stripe!.createToken({
        // country and account_number are automatically populated from the IBAN Element.
        // https://stripe.com/docs/stripe-js/reference#stripe-create-token
        // @ts-ignore (seems to be a bug in typescript types of stripe)
        type: 'bank_account',
        currency: stripeInput.currency
      })

      if (externalAccountToken.error) {
        this.setErrorFromStripeToken(externalAccountToken.error)
        return
      }
    } catch (error) {
      this.setErrorFromStripeToken(error)
      return
    }

    const stripeAccountInput: IStripeAccountInput = {
      country: stripeInput.country,
      defaultCurrency: stripeInput.currency,
      accountTokenId: accountToken.token.id,
      externalAccountTokenId: externalAccountToken.token.id
    }

    this.setState({
      stateForm: StateForm.waitForSubmitReply,
      errorMessage: null
    })
    createStripeAccount(stripeAccountInput)
  }

  private setErrorFromStripeToken = (error: any) => {
    this.setState({
      stateForm: StateForm.errorMessage,
      errorMessage: error.message ? error.message : error.type
    })
  }
}

export default injectStripe(CreateMediaStripeAccount)
