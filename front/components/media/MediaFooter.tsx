import React from 'react'
import { createStyles, withStyles, WithStyles } from '@material-ui/core'
import { getSubscribeToMediaUrl } from '../../services/urlService'
import LinkButton from '../utils/LinkButton'
import Typography from '@material-ui/core/Typography/Typography'
import MediaSnippet from '../media/MediaSnippet'

const styles = () =>
  createStyles({
    root: {
      width: '100%',
      textAlign: 'center'
    },
    button: {
      display: 'block',
      margin: '0 auto',
      padding: 16
    },
    textBlock: {
      margin: 8
    },
    media: {
      display: 'inline-block'
    },
    listSubscribeArgs: {
      listStyleType: 'none'
    }
  })

interface IMedia {
  uri: string
  name: string
  description: string
  logoImageUrl: string
  unpublished: boolean
  owner: IUserPublicProfile
  subscriptionPlans: ISubscriptionPlan[]
}

interface ISubscriptionPlan {
  uri: string
}

interface IUserPublicProfile {
  uri: string
  firstName: string
  lastName: string
}

interface IProps extends WithStyles<typeof styles> {
  media: IMedia
  isUserAlreadySubscribed: boolean
}

class MediaFooter extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props)
  }

  public render() {
    const { classes, media } = this.props

    const subscribeToMediaUrl = getSubscribeToMediaUrl(media.uri)

    return (
      <div className={classes.root}>
        {media.subscriptionPlans.length === 1 &&
          !this.props.isUserAlreadySubscribed && (
            <div>
              <div className={classes.button}>
                <LinkButton
                  url={subscribeToMediaUrl}
                  variant={'contained'}
                  color={'primary'}
                  themePrimaryColor={''}
                >
                  Subscribe
                </LinkButton>
              </div>
              <Typography variant="h6" className={classes.textBlock}>
                {`Subscribe to ${this.props.media.name} for 7.99€/month`}
              </Typography>
              <Typography variant="subtitle1" className={classes.textBlock}>
                <ul className={classes.listSubscribeArgs}>
                  <li>
                    {`Support ${
                      this.props.media.owner.firstName
                    } and the important work of independent journalism`}
                  </li>
                  <li>
                    Get exclusive and unlimited access to all others medias on
                    Globers
                  </li>
                </ul>
              </Typography>
            </div>
          )}
        <div className={classes.media}>
          <MediaSnippet isContributor={false} media={media} />
        </div>
      </div>
    )
  }
}

export default withStyles(styles)(MediaFooter)
