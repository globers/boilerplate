import React from 'react'
import { createStyles, withStyles, WithStyles } from '@material-ui/core'
import AppBar from '../AppBar'
import { getMediaAdminUrl, getMediaUrl } from '../../services/urlService'
import LinkButton from '../utils/LinkButton'

const styles = () =>
  createStyles({
    switchLabel: {
      minWidth: '160px', // hack: minWidth so that "admin view" is displayed on one line
      marginLeft: '16px',
      marginRight: '16px'
    }
  })

interface IMedia {
  uri: string
  logoUrl: string
  name: string
  unpublished: boolean
  themePrimaryColor: string
}

interface IProps extends WithStyles<typeof styles> {
  media: IMedia
  userFirstName: string
  isAdminView: boolean
}

class MediaAdminAppBar extends React.PureComponent<IProps, {}> {
  constructor(props: IProps) {
    super(props)
  }

  public render() {
    const { media, userFirstName, isAdminView } = this.props

    const publicViewUrl = getMediaUrl(media.uri)
    const adminViewUrl = getMediaAdminUrl(media.uri)

    return (
      <AppBar
        isHome={false}
        logoUrl={null}
        media={media}
        userFirstName={userFirstName}
      >
        {isAdminView &&
          !media.unpublished && (
            <LinkButton
              themePrimaryColor={media.themePrimaryColor}
              variant={'outlined'}
              url={publicViewUrl}
            >
              Public view
            </LinkButton>
          )}
        {!isAdminView && (
          <LinkButton
            themePrimaryColor={media.themePrimaryColor}
            variant={'outlined'}
            url={adminViewUrl}
          >
            Admin view
          </LinkButton>
        )}
      </AppBar>
    )
  }
}

export default withStyles(styles)(MediaAdminAppBar)
