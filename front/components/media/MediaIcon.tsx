import React from 'react'
import { WithStyles } from '@material-ui/core'
import Link from 'next/link'
import withStyles from '@material-ui/core/styles/withStyles'
// @ts-ignore (no typescript defs for cloudinary-react
import { Image, Transformation } from 'cloudinary-react'
import { getFormatedLogoUrl } from '../../services/imageService'
import { getMediaAdminUrl, getMediaUrl } from '../../services/urlService'

const styles = {
  a: {
    maxWidth: 'inherit',
    maxHeight: 'inherit',
    height: 'inherit',
    width: 'inherit'
  },
  logoMedia: {
    maxWidth: 'inherit',
    maxHeight: 'inherit',
    height: 'auto',
    width: 'auto',
    borderRadius: 10
  }
}

interface IProps extends WithStyles<typeof styles> {
  media: { uri: string; logoUrl: string; name: string }
  isMediaAdmin: boolean
}

const MediaIcon = (props: IProps) => {
  const logoUrl = getFormatedLogoUrl(props.media.logoUrl)

  const mediaUrl = props.isMediaAdmin
    ? getMediaAdminUrl(props.media.uri)
    : getMediaUrl(props.media.uri)

  return (
    <Link as={mediaUrl.as} href={mediaUrl.href}>
      <a className={props.classes.a}>
        <img
          src={logoUrl}
          className={props.classes.logoMedia}
          alt={props.media.name}
          id={`logo-media-${props.media.uri}`}
        />
      </a>
    </Link>
  )
}

export default withStyles(styles)(MediaIcon)
