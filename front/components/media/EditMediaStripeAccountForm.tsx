import React from 'react'
import {
  createStyles,
  TextField,
  withStyles,
  WithStyles,
  WithTheme
} from '@material-ui/core'
import BigFormLayout from '../BigFormLayout'
import FormControl from '@material-ui/core/FormControl/FormControl'
import InputLabel from '@material-ui/core/InputLabel/InputLabel'
import Select from '@material-ui/core/Select/Select'
import { IbanElement, ReactStripeElements } from 'react-stripe-elements'
import Button from '@material-ui/core/Button/Button'
import LoadingProgress from '../utils/loadingProgress'
import Typography from '@material-ui/core/Typography/Typography'
import withTheme from '@material-ui/core/styles/withTheme'
import Checkbox from '@material-ui/core/Checkbox/Checkbox'
import { StripeCurrency } from '../../graphql/schema/StripeCurrency'
import { StripeCountry } from '../../graphql/schema/StripeCountry'
import AddressForm from '../AddressForm'

const styles = () =>
  createStyles({
    button: {
      display: 'block',
      margin: '20px auto'
    },
    textField: {},
    birthday: {
      display: 'inline-block',
      marginTop: '16px'
    },
    groupInfo: {
      borderStyle: 'solid',
      borderWidth: 1,
      padding: 16,
      margin: 16,
      borderRadius: 16,
      borderColor: '#BFBCBC'
    },
    tosSection: {
      paddingLeft: 16,
      paddingRight: 16,
      margin: 16
    },
    select: {
      marginTop: '16px', // to get perfect alignment between input and select
      marginBottom: '8px'
    },
    iban: {
      marginTop: '16px', // to get perfect alignment between input and select
      marginBottom: '8px',
      width: '100%',
      padding: '6px 0 7px',
      cursor: 'text'
    },
    error: {
      color: 'red'
    },
    listFields: {
      margin: '0 auto', // center horizontally
      listStyleType: 'none',
      padding: 0,
      width: 600,
      '@media (max-width: 420px)': {
        width: 300
      }
    }
  })

function currencyToLabel(currency: StripeCurrency) {
  switch (currency) {
    case StripeCurrency.EUR:
      return 'Euro €'
  }
}

export interface IStripeInput {
  currency: StripeCurrency
  country: StripeCountry
  firstName: string
  lastName: string
  addressCity: string
  addressLine1: string
  addressLine2: string
  addressPostalCode: string
  dobDay: number
  dobMonth: number
  dobYear: number
  type: 'individual' | 'company'
  stripeTosAccepted: boolean
}

interface IProps extends WithStyles<typeof styles>, WithTheme {
  initFirstName: string
  initLastName: string
  waitForSubmitReply: boolean

  onSubmit: (stripeInput: IStripeInput) => void
}

interface IState {
  currency: StripeCurrency
  country: StripeCountry
  firstName: string
  lastName: string
  addressCity: string
  addressLine1: string
  addressLine2: string
  addressPostalCode: string
  dobDay: number
  dobMonth: number
  dobYear: number

  type: 'individual'
  tosChecked: boolean

  emptyFirstNameError: boolean
  emptyLastNameError: boolean
  emptyAddressLine1Error: boolean
  emptyAddressLine2Error: boolean
  emptyCityError: boolean
  emptyPostalCodeError: boolean
  uncheckedTosError: boolean
  emptyIbanError: boolean
  invalidIbanErrorMessage: string | null

  isIbanFocused: boolean
  isIbanEmpty: boolean
  isIbanComplete: boolean
}

class CreateMediaStripeAccount extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    this.state = {
      currency: StripeCurrency.EUR,
      country: StripeCountry.FR,
      firstName: props.initFirstName,
      lastName: props.initLastName,
      addressCity: '',
      addressLine1: '',
      addressLine2: '',
      addressPostalCode: '',
      dobDay: 1,
      dobMonth: 1,
      dobYear: 2000,

      type: 'individual',

      emptyFirstNameError: false,
      emptyLastNameError: false,
      emptyAddressLine1Error: false,
      emptyAddressLine2Error: false,
      emptyCityError: false,
      emptyPostalCodeError: false,
      emptyIbanError: false,
      invalidIbanErrorMessage: null,
      uncheckedTosError: false,

      isIbanFocused: false,
      isIbanEmpty: true,
      isIbanComplete: false,
      tosChecked: false
    }
  }

  public render() {
    const { classes, theme, waitForSubmitReply } = this.props

    const {
      emptyFirstNameError,
      emptyLastNameError,
      emptyAddressLine1Error,
      emptyCityError,
      emptyPostalCodeError,
      uncheckedTosError,
      firstName,
      lastName,
      addressCity,
      addressLine1,
      addressLine2,
      addressPostalCode,
      dobDay,
      dobMonth,
      dobYear,
      country,
      currency
    } = this.state

    // Iban inspired by
    // https://gist.github.com/lfalke/1c5e7168424c8b2a65dcfba425fcc310#file-stripeinput-js-L8

    return (
      <BigFormLayout title={'Connect your bank account*'}>
        <ul className={classes.listFields}>
          <li>
            <div className={classes.groupInfo}>
              <Typography variant="h6">Personal info</Typography>
              <TextField
                value={firstName}
                className={classes.textField}
                id="firstName-input"
                label="First name"
                margin="normal"
                autoComplete="given-name"
                onChange={this.onChangeFirstName}
                error={emptyFirstNameError}
                helperText={emptyFirstNameError ? 'First name is empty' : null}
              />
              <TextField
                value={lastName}
                className={classes.textField}
                id="lastName-input"
                label="Last name"
                autoComplete="family-name"
                margin="normal"
                onChange={this.onChangeLastName}
                error={emptyLastNameError}
                helperText={emptyLastNameError ? 'Last name is empty' : null}
              />

              <FormControl className={classes.birthday}>
                <InputLabel>Birthday</InputLabel>
                <Select
                  native={true}
                  value={dobDay}
                  onChange={this.onChangeDobDay}
                >
                  {[...Array(31).keys()].map(x => x + 1).map(c => (
                    <option key={c} value={c}>
                      {c}
                    </option>
                  ))}
                </Select>
                <Select
                  native={true}
                  value={dobMonth}
                  onChange={this.onChangeDobMonth}
                >
                  {[...Array(12).keys()].map(x => x + 1).map(c => (
                    <option key={c} value={c}>
                      {new Date(2000, c - 1, 1).toLocaleString(undefined, {
                        month: 'short'
                      })}
                    </option>
                  ))}
                </Select>
                <Select
                  native={true}
                  value={dobYear}
                  onChange={this.onChangeDobYear}
                >
                  {[...Array(100).keys()]
                    .map(x => x + new Date().getUTCFullYear() - 100)
                    .map(c => (
                      <option key={c} value={c}>
                        {c}
                      </option>
                    ))}
                </Select>
              </FormControl>
            </div>
          </li>
          <li>
            <div className={classes.groupInfo}>
              <AddressForm
                onChangeCountry={this.onCountryChanged}
                emptyPostalCodeError={emptyPostalCodeError}
                onChangePostalCode={this.onChangePostalCode}
                emptyAddressLine1Error={emptyAddressLine1Error}
                addressLine2={addressLine2}
                addressLine1={addressLine1}
                addressCity={addressCity}
                onChangeAddressLine1={this.onChangeAddressLine1}
                addressPostalCode={addressPostalCode}
                onChangeCity={this.onChangeCity}
                emptyCityError={emptyCityError}
                onChangeAddressLine2={this.onChangeAddressLine2}
                country={country}
              />
            </div>
          </li>

          <li>
            <div className={classes.groupInfo}>
              <Typography variant="h6">Bank account</Typography>
              <FormControl className={classes.select}>
                <InputLabel>Currency</InputLabel>
                <Select
                  fullWidth={true}
                  native={true}
                  value={currency}
                  onChange={this.onCurrencyChanged}
                >
                  {Object.keys(StripeCurrency)
                    .map(
                      c => (StripeCurrency[c as any] as any) as StripeCurrency
                    )
                    .map(c => (
                      <option key={c} value={c}>
                        {currencyToLabel(c)}
                      </option>
                    ))}
                </Select>
              </FormControl>

              <FormControl fullWidth={true} className={classes.select}>
                <InputLabel focused={this.state.isIbanFocused} shrink={true}>
                  IBAN
                </InputLabel>
                <IbanElement
                  className={classes.iban}
                  supportedCountries={['SEPA']}
                  onBlur={this.handleBlurIban}
                  onFocus={this.handleFocusIban}
                  onChange={this.handleOnChangeIban}
                  style={{
                    base: {
                      fontSize: `${theme.typography.fontSize}px`,
                      fontFamily: theme.typography.fontFamily,
                      color: '#000000de'
                    }
                  }}
                />
                {this.state.emptyIbanError && (
                  <div className={classes.error}>IBAN is empty</div>
                )}
                {this.state.invalidIbanErrorMessage && (
                  <div className={classes.error}>
                    {this.state.invalidIbanErrorMessage}
                  </div>
                )}
              </FormControl>
            </div>
          </li>

          <li>
            <div className={classes.tosSection}>
              <Typography variant="caption">
                (*) Those information are securely transmitted to our payment
                processor <b>Stripe</b>. They are only used to transfer money to
                your bank account and are not stored by Globers.
              </Typography>
              <Checkbox
                onChange={this.onTosChanged}
                value="checkedB"
                color="primary"
              />
              By clicking, you agree to{' '}
              <a
                href="https://stripe.com/us/legal"
                target="_blank"
                rel="noopener noreferrer"
              >
                Stripe Connected Account Agreement
              </a>.
              {uncheckedTosError && (
                <div className={classes.error}>
                  Please accept stripe conditions to validate
                </div>
              )}
            </div>
          </li>
          <li>
            <Button
              className={classes.button}
              variant="contained"
              color="primary"
              onClick={this.onClickSubmit}
              disabled={waitForSubmitReply}
            >
              {waitForSubmitReply && <LoadingProgress />}
              {'Connect Bank Account'}
            </Button>
          </li>
        </ul>
      </BigFormLayout>
    )
  }

  private handleFocusIban = () => this.setState({ isIbanFocused: true })
  private handleBlurIban = () => this.setState({ isIbanFocused: false })

  private handleOnChangeIban = (
    response: ReactStripeElements.ElementChangeResponse
  ) => {
    const invalidIbanErrorMessage = response.error
      ? response.error.message
        ? response.error.message
        : response.error.type
      : null

    this.setState({
      isIbanEmpty: response.empty,
      isIbanComplete: response.complete,
      invalidIbanErrorMessage,
      emptyIbanError: this.state.emptyIbanError && !invalidIbanErrorMessage
    })
  }

  private onClickSubmit = () => {
    const isFirstNameEmpty = isEmpty(this.state.firstName)
    const isLastNameEmpty = isEmpty(this.state.lastName)
    const isAddressLine1Empty = isEmpty(this.state.addressLine1)
    const isAddressPostalCodeEmpty = isEmpty(this.state.addressPostalCode)
    const isAddressCityEmpty = isEmpty(this.state.addressCity)

    const isIbanEmpty = this.state.isIbanEmpty
    const isIbanInvalid = !this.state.isIbanComplete
    const uncheckedTos = !this.state.tosChecked

    if (
      isFirstNameEmpty ||
      isLastNameEmpty ||
      isAddressLine1Empty ||
      isAddressPostalCodeEmpty ||
      isAddressCityEmpty ||
      isIbanEmpty ||
      isIbanInvalid ||
      uncheckedTos
    ) {
      // NB invalid IBAN error is dealt with directly on input
      this.setState({
        emptyFirstNameError: isFirstNameEmpty,
        emptyLastNameError: isLastNameEmpty,
        emptyAddressLine1Error: isAddressLine1Empty,
        emptyPostalCodeError: isAddressPostalCodeEmpty,
        emptyCityError: isAddressCityEmpty,
        emptyIbanError: isIbanEmpty,
        uncheckedTosError: uncheckedTos
      })

      return
    }

    this.props.onSubmit({
      currency: this.state.currency,
      country: this.state.country,
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      addressCity: this.state.addressCity,
      addressLine1: this.state.addressLine1,
      addressLine2: this.state.addressLine2,
      addressPostalCode: this.state.addressPostalCode,
      dobDay: this.state.dobDay,
      dobMonth: this.state.dobMonth,
      dobYear: this.state.dobYear,
      type: this.state.type,
      stripeTosAccepted: this.state.tosChecked
    })
  }

  private onChangeFirstName = (e: any) =>
    this.setState({ firstName: e.target.value, emptyFirstNameError: false })
  private onChangeLastName = (e: any) =>
    this.setState({ lastName: e.target.value, emptyLastNameError: false })

  private onCurrencyChanged = (e: any) =>
    this.setState({ currency: e.target.value })

  private onCountryChanged = (country: StripeCountry) =>
    this.setState({ country })

  private onChangeAddressLine1 = (addressLine1: string) =>
    this.setState({
      addressLine1,
      emptyAddressLine1Error: false
    })
  private onChangeAddressLine2 = (addressLine2: string) =>
    this.setState({ addressLine2 })

  private onChangeCity = (addressCity: string) =>
    this.setState({ addressCity, emptyCityError: false })

  private onChangePostalCode = (addressPostalCode: string) =>
    this.setState({
      addressPostalCode,
      emptyPostalCodeError: false
    })

  private onChangeDobDay = (e: any) => this.setState({ dobDay: e.target.value })
  private onChangeDobMonth = (e: any) =>
    this.setState({ dobMonth: e.target.value })
  private onChangeDobYear = (e: any) =>
    this.setState({ dobYear: e.target.value })

  private onTosChanged = () => {
    this.setState({
      tosChecked: !this.state.tosChecked,
      uncheckedTosError: false
    })
  }
}

function isEmpty(str: string): boolean {
  return !str || !str.trim()
}

export default withStyles(styles)(withTheme()(CreateMediaStripeAccount))
