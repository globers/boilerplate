import * as React from 'react'
import gql from 'graphql-tag'
import { Mutation } from 'react-apollo'
import EditMediaBehavior, { IMediaInput } from './EditMediaBehavior'
import {
  getConnectBankAccountToMediaUrl,
  getMediaAdminUrl
} from '../../services/urlService'
import Router from 'next/router'
import { SaveMediaFailureReason, SaveMediaReply } from './SaveMediaReply'
import { Dialog } from '@material-ui/core'
import DialogTitle from '@material-ui/core/DialogTitle/DialogTitle'
import DialogActions from '@material-ui/core/DialogActions/DialogActions'
import Button from '@material-ui/core/Button/Button'
import DialogContent from '@material-ui/core/DialogContent/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText/DialogContentText'
import BigFormLayout from '../BigFormLayout'

const CreateMediaMut = gql`
  mutation CreateMedia($media: MediaInput!) {
    createMedia(media: $media) {
      ... on SaveMediaFailure {
        failureReason
      }
      ... on SaveMediaSuccess {
        contributedMedia {
          stripeAccountConnected
          uri
          media {
            uri
            name
            description
            logoUrl
            themePrimaryColor
          }
        }
      }
    }
  }
`

interface IMutationReply {
  createMedia: SaveMediaReply
}

interface IState {
  saveFailureReason: SaveMediaFailureReason | null
  waitForCreateMediaReply: boolean
  createdMediaUri: string | null
  proposeBankAccountConnect: boolean
}

export class CreateMedia extends React.Component<{}, IState> {
  constructor(props: {}) {
    super(props)
    this.state = {
      saveFailureReason: null,
      waitForCreateMediaReply: false,
      proposeBankAccountConnect: false,
      createdMediaUri: null
    }
  }

  public render() {
    return (
      <Mutation mutation={CreateMediaMut} onCompleted={this.onMutationResult}>
        {mutation => {
          const callMutation = (mediaInput: IMediaInput) => {
            this.setState({ waitForCreateMediaReply: true })
            mutation({
              variables: {
                media: mediaInput
              }
            })
          }

          return (
            <>
              <BigFormLayout title={'Create your media'}>
                <EditMediaBehavior
                  initialMedia={null}
                  saveFailureReason={this.state.saveFailureReason}
                  onSave={callMutation}
                  waitForSubmitReply={this.state.waitForCreateMediaReply}
                  stripeAccountConnected={false}
                />
              </BigFormLayout>
              <Dialog
                open={this.state.proposeBankAccountConnect}
                onClose={this.onProposeBankAccountConnectCancel}
                aria-labelledby="alert-dialog-created"
                aria-describedby="alert-dialog-description"
              >
                <DialogTitle id="alert-dialog-created">
                  Congratulation, you media is created!
                </DialogTitle>
                <DialogContent>
                  <DialogContentText id="alert-dialog-description">
                    Last step to be able to receive your money: connect your
                    bank account
                  </DialogContentText>
                </DialogContent>
                <DialogActions>
                  <Button
                    onClick={this.onProposeBankAccountConnectCancel}
                    color="default"
                    autoFocus={false}
                  >
                    Connect later
                  </Button>
                  <Button
                    variant={'contained'}
                    onClick={this.onProposeBankAccountConnectAccept}
                    color="primary"
                    autoFocus={true}
                  >
                    Connect Bank account
                  </Button>
                </DialogActions>
              </Dialog>
            </>
          )
        }}
      </Mutation>
    )
  }

  private onProposeBankAccountConnectCancel = () => {
    const mediaUrl = getMediaAdminUrl(this.state.createdMediaUri as string)
    Router.push(mediaUrl.href, mediaUrl.as)
  }

  private onProposeBankAccountConnectAccept = () => {
    const mediaBankUrl = getConnectBankAccountToMediaUrl(this.state
      .createdMediaUri as string)
    Router.push(mediaBankUrl.href, mediaBankUrl.as)
  }

  private onMutationResult = (result: IMutationReply) => {
    this.setState({ waitForCreateMediaReply: false })
    switch (result.createMedia.__typename) {
      case 'SaveMediaFailure':
        this.setState({ saveFailureReason: result.createMedia.failureReason })
        break
      case 'SaveMediaSuccess':
        this.setState({
          saveFailureReason: null,
          createdMediaUri: result.createMedia.contributedMedia.media.uri,
          proposeBankAccountConnect: true
        })
        break
    }
  }
}
