export enum SaveMediaFailureReason {
  NAME_ALREADY_EXISTS = 'NAME_ALREADY_EXISTS',
  EMPTY_NAME = 'EMPTY_NAME',
  EMPTY_DESCRIPTION = 'EMPTY_DESCRIPTION',
  EMPTY_LOGO_IMAGE = 'EMPTY_LOGO_IMAGE',
  INVALID_LOGO_IMAGE = 'INVALID_LOGO_IMAGE'
}

interface IMedia {
  uri: string
}

interface IContributedMedia {
  media: IMedia
  stripeAccountConnected: boolean
}

interface ISaveMediaSuccess {
  __typename: 'SaveMediaSuccess'

  contributedMedia: IContributedMedia
}

interface ISaveMediaFailure {
  __typename: 'SaveMediaFailure'
  failureReason: SaveMediaFailureReason
}

export type SaveMediaReply = ISaveMediaSuccess | ISaveMediaFailure
