import * as React from 'react'
import { Divider, IconButton, Paper, WithStyles } from '@material-ui/core'
import { StyleRules, withStyles } from '@material-ui/core/styles'
import CloseIcon from '@material-ui/icons/Close'
import { globersLogoUrl } from '../services/imageService'
import { ReactNode } from 'react'

const styles: StyleRules = {
  root: {
    maxWidth: 400,
    minWidth: 250,
    margin: '0 auto' // center horizontally
  },
  listFields: {
    listStyleType: 'none',
    padding: 20
  },
  header: {
    width: '100%',
    display: 'flex'
  },
  close: {
    float: 'right'
  },
  title: {
    display: 'block',
    margin: '0 auto' // center horizontally
  },
  logo: {
    width: 100
  }
}

interface IProps extends WithStyles<typeof styles> {
  onClose: () => void
  closable: boolean
  children: ReactNode
}

class FormLayout extends React.Component<IProps, {}> {
  public render() {
    const { classes } = this.props

    return (
      <div className={classes.root}>
        <Paper>
          <form noValidate={true}>
            <ul className={classes.listFields}>
              <li>
                <div className={classes.header}>
                  <div className={classes.title}>
                    <img
                      src={globersLogoUrl}
                      alt={'Globers'}
                      className={classes.logo}
                    />
                  </div>
                  {this.props.closable && (
                    <IconButton
                      onClick={this.props.onClose}
                      aria-label="Close"
                      className={classes.close}
                    >
                      <CloseIcon color="disabled" />
                    </IconButton>
                  )}
                </div>
                <Divider />
              </li>
              {this.props.children}
            </ul>
          </form>
        </Paper>
      </div>
    )
  }
}

export default withStyles(styles)(FormLayout)
