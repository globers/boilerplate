import React, { ReactNode } from 'react'

import gql from 'graphql-tag'
import { Mutation } from 'react-apollo'

const mut = gql`
  mutation SendResetPasswordEmail($email: String!) {
    sendResetPasswordEmail(email: $email)
  }
`

export enum SendResetPasswordEmailReply {
  SUCCESS = 'SUCCESS',
  UNKNOWN_USER = 'UNKNOWN_USER',
  EMAIL_DELIVERY_FAILURE = 'EMAIL_DELIVERY_FAILURE'
}

interface IProps {
  email: string
  children: (sendResetPasswordEmail: () => void) => ReactNode
  onReply: (reply: SendResetPasswordEmailReply) => void
}

class SendResetPasswordEmail extends React.Component<IProps> {
  public render() {
    const { email, children } = this.props

    return (
      <Mutation mutation={mut} onCompleted={this.onReply}>
        {mutation => {
          function callMutation() {
            mutation({
              variables: {
                email
              }
            })
          }

          return <>{children(callMutation)}</>
        }}
      </Mutation>
    )
  }

  private onReply = (reply: {
    sendResetPasswordEmail: SendResetPasswordEmailReply
  }) => this.props.onReply(reply.sendResetPasswordEmail)
}

export default SendResetPasswordEmail
