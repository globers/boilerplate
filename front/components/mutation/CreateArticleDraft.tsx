import React, { ReactNode } from 'react'

import gql from 'graphql-tag'
import { Mutation } from 'react-apollo'

const mut = gql`
  mutation CreateArticleDraft($mediaUri: ID!) {
    createArticleDraft(mediaUri: $mediaUri) {
      uri
    }
  }
`

interface IProps {
  mediaUri: string
  children: (createDraft: () => void) => ReactNode
  onDraftCreated: (mediaUri: string, articleDraftUri: string) => void
}

class CreateArticleDraft extends React.Component<IProps> {
  public render() {
    const { mediaUri, children } = this.props

    return (
      <Mutation mutation={mut} onCompleted={this.onDraftCreated}>
        {mutation => {
          function callMutation() {
            mutation({
              variables: {
                mediaUri
              }
            })
          }

          return <>{children(callMutation)}</>
        }}
      </Mutation>
    )
  }

  private onDraftCreated = (reply: { createArticleDraft: { uri: string } }) =>
    this.props.onDraftCreated(this.props.mediaUri, reply.createArticleDraft.uri)
}

export default CreateArticleDraft
