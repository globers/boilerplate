import React, { ReactNode } from 'react'

import gql from 'graphql-tag'
import { Mutation } from 'react-apollo'
import { GraphqlFile } from '../../graphql/schema/GraphqlFile'

const mut = gql`
  mutation UploadImageForDraft($draftUri: ID!, $imageFile: GraphqlFile!) {
    uploadImageForDraft(draftUri: $draftUri, imageFile: $imageFile) {
      ... on UploadImageForDraftSuccess {
        imageUrl
      }
      ... on UploadImageForDraftFailure {
        failureReason
      }
    }
  }
`

interface IMutationReply {
  uploadImageForDraft: UploadImageForDraftReply
}

type UploadImageForDraftReply =
  | IUploadImageForDraftSuccess
  | IUploadImageForDraftFailure

interface IUploadImageForDraftSuccess {
  __typename: 'UploadImageForDraftSuccess'
  imageUrl: string
}

interface IUploadImageForDraftFailure {
  __typename: 'UploadImageForDraftFailure'
  failureReason: UploadImageForDraftFailureReason
}

export enum UploadImageForDraftFailureReason {
  INVALID_IMAGE_FILE = 'INVALID_IMAGE_FILE',
  INVALID_DRAFT_URI = 'INVALID_DRAFT_URI'
}

interface IProps {
  draftUri: string
  children: (uploadFile: (imageFile: GraphqlFile) => void) => ReactNode
  onImageUploaded: (imageUrl: string) => void
  onFailure: (reason: UploadImageForDraftFailureReason) => void
}

class UploadImageForDraft extends React.Component<IProps> {
  public render() {
    const { draftUri, children } = this.props

    return (
      <Mutation mutation={mut} onCompleted={this.onCompleted}>
        {mutation => {
          function callMutation(imageFile: GraphqlFile) {
            mutation({
              variables: {
                draftUri,
                imageFile
              }
            })
          }

          return <>{children(callMutation)}</>
        }}
      </Mutation>
    )
  }

  private onCompleted = (reply: IMutationReply) => {
    switch (reply.uploadImageForDraft.__typename) {
      case 'UploadImageForDraftFailure':
        this.props.onFailure(reply.uploadImageForDraft.failureReason)
        break
      case 'UploadImageForDraftSuccess':
        this.props.onImageUploaded(reply.uploadImageForDraft.imageUrl)
        break
    }
  }
}

export default UploadImageForDraft
