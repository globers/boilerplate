import React, { ReactNode } from 'react'

import gql from 'graphql-tag'
import { Mutation } from 'react-apollo'
import { GraphqlFile } from '../../graphql/schema/GraphqlFile'

const mut = gql`
  mutation UpdateUser($user: UserInput!) {
    updateUser(user: $user) {
      ... on SaveUserSuccess {
        user {
          uri
          firstName
          lastName
          profilePictureUrl
        }
      }
      ... on SaveUserFailure {
        failureReason
      }
    }
  }
`

export interface IUserInput {
  firstName: string
  lastName: string
  profilePicture: GraphqlFile | null
}

interface IMutationReply {
  updateUser: SaveUserReply
}

type SaveUserReply = ISaveUserSuccess | ISaveUserFailure

export interface IUser {
  uri: string
  firstName: string
  lastName: string
  profilePictureUrl: string | null
}

interface ISaveUserSuccess {
  __typename: 'SaveUserSuccess'
  user: IUser
}

interface ISaveUserFailure {
  __typename: 'SaveUserFailure'
  failureReason: SaveUserFailureReason
}

export enum SaveUserFailureReason {
  EMAIL_ALREADY_EXISTS = 'EMAIL_ALREADY_EXISTS',
  INVALID_EMAIL = 'INVALID_EMAIL',
  PASSWORD_TOO_SHORT = 'PASSWORD_TOO_SHORT',
  EMPTY_FIRSTNAME = 'EMPTY_FIRSTNAME',
  EMPTY_LASTNAME = 'EMPTY_LASTNAME',
  INVALID_PROFILE_PICTURE = 'INVALID_PROFILE_PICTURE'
}

interface IProps {
  children: (updateUser: (user: IUserInput) => void) => ReactNode
  onUserUpdated: (user: IUser) => void
  onUserUpdateFailure: (reason: SaveUserFailureReason) => void
}

class UpdateUser extends React.Component<IProps> {
  public render() {
    const { children } = this.props

    return (
      <Mutation mutation={mut} onCompleted={this.onUserUpdated}>
        {mutation => {
          function callMutation(user: IUserInput) {
            mutation({
              variables: {
                user
              }
            })
          }

          return <>{children(callMutation)}</>
        }}
      </Mutation>
    )
  }

  private onUserUpdated = (reply: IMutationReply) => {
    switch (reply.updateUser.__typename) {
      case 'SaveUserFailure':
        this.props.onUserUpdateFailure(reply.updateUser.failureReason)
        break
      case 'SaveUserSuccess':
        this.props.onUserUpdated(reply.updateUser.user)
        break
    }
  }
}

export default UpdateUser
