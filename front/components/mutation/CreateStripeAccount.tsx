import React, { ReactNode } from 'react'

import gql from 'graphql-tag'
import { Mutation } from 'react-apollo'
import { StripeCountry } from '../../graphql/schema/StripeCountry'
import { StripeCurrency } from '../../graphql/schema/StripeCurrency'

const mut = gql`
  mutation CreateStripeAccount($mediaUri: ID!, $account: StripeAccountInput!) {
    createStripeAccount(mediaUri: $mediaUri, account: $account) {
      ... on SaveStripeAccountFailure {
        failureReason
        stripeErrorMessage
      }
      ... on SaveStripeAccountSuccess {
        ok
      }
    }
  }
`

interface ISaveStripeAccountSuccess {
  __typename: 'SaveStripeAccountSuccess'
  ok: boolean
}

export enum SaveStripeAccountReason {
  STRIPE_ERROR = 'STRIPE_ERROR'
}

export interface ISaveStripeAccountFailure {
  __typename: 'SaveStripeAccountFailure'
  failureReason: SaveStripeAccountReason
  stripeErrorMessage: string
}

interface IMutationReply {
  createStripeAccount: SaveStripeAccountReply
}

type SaveStripeAccountReply =
  | ISaveStripeAccountSuccess
  | ISaveStripeAccountFailure

export interface IStripeAccountInput {
  country: StripeCountry
  defaultCurrency: StripeCurrency
  accountTokenId: string
  externalAccountTokenId: string
}

interface IProps {
  mediaUri: string

  children: (
    createStripeAccount: (account: IStripeAccountInput) => void
  ) => ReactNode

  onAccountCreated: () => void
  onAccountCreationFailed: (failure: ISaveStripeAccountFailure) => void
}

class CreateStripeAccount extends React.Component<IProps> {
  public render() {
    const { mediaUri, children } = this.props

    return (
      <Mutation mutation={mut} onCompleted={this.onMutationCompleted}>
        {mutation => {
          function callMutation(account: IStripeAccountInput) {
            mutation({
              variables: {
                mediaUri,
                account
              }
            })
          }

          return <>{children(callMutation)}</>
        }}
      </Mutation>
    )
  }

  private onMutationCompleted = (reply: IMutationReply) => {
    switch (reply.createStripeAccount.__typename) {
      case 'SaveStripeAccountFailure':
        this.props.onAccountCreationFailed(reply.createStripeAccount)
        break
      case 'SaveStripeAccountSuccess':
        this.props.onAccountCreated()
        break
    }
  }
}

export default CreateStripeAccount
