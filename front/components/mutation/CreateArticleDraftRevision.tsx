import React, { ReactNode } from 'react'

import gql from 'graphql-tag'
import { Mutation } from 'react-apollo'
import { IGraphqlFile } from '../../graphql/schema/GraphqlFile'
import { IGraphqlDate } from '../../graphql/schema/GraphqlDate'
import { ImageFile, toGraphqlFile } from '../utils/imageDropZoneField'
import { htmlSerializer } from '../editor/HtmlSlateSerializer'
// @ts-ignore
import { Value } from 'slate'

const mut = gql`
  mutation CreateArticleDraftRevision(
    $draftUri: ID!
    $revision: ArticleDraftRevisionInput!
  ) {
    createArticleDraftRevision(draftUri: $draftUri, revision: $revision) {
      ... on SaveArticleDraftRevisionSuccess {
        revision {
          uri
          title
          subtitle
          imageUrl
          savingDate {
            nbSecFrom1970
          }
        }
      }
      ... on SaveArticleDraftRevisionFailure {
        failureReason
      }
    }
  }
`

export interface IArticleDraftRevisionInput {
  title: string | null
  subtitle: string | null
  content: string | null
  image: IGraphqlFile | null
  slatejsValue: string | null
}

interface ISaveArticleDraftRevisionSuccess {
  __typename: 'SaveArticleDraftRevisionSuccess'

  revision: IArticleDraftRevision
}

export interface IArticleDraftRevision {
  uri: string
  title: string | null
  subtitle: string | null
  imageUrl: string | null
  savingDate: IGraphqlDate
}

export enum SaveArticleDraftRevisionFailureReason {
  INVALID_ARTICLE_IMAGE = 'INVALID_ARTICLE_IMAGE'
}

interface ISaveArticleDraftRevisionFailure {
  __typename: 'SaveArticleDraftRevisionFailure'
  failureReason: SaveArticleDraftRevisionFailureReason
}

export type SaveArticleDraftRevisionReply =
  | ISaveArticleDraftRevisionSuccess
  | ISaveArticleDraftRevisionFailure

interface IMutationReply {
  createArticleDraftRevision: SaveArticleDraftRevisionReply
}

interface IProps {
  draftUri: string
  children: (
    createRevision: (input: IArticleDraftRevisionInput) => void
  ) => ReactNode
  onRevisionCreated: (revision: IArticleDraftRevision) => void
  onRevisionFailure: (reason: SaveArticleDraftRevisionFailureReason) => void
}

class CreateArticleDraftRevision extends React.Component<IProps> {
  public render() {
    const { draftUri, children } = this.props

    return (
      <Mutation mutation={mut} onCompleted={this.onRevisionCreated}>
        {mutation => {
          async function callMutation(input: IArticleDraftRevisionInput) {
            mutation({
              variables: {
                draftUri,
                revision: input
              }
            })
          }

          return <>{children(callMutation)}</>
        }}
      </Mutation>
    )
  }

  private onRevisionCreated = (reply: IMutationReply) => {
    switch (reply.createArticleDraftRevision.__typename) {
      case 'SaveArticleDraftRevisionFailure':
        this.props.onRevisionFailure(
          reply.createArticleDraftRevision.failureReason
        )
        break
      case 'SaveArticleDraftRevisionSuccess':
        this.props.onRevisionCreated(reply.createArticleDraftRevision.revision)
        break
    }
  }
}

export async function toRevisionInput(
  title: string | null,
  subtitle: string | null,
  articleImage: ImageFile | null,
  slatejsValue: Value | null
) {
  return {
    title,
    subtitle,
    slatejsValue: slatejsValue ? JSON.stringify(slatejsValue.toJSON()) : null,
    image: articleImage ? await toGraphqlFile(articleImage) : null,
    content: htmlSerializer.serialize(slatejsValue)
  }
}

export default CreateArticleDraftRevision
