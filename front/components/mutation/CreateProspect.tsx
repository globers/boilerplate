import React, { ReactNode } from 'react'

import gql from 'graphql-tag'
import { Mutation } from 'react-apollo'

const mut = gql`
  mutation CreateProspect($email: String!, $origin: String!) {
    createProspect(email: $email, origin: $origin)
  }
`

interface IProps {
  email: string
  origin: string
  children: (createProspect: () => void) => ReactNode
  onProspectCreated: () => void
}

class CreateProspect extends React.Component<IProps> {
  public render() {
    const { email, origin, children } = this.props

    return (
      <Mutation mutation={mut} onCompleted={this.onReply}>
        {mutation => {
          function callMutation() {
            mutation({
              variables: {
                email,
                origin
              }
            })
          }

          return <>{children(callMutation)}</>
        }}
      </Mutation>
    )
  }

  private onReply = () => {
    this.props.onProspectCreated()
  }
}

export default CreateProspect
