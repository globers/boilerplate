import React, { ReactNode } from 'react'

import gql from 'graphql-tag'
import { Mutation } from 'react-apollo'

const mut = gql`
  mutation SubscribeToMedia(
    $mediaSubscriptionPlanUri: ID!
    $subscriptionInput: StripeUserSubscriptionInput!
  ) {
    subscribeToMedia(
      mediaSubscriptionPlanUri: $mediaSubscriptionPlanUri
      subscriptionInput: $subscriptionInput
    ) {
      ... on SubscribeToMediaFailure {
        failureReason
        stripeErrorMessage
      }
      ... on SubscribeToMediaSuccess {
        ok
      }
    }
  }
`

interface ISubscribeToMediaSuccess {
  __typename: 'SubscribeToMediaSuccess'
  ok: boolean
}

export enum SubscribeToMediaFailureReason {
  ALREADY_SUBSCRIBED = 'ALREADY_SUBSCRIBED',
  STRIPE_ERROR = 'STRIPE_ERROR'
}

export interface ISubscribeToMediaFailure {
  __typename: 'SubscribeToMediaFailure'
  failureReason: SubscribeToMediaFailureReason
  stripeErrorMessage: string
}

interface IMutationReply {
  subscribeToMedia: SubscribeToMediaReply
}

type SubscribeToMediaReply = ISubscribeToMediaSuccess | ISubscribeToMediaFailure

export interface IStripeUserSubscriptionInput {
  userSourceTokenId: string
}

interface IProps {
  mediaSubscriptionPlanUri: string

  children: (
    subscribeToMedia: (subscriptionInput: IStripeUserSubscriptionInput) => void
  ) => ReactNode

  onSubscribed: () => void
  onSubscriptionFailed: (failure: ISubscribeToMediaFailure) => void
}

class SubscribeToMedia extends React.Component<IProps> {
  public render() {
    const { mediaSubscriptionPlanUri, children } = this.props

    return (
      <Mutation mutation={mut} onCompleted={this.onMutationCompleted}>
        {mutation => {
          function callMutation(
            subscriptionInput: IStripeUserSubscriptionInput
          ) {
            mutation({
              variables: {
                mediaSubscriptionPlanUri,
                subscriptionInput
              }
            })
          }

          return <>{children(callMutation)}</>
        }}
      </Mutation>
    )
  }

  private onMutationCompleted = (reply: IMutationReply) => {
    switch (reply.subscribeToMedia.__typename) {
      case 'SubscribeToMediaFailure':
        this.props.onSubscriptionFailed(reply.subscribeToMedia)
        break
      case 'SubscribeToMediaSuccess':
        this.props.onSubscribed()
        break
    }
  }
}

export default SubscribeToMedia
