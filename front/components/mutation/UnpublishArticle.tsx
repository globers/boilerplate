import React, { ReactNode } from 'react'

import gql from 'graphql-tag'
import { Mutation } from 'react-apollo'
import { query as mediaQuery } from '../../pages/media'
import { query as mediaAdminQuery } from '../../pages/mediaAdmin'

const mut = gql`
  mutation UnpublishArticle($articleUri: ID!) {
    unpublishArticle(articleUri: $articleUri)
  }
`

interface IMutationReply {
  unpublishArticle: boolean
}

interface IProps {
  children: (unpublishArticle: () => void) => ReactNode
  onArticleUnpublished: (unpublishSucceed: boolean) => void
  articleUri: string
  mediaUri: string
}

class UnpublishArticle extends React.Component<IProps> {
  public render() {
    const { children, articleUri } = this.props

    return (
      <Mutation
        mutation={mut}
        onCompleted={this.onMutationCompleted}
        refetchQueries={[
          { query: mediaQuery, variables: { mediaUri: this.props.mediaUri } },
          {
            query: mediaAdminQuery,
            variables: { mediaUri: this.props.mediaUri }
          }
        ]}
      >
        {mutation => {
          function callMutation() {
            mutation({
              variables: {
                articleUri
              }
            })
          }

          return <>{children(callMutation)}</>
        }}
      </Mutation>
    )
  }

  private onMutationCompleted = (reply: IMutationReply) => {
    this.props.onArticleUnpublished(reply.unpublishArticle)
  }
}

export default UnpublishArticle
