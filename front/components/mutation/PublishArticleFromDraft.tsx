import React, { ReactNode } from 'react'

import gql from 'graphql-tag'
import { Mutation } from 'react-apollo'
import { query as mediaQuery } from '../../pages/media'
import { query as mediaAdminQuery } from '../../pages/mediaAdmin'

const mut = gql`
  mutation PublishArticleFromDraft($articleDraftRevisionUri: ID!) {
    publishArticleFromDraft(articleDraftRevisionUri: $articleDraftRevisionUri) {
      ... on PublishArticleSuccess {
        article {
          uri
        }
      }
      ... on PublishArticleFailure {
        failureReason
      }
    }
  }
`

interface IArticle {
  uri: string
}

interface IPublishArticleSuccess {
  __typename: 'PublishArticleSuccess'

  article: IArticle
}

export enum PublishArticleFailureReason {
  EMPTY_TITLE = 'EMPTY_TITLE',
  EMPTY_SUBTITLE = 'EMPTY_SUBTITLE',
  EMPTY_CONTENT = 'EMPTY_CONTENT',
  EMPTY_IMAGE = 'EMPTY_IMAGE'
}

interface IPublishArticleFailure {
  __typename: 'PublishArticleFailure'
  failureReason: PublishArticleFailureReason
}

type PublishArticleReply = IPublishArticleSuccess | IPublishArticleFailure

interface IMutationReply {
  publishArticleFromDraft: PublishArticleReply
}

interface IProps {
  children: (
    publishArticle: (articleDraftRevisionUri: string) => void
  ) => ReactNode
  onArticlePublished: (articleUri: string) => void
  onPublishArticleFailure: (reason: PublishArticleFailureReason) => void
  mediaUri: string
}

class PublishArticleFromDraft extends React.Component<IProps> {
  public render() {
    const { children } = this.props

    return (
      <Mutation
        mutation={mut}
        onCompleted={this.onMutationCompleted}
        refetchQueries={[
          { query: mediaQuery, variables: { mediaUri: this.props.mediaUri } },
          {
            query: mediaAdminQuery,
            variables: { mediaUri: this.props.mediaUri }
          }
        ]}
      >
        {mutation => {
          function callMutation(articleDraftRevisionUri: string) {
            mutation({
              variables: {
                articleDraftRevisionUri
              }
            })
          }

          return <>{children(callMutation)}</>
        }}
      </Mutation>
    )
  }

  private onMutationCompleted = (reply: IMutationReply) => {
    switch (reply.publishArticleFromDraft.__typename) {
      case 'PublishArticleFailure':
        this.props.onPublishArticleFailure(
          reply.publishArticleFromDraft.failureReason
        )
        break
      case 'PublishArticleSuccess':
        this.props.onArticlePublished(reply.publishArticleFromDraft.article.uri)
        break
    }
  }
}

export default PublishArticleFromDraft
