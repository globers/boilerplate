import React, { ReactNode } from 'react'

import gql from 'graphql-tag'
import { Mutation } from 'react-apollo'
import {
  SaveArticleDraftRevisionFailureReason,
  SaveArticleDraftRevisionReply,
  IArticleDraftRevision,
  IArticleDraftRevisionInput
} from './CreateArticleDraftRevision'
// @ts-ignore
import { Value } from 'slate'

const mut = gql`
  mutation UpdateArticleDraftRevision(
    $draftRevisionUri: ID!
    $revision: ArticleDraftRevisionInput!
  ) {
    updateArticleDraftRevision(
      draftRevisionUri: $draftRevisionUri
      revision: $revision
    ) {
      ... on SaveArticleDraftRevisionSuccess {
        revision {
          uri
          title
          subtitle
          imageUrl
          savingDate {
            nbSecFrom1970
          }
        }
      }
      ... on SaveArticleDraftRevisionFailure {
        failureReason
      }
    }
  }
`

interface IMutationReply {
  updateArticleDraftRevision: SaveArticleDraftRevisionReply
}

interface IProps {
  draftRevisionUri: string
  children: (
    updateRevision: (input: IArticleDraftRevisionInput) => void
  ) => ReactNode
  onRevisionUpdated: (revision: IArticleDraftRevision) => void
  onRevisionFailure: (reason: SaveArticleDraftRevisionFailureReason) => void
}

class UpdateArticleDraftRevision extends React.Component<IProps> {
  public render() {
    const { draftRevisionUri, children } = this.props

    return (
      <Mutation mutation={mut} onCompleted={this.onRevisionUpdated}>
        {mutation => {
          function callMutation(input: IArticleDraftRevisionInput) {
            mutation({
              variables: {
                draftRevisionUri,
                revision: input
              }
            })
          }

          return <>{children(callMutation)}</>
        }}
      </Mutation>
    )
  }

  private onRevisionUpdated = (reply: IMutationReply) => {
    switch (reply.updateArticleDraftRevision.__typename) {
      case 'SaveArticleDraftRevisionFailure':
        this.props.onRevisionFailure(
          reply.updateArticleDraftRevision.failureReason
        )
        break
      case 'SaveArticleDraftRevisionSuccess':
        this.props.onRevisionUpdated(reply.updateArticleDraftRevision.revision)
        break
    }
  }
}

export default UpdateArticleDraftRevision
