import React, { ReactNode } from 'react'

import gql from 'graphql-tag'
import { Mutation } from 'react-apollo'

const mut = gql`
  mutation ResetPassword(
    $plainTextNewPassword: String!
    $resetPasswordUri: String!
  ) {
    resetPassword(
      plainTextNewPassword: $plainTextNewPassword
      resetPasswordUri: $resetPasswordUri
    ) {
      ... on ResetPasswordSuccess {
        jwtToken
      }
      ... on ResetPasswordFailure {
        failureReason
      }
    }
  }
`

interface IResetPasswordSuccess {
  __typename: 'ResetPasswordSuccess'
  jwtToken: string
}

interface IResetPasswordFailure {
  __typename: 'ResetPasswordFailure'
  failureReason: ResetPasswordFailureReason
}

export enum ResetPasswordFailureReason {
  PASSWORD_TOO_SHORT = 'PASSWORD_TOO_SHORT',
  RESET_PASSWORD_URI_EXPIRED = 'RESET_PASSWORD_URI_EXPIRED',
  INVALID_RESET_PASSWORD_URI = 'INVALID_RESET_PASSWORD_URI'
}

export type ResetPasswordReply = IResetPasswordSuccess | IResetPasswordFailure

interface IProps {
  plainTextNewPassword: string
  resetPasswordUri: string
  children: (resetPasswordReply: () => void) => ReactNode
  onReply: (reply: ResetPasswordReply) => void
}

class ResetPassword extends React.Component<IProps> {
  public render() {
    const { plainTextNewPassword, resetPasswordUri, children } = this.props

    return (
      <Mutation mutation={mut} onCompleted={this.onReply}>
        {mutation => {
          function callMutation() {
            mutation({
              variables: {
                plainTextNewPassword,
                resetPasswordUri
              }
            })
          }

          return <>{children(callMutation)}</>
        }}
      </Mutation>
    )
  }

  private onReply = (reply: { resetPassword: ResetPasswordReply }) =>
    this.props.onReply(reply.resetPassword)
}

export default ResetPassword
