import React, { ReactNodeArray } from 'react'
import { withStyles, Theme } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import IconButton from '@material-ui/core/IconButton'
import ArrowBack from '@material-ui/icons/ArrowBack'
import { Button, WithStyles } from '@material-ui/core'
import LoginMenu from './loginMenu'
import {
  globersLogoUrl,
  globersIconUrl,
  getFormatedLogoUrl
} from '../services/imageService'
import Router from 'next/router'
import Link from 'next/link'
import { getMediaUrl, IUrl } from '../services/urlService'
import { isDark } from '../services/styleServices'

const styles = (theme: Theme) => ({
  root: {
    position: 'relative' as 'relative',
    maxHeight: '64px',
    zIndex: theme.zIndex.drawer + 1
  },
  logoDiv: {
    display: 'block',
    margin: '0 auto' // center horizontally
  },
  globersLogo: {
    padding: 0,
    maxHeight: 42,
    margin: '0 auto',
    display: 'block'
  },
  mediaLogo: {
    padding: 0,
    maxHeight: 46,
    maxWidth: 140,
    borderRadius: 5,
    margin: '0 0',
    display: 'block'
  },
  childrenButtons: {
    alignItems: 'center' as 'center',
    display: 'flex',
    marginLeft: 'auto', // to move items to the right, toolbar is a flex container
    '@media(max-width: 767px)': {
      display: 'none'
    }
  },
  loginMenu: {
    '@media(max-width: 767px)': {
      // login menu fixed to the left of childrenButtons, but if there is none => we force it
      marginLeft: 'auto' // to move items to the right, toolbar is a flex container
    }
  }
})

interface IMedia {
  uri: string
  logoUrl: string
  name: string
  themePrimaryColor: string
}

interface IProps extends WithStyles<typeof styles> {
  isHome: boolean
  media: IMedia | null
  userFirstName: string | null
  childrenIfNotMobile: ReactNodeArray | null
}

class AppBarGlobers extends React.Component<IProps, {}> {
  constructor(props: IProps) {
    super(props)
  }

  public render() {
    const { classes, isHome, media, userFirstName } = this.props

    let toolbarStyle = {}
    let mediaUrl = null

    let globersIconUrlContrast = globersIconUrl

    const isDarkBackground =
      media && media.themePrimaryColor && isDark(media.themePrimaryColor)
    const contrastColor = isDarkBackground ? 'white' : 'black'

    if (media) {
      toolbarStyle = {
        backgroundColor: media.themePrimaryColor
      }
      mediaUrl = getMediaUrl(media.uri)
      globersIconUrlContrast += '_' + contrastColor
    }

    return (
      <div className={classes.root}>
        <AppBar color="inherit" elevation={0}>
          <Toolbar style={toolbarStyle}>
            {isHome ? null : (
              <>
                <IconButton color="inherit" onClick={this.onBackClick}>
                  <ArrowBack style={{ color: contrastColor }} />
                </IconButton>
              </>
            )}

            {media ? (
              <>
                <Link href={'/'}>
                  <a>
                    <Button>
                      <img
                        src={globersIconUrlContrast}
                        className={classes.globersLogo}
                      />
                    </Button>
                  </a>
                </Link>
                <Link as={(mediaUrl as IUrl).as} href={(mediaUrl as IUrl).href}>
                  <a>
                    <Button>
                      <img
                        src={getFormatedLogoUrl(media.logoUrl)}
                        className={classes.mediaLogo}
                        alt={media.name}
                        id={`logo-media-${media.uri}`}
                      />
                    </Button>
                  </a>
                </Link>
              </>
            ) : (
              <Link href={'/'}>
                <a>
                  <Button>
                    <img src={globersLogoUrl} className={classes.mediaLogo} />
                  </Button>
                </a>
              </Link>
            )}

            <div className={classes.childrenButtons}>{this.props.children}</div>
            <div className={classes.loginMenu}>
              <LoginMenu
                userFirstName={userFirstName}
                isDarkBackground={isDarkBackground}
              />
            </div>
          </Toolbar>
        </AppBar>
      </div>
    )
  }

  private onBackClick = () => {
    Router.back()
  }
}

export default withStyles(styles)(AppBarGlobers)
