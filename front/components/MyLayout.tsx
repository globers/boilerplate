import React, { ReactNode, ReactNodeArray } from 'react'
import { withStyles } from '@material-ui/core/styles'
import { WithStyles } from '@material-ui/core'
import Head from 'next/head'

const styles = {
  paddingForHeader: {
    paddingTop: '64px'
  }
}

interface IProps extends WithStyles<typeof styles> {
  children: ReactNodeArray
  appBar: ReactNode
  pageTitle: string
}

const Layout = (props: IProps) => {
  const { classes, appBar, children, pageTitle } = props
  return (
    <>
      <Head>
        <title>{pageTitle}</title>
      </Head>
      {appBar}
      <div className={classes.paddingForHeader}>{children}</div>
    </>
  )
}

export default withStyles(styles)(Layout)
