import * as React from 'react'
import {
  Button,
  createStyles,
  IconButton,
  InputAdornment,
  TextField,
  Theme,
  WithStyles
} from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import FormLayout from '../FormLayout'
import Typography from '@material-ui/core/Typography/Typography'
import ResetPassword, {
  ResetPasswordFailureReason,
  ResetPasswordReply
} from '../mutation/ResetPassword'
import { Visibility, VisibilityOff } from '@material-ui/icons'
import Link from 'next/link'
import { logInUserAndGoHome } from '../../services/userService'

const styles = (theme: Theme) =>
  createStyles({
    button: {
      marginTop: 20,
      marginBottom: 20
    },
    textField: {
      marginLeft: theme.spacing.unit,
      marginRight: theme.spacing.unit
    },
    title: {
      textAlign: 'center',
      paddingLeft: '20px',
      paddingRight: '20px'
    },
    okReply: {
      padding: '20px',
      color: 'green'
    },
    koReply: {
      padding: '20px',
      color: 'red'
    }
  })

interface IProps extends WithStyles<typeof styles> {
  resetPasswordUri: string
}

interface IState {
  reply: ResetPasswordReply | null
  password: string
  showPassword: boolean
}

class ResetPasswordForm extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    this.state = {
      password: '',
      reply: null,
      showPassword: false
    }
  }

  public render() {
    const { classes } = this.props
    const { password, reply } = this.state

    let passwordTooShortError = false
    let invalidUri = false
    let uriExpired = false
    if (reply !== null && reply.__typename === 'ResetPasswordFailure') {
      const reason = reply.failureReason
      passwordTooShortError =
        reason === ResetPasswordFailureReason.PASSWORD_TOO_SHORT
      invalidUri =
        reason === ResetPasswordFailureReason.INVALID_RESET_PASSWORD_URI
      uriExpired =
        reason === ResetPasswordFailureReason.RESET_PASSWORD_URI_EXPIRED
    }

    return (
      <FormLayout onClose={this.noOp} closable={false}>
        <li>
          <Typography variant="h6" className={classes.title}>
            Enter your new password.
          </Typography>
        </li>
        <li>
          <TextField
            id="password-input"
            label="Password"
            className={classes.textField}
            type={this.state.showPassword ? 'text' : 'password'}
            autoComplete="new-password"
            margin="normal"
            value={password}
            fullWidth={true}
            onChange={this.onChangePassword}
            error={passwordTooShortError}
            helperText={
              passwordTooShortError
                ? 'Password must be at least 6 characters'
                : null
            }
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    onClick={this.OnClickShowPassword}
                    aria-label="Toggle password visibility"
                  >
                    {this.state.showPassword ? (
                      <Visibility />
                    ) : (
                      <VisibilityOff />
                    )}
                  </IconButton>
                </InputAdornment>
              )
            }}
          />
        </li>
        <li>
          <ResetPassword
            plainTextNewPassword={this.state.password}
            resetPasswordUri={this.props.resetPasswordUri}
            onReply={this.onReply}
          >
            {(resetPasswordReply: () => void) => (
              <Button
                variant="contained"
                color="primary"
                className={classes.button}
                fullWidth={true}
                size="large"
                onClick={resetPasswordReply}
              >
                Reset password
              </Button>
            )}
          </ResetPassword>
        </li>
        {invalidUri && (
          <li>
            <Typography variant="subtitle1" className={classes.koReply}>
              Error: reset password failed. Please try again. If the problem
              persists, please contact us at <b>support@globers.co</b>.
              <Link href={'/sendResetPasswordEmail'}>
                <a>Send a new reset password email</a>
              </Link>
            </Typography>
          </li>
        )}
        {uriExpired && (
          <li>
            <Typography variant="subtitle1" className={classes.koReply}>
              Error: reset password link expired.
              <Link href={'/sendResetPasswordEmail'}>
                <a>Send a new reset password email</a>
              </Link>
            </Typography>
          </li>
        )}
      </FormLayout>
    )
  }

  // tslint:disable-next-line
  private noOp = () => {}

  private onChangePassword = (e: any) =>
    this.setState({ password: e.target.value })

  private onReply = (reply: ResetPasswordReply) => {
    this.setState({ reply })
    if (reply.__typename === 'ResetPasswordSuccess') {
      logInUserAndGoHome(reply.jwtToken)
    }
  }

  private OnClickShowPassword = () => {
    this.setState({ showPassword: !this.state.showPassword })
  }
}

export default withStyles(styles)(ResetPasswordForm)
