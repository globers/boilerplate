import * as React from 'react'
import {
  Button,
  createStyles,
  IconButton,
  InputAdornment,
  TextField,
  Theme,
  WithStyles
} from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import { Visibility, VisibilityOff } from '@material-ui/icons'
import FormLayout from '../FormLayout'
import Link from 'next/link'

const styles = (theme: Theme) =>
  createStyles({
    button: {
      marginTop: 20
    },
    textField: {
      marginLeft: theme.spacing.unit,
      marginRight: theme.spacing.unit
    }
  })

interface IProps extends WithStyles<typeof styles> {
  onSubmit: (email: string, password: string) => void
  onClickCreateAccount: () => void
  onClose: () => void
  invalidPasswordError: boolean
  unknownUserError: boolean
  closable: boolean
}

interface IState {
  email: string
  password: string
  showPassword: boolean
}

class AccountForm extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    this.state = {
      email: '',
      password: '',
      showPassword: false
    }
  }

  public render() {
    const {
      classes,
      onSubmit,
      unknownUserError,
      invalidPasswordError
    } = this.props
    const { email, password } = this.state
    const onClickSubmit = () => onSubmit(email, password)

    return (
      <FormLayout onClose={this.props.onClose} closable={this.props.closable}>
        <li>
          <TextField
            className={classes.textField}
            id="email-input"
            label="Email"
            margin="normal"
            autoComplete="email"
            onChange={this.onChangeEmail}
            fullWidth={true}
            error={unknownUserError}
            helperText={unknownUserError ? 'Account not found' : null}
          />
        </li>
        <li>
          <TextField
            id="password-input"
            label="Password"
            className={classes.textField}
            type={this.state.showPassword ? 'text' : 'password'}
            autoComplete="current-password"
            margin="normal"
            fullWidth={true}
            onChange={this.onChangePassword}
            error={invalidPasswordError}
            helperText={invalidPasswordError ? 'Invalid password' : null}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    onClick={this.OnClickShowPassword}
                    aria-label="Toggle password visibility"
                  >
                    {this.state.showPassword ? (
                      <Visibility />
                    ) : (
                      <VisibilityOff />
                    )}
                  </IconButton>
                </InputAdornment>
              )
            }}
          />
          <Link href={'/sendResetPasswordEmail'}>
            <a>Forgot password?</a>
          </Link>
        </li>
        <li>
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            fullWidth={true}
            size="large"
            onClick={onClickSubmit}
          >
            Log in
          </Button>
          <Button
            variant="outlined"
            color="default"
            className={classes.button}
            fullWidth={true}
            onClick={this.props.onClickCreateAccount}
          >
            Create account
          </Button>
        </li>
      </FormLayout>
    )
  }

  private onChangeEmail = (e: any) => this.setState({ email: e.target.value })

  private onChangePassword = (e: any) =>
    this.setState({ password: e.target.value })

  private OnClickShowPassword = () => {
    this.setState({ showPassword: !this.state.showPassword })
  }
}

export default withStyles(styles)(AccountForm)
