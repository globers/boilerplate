import React from 'react'
import { injectStripe, ReactStripeElements } from 'react-stripe-elements'

import { Dialog } from '@material-ui/core'
import DialogTitle from '@material-ui/core/DialogTitle/DialogTitle'
import DialogActions from '@material-ui/core/DialogActions/DialogActions'
import Button from '@material-ui/core/Button/Button'
import { getMediaUrl } from '../../services/urlService'
import Router from 'next/router'
import DialogContentText from '@material-ui/core/DialogContentText/DialogContentText'
import DialogContent from '@material-ui/core/DialogContent/DialogContent'
import SubscribeToMedia, {
  IStripeUserSubscriptionInput,
  ISubscribeToMediaFailure,
  SubscribeToMediaFailureReason
} from '../mutation/SubscribeToMedia'
import SubscribeToMediaForm from './SubscribeToMediaForm'
import { StripeCountry } from '../../graphql/schema/StripeCountry'
import { StripeCurrency } from '../../graphql/schema/StripeCurrency'

enum StateForm {
  errorMessage,
  alreadySubscribed,
  subscriptionValidated,
  waitForSubmitReply,
  input
}

interface IState {
  stateForm: StateForm
  errorMessage: string | null // not null if errorMessage == errorMessage
}

interface ISubscriptionPlan {
  uri: string
  price: number
  currency: StripeCurrency
  country: StripeCountry
}

interface IProps {
  mediaUri: string
  mediaName: string
  subscriptionPlan: ISubscriptionPlan
}

// NB: parent should include StripeProvider / Elements components
// https://github.com/stripe/react-stripe-elements#setting-up-your-payment-form-injectstripe
class SubscribeToMediaBehavior extends React.Component<
  IProps & ReactStripeElements.InjectedStripeProps,
  IState
> {
  constructor(props: IProps) {
    super(props)
    this.state = {
      errorMessage: null,
      stateForm: StateForm.input
    }
  }

  public render() {
    const { subscriptionPlan, mediaName } = this.props

    return (
      <>
        <SubscribeToMedia
          mediaSubscriptionPlanUri={subscriptionPlan.uri}
          onSubscribed={this.onSubscribed}
          onSubscriptionFailed={this.onSubscriptionFailed}
        >
          {(
            subscribeToMedia: (
              subscriptionInput: IStripeUserSubscriptionInput
            ) => void
          ) => (
            <SubscribeToMediaForm
              onSubmit={this.onSubmit(subscribeToMedia)}
              waitForSubmitReply={
                this.state.stateForm === StateForm.waitForSubmitReply
              }
              mediaName={mediaName}
              price={subscriptionPlan.price}
              currency={subscriptionPlan.currency}
            />
          )}
        </SubscribeToMedia>
        {this.state.stateForm === StateForm.errorMessage && <div>ERROR</div>}

        <Dialog
          open={this.state.stateForm === StateForm.errorMessage}
          onClose={this.onAccountErrorMessageClose}
          aria-labelledby="alert-dialog-error"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-error">
            Bank account connection failed
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              {this.state.errorMessage}
              <br />
              <br />
              <b>
                If you need help to solve this problem, please contact
                support@globers.co
              </b>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button
              onClick={this.onAccountErrorMessageClose}
              color="primary"
              autoFocus={true}
            >
              Ok
            </Button>
          </DialogActions>
        </Dialog>

        <Dialog
          open={this.state.stateForm === StateForm.subscriptionValidated}
          onClose={this.onAccountCreatedMessageClose}
          aria-labelledby="alert-dialog-created"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-created">
            Bank account connected!
          </DialogTitle>
          <DialogActions>
            <Button
              onClick={this.onAccountCreatedMessageClose}
              color="primary"
              autoFocus={true}
            >
              Ok
            </Button>
          </DialogActions>
        </Dialog>

        <Dialog
          open={this.state.stateForm === StateForm.alreadySubscribed}
          onClose={this.onAccountCreatedMessageClose}
          aria-labelledby="alert-dialog-created"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-created">
            You are already subscribed
          </DialogTitle>
          <DialogActions>
            <Button
              onClick={this.onAccountCreatedMessageClose}
              color="primary"
              autoFocus={true}
            >
              Ok
            </Button>
          </DialogActions>
        </Dialog>
      </>
    )
  }

  private onAccountErrorMessageClose = () => {
    this.setState({
      stateForm: StateForm.input,
      errorMessage: null
    })
  }

  private onAccountCreatedMessageClose = () => {
    const mediaUrl = getMediaUrl(this.props.mediaUri)
    Router.push(mediaUrl.href, mediaUrl.as)
  }

  private onSubscribed = () => {
    this.setState({
      stateForm: StateForm.subscriptionValidated,
      errorMessage: null
    })
  }

  private onSubscriptionFailed = (failure: ISubscribeToMediaFailure) => {
    switch (failure.failureReason) {
      case SubscribeToMediaFailureReason.STRIPE_ERROR:
        const error = failure.stripeErrorMessage as string
        this.setState({
          stateForm: StateForm.errorMessage,
          errorMessage: error
        })
        break
      case SubscribeToMediaFailureReason.ALREADY_SUBSCRIBED:
        this.setState({
          stateForm: StateForm.alreadySubscribed,
          errorMessage: null
        })
    }
  }

  private onSubmit = (
    subscribeToMedia: (subscriptionInput: IStripeUserSubscriptionInput) => void
  ) => async () => {
    this.setState({
      stateForm: StateForm.waitForSubmitReply,
      errorMessage: null
    })

    let externalAccountToken
    // On react stripe lib, stripe will look for needed elements itself (here IbanElement)
    // so no need to specify it in createToken call

    try {
      externalAccountToken = await this.props.stripe!.createToken({
        // https://stripe.com/docs/stripe-js/reference#stripe-create-token
        // @ts-ignore (seems to be a bug in typescript types of stripe)
        type: 'card'
      })

      if (externalAccountToken.error) {
        this.setErrorFromStripeToken(externalAccountToken.error)
        return
      }
    } catch (error) {
      this.setErrorFromStripeToken(error)
      return
    }

    const stripeAccountInput: IStripeUserSubscriptionInput = {
      userSourceTokenId: externalAccountToken.token.id
    }

    this.setState({
      stateForm: StateForm.waitForSubmitReply,
      errorMessage: null
    })
    subscribeToMedia(stripeAccountInput)
  }

  private setErrorFromStripeToken = (error: any) => {
    this.setState({
      stateForm: StateForm.errorMessage,
      errorMessage: error.message ? error.message : error.type
    })
  }
}

export default injectStripe(SubscribeToMediaBehavior)
