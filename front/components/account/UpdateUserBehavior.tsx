import React from 'react'

import UpdateUser, {
  IUserInput,
  SaveUserFailureReason
} from '../mutation/UpdateUser'
import UpdateUserForm from './UpdateUserForm'
import { Dialog } from '@material-ui/core'
import DialogTitle from '@material-ui/core/DialogTitle/DialogTitle'
import DialogActions from '@material-ui/core/DialogActions/DialogActions'
import Button from '@material-ui/core/Button/Button'
import { ImageFile, toGraphqlFile } from '../utils/imageDropZoneField'

enum StateForm {
  userSaved,
  waitingForSave,
  input
}

interface IState {
  firstName: string
  lastName: string
  profilePicture: ImageFile | null
  stateForm: StateForm
  updateUserFailureReason: SaveUserFailureReason | null
}

interface IProps {
  initialFirstName: string
  initialLastName: string
  initialProfilePictureUrl: string | null
  onAccountSaved: () => void
}

class UpdateUserBehavior extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    this.state = {
      firstName: props.initialFirstName,
      lastName: props.initialLastName,
      stateForm: StateForm.input,
      updateUserFailureReason: null,
      profilePicture: props.initialProfilePictureUrl
        ? { type: 'IFileAsUrl', fileUrl: props.initialProfilePictureUrl }
        : null
    }
  }

  public render() {
    const {
      firstName,
      lastName,
      stateForm,
      profilePicture,
      updateUserFailureReason
    } = this.state

    return (
      <>
        <UpdateUser
          onUserUpdated={this.onUserUpdated}
          onUserUpdateFailure={this.onUserUpdateFailure}
        >
          {(updateUser: (user: IUserInput) => void) => (
            <UpdateUserForm
              lastName={lastName}
              firstName={firstName}
              profilePicture={profilePicture}
              emptyFirstNameError={
                updateUserFailureReason ===
                SaveUserFailureReason.EMPTY_FIRSTNAME
              }
              emptyLastNameError={
                updateUserFailureReason === SaveUserFailureReason.EMPTY_LASTNAME
              }
              invalidProfilePictureError={
                updateUserFailureReason ===
                SaveUserFailureReason.INVALID_PROFILE_PICTURE
              }
              onClickSubmit={this.onSubmit(updateUser)}
              waitForSubmitReply={stateForm === StateForm.waitingForSave}
              onLastNameChange={this.onLastNameChange}
              onFirstNameChange={this.onFirstNameChange}
              onProfilePictureChange={this.onProfilePictureChange}
            />
          )}
        </UpdateUser>
        <Dialog
          open={this.state.stateForm === StateForm.userSaved}
          onClose={this.onAccountSavedMessageClose}
        >
          <DialogTitle id="alert-dialog-created">
            User profile saved
          </DialogTitle>
          <DialogActions>
            <Button
              onClick={this.onAccountSavedMessageClose}
              color="primary"
              autoFocus={true}
            >
              Ok
            </Button>
          </DialogActions>
        </Dialog>
      </>
    )
  }

  private onAccountSavedMessageClose = () => {
    this.setState({
      updateUserFailureReason: null,
      stateForm: StateForm.input
    })
    this.props.onAccountSaved()
  }

  private onFirstNameChange = (firstName: string) => {
    this.setState({
      firstName,
      updateUserFailureReason: null
    })
  }

  private onLastNameChange = (lastName: string) => {
    this.setState({
      lastName,
      updateUserFailureReason: null
    })
  }

  private onProfilePictureChange = (image: ImageFile | null) => {
    this.setState({
      profilePicture: image
    })
  }

  private onUserUpdated = () => {
    this.setState({
      stateForm: StateForm.userSaved,
      updateUserFailureReason: null
    })
  }

  private onUserUpdateFailure = (
    updateUserFailureReason: SaveUserFailureReason
  ) => {
    this.setState({
      updateUserFailureReason,
      stateForm: StateForm.input
    })
  }

  private onSubmit = (updateUser: (user: IUserInput) => void) => async () => {
    this.setState({
      stateForm: StateForm.waitingForSave,
      updateUserFailureReason: null
    })

    let pictureFile = null
    if (this.state.profilePicture) {
      pictureFile = await toGraphqlFile(this.state.profilePicture)
    }

    updateUser({
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      profilePicture: pictureFile
    })
  }
}

export default UpdateUserBehavior
