import * as React from 'react'
import {
  Button,
  createStyles,
  TextField,
  Theme,
  WithStyles
} from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import LoadingProgress from '../utils/loadingProgress'
import FormLayout from '../FormLayout'
import ImageDropZoneField, { ImageFile } from '../utils/imageDropZoneField'

const styles = (theme: Theme) =>
  createStyles({
    button: {
      display: 'block',
      margin: '20px auto 0px auto'
    },
    textField: {
      marginLeft: theme.spacing.unit,
      marginRight: theme.spacing.unit
    },
    errorMessage: {
      color: 'red'
    },
    pictureDropZoneOutside: {
      margin: '20px auto',
      width: '200px',
      height: '200px',
      maxWidth: '200px',
      maxHeight: '200px',
      display: 'block'
    }
  })

interface IProps extends WithStyles<typeof styles> {
  onClickSubmit: () => void

  firstName: string
  lastName: string
  profilePicture: ImageFile | null

  onFirstNameChange: (firstName: string) => void
  onLastNameChange: (lastName: string) => void
  onProfilePictureChange: (image: ImageFile | null) => void

  waitForSubmitReply: boolean
  emptyFirstNameError: boolean
  emptyLastNameError: boolean
  invalidProfilePictureError: boolean
}

class UpdateUserForm extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props)
  }

  public render() {
    const {
      classes,
      onClickSubmit,
      emptyFirstNameError,
      emptyLastNameError,
      firstName,
      lastName
    } = this.props

    return (
      <FormLayout onClose={this.noOp} closable={false}>
        <li>
          <TextField
            className={classes.textField}
            id="firstName-input"
            label="First name"
            margin="normal"
            autoComplete="given-name"
            onChange={this.onChangeFirstName}
            value={firstName}
            fullWidth={true}
            error={emptyFirstNameError}
            helperText={emptyFirstNameError ? 'First name is empty' : null}
          />
        </li>
        <li>
          <TextField
            className={classes.textField}
            id="lastName-input"
            label="Last name"
            autoComplete="family-name"
            margin="normal"
            onChange={this.onChangeLastName}
            value={lastName}
            fullWidth={true}
            error={emptyLastNameError}
            helperText={emptyLastNameError ? 'Last name is empty' : null}
          />
        </li>
        <li>
          <div className={classes.pictureDropZoneOutside}>
            <ImageDropZoneField
              onImageFileDropped={this.props.onProfilePictureChange}
              imageFile={this.props.profilePicture}
              messageInDropZone="Profile picture"
              displayLoading={false}
              coverAndTakeCenter={true}
              circle={true}
            />
          </div>
          {this.props.invalidProfilePictureError && (
            <div className={classes.errorMessage}>Cannot read image file</div>
          )}
        </li>
        <li>
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            fullWidth={false}
            size="large"
            onClick={onClickSubmit}
            disabled={this.props.waitForSubmitReply}
          >
            {this.props.waitForSubmitReply && <LoadingProgress />}
            Save account
          </Button>
        </li>
      </FormLayout>
    )
  }

  // tslint:disable-next-line
  private noOp = () => {}

  private onChangeFirstName = (e: any) => {
    this.props.onFirstNameChange(e.target.value)
  }

  private onChangeLastName = (e: any) => {
    this.props.onLastNameChange(e.target.value)
  }
}

export default withStyles(styles)(UpdateUserForm)
