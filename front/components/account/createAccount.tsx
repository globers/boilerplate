import * as React from 'react'
import gql from 'graphql-tag'
import { Mutation } from 'react-apollo'
import AccountForm from './AccountForm'
import { logInUser } from '../../services/userService'

const CreateUserMut = gql`
  mutation CreateUser(
    $email: String!
    $firstName: String!
    $lastName: String!
    $password: String!
  ) {
    createUser(
      user: { firstName: $firstName, lastName: $lastName }
      login: { email: $email, plainTextPassword: $password }
    ) {
      ... on SaveUserSuccess {
        user {
          email
        }
        jwtToken
      }
      ... on SaveUserFailure {
        failureReason
      }
    }
  }
`

interface ICreateAccountProps {
  onAccountCreated: () => void
  onClickLogIn: () => void
  onClose: () => void
  closable: boolean
}

interface IUser {
  email: string
}

interface ISaveUserSuccess {
  __typename: 'SaveUserSuccess'

  jwtToken: string
  user: IUser
}

enum SaveUserFailureReason {
  EMAIL_ALREADY_EXISTS = 'EMAIL_ALREADY_EXISTS',
  INVALID_EMAIL = 'INVALID_EMAIL',
  PASSWORD_TOO_SHORT = 'PASSWORD_TOO_SHORT',
  EMPTY_FIRSTNAME = 'EMPTY_FIRSTNAME',
  EMPTY_LASTNAME = 'EMPTY_LASTNAME'
}

interface ISaveUserFailure {
  __typename: 'SaveUserFailure'
  failureReason: SaveUserFailureReason
}

type SaveUserReply = ISaveUserSuccess | ISaveUserFailure

interface IMutationReply {
  createUser: SaveUserReply
}

interface IState {
  createUserFailure: SaveUserFailureReason | null
  waitForCreateUserReply: boolean
}

class CreateAccount extends React.Component<ICreateAccountProps, IState> {
  constructor(props: ICreateAccountProps) {
    super(props)
    this.state = {
      createUserFailure: null,
      waitForCreateUserReply: false
    }
  }

  public render() {
    const props = this.props

    return (
      <Mutation mutation={CreateUserMut} onCompleted={this.onMutationResult}>
        {mutation => {
          const callMutation = (
            email: string,
            firstName: string,
            lastName: string,
            password: string
          ) => {
            this.setState({ waitForCreateUserReply: true })
            mutation({ variables: { email, firstName, lastName, password } })
          }

          return (
            <AccountForm
              waitForSubmitReply={this.state.waitForCreateUserReply}
              closable={this.props.closable}
              onSubmit={callMutation}
              onClickLogIn={props.onClickLogIn}
              onClose={props.onClose}
              emailAlreadyExistsError={
                this.state.createUserFailure ===
                SaveUserFailureReason.EMAIL_ALREADY_EXISTS
              }
              emptyFirstNameError={
                this.state.createUserFailure ===
                SaveUserFailureReason.EMPTY_FIRSTNAME
              }
              emptyLastNameError={
                this.state.createUserFailure ===
                SaveUserFailureReason.EMPTY_LASTNAME
              }
              invalidEmailError={
                this.state.createUserFailure ===
                SaveUserFailureReason.INVALID_EMAIL
              }
              passwordTooShortError={
                this.state.createUserFailure ===
                SaveUserFailureReason.PASSWORD_TOO_SHORT
              }
            />
          )
        }}
      </Mutation>
    )
  }

  private onMutationResult = (result: IMutationReply) => {
    this.setState({ waitForCreateUserReply: false })
    switch (result.createUser.__typename) {
      case 'SaveUserFailure':
        this.setState({ createUserFailure: result.createUser.failureReason })
        break
      case 'SaveUserSuccess':
        this.setState({ createUserFailure: null })
        logInUser(result.createUser.jwtToken)
        this.props.onAccountCreated()
        break
    }
  }
}

export default CreateAccount
