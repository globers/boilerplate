import * as React from 'react'
import { Dialog } from '@material-ui/core'
import CreateAccount from './createAccount'
import LoginAccount from './loginAccount'
import DialogTitle from '@material-ui/core/DialogTitle/DialogTitle'
import DialogActions from '@material-ui/core/DialogActions/DialogActions'
import Button from '@material-ui/core/Button/Button'
import DialogContentText from '@material-ui/core/DialogContentText/DialogContentText'
import DialogContent from '@material-ui/core/DialogContent/DialogContent'

interface IState {
  modalStatus: ModalStatus
}

interface IProps {
  initialModalStatus: ModalStatus
  closable: boolean
}

export enum ModalStatus {
  Closed,
  CreateAccount,
  Login,
  NeedLogin
}

class LoginOrCreateAccountModals extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    this.state = { modalStatus: this.props.initialModalStatus }
  }

  public render() {
    return (
      <React.Fragment>
        <Dialog
          open={this.state.modalStatus === ModalStatus.NeedLogin}
          onClose={this.closeModal}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">Join Globers !</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              Create an account to bookmark articles and follow your favorite
              media. No spam, no ads.
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.closeModal} color="primary">
              No
            </Button>
            <Button
              onClick={this.openCreateAccountModal}
              color="primary"
              autoFocus={true}
              variant="contained"
            >
              Create account
            </Button>
          </DialogActions>
        </Dialog>

        <Dialog
          open={this.state.modalStatus === ModalStatus.CreateAccount}
          onClose={this.closeModal}
          fullScreen={true}
        >
          <CreateAccount
            onAccountCreated={this.closeModal}
            onClickLogIn={this.openLoginModal}
            onClose={this.closeModal}
            closable={this.props.closable}
          />
        </Dialog>
        <Dialog
          open={this.state.modalStatus === ModalStatus.Login}
          onClose={this.closeModal}
          fullScreen={true}
        >
          <LoginAccount
            onAccountLoggedIn={this.closeModal}
            onClickCreateAccount={this.openCreateAccountModal}
            onClose={this.closeModal}
            closable={this.props.closable}
          />
        </Dialog>
      </React.Fragment>
    )
  }

  public openLoginModal = () => {
    this.setState({ modalStatus: ModalStatus.Login })
  }

  public openNeedLogin = () => {
    this.setState({ modalStatus: ModalStatus.NeedLogin })
  }

  public openCreateAccountModal = () => {
    this.setState({ modalStatus: ModalStatus.CreateAccount })
  }

  private closeModal = () => {
    this.setState({ modalStatus: ModalStatus.Closed })
  }
}

export default LoginOrCreateAccountModals
