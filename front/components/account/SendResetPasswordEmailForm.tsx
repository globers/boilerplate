import * as React from 'react'
import {
  Button,
  createStyles,
  TextField,
  Theme,
  WithStyles
} from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import FormLayout from '../FormLayout'
import SendResetPasswordEmail, {
  SendResetPasswordEmailReply
} from '../mutation/SendResetPasswordEmail'
import Typography from '@material-ui/core/Typography/Typography'

const styles = (theme: Theme) =>
  createStyles({
    button: {
      marginTop: 20
    },
    textField: {
      marginLeft: theme.spacing.unit,
      marginRight: theme.spacing.unit
    },
    title: {
      textAlign: 'center',
      paddingLeft: '20px',
      paddingRight: '20px'
    },
    okReply: {
      padding: '20px',
      color: 'green'
    },
    koReply: {
      padding: '20px',
      color: 'red'
    }
  })

interface IProps extends WithStyles<typeof styles> {}

interface IState {
  email: string
  reply: SendResetPasswordEmailReply | null
}

class SendResetPasswordEmailForm extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    this.state = {
      email: '',
      reply: null
    }
  }

  public render() {
    const { classes } = this.props
    const { email, reply } = this.state

    return (
      <FormLayout onClose={this.noOp} closable={false}>
        <li>
          <Typography variant="h6" className={classes.title}>
            Enter your email address and we will send you a link to reset your
            password.
          </Typography>
        </li>
        <li>
          <TextField
            className={classes.textField}
            id="email-input"
            label="Email"
            margin="normal"
            autoComplete="email"
            value={email}
            onChange={this.onChangeEmail}
            fullWidth={true}
            error={reply === SendResetPasswordEmailReply.UNKNOWN_USER}
            helperText={
              reply === SendResetPasswordEmailReply.UNKNOWN_USER
                ? 'Account not found. Please verify your email'
                : null
            }
          />
        </li>
        <li>
          <SendResetPasswordEmail
            email={this.state.email}
            onReply={this.onReply}
          >
            {(sendResetPasswordEmail: () => void) => (
              <Button
                variant="contained"
                color="primary"
                className={classes.button}
                fullWidth={true}
                size="large"
                onClick={sendResetPasswordEmail}
              >
                Send reset password email
              </Button>
            )}
          </SendResetPasswordEmail>
        </li>
        {reply === SendResetPasswordEmailReply.EMAIL_DELIVERY_FAILURE && (
          <li>
            <Typography variant="subtitle1" className={classes.koReply}>
              Error: the email could not be sent. Please retry. If the problem
              persists, please contact us at <b>support@globers.co</b>.
            </Typography>
          </li>
        )}
        {reply === SendResetPasswordEmailReply.SUCCESS && (
          <li>
            <Typography variant="subtitle1" className={classes.okReply}>
              An email with a link to reset your password has been sent to{' '}
              <b>{this.state.email}</b>. It might take a few minutes.
            </Typography>
          </li>
        )}
      </FormLayout>
    )
  }

  // tslint:disable-next-line
  private noOp = () => {}

  private onChangeEmail = (e: any) => this.setState({ email: e.target.value })

  private onReply = (reply: SendResetPasswordEmailReply) => {
    this.setState({ reply })
  }
}

export default withStyles(styles)(SendResetPasswordEmailForm)
