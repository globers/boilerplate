import * as React from 'react'
import gql from 'graphql-tag'
import { Mutation } from 'react-apollo'
import { logInUser } from '../../services/userService'
import LoginForm from './LoginForm'

const LoginUserMut = gql`
  mutation loginUser($email: String!, $password: String!) {
    loginUser(login: { email: $email, plainTextPassword: $password }) {
      ... on LoginSuccess {
        jwtToken
      }
      ... on LoginFailure {
        failureReason
      }
    }
  }
`

interface IProps {
  onAccountLoggedIn: () => void
  onClickCreateAccount: () => void
  onClose: () => void
  closable: boolean
}

interface ILoginSuccess {
  __typename: 'LoginSuccess'
  jwtToken: string
}

enum LoginFailureReason {
  UNKNOWN_USER = 'UNKNOWN_USER',
  INVALID_PASSWORD = 'INVALID_PASSWORD'
}

interface ILoginFailure {
  __typename: 'LoginFailure'
  failureReason: LoginFailureReason
}

type LoginReply = ILoginSuccess | ILoginFailure

interface IMutationReply {
  loginUser: LoginReply
}

interface IState {
  loginFailure: LoginFailureReason | null
}

class LoginAccount extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    this.state = {
      loginFailure: null
    }
  }

  public render() {
    return (
      <Mutation mutation={LoginUserMut} onCompleted={this.onMutationResult}>
        {mutation => {
          function callMutation(email: string, password: string) {
            mutation({ variables: { email, password } })
          }

          return (
            <LoginForm
              closable={this.props.closable}
              onSubmit={callMutation}
              onClickCreateAccount={this.props.onClickCreateAccount}
              onClose={this.props.onClose}
              unknownUserError={
                this.state.loginFailure === LoginFailureReason.UNKNOWN_USER
              }
              invalidPasswordError={
                this.state.loginFailure === LoginFailureReason.INVALID_PASSWORD
              }
            />
          )
        }}
      </Mutation>
    )
  }

  private onMutationResult = (result: IMutationReply) => {
    if (result.loginUser.__typename === 'LoginFailure') {
      this.setState({ loginFailure: result.loginUser.failureReason })
    } else {
      this.setState({ loginFailure: null })
      logInUser(result.loginUser.jwtToken)
      this.props.onAccountLoggedIn()
    }
  }
}

export default LoginAccount
