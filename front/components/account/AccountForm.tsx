import * as React from 'react'
import {
  Button,
  createStyles,
  IconButton,
  InputAdornment,
  TextField,
  Theme,
  WithStyles
} from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import { Visibility, VisibilityOff } from '@material-ui/icons'
import FormLayout from '../FormLayout'
import LoadingProgress from '../utils/loadingProgress'
import Typography from '@material-ui/core/Typography/Typography'

const styles = (theme: Theme) =>
  createStyles({
    button: {
      marginTop: 20
    },
    textField: {
      marginLeft: theme.spacing.unit,
      marginRight: theme.spacing.unit
    },
    tosSection: {
      paddingLeft: 16,
      paddingRight: 16,
      margin: 16
    }
  })

interface IProps extends WithStyles<typeof styles> {
  onSubmit: (
    email: string,
    firstName: string,
    lastName: string,
    password: string
  ) => void

  onClickLogIn: () => void
  onClose: () => void
  closable: boolean
  waitForSubmitReply: boolean

  emailAlreadyExistsError: boolean
  invalidEmailError: boolean
  passwordTooShortError: boolean
  emptyFirstNameError: boolean
  emptyLastNameError: boolean
}

interface IState {
  email: string
  firstName: string
  lastName: string
  password: string
  showPassword: boolean
}

class AccountForm extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    this.state = {
      email: '',
      firstName: '',
      lastName: '',
      password: '',
      showPassword: false
    }
  }

  public render() {
    const {
      classes,
      onSubmit,
      emailAlreadyExistsError,
      emptyFirstNameError,
      emptyLastNameError,
      invalidEmailError,
      passwordTooShortError
    } = this.props
    const { email, firstName, lastName, password } = this.state

    const onClickSubmit = () => onSubmit(email, firstName, lastName, password)

    return (
      <FormLayout onClose={this.props.onClose} closable={this.props.closable}>
        <li>
          <TextField
            className={classes.textField}
            id="firstName-input"
            label="First name"
            margin="normal"
            autoComplete="given-name"
            onChange={this.onChangeFirstName}
            fullWidth={true}
            error={emptyFirstNameError}
            helperText={emptyFirstNameError ? 'First name is empty' : null}
          />
        </li>
        <li>
          <TextField
            className={classes.textField}
            id="lastName-input"
            label="Last name"
            autoComplete="family-name"
            margin="normal"
            onChange={this.onChangeLastName}
            fullWidth={true}
            error={emptyLastNameError}
            helperText={emptyLastNameError ? 'Last name is empty' : null}
          />
        </li>
        <li>
          <TextField
            className={classes.textField}
            id="email-input"
            label="Email"
            margin="normal"
            autoComplete="email"
            onChange={this.onChangeEmail}
            fullWidth={true}
            error={emailAlreadyExistsError || invalidEmailError}
            helperText={
              emailAlreadyExistsError
                ? 'Email already exists'
                : invalidEmailError
                  ? 'Invalid email'
                  : 'No spam. Promise!'
            }
          />
        </li>
        <li>
          <TextField
            id="password-input"
            label="Password"
            className={classes.textField}
            type={this.state.showPassword ? 'text' : 'password'}
            autoComplete="new-password"
            margin="normal"
            fullWidth={true}
            onChange={this.onChangePassword}
            error={passwordTooShortError}
            helperText={
              passwordTooShortError
                ? 'Password must be at least 6 characters'
                : null
            }
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    onClick={this.OnClickShowPassword}
                    aria-label="Toggle password visibility"
                  >
                    {this.state.showPassword ? (
                      <Visibility />
                    ) : (
                      <VisibilityOff />
                    )}
                  </IconButton>
                </InputAdornment>
              )
            }}
          />
        </li>
        <li>
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            fullWidth={true}
            size="large"
            onClick={onClickSubmit}
            disabled={this.props.waitForSubmitReply}
          >
            {this.props.waitForSubmitReply && <LoadingProgress />}
            Create account
          </Button>
          <Button
            variant="outlined"
            color="default"
            className={classes.button}
            fullWidth={true}
            onClick={this.props.onClickLogIn}
            disabled={this.props.waitForSubmitReply}
          >
            Log in
          </Button>
        </li>
        <li>
          <div className={classes.tosSection}>
            <Typography variant="caption">
              By clicking "Create account", you accept our{' '}
              <a
                href="/static/termsOfService.html"
                target="_blank"
                rel="noopener noreferrer"
              >
                terms of service
              </a>.
            </Typography>
          </div>
        </li>
      </FormLayout>
    )
  }

  private onChangeEmail = (e: any) => this.setState({ email: e.target.value })
  private onChangeFirstName = (e: any) =>
    this.setState({ firstName: e.target.value })
  private onChangeLastName = (e: any) =>
    this.setState({ lastName: e.target.value })

  private onChangePassword = (e: any) =>
    this.setState({ password: e.target.value })

  private OnClickShowPassword = () => {
    this.setState({ showPassword: !this.state.showPassword })
  }
}

export default withStyles(styles)(AccountForm)
