import React from 'react'
import {
  createStyles,
  withStyles,
  WithStyles,
  WithTheme
} from '@material-ui/core'
import BigFormLayout from '../BigFormLayout'
import FormControl from '@material-ui/core/FormControl/FormControl'
import InputLabel from '@material-ui/core/InputLabel/InputLabel'
import { CardElement, ReactStripeElements } from 'react-stripe-elements'
import Button from '@material-ui/core/Button/Button'
import LoadingProgress from '../utils/loadingProgress'
import Typography from '@material-ui/core/Typography/Typography'
import withTheme from '@material-ui/core/styles/withTheme'
import { StripeCurrency } from '../../graphql/schema/StripeCurrency'

const styles = () =>
  createStyles({
    button: {
      display: 'block',
      margin: '20px auto'
    },
    groupInfo: {
      borderStyle: 'solid',
      borderWidth: 1,
      padding: 16,
      margin: 16,
      borderRadius: 16,
      borderColor: '#BFBCBC'
    },
    tosSection: {
      paddingLeft: 16,
      paddingRight: 16,
      margin: 16
    },
    card: {
      marginTop: '16px', // to get perfect alignment between input and select
      marginBottom: '8px',
      width: '100%',
      padding: '6px 0 7px',
      cursor: 'text'
    },
    error: {
      color: 'red'
    },
    select: {
      marginTop: '16px', // to get perfect alignment between input and select
      marginBottom: '8px'
    },
    listFields: {
      margin: '0 auto', // center horizontally
      listStyleType: 'none',
      padding: 0,
      maxWidth: 600,
      '@media (max-width: 420px)': {
        width: 300
      }
    }
  })

interface IProps extends WithStyles<typeof styles>, WithTheme {
  onSubmit: () => void
  waitForSubmitReply: boolean

  mediaName: string
  price: number
  currency: StripeCurrency
}

interface IState {
  emptyCardError: boolean
  invalidCardErrorMessage: string | null

  isCardFocused: boolean
  isCardEmpty: boolean
  isCardComplete: boolean
}

function toLabel(currency: StripeCurrency) {
  switch (currency) {
    case StripeCurrency.EUR:
      return '€'
  }
}

class SubscribeToMediaForm extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    this.state = {
      emptyCardError: false,
      invalidCardErrorMessage: null,

      isCardFocused: false,
      isCardEmpty: true,
      isCardComplete: false
    }
  }

  public render() {
    const {
      classes,
      theme,
      waitForSubmitReply,
      mediaName,
      currency,
      price
    } = this.props

    return (
      <BigFormLayout
        title={`Abonnez-vous à  ${mediaName} pour ${price}${toLabel(
          currency
        )}/mois`}
      >
        <ul className={classes.listFields}>
          <li>
            <div className={classes.groupInfo}>
              <FormControl fullWidth={true} className={classes.select}>
                <InputLabel focused={this.state.isCardFocused} shrink={true}>
                  Carte de crédit
                </InputLabel>
                <CardElement
                  className={classes.card}
                  onBlur={this.handleBlurCard}
                  onFocus={this.handleFocusCard}
                  onChange={this.handleOnChangeIban}
                  style={{
                    base: {
                      fontSize: `${theme.typography.fontSize}px`,
                      fontFamily: theme.typography.fontFamily,
                      color: '#000000de'
                    }
                  }}
                />
                {this.state.emptyCardError && (
                  <div className={classes.error}>IBAN is empty</div>
                )}
                {this.state.invalidCardErrorMessage && (
                  <div className={classes.error}>
                    {this.state.invalidCardErrorMessage}
                  </div>
                )}
              </FormControl>
            </div>
          </li>

          <li>
            <div className={classes.tosSection}>
              <Typography variant="caption">
                En cliquant sur “s'abonner”, vous acceptez nos{' '}
                <a
                  href="/static/termsOfService.html"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  conditions générales d'utilisation
                </a>. Vous serez débité d'un montant mensuel de{' '}
                {`${price}${toLabel(currency)}`}, sauf si vous décidez
                d'annuler.
              </Typography>
            </div>
          </li>
          <li>
            <Button
              className={classes.button}
              variant="contained"
              color="primary"
              onClick={this.onClickSubmit}
              disabled={waitForSubmitReply}
            >
              {waitForSubmitReply && <LoadingProgress />}
              {"S'abonner"}
            </Button>
          </li>
        </ul>
      </BigFormLayout>
    )
  }

  private handleFocusCard = () => this.setState({ isCardFocused: true })
  private handleBlurCard = () => this.setState({ isCardFocused: false })

  private handleOnChangeIban = (
    response: ReactStripeElements.ElementChangeResponse
  ) => {
    const invalidCardErrorMessage = response.error
      ? response.error.message
        ? response.error.message
        : response.error.type
      : null

    this.setState({
      isCardEmpty: response.empty,
      isCardComplete: response.complete,
      invalidCardErrorMessage,
      emptyCardError: this.state.emptyCardError && !invalidCardErrorMessage
    })
  }

  private onClickSubmit = () => {
    const isCardEmpty = this.state.isCardEmpty
    const isCardInvalid = !this.state.isCardComplete

    if (isCardEmpty || isCardInvalid) {
      // NB invalid Card error is dealt with directly on input
      this.setState({
        emptyCardError: isCardEmpty
      })

      return
    }

    this.props.onSubmit()
  }
}

export default withStyles(styles)(withTheme()(SubscribeToMediaForm))
