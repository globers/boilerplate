import React from 'react'
import AppBarGlobers from '../components/AppBar'
import Layout from '../components/MyLayout'
import NextError from 'next/error'

interface IProps {
  statusCode: number
}

export default class ErrorPage extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props)
  }

  public render() {
    return (
      <Layout
        appBar={
          <AppBarGlobers isHome={false} userFirstName={null} media={null} />
        }
      >
        <NextError statusCode={this.props.statusCode} />
      </Layout>
    )
  }
}
