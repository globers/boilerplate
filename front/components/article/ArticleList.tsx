import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles'
import { List, ListItem, Paper, WithStyles } from '@material-ui/core'
import ArticleSnippet from './ArticleSnippet'
import { IGraphqlDate } from '../../graphql/schema/GraphqlDate'

const styles = {
  articlesListPaper: {
    maxWidth: 700,
    margin: '24px auto' // center horizontally
  },
  articleList: {
    padding: 0
  }
}

interface IUserMedia {
  media: {
    uri: string
  }
  userArticles: Array<{
    article: {
      uri: string
      title: string
      subtitle: string
      imageUrl: string
      firstPublication: IGraphqlDate
    }
    bookmarked: boolean
  }>
}

interface IProps extends WithStyles<typeof styles> {
  userMedia: IUserMedia
  isLoggedIn: boolean
}

class ArticleList extends Component<IProps> {
  public render() {
    const { classes, userMedia, isLoggedIn } = this.props
    return (
      <Paper className={classes.articlesListPaper} elevation={0}>
        <List className={classes.articleList}>
          {userMedia.userArticles.map(
            (a: {
              article: { uri: string; title: string; imageUrl: string }
              bookmarked: boolean
            }) => (
              <ListItem divider={true} button={false} key={a.article.uri}>
                <ArticleSnippet
                  isLoggedIn={isLoggedIn}
                  media={userMedia.media}
                  userArticle={a}
                />
              </ListItem>
            )
          )}
        </List>
      </Paper>
    )
  }
}

export default withStyles(styles)(ArticleList)
