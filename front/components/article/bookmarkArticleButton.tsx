import React, { RefObject } from 'react'
import gql from 'graphql-tag'
import { Mutation } from 'react-apollo'
import LoginOrCreateAccountModals, {
  ModalStatus
} from '../account/loginOrCreateAccountModals'
import { Bookmark, BookmarkBorder } from '@material-ui/icons'
import IconButton from '@material-ui/core/IconButton/IconButton'

const bookmarkArticleMut = gql`
  mutation bookmarkArticle($articleUri: ID!, $bookmark: Boolean!) {
    bookmarkArticle(articleUri: $articleUri, bookmark: $bookmark) {
      uri
      bookmarked
    }
  }
`

interface IProps {
  articleUri: string
  isArticleBookmarked: boolean
  isLoggedIn: boolean
}

class BookmarkArticleButton extends React.Component<IProps, {}> {
  private readonly loginRef: RefObject<LoginOrCreateAccountModals>

  constructor(props: IProps) {
    super(props)
    this.loginRef = React.createRef()
  }

  public render() {
    const { articleUri, isLoggedIn, isArticleBookmarked } = this.props

    return (
      <React.Fragment>
        <Mutation mutation={bookmarkArticleMut}>
          {mutation => {
            const callMutation = () => {
              if (isLoggedIn) {
                mutation({
                  variables: {
                    articleUri,
                    bookmark: !isArticleBookmarked
                  }
                })
              } else {
                if (this.loginRef.current) {
                  this.loginRef.current.openNeedLogin()
                }
              }
            }

            return (
              <IconButton onClick={callMutation}>
                {isArticleBookmarked ? (
                  <Bookmark color="primary" fontSize={'small'} />
                ) : (
                  <BookmarkBorder color="disabled" fontSize={'small'} />
                )}
              </IconButton>
            )
          }}
        </Mutation>
        <LoginOrCreateAccountModals
          closable={true}
          ref={this.loginRef}
          initialModalStatus={ModalStatus.Closed}
        />
      </React.Fragment>
    )
  }
}

export default BookmarkArticleButton
