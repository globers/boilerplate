import React from 'react'
import { withStyles, StyleRules, WithStyles } from '@material-ui/core/styles'
import ArticleSnippetLayout from './ArticleSnippetLayout'
import {
  getArticleUrl,
  getEditArticleUrl,
  IUrl
} from '../../services/urlService'
import { noImageUrl } from '../../services/imageService'
import Link from 'next/link'
import Typography from '@material-ui/core/Typography/Typography'
import { IGraphqlDate } from '../../graphql/schema/GraphqlDate'
import { formatGraphqlDate } from '../../services/dateServices'

const styles: StyleRules = {
  buttonsWrapper: {
    paddingTop: '32px'
  }
}

export interface IContributedArticle {
  uri: string
  draft: IArticleDraft
  publishedArticle: IPublishedArticle | null
}

interface IArticleDraft {
  uri: string
  revisions: IArticleDraftRevision[]
}

interface IPublishedArticle {
  uri: string
  article: IArticle
  revision: { uri: string }
}

interface IArticle {
  uri: string
  firstPublication: IGraphqlDate
  lastPublication: IGraphqlDate
}

interface IProps extends WithStyles<typeof styles> {
  mediaUri: string
  contributedArticle: IContributedArticle
}

interface IArticleDraftRevision {
  uri: string
  title: string | null
  subtitle: string | null
  imageUrl: string | null
  savingDate: IGraphqlDate
}

class DraftSnippet extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props)
  }

  public render() {
    const { mediaUri, contributedArticle, classes } = this.props

    const editArticleUrl = getEditArticleUrl(
      mediaUri,
      contributedArticle.draft.uri
    )

    let publishedArticleUrl = null
    let modifiedSincePublished = false
    let firstPublication: IGraphqlDate | null = null
    let lastPublication: IGraphqlDate | null = null
    if (contributedArticle.publishedArticle) {
      publishedArticleUrl = getArticleUrl(
        mediaUri,
        contributedArticle.publishedArticle.article.uri
      )
      modifiedSincePublished =
        contributedArticle.publishedArticle.revision.uri !==
        contributedArticle.draft.revisions[0].uri
      firstPublication =
        contributedArticle.publishedArticle.article.firstPublication
      lastPublication =
        contributedArticle.publishedArticle.article.lastPublication
    }

    const revision = contributedArticle.draft.revisions[0]

    let dateStr
    if (contributedArticle.publishedArticle) {
      if (
        (firstPublication as IGraphqlDate).nbSecFrom1970 ===
        (lastPublication as IGraphqlDate).nbSecFrom1970
      ) {
        dateStr = `Last saved: ${formatGraphqlDate(
          revision.savingDate
        )}, published: ${formatGraphqlDate(firstPublication as IGraphqlDate)}`
      } else {
        dateStr = `Last saved: ${formatGraphqlDate(
          revision.savingDate
        )}, first published: ${formatGraphqlDate(
          firstPublication as IGraphqlDate
        )}, last revision: ${formatGraphqlDate(
          lastPublication as IGraphqlDate
        )}`
      }
    } else {
      dateStr = `Last saved: ${formatGraphqlDate(revision.savingDate)}`
    }

    return (
      <ArticleSnippetLayout
        imageUrl={revision.imageUrl ? revision.imageUrl : noImageUrl}
        articleUrl={editArticleUrl}
        linkReactId={`link-draft-${contributedArticle.draft.uri}`}
        title={revision.title ? revision.title : '[No title]'}
        subtitle={revision.subtitle ? revision.subtitle : '[No subtitle]'}
      >
        <Typography
          variant={'caption'}
          color="textSecondary"
          className={classes.publicationDate}
        >
          {dateStr}
        </Typography>
        <div className={classes.buttonsWrapper}>
          {publishedArticleUrl && (
            <Link
              href={(publishedArticleUrl as IUrl).href}
              as={(publishedArticleUrl as IUrl).as}
            >
              <a>
                <Typography variant="caption">
                  Go to published article
                </Typography>
              </a>
            </Link>
          )}
          {modifiedSincePublished && (
            <Typography variant="caption" color="textSecondary">
              (Article modified since published)
            </Typography>
          )}
        </div>
      </ArticleSnippetLayout>
    )
  }
}

export default withStyles(styles)(DraftSnippet)
