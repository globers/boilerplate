import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles'
import ListItem from '@material-ui/core/ListItem'
import { List, Paper, WithStyles } from '@material-ui/core'
import DraftSnippet, { IContributedArticle } from './DraftSnippet'

const styles = {
  articlesListPaper: {
    width: '100%'
  },
  articleList: {
    padding: 0
  }
}

interface IProps extends WithStyles<typeof styles> {
  mediaUri: string
  contributedArticles: IContributedArticle[]
}

class DraftList extends Component<IProps> {
  public render() {
    const { classes, mediaUri, contributedArticles } = this.props
    return (
      <Paper className={classes.articlesListPaper} elevation={0}>
        <List className={classes.articleList}>
          {contributedArticles
            .filter(c => c.draft.revisions.length > 0)
            .sort(
              (a: IContributedArticle, b: IContributedArticle) =>
                b.draft.revisions[0].savingDate.nbSecFrom1970 -
                a.draft.revisions[0].savingDate.nbSecFrom1970
            )
            .map(c => (
              <ListItem divider={true} key={c.uri}>
                <DraftSnippet contributedArticle={c} mediaUri={mediaUri} />
              </ListItem>
            ))}
        </List>
      </Paper>
    )
  }
}

export default withStyles(styles)(DraftList)
