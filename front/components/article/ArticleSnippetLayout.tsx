import React, { ReactNode } from 'react'
import { withStyles, StyleRules, WithStyles } from '@material-ui/core/styles'
import Link from 'next/link'
import { Typography } from '@material-ui/core'
import { getFormatedImageUrl } from '../../services/imageService'
import { IUrl } from '../../services/urlService'

// https://css-tricks.com/centering-css-complete-guide/
// StyleRules type for https://github.com/mui-org/material-ui/issues/9160
const styles: StyleRules = {
  articleLink: {
    width: '100%',
    paddingRight: 8
  },
  link: {
    textDecoration: 'none'
  },
  imgDiv: {
    width: 160,
    height: 100,
    float: 'right'
  },
  titleAndButtons: {
    width: '100%'
  },
  img: {
    position: 'relative',
    float: 'left',
    width: '100%',
    height: '100%',
    backgroundPosition: '50% 50%',
    backgroundRepeat: 'no-repeat',
    objectFit: 'cover',
    borderRadius: '16px'
  }
}

interface IProps extends WithStyles<typeof styles> {
  children: ReactNode // buttons : share / bookmark, edit draft
  articleUrl: IUrl
  linkReactId: string
  title: string
  subtitle: string
  imageUrl: string
}

class ArticleSnippetLayout extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props)
  }

  public render() {
    const {
      children,
      articleUrl,
      linkReactId,
      title,
      subtitle,
      imageUrl,
      classes
    } = this.props

    const scaledImageUrl = getFormatedImageUrl(
      imageUrl,
      'c_scale,f_auto,q_auto,w_320'
    )

    return (
      <React.Fragment>
        <div className={classes.titleAndButtons}>
          <Link as={articleUrl.as} href={articleUrl.href}>
            <a className={classes.link}>
              <div className={classes.articleLink}>
                <Typography
                  variant="subtitle1"
                  gutterBottom={false}
                  id={linkReactId}
                >
                  <b>{title}</b>
                </Typography>
              </div>
              <div className={classes.articleSubtitleLink}>
                <Typography
                  variant="subtitle2"
                  gutterBottom={false}
                  id={linkReactId}
                >
                  {subtitle}
                </Typography>
              </div>
            </a>
          </Link>

          <div className={classes.articleButtons}>{children}</div>
        </div>

        <Link as={articleUrl.as} href={articleUrl.href}>
          <a>
            <div className={classes.imgDiv}>
              <img src={scaledImageUrl} className={classes.img} />
            </div>
          </a>
        </Link>
      </React.Fragment>
    )
  }
}

export default withStyles(styles)(ArticleSnippetLayout)
