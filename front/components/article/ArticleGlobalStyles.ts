import { Theme } from '@material-ui/core/styles'
import { quotemarkUrl } from '../../services/imageService'

const rootLayout = {
  maxWidth: 1200,
  margin: '0 auto', // center horizontally
  marginTop: 0
}

const articleHeaderImgStyle = {
  maxWidth: '100%',
  maxHeight: '400px',
  margin: 'auto',
  marginTop: '20px',
  display: 'block'
}

const articleTitleAndContentLayout = {
  width: '700px',
  maxWidth: '100%',
  margin: '0 auto', // center horizontally
  paddingLeft: '16px',
  paddingRight: '16px'
}

const contentStyle = (theme: Theme) => ({
  h1: theme.typography.h4,
  h2: theme.typography.h5,
  h3: theme.typography.h6,
  h4: theme.typography.h6,
  h5: theme.typography.h6,
  h6: theme.typography.h6,
  p: {
    ...theme.typography.body1,
    marginTop: 16
  },
  li: theme.typography.body1,
  lo: theme.typography.body1,
  blockquote: {
    ...theme.typography.subtitle1,
    fontStyle: 'italic',
    background: `url("${quotemarkUrl}")`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: '20px',
    marginLeft: '0px',
    paddingLeft: '30px'
  },
  a: {
    color: 'inherit' // prevent ugly blue
  },
  img: {
    display: 'block', // use one full line
    maxHeight: 'inherit',
    maxWidth: 'inherit',
    padding: 0,
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  figure: {
    display: 'block', // use one full line
    marginLeft: 'auto',
    marginRight: 'auto',
    maxHeight: 'inherit',
    maxWidth: 'inherit',
    paddingTop: 10,
    paddingBottom: 10
  },
  figcaption: imageCaptionStyle(theme)
})

const titleStyle = (theme: Theme) => ({
  ...theme.typography.h4,
  fontWeight: 'bold' as 'bold',
  paddingLeft: 0,
  paddingRight: 0,
  paddingTop: 20,
  paddingBottom: 10
})

const subtitleStyle = (theme: Theme) => ({
  ...theme.typography.subtitle1,
  fontStyle: 'italic',
  paddingLeft: 0,
  paddingRight: 0,
  paddingTop: 10,
  paddingBottom: 10,
  color: 'grey'
})

const imageCaptionStyle = (theme: Theme) => ({
  ...theme.typography.caption,
  padding: 0,
  textAlign: 'center' as 'center'
})

export {
  contentStyle,
  titleStyle,
  subtitleStyle,
  rootLayout,
  articleTitleAndContentLayout,
  articleHeaderImgStyle,
  imageCaptionStyle
}
