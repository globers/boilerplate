import React from 'react'
import { withStyles, StyleRules, WithStyles } from '@material-ui/core/styles'
import { IconButton } from '@material-ui/core'
import Share from '@material-ui/icons/Share'
import Drawer from '@material-ui/core/Drawer'
import {
  EmailIcon,
  EmailShareButton,
  FacebookIcon,
  FacebookShareButton,
  TwitterIcon,
  TwitterShareButton
} from 'react-share'
import BookmarkArticleButton from './bookmarkArticleButton'
import { getArticleUrl } from '../../services/urlService'
import ArticleSnippetLayout from './ArticleSnippetLayout'
import { IGraphqlDate } from '../../graphql/schema/GraphqlDate'
import Typography from '@material-ui/core/Typography/Typography'
import { formatGraphqlDate } from '../../services/dateServices'

// https://css-tricks.com/centering-css-complete-guide/
// StyleRules type for https://github.com/mui-org/material-ui/issues/9160
const styles: StyleRules = {
  sharingPanel: {
    display: 'flex',
    justifyContent: 'space-around'
  },
  sharingButtons: {
    padding: '5px'
  }
}

interface IProps extends WithStyles<typeof styles> {
  isLoggedIn: boolean
  media: { uri: string }
  userArticle: {
    article: {
      uri: string
      title: string
      subtitle: string
      imageUrl: string
      firstPublication: IGraphqlDate
    }
    bookmarked: boolean
  }
}

interface IState {
  sharingDrawerOpened: boolean
}

class ArticleSnippet extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    this.state = {
      sharingDrawerOpened: false
    }
  }

  public render() {
    const {
      isLoggedIn,
      classes,
      media,
      userArticle: { article, bookmarked }
    } = this.props

    const articleUrl = getArticleUrl(media.uri, article.uri)

    const dateStr = formatGraphqlDate(article.firstPublication)

    const linkAbsoluteUrl = 'https://globers.co' + articleUrl.as
    return (
      <React.Fragment>
        <ArticleSnippetLayout
          imageUrl={article.imageUrl}
          articleUrl={articleUrl}
          linkReactId={`link-article-${article.uri}`}
          title={article.title}
          subtitle={article.subtitle}
        >
          <Typography variant={'caption'} color="textSecondary">
            {dateStr}
          </Typography>
          <div>
            <BookmarkArticleButton
              articleUri={article.uri}
              isArticleBookmarked={bookmarked}
              isLoggedIn={isLoggedIn}
            />
            <IconButton onClick={this.onClickShare}>
              <Share color="disabled" fontSize={'small'} />
            </IconButton>
          </div>
        </ArticleSnippetLayout>

        <Drawer
          anchor="bottom"
          open={this.state.sharingDrawerOpened}
          onClose={this.closeShare}
        >
          <div className={classes.sharingPanel}>
            <div className={classes.sharingButtons}>
              <FacebookShareButton url={linkAbsoluteUrl}>
                <FacebookIcon round={true} />
              </FacebookShareButton>
            </div>
            <div className={classes.sharingButtons}>
              <TwitterShareButton url={linkAbsoluteUrl} title={article.title}>
                <TwitterIcon round={true} />
              </TwitterShareButton>
            </div>
            <div className={classes.sharingButtons}>
              <EmailShareButton url={linkAbsoluteUrl}>
                <EmailIcon round={true} />
              </EmailShareButton>
            </div>
          </div>
        </Drawer>
      </React.Fragment>
    )
  }

  private onClickShare = () => {
    this.setState({ sharingDrawerOpened: true })
  }

  private closeShare = () => {
    this.setState({ sharingDrawerOpened: false })
  }
}

export default withStyles(styles)(ArticleSnippet)
