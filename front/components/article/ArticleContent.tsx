import React, { Component } from 'react'
import { StyleRules, Theme, withStyles } from '@material-ui/core/styles'
import { WithStyles } from '@material-ui/core/es'
import { getFormatedImageUrl } from '../../services/imageService'
import {
  articleTitleAndContentLayout,
  contentStyle,
  titleStyle,
  rootLayout,
  subtitleStyle,
  articleHeaderImgStyle
} from './ArticleGlobalStyles'
import { IGraphqlDate } from '../../graphql/schema/GraphqlDate'
import Typography from '@material-ui/core/Typography/Typography'
import { formatGraphqlDate } from '../../services/dateServices'
import { canReadArticle } from '../../services/userService'
import MediaFooter from '../media/MediaFooter'
import Divider from '@material-ui/core/Divider/Divider'

const styles = (theme: Theme): StyleRules => ({
  root: rootLayout,
  headerImg: articleHeaderImgStyle,
  articleTitleAndContent: articleTitleAndContentLayout,
  title: titleStyle(theme),
  subtitle: subtitleStyle(theme),
  content: {
    maxWidth: 'inherit',
    '@global': contentStyle(theme)
  },
  publicationDate: {
    color: 'grey'
  },
  mediaWrapper: {
    textAlign: 'center'
  },
  media: {
    display: 'inline-block',
    margin: '0 auto'
  },
  divider: {
    margin: 16
  },
  paywallMessage: {
    textAlign: 'center'
  }
})

interface IMedia {
  uri: string
  name: string
  description: string
  logoImageUrl: string
  unpublished: boolean
  owner: IUserPublicProfile
  subscriptionPlans: ISubscriptionPlan[]
}

interface ISubscriptionPlan {
  uri: string
}

interface IUserPublicProfile {
  uri: string
  firstName: string
  lastName: string
}

interface IProps extends WithStyles<typeof styles> {
  mediaCanBeSubscribed: boolean
  isMediaAdmin: boolean
  isUserSubscribed: boolean
  article: {
    uri: string
    title: string
    subtitle: string
    imageUrl: string
    content: string
    firstPublication: IGraphqlDate
  }
  media: IMedia
}

interface IState {
  canRead: boolean
}

class ArticleContent extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    this.state = {
      canRead: true
    }
  }

  public render() {
    const { classes, article } = this.props
    const imageUrl = getFormatedImageUrl(
      article.imageUrl,
      'c_scale,f_auto,q_auto,w_1200'
    )
    const dateStr = formatGraphqlDate(article.firstPublication)
    return (
      <div className={classes.root}>
        <img src={imageUrl} className={classes.headerImg} />
        <div className={classes.articleTitleAndContent}>
          <div className={classes.title} id="title">
            {article.title}
          </div>
          <Typography variant={'caption'} color="textSecondary">
            {dateStr}
          </Typography>
          <div className={classes.subtitle} id="subtitle">
            {article.subtitle}
          </div>
          {this.state.canRead ? (
            <div
              className={classes.content}
              dangerouslySetInnerHTML={{ __html: article.content }}
            />
          ) : (
            <Typography variant="h6" className={classes.paywallMessage}>
              <b> You have read your 3 free articles this month</b>
            </Typography>
          )}
          <Divider className={classes.divider} light={true} />
          <MediaFooter
            media={this.props.media}
            isUserAlreadySubscribed={this.props.isUserSubscribed}
          />
        </div>
      </div>
    )
  }

  public componentDidMount() {
    if (
      this.props.mediaCanBeSubscribed &&
      !this.props.isUserSubscribed &&
      !this.props.isMediaAdmin &&
      !canReadArticle(this.props.article.uri)
    ) {
      this.setState({
        canRead: false
      })
    }
  }
}

export default withStyles(styles)(ArticleContent)
