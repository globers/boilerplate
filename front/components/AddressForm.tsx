import React from 'react'
import {
  createStyles,
  TextField,
  withStyles,
  WithStyles
} from '@material-ui/core'
import Typography from '@material-ui/core/Typography/Typography'
import FormControl from '@material-ui/core/FormControl/FormControl'
import InputLabel from '@material-ui/core/InputLabel/InputLabel'
import Select from '@material-ui/core/Select/Select'
import { StripeCountry } from '../graphql/schema/StripeCountry'

const styles = () =>
  createStyles({
    select: {
      marginTop: '16px', // to get perfect alignment between input and select
      marginBottom: '8px'
    },
    textField: {}
  })

interface IProps extends WithStyles<typeof styles> {
  country: StripeCountry
  addressCity: string
  addressLine1: string
  addressLine2: string
  addressPostalCode: string

  emptyAddressLine1Error: boolean
  emptyCityError: boolean
  emptyPostalCodeError: boolean

  onChangeCountry: (country: StripeCountry) => void
  onChangeAddressLine1: (addressLine1: string) => void
  onChangeAddressLine2: (addressLine2: string) => void
  onChangeCity: (city: string) => void
  onChangePostalCode: (postalCode: string) => void
}

function countryToLabel(country: StripeCountry) {
  switch (country) {
    case StripeCountry.FR:
      return 'France'
  }
}

class AddressForm extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props)
  }

  public render() {
    const {
      classes,
      emptyAddressLine1Error,
      emptyCityError,
      emptyPostalCodeError,
      addressCity,
      addressLine1,
      addressLine2,
      addressPostalCode,
      country
    } = this.props

    return (
      <div>
        <Typography variant="h6">Address</Typography>
        <TextField
          value={addressLine1}
          className={classes.textField}
          id="addressline1-input"
          label="Street address"
          autoComplete="address-line1"
          margin="normal"
          onChange={this.onChangeAddressLine1}
          fullWidth={true}
          error={emptyAddressLine1Error}
          helperText={emptyAddressLine1Error ? 'Address is empty' : null}
        />
        <TextField
          value={addressLine2}
          className={classes.textField}
          id="addressline2-input"
          label="Apt, building, floor..."
          autoComplete="address-line2"
          margin="normal"
          onChange={this.onChangeAddressLine2}
          fullWidth={true}
        />
        <TextField
          value={addressPostalCode}
          className={classes.textField}
          id="postalcode-input"
          label="Postal code"
          autoComplete="postal-code"
          margin="normal"
          onChange={this.onChangePostalCode}
          error={emptyPostalCodeError}
          helperText={emptyPostalCodeError ? 'Postal code is empty' : null}
        />
        <TextField
          value={addressCity}
          className={classes.textField}
          id="city-input"
          label="City"
          autoComplete="address-level2"
          margin="normal"
          onChange={this.onChangeCity}
          error={emptyCityError}
          helperText={emptyCityError ? 'City is empty' : null}
        />
        <FormControl className={classes.select}>
          <InputLabel>Country</InputLabel>
          <Select
            fullWidth={true}
            native={true}
            value={country}
            onChange={this.onCountryChanged}
          >
            {Object.keys(StripeCountry)
              .map(c => (StripeCountry[c as any] as any) as StripeCountry)
              .map(c => (
                <option key={c} value={c}>
                  {countryToLabel(c)}
                </option>
              ))}
          </Select>
        </FormControl>
      </div>
    )
  }

  private onCountryChanged = (e: any) =>
    this.props.onChangeCountry(e.target.value)

  private onChangeAddressLine1 = (e: any) =>
    this.props.onChangeAddressLine1(e.target.value)

  private onChangeAddressLine2 = (e: any) =>
    this.props.onChangeAddressLine2(e.target.value)

  private onChangeCity = (e: any) => this.props.onChangeCity(e.target.value)

  private onChangePostalCode = (e: any) =>
    this.props.onChangePostalCode(e.target.value)
}

export default withStyles(styles)(AddressForm)
