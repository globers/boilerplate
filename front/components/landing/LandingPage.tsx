import React, { RefObject } from 'react'
import { createStyles, withStyles, WithStyles } from '@material-ui/core'
import VideoAndCallToAction from './VideoAndCallToAction'
import MediaSnippetFlex from './MediaSnippetFlex'
import HowItWorks from './HowItWorks'
import WhyGlobers from './WhyGlobers'
import LandingFooter from './LandingFooter'

import EmailPopup from './EmailPopup'

interface IMedia {
  uri: string
  name: string
  logoUrl: string
  description: string
  unpublished: boolean
  owner: IUserPublicProfile
  nbArticles: number
}

interface IUserPublicProfile {
  uri: string
  firstName: string
  lastName: string
  profilePictureUrl: string | null
}

interface IUserMedia {
  uri: string
  media: IMedia
  followed: boolean
}

interface IContributedMedia {
  uri: string
  media: IMedia
}

const styles = () =>
  createStyles({
    root: {
      width: '100%'
    }
  })

interface IProps extends WithStyles<typeof styles> {
  userMedias: IUserMedia[]
  contributedMedias: IContributedMedia[]
  isLoggedIn: boolean
}

class LandingPage extends React.Component<IProps> {
  private readonly howItWorksRef: RefObject<HTMLDivElement>

  constructor(props: IProps) {
    super(props)
    this.howItWorksRef = React.createRef()
  }

  public render() {
    const { classes, userMedias, contributedMedias, isLoggedIn } = this.props
    return (
      <div className={classes.root}>
        <VideoAndCallToAction onClickLearnMore={this.scrollToHowItWorks} />
        <MediaSnippetFlex
          userMedias={userMedias}
          contributedMedias={contributedMedias}
        />
        <div ref={this.howItWorksRef}>
          <HowItWorks />
        </div>
        <WhyGlobers />
        <LandingFooter />
        {!isLoggedIn && <EmailPopup />}
      </div>
    )
  }

  private scrollToHowItWorks = () => {
    const elt = this.howItWorksRef.current
    if (elt) {
      elt.scrollIntoView({ behavior: 'smooth' })
    }
  }
}

export default withStyles(styles)(LandingPage)
