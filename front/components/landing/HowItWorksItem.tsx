import React, { ReactNode } from 'react'
import { createStyles, Theme, withStyles, WithStyles } from '@material-ui/core'
import Typography from '@material-ui/core/Typography/Typography'

const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: '12px',
      padding: '16px',
      width: '250px',
      borderStyle: 'solid',
      borderWidth: '1px',
      borderColor: '#D3D3D3',
      backgroundColor: theme.palette.background.paper
    },
    indexTypo: {},
    titleTypo: {
      marginTop: 16,
      marginBottom: 16,
      marginLeft: 10
    },
    descriptionTypo: {},
    icon: {
      textAlign: 'center',
      color: theme.palette.primary.main,
      marginTop: 16,
      marginBottom: 16
    }
  })

interface IProps extends WithStyles<typeof styles> {
  index: number
  icon: ReactNode
  title: string
  description: string
}

class HowItWorksItem extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props)
  }

  public render() {
    const { classes, icon, title, description } = this.props
    return (
      <div className={classes.root}>
        <div className={classes.icon}>{icon}</div>
        <Typography variant="h6" className={classes.titleTypo}>
          {title}
        </Typography>

        <Typography variant="subtitle2" className={classes.descriptionTypo}>
          {description}
        </Typography>
      </div>
    )
  }
}

export default withStyles(styles)(HowItWorksItem)
