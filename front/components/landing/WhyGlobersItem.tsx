import React, { ReactNode } from 'react'
import { createStyles, Theme, withStyles, WithStyles } from '@material-ui/core'
import Typography from '@material-ui/core/Typography/Typography'

const styles = (theme: Theme) =>
  createStyles({
    root: {
      marginLeft: '20px',
      marginRight: '20px',
      padding: '10px',
      width: '400px',
    },
    indexTypo: {},
    titleTypo: {
      textAlign: 'left',
      marginTop: 16,
      marginBottom: 16
    },
    descriptionTypo: {},
    icon: {
      textAlign: 'left',
      color: theme.palette.primary.main,
      marginTop: 16,
      marginBottom: 16
    }
  })

interface IProps extends WithStyles<typeof styles> {
  icon: ReactNode
  title: string
  description: string
}

class WhyGlobersItem extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props)
  }

  public render() {
    const { classes, icon, title, description } = this.props
    return (
      <div className={classes.root}>
        <div className={classes.icon}>{icon}</div>
        <Typography variant="h6" className={classes.titleTypo}>
          {title}
        </Typography>
        <Typography variant="subtitle2" className={classes.descriptionTypo}>
          {description}
        </Typography>
      </div>
    )
  }
}

export default withStyles(styles)(WhyGlobersItem)
