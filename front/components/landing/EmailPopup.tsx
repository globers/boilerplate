import React from 'react'
import { createStyles, withStyles, WithStyles } from '@material-ui/core'
import Dialog from '@material-ui/core/Dialog/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle/DialogTitle'
import TextField from '@material-ui/core/TextField/TextField'
import DialogContent from '@material-ui/core/DialogContent/DialogContent'
import DialogActions from '@material-ui/core/DialogActions/DialogActions'
import Button from '@material-ui/core/Button/Button'
import { isEmailValid } from '../../services/inputChecksServices'
import { globersLogoUrl } from '../../services/imageService'
// @ts-ignore
import exitIntent from 'exit-intent'
import CreateProspect from '../mutation/CreateProspect'
import {
  isSubscribedToNewsletter,
  setSubscribedToNewsletter
} from '../../services/userService'

const styles = () =>
  createStyles({
    title: {
      display: 'block',
      margin: '16px auto 0px auto' // center horizontally
    },
    logo: {
      width: 100
    }
  })

enum FormStatus {
  Closed,
  Open,
  Thanks
}

interface IState {
  email: string
  invalidEmail: boolean
  formStatus: FormStatus
}

interface IProps extends WithStyles<typeof styles> {}

class EmailPopup extends React.Component<IProps, IState> {
  private removeExitIntent: any | null

  constructor(props: IProps) {
    super(props)
    this.state = {
      email: '',
      invalidEmail: false,
      formStatus: FormStatus.Closed
    }
    this.removeExitIntent = null
  }

  public componentDidMount() {
    if (!isSubscribedToNewsletter()) {
      this.removeExitIntent = exitIntent({
        onExitIntent: () => {
          this.setState({
            formStatus: FormStatus.Open
          })
        }
      })
    }
  }

  public componentWillUnmount() {
    if (this.removeExitIntent) {
      this.removeExitIntent()
    }
  }

  public render() {
    const { classes } = this.props

    return (
      <Dialog
        open={this.state.formStatus !== FormStatus.Closed}
        onClose={this.onClose}
        aria-labelledby="form-dialog-title"
      >
        <div className={classes.title}>
          <img src={globersLogoUrl} alt={'Globers'} className={classes.logo} />
        </div>

        {this.state.formStatus === FormStatus.Open && (
          <>
            {' '}
            <DialogTitle id="form-dialog-title">
              Abonne-toi à la newsletter
            </DialogTitle>
            <DialogContent>
              <TextField
                id="email-input"
                label="Email"
                margin="normal"
                autoComplete="email"
                onChange={this.onChangeEmail}
                fullWidth={true}
                autoFocus={true}
                error={this.state.invalidEmail}
                helperText={
                  this.state.invalidEmail
                    ? 'Email invalide'
                    : 'pas de spam, promis'
                }
              />
            </DialogContent>
            <DialogActions>
              <CreateProspect
                email={this.state.email}
                origin={'globers.co/index/newsletter'}
                onProspectCreated={this.onProspectCreated}
              >
                {(createProspect: () => void) => (
                  <Button
                    onClick={this.onSubscribe(createProspect)}
                    color="primary"
                    variant={'contained'}
                    fullWidth={true}
                  >
                    Tenez-moi au courant!
                  </Button>
                )}
              </CreateProspect>
            </DialogActions>
            <Button
              id={'email-popup-cancel-button'}
              onClick={this.closeEmailPopup}
              color="default"
              variant={'text'}
              size={'small'}
              fullWidth={true}
            >
              Non merci
            </Button>
          </>
        )}
        {this.state.formStatus === FormStatus.Thanks && (
          <>
            <DialogTitle id="form-dialog-title">Merci, à bientôt!</DialogTitle>
          </>
        )}
      </Dialog>
    )
  }

  private onClose = () => {
    if (this.state.formStatus === FormStatus.Thanks) {
      this.closeEmailPopup()
    }
    // if email not set yet, don't close popup if not asked explicitly
  }

  private onProspectCreated = () => {
    setSubscribedToNewsletter()
    this.setState({
      formStatus: FormStatus.Thanks
    })
  }

  private closeEmailPopup = () =>
    this.setState({ formStatus: FormStatus.Closed })

  private onSubscribe = (createProspect: () => void) => () => {
    if (isEmailValid(this.state.email)) {
      createProspect()
    } else {
      this.setState({
        invalidEmail: true
      })
    }
  }

  private onChangeEmail = (e: any) =>
    this.setState({
      email: e.target.value,
      invalidEmail: false
    })
}

export default withStyles(styles)(EmailPopup)
