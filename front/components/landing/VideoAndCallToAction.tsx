import React from 'react'
import { createStyles, Theme, withStyles, WithStyles } from '@material-ui/core'
import Typography from '@material-ui/core/Typography/Typography'
import Link from 'next/link'
import Button from '@material-ui/core/Button/Button'

const styles = (theme: Theme) =>
  createStyles({
    root: {
      backgroundColor: theme.palette.primary.main,
      padding: '80px 0px 40px 0px'
    },
    flexbox: {
      margin: '0 auto',
      maxWidth: '1300px',
      display: 'flex',
      flexWrap: 'wrap'
    },
    callToActionDiv: {
      flexGrow: 1,
      paddingTop: 20,
      paddingBottom: 20
    },
    callToActionWrapper: {
      maxWidth: '640px',
      margin: '0 auto' // center in parent div
    },
    videoDiv: {
      paddingTop: 20,
      paddingBottom: 20,
      flexGrow: 1,
      padding: '0px',
      textAlign: 'center'
    },
    videoFrameWrapper: {
      margin: '0 auto', // center in parent div
      display: 'inline-block', // fit to content (video iframe)
      width: 560,
      height: 315,
      '@media (max-width: 560px)': {
        width: '100%'
      }
    },
    title: {
      padding: '0px 20px 40px 20px',
      color: 'white'
    },
    button: {
      margin: 16
    },
    buttonGroup: {
      display: 'flex',
      float: 'right',
      margin: '20px auto 60px auto'
    },
    link: {
      textDecoration: 'none'
    }
  })

interface IProps extends WithStyles<typeof styles> {
  onClickLearnMore: () => void
}

class VideoAndCallToAction extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props)
  }

  public render() {
    const { classes } = this.props
    return (
      <div className={classes.root}>
        <div className={classes.flexbox}>
          <div className={classes.callToActionDiv}>
            <div className={classes.callToActionWrapper}>
              <Typography variant="h4" className={classes.title}>
                <b>{"Le futur du journalisme t'appartient"}</b>
              </Typography>
              <Typography variant="h5" className={classes.title}>
                Globers te permet de créer ton média simplement et d'être
                rémunéré par tes abonnés.
              </Typography>
              <div className={classes.buttonGroup}>
                <Button
                  variant="contained"
                  color="default"
                  className={classes.button}
                  onClick={this.props.onClickLearnMore}
                >
                  En savoir plus
                </Button>
                <Link href="\newMedia">
                  <a className={classes.link}>
                    <Button
                      size="large"
                      variant="contained"
                      color="secondary"
                      className={classes.button}
                    >
                      <b>Créer mon média</b>
                    </Button>
                  </a>
                </Link>
              </div>
            </div>
          </div>
          <div className={classes.videoDiv}>
            <div className={classes.videoFrameWrapper}>
              <iframe
                width="100%"
                height="100%"
                src="https://www.youtube.com/embed/jGy_c2T590g?rel=0"
                frameBorder="0"
                // @ts-ignore
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen={true}
              />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default withStyles(styles)(VideoAndCallToAction)
