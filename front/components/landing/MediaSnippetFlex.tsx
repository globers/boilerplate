import React from 'react'
import { withStyles, WithStyles } from '@material-ui/core'
import MediaSnippet from '../media/MediaSnippet'
import { StyleRules } from '@material-ui/core/styles'
import LandingPageSection from './LandingSection'

interface IMedia {
  uri: string
  name: string
  logoUrl: string
  description: string
  unpublished: boolean
  owner: IUserPublicProfile
  nbArticles: number
}

interface IUserPublicProfile {
  uri: string
  firstName: string
  lastName: string
  profilePictureUrl: string | null
}

interface IUserMedia {
  uri: string
  media: IMedia
  followed: boolean
}

interface IContributedMedia {
  uri: string
  media: IMedia
}

const styles = (): StyleRules => ({
  mediaList: {
    maxWidth: 1200,
    display: 'flex',
    flexWrap: 'wrap',
    margin: '0 auto', // center horizontally,
    alignItems: 'center',
    justifyContent: 'center'
  },
  mediaSnippetWrapper: {
    margin: 12
  }
})

interface IProps extends WithStyles<typeof styles> {
  userMedias: IUserMedia[]
  contributedMedias: IContributedMedia[]
}

class MediaSnippetFlex extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props)
  }

  public render() {
    const { classes, userMedias, contributedMedias } = this.props

    return (
      <LandingPageSection
        background={'light'}
        title={`Ils sont parmi les ${
          userMedias.length
        } médias qui ont choisi Globers`}
      >
        <div className={classes.mediaList}>
          {contributedMedias.map(c => (
            <div className={classes.mediaSnippetWrapper}>
              <MediaSnippet key={c.uri} media={c.media} isContributor={true} />
            </div>
          ))}
        </div>
        <div className={classes.mediaList}>
          {userMedias
            .sort(
              (a: IUserMedia, b: IUserMedia) =>
                b.media.nbArticles - a.media.nbArticles
            )
            .slice(0, 3)
            .map(u => (
              <div className={classes.mediaSnippetWrapper}>
                <MediaSnippet
                  key={u.uri}
                  media={u.media}
                  isContributor={false}
                />
              </div>
            ))}
        </div>
      </LandingPageSection>
    )
  }
}

export default withStyles(styles)(MediaSnippetFlex)
