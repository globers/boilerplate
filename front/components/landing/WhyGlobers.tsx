import React from 'react'
import { createStyles, withStyles, WithStyles } from '@material-ui/core'
import LandingPageSection from './LandingSection'
import WhyGlobersItem from './WhyGlobersItem'
import {
  Add,
  CreditCard,
  FormatAlignLeft,
  Language,
  ThreeSixty,
  TouchApp
} from '@material-ui/icons'

const styles = () => createStyles({})

interface IProps extends WithStyles<typeof styles> {}

class WhyGlobers extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props)
  }

  public render() {
    return (
      <LandingPageSection background={'light'} title={'Pourquoi Globers ?'}>
        <WhyGlobersItem
          icon={
            <>
              <FormatAlignLeft fontSize="large" />
              <Add />

              <CreditCard fontSize="large" />
            </>
          }
          title={'Une solution tout-en-un'}
          description={
            "Globers intègre un outil d'écriture et de publication simple et une solution de paiement."
          }
        />
        <WhyGlobersItem
          icon={<ThreeSixty fontSize="large" />}
          title={'Un revenu récurrent'}
          description={
            "L'abonnement mensuel fidélise tes abonnés et t' assure un revenu prévisible et récurrent."
          }
        />
        <WhyGlobersItem
          icon={<TouchApp fontSize="large" />}
          title={'Libre et indépendant'}
          description={
            'Ton média, tes abonnés, ta ligne éditoriale. Globers est un outil à ton service, pas un média.'
          }
        />
        <WhyGlobersItem
          icon={<Language fontSize="large" />}
          title={'Une communauté forte'}
          description={
            "Un abonnement Globers ouvre l'accès à l'ensemble du contenu et favorise la découverte de nouveaux journalistes."
          }
        />
      </LandingPageSection>
    )
  }
}

export default withStyles(styles)(WhyGlobers)
