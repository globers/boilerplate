import React from 'react'
import HowItWorksItem from './HowItWorksItem'
import { Create, EuroSymbol, Group, Share } from '@material-ui/icons'
import LandingPageSection from './LandingSection'

class HowItWorks extends React.Component<{}> {
  constructor(props: {}) {
    super(props)
  }

  public render() {
    return (
      <LandingPageSection background={'dark'} title={'Comment ça marche '}>
        <HowItWorksItem
          icon={<Create fontSize="large" />}
          title={'1. Publie sur Globers'}
          description={
            'Créé ton média gratuitement sur Globers.co et publie tes premiers articles'
          }
        />
        <HowItWorksItem
          icon={<Share fontSize="large" />}
          title={'2. Fidélise ta communauté'}
          description={
            "Promeus ton travail à l'ensemble de ton réseau (Twitter, Facebook...)"
          }
        />
        <HowItWorksItem
          icon={<EuroSymbol fontSize="large" />}
          title={'3. Génère un revenu'}
          description={
            'Tes lecteurs te soutiennent par un abonnement mensuel de 7,99€. La commission est de 25%'
          }
        />
        <HowItWorksItem
          icon={<Group fontSize="large" />}
          title={"4. L'union fait la force"}
          description={
            "En s'abonnant à ton média, tes lecteurs ont accès à tous les médias de Globers"
          }
        />
      </LandingPageSection>
    )
  }
}

export default HowItWorks
