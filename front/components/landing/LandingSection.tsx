import React, { ReactNodeArray } from 'react'
import { createStyles, Theme, withStyles, WithStyles } from '@material-ui/core'
import Typography from '@material-ui/core/Typography/Typography'

const styles = (theme: Theme) =>
  createStyles({
    flexBox: {
      maxWidth: 1200,
      display: 'flex',
      flexWrap: 'wrap',
      margin: '0 auto', // center horizontally,
      justifyContent: 'center'
    },
    rootDark: {
      width: '100%',
      backgroundColor: theme.palette.primary.main,
      paddingTop: 30,
      paddingBottom: 30
    },
    rootLight: {
      width: '100%',
      backgroundColor: 'white',
      paddingTop: 30,
      paddingBottom: 30
    },
    titleDark: {
      color: theme.palette.primary.main,
      padding: 16,
      textAlign: 'center'
    },
    titleLight: { color: 'white', padding: 16, textAlign: 'center' }
  })

interface IProps extends WithStyles<typeof styles> {
  children: ReactNodeArray
  title: string
  background: 'dark' | 'light'
}

class LandingPageSection extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props)
  }

  public render() {
    const { classes, children, title, background } = this.props

    const rootClass =
      background === 'dark' ? classes.rootDark : classes.rootLight
    const titleClass =
      background === 'dark' ? classes.titleLight : classes.titleDark
    return (
      <div className={rootClass}>
        <Typography variant="h4" className={titleClass}>
          <b>{title}</b>
        </Typography>
        <div className={classes.flexBox}>{children}</div>
      </div>
    )
  }
}

export default withStyles(styles)(LandingPageSection)
