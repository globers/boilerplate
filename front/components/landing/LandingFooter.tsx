import React from 'react'
import { createStyles, withStyles, WithStyles } from '@material-ui/core'
import LandingPageSection from './LandingSection'
import Link from 'next/link'
import Button from '@material-ui/core/Button/Button'
import { FacebookIcon, TwitterIcon } from 'react-share'
import Typography from '@material-ui/core/Typography/Typography'

const styles = () =>
  createStyles({
    button: {
      margin: 80
    },
    link: {
      textDecoration: 'none'
    },
    social: {
      width: '500px',
      margin: '0 auto'
    },
    socialLink: {
      margin: 20
    },
    copyright: {
      padding: 10
    }
  })

interface IProps extends WithStyles<typeof styles> {}

class LandingFooter extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props)
  }

  public render() {
    const { classes } = this.props
    return (
      <>
        <LandingPageSection
          background={'dark'}
          title={'Un voyage commence toujours par un premier pas...'}
        >
          <Link href="\newMedia">
            <a className={classes.link}>
              <Button
                size="large"
                variant="contained"
                color="secondary"
                className={classes.button}
              >
                <b>Je créé mon média</b>
              </Button>
            </a>
          </Link>
        </LandingPageSection>
        <LandingPageSection
          background={'light'}
          title={'Suivez-nous sur les réseaux sociaux'}
        >
          <a
            target="_blank"
            rel="noopener noreferrer"
            href={'https://www.facebook.com/meetglobers/'}
            className={classes.socialLink}
          >
            <FacebookIcon round={true} />
          </a>
          <a
            target="_blank"
            rel="noopener noreferrer"
            href={'https://twitter.com/HelloGlobers'}
            className={classes.socialLink}
          >
            <TwitterIcon round={true} />
          </a>
          <Typography variant="subtitle1" className={classes.socialLink}>
            {process.browser && (
              <a
                className={classes.link}
                href="m&#97;ilt&#111;&#58;hello&#64;glo&#98;e&#114;s&#46;%63&#111;"
              >
                he&#108;&#108;o&#64;glo&#98;&#101;&#114;s&#46;co
              </a>
            )}
          </Typography>
        </LandingPageSection>
        <div className={classes.copyright}>
          © Copyright 2019 Globers Inc. All rights reserved.
        </div>
      </>
    )
  }
}

export default withStyles(styles)(LandingFooter)
