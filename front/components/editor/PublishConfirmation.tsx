import React from 'react'
import { Dialog } from '@material-ui/core'
import Button from '@material-ui/core/Button/Button'
import DialogActions from '@material-ui/core/DialogActions/DialogActions'
import DialogTitle from '@material-ui/core/DialogTitle/DialogTitle'

interface IProps {
  publishArticle: () => void
  onClose: () => void
  open: boolean
}

class PublishConfirmation extends React.PureComponent<IProps> {
  public render() {
    const { open, publishArticle, onClose } = this.props
    return (
      <Dialog
        open={open}
        onClose={onClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Publish now?</DialogTitle>
        <DialogActions>
          <Button onClick={onClose} color="primary">
            Cancel
          </Button>
          <Button onClick={publishArticle} color="primary" autoFocus={true}>
            Publish
          </Button>
        </DialogActions>
      </Dialog>
    )
  }
}

export default PublishConfirmation
