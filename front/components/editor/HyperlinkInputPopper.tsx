import React from 'react'
import {
  createStyles,
  InputAdornment,
  Theme,
  withStyles,
  WithStyles
} from '@material-ui/core'
import TextField from '@material-ui/core/TextField/TextField'
import Popper from '@material-ui/core/Popper/Popper'
import { Done, InsertLink } from '@material-ui/icons'
import IconButton from '@material-ui/core/IconButton/IconButton'

const styles = (theme: Theme) =>
  createStyles({
    urlField: {
      width: '300px',
      borderStyle: 'solid',
      borderWidth: '1px',
      borderColor: '#eaeaea',
      borderRadius: 50,
      padding: 5,
      paddingLeft: 20,
      paddingRight: 20,
      backgroundColor: theme.palette.background.default
    },
    link: {
      color: 'inherit',
      paddingRight: 12 // to compensate for link button padding: 12
    },
    linkIconAndLink: {
      borderRadius: 50,
      padding: 5,
      backgroundColor: theme.palette.background.default,
      opacity: 0.9
    }
  })

interface IState {
  isUrlBoxOpened: boolean
  url: string | null
}

interface IProps extends WithStyles<typeof styles> {
  selectedLinkText: string | null
  anchorElement: any
  onLinkAdded: (url: string) => void
  onLinkRemoved: () => void
}

class HyperlinkInputPopover extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    this.state = {
      isUrlBoxOpened: false,
      url: null
    }
  }

  public render() {
    const { anchorElement, classes, selectedLinkText } = this.props
    return (
      <Popper open={true} anchorEl={anchorElement} placement="top">
        <div>
          {this.state.isUrlBoxOpened ? (
            <div className={classes.urlField}>
              <TextField
                label="Paste ot type a link..."
                placeholder="https://mycoollink.com"
                fullWidth={true}
                onChange={this.onChange}
                autoFocus={true}
                onKeyPress={this.onKeyPress}
                margin="dense"
                InputProps={
                  this.state.url
                    ? {
                        endAdornment: (
                          <InputAdornment position="end">
                            <IconButton
                              onClick={this.onValidateHyperlink}
                              aria-label="Add hyperlink"
                              color="primary"
                            >
                              <Done />
                            </IconButton>
                          </InputAdornment>
                        )
                      }
                    : undefined
                }
              />
            </div>
          ) : (
            <div className={classes.linkIconAndLink}>
              <IconButton
                onClick={this.onButtonLinkClick}
                color={selectedLinkText ? 'primary' : 'default'}
              >
                <InsertLink fontSize={'small'} />
              </IconButton>
              {selectedLinkText && (
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href={selectedLinkText}
                  className={classes.link}
                >
                  {selectedLinkText}
                </a>
              )}
            </div>
          )}
        </div>
      </Popper>
    )
  }

  private onButtonLinkClick = () => {
    if (this.props.selectedLinkText) {
      this.props.onLinkRemoved()
    } else {
      this.setState({ isUrlBoxOpened: true })
    }
  }

  private onKeyPress = (event: any) => {
    if (event.key === 'Enter') {
      if (this.state.url) {
        this.onValidateHyperlink()
      }
    }
  }

  private onChange = (event: any) => this.setState({ url: event.target.value })

  private onValidateHyperlink = () => {
    this.props.onLinkAdded(this.state.url as string)
  }
}

export default withStyles(styles)(HyperlinkInputPopover)
