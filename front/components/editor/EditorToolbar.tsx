import React from 'react'
import {
  createStyles,
  withStyles,
  WithStyles,
  Tooltip
} from '@material-ui/core'
import {
  FormatBold,
  FormatItalic,
  FormatListBulleted,
  FormatListNumbered,
  FormatQuote,
  AddAPhoto,
  AddCircleOutline
} from '@material-ui/icons'
import EditorButton from './EditorButton'

const styles = () =>
  createStyles({
    root: {
      borderStyle: 'solid',
      borderWidth: '1px',
      borderColor: '#eaeaea',
      borderRadius: 20,
      padding: 5
    },
    group: {
      textAlign: 'right'
    },
    tooltipInner: {
      // https://github.com/mui-org/material-ui/issues/8416
      display: 'inline-block' // for some reason, tooltip doesn't appear if there is not div between tooltip and button
    }
  })

interface IEditorButtonProps {
  isActive: boolean
  onClick: (event: any) => void
}

interface IProps extends WithStyles<typeof styles> {
  headerProps: IEditorButtonProps
  header2Props: IEditorButtonProps
  boldProps: IEditorButtonProps
  italicProps: IEditorButtonProps
  numberedListProps: IEditorButtonProps
  bulletedListProps: IEditorButtonProps
  quoteProps: IEditorButtonProps
  imageProps: IEditorButtonProps
  embedProps: IEditorButtonProps
}

class EditorToolbar extends React.PureComponent<IProps> {
  constructor(props: IProps) {
    super(props)
  }

  public render() {
    const { classes } = this.props
    return (
      <div className={classes.root}>
        <div className={classes.group}>
          <Tooltip title="Title">
            <div className={classes.tooltipInner}>
              <EditorButton {...this.props.headerProps}>
                <span>
                  H<sub>1</sub>
                </span>
              </EditorButton>
            </div>
          </Tooltip>
          <Tooltip title="Subtitle">
            <div className={classes.tooltipInner}>
              <EditorButton {...this.props.header2Props}>
                <span>
                  H<sub>2</sub>
                </span>
              </EditorButton>
            </div>
          </Tooltip>
        </div>
        <div className={classes.group}>
          <Tooltip title="Bold">
            <div className={classes.tooltipInner}>
              <EditorButton {...this.props.boldProps}>
                <FormatBold fontSize={'small'} />
              </EditorButton>
            </div>
          </Tooltip>
          <Tooltip title="Italic">
            <div className={classes.tooltipInner}>
              <EditorButton {...this.props.italicProps}>
                <FormatItalic fontSize={'small'} />
              </EditorButton>
            </div>
          </Tooltip>
        </div>
        <div className={classes.group}>
          <Tooltip title="Numbered list">
            <div className={classes.tooltipInner}>
              <EditorButton {...this.props.numberedListProps}>
                <FormatListNumbered fontSize={'small'} />
              </EditorButton>
            </div>
          </Tooltip>
          <Tooltip title="Bulleted list">
            <div className={classes.tooltipInner}>
              <EditorButton {...this.props.bulletedListProps}>
                <FormatListBulleted fontSize={'small'} />
              </EditorButton>
            </div>
          </Tooltip>
        </div>
        <div className={classes.group}>
          <Tooltip title="Embed video, tweet...">
            <div className={classes.tooltipInner}>
              <EditorButton {...this.props.embedProps}>
                <AddCircleOutline fontSize={'small'} />
              </EditorButton>
            </div>
          </Tooltip>
          <Tooltip title="Embed image">
            <div className={classes.tooltipInner}>
              <EditorButton {...this.props.imageProps}>
                <AddAPhoto fontSize={'small'} />
              </EditorButton>
            </div>
          </Tooltip>
          <Tooltip title="Quote">
            <div className={classes.tooltipInner}>
              <EditorButton {...this.props.quoteProps}>
                <FormatQuote fontSize={'small'} />
              </EditorButton>
            </div>
          </Tooltip>
        </div>
      </div>
    )
  }
}

export default withStyles(styles)(EditorToolbar)
