import React from 'react'
import { createStyles, Theme, withStyles, WithStyles } from '@material-ui/core'
import ImageDropZoneField, {
  IFileAsDropZone,
  toGraphqlFile
} from '../utils/imageDropZoneField'
import UploadImageForDraft, {
  UploadImageForDraftFailureReason
} from '../mutation/UploadImageForDraft'
import { GraphqlFile } from '../../graphql/schema/GraphqlFile'
import { getFormatedImageUrl } from '../../services/imageService'
import TextareaAutosize from 'react-textarea-autosize'
import { imageCaptionStyle } from '../article/ArticleGlobalStyles'

const styles = (theme: Theme) =>
  createStyles({
    rootWithoutImage: {
      width: '400px',
      height: '200px',
      margin: 'auto'
    },
    rootWithImage: {
      margin: 'auto',
      maxWidth: '100%',
      maxHeight: '600px'
    },
    captionTextArea: {
      resize: 'none', // Trick to remove manual resize button and let autosize function work
      border: 'none',
      outline: 'none',
      backgroundColor: 'transparent',
      overflow: 'hidden', // Trick to remove manual resize button and let autosize function work
      width: '100%',
      ...imageCaptionStyle(theme)
    }
  })

interface IProps extends WithStyles<typeof styles> {
  draftUri: string
  imageUrl: string | null
  captionText: string | null
  onCaptionTextChanged: (captionText: string | null) => void
  onImageUploaded: (url: string) => void
  onImagedRemoved: () => void
}

interface IState {
  imageWaitingForUpload: IFileAsDropZone | null
  imageUploadFailed: boolean
  captionText: string | null
}

class ImageDropZoneTextEditor extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    this.state = {
      imageWaitingForUpload: null,
      imageUploadFailed: false,
      captionText: props.captionText
    }
  }

  public render() {
    const { classes, imageUrl, draftUri } = this.props

    const imageToDisplay = this.state.imageWaitingForUpload
      ? this.state.imageWaitingForUpload
      : imageUrl
        ? { type: 'IFileAsUrl', fileUrl: imageUrl }
        : null

    return (
      <UploadImageForDraft
        draftUri={draftUri}
        onImageUploaded={this.onImageUploaded}
        onFailure={this.onFailure}
      >
        {(uploadFile: (imageFile: GraphqlFile) => void) => (
          <>
            <div
              className={
                imageToDisplay
                  ? classes.rootWithImage
                  : classes.rootWithoutImage
              }
            >
              <ImageDropZoneField
                displayLoading={this.state.imageWaitingForUpload}
                onImageFileDropped={this.onImageFileDropped(uploadFile)}
                imageFile={imageToDisplay}
                messageInDropZone={
                  this.state.imageUploadFailed
                    ? 'Image upload failed. please try again'
                    : 'Drag-and-drop or select you image'
                }
              />
            </div>
            {imageToDisplay && (
              <TextareaAutosize
                className={classes.captionTextArea}
                placeholder="Type caption for image (optional)"
                rows={1}
                maxRows={1}
                onChange={this.onCaptionTextChange}
                onKeyPress={this.onCaptionKeyPress}
                value={
                  this.state.captionText ? this.state.captionText : undefined
                }
                onClick={this.onClick}
                onBlur={this.onBlur}
              />
            )}
          </>
        )}
      </UploadImageForDraft>
    )
  }

  private onCaptionKeyPress = (event: any) => {
    if (event.key === 'Enter') {
      this.props.onCaptionTextChanged(this.state.captionText)
      event.preventDefault()
    }
  }

  private onCaptionTextChange = (event: any) => {
    this.setState({ captionText: event.target.value })
  }

  // we save caption when textarea loose focus (because it needs a full slatejs block creation to save new state)
  // proper (but more complex) solution would be to have 3 node types, a "figure" with two child: image and caption
  private onBlur = () => {
    if (this.state.captionText !== this.props.captionText) {
      this.props.onCaptionTextChanged(this.state.captionText)
    }
  }

  // As for EmbedInput: prevent lost of focus slateJS editor
  private onClick = (event: any) => {
    event.stopPropagation()
  }

  private onImageUploaded = (imageUrl: string) => {
    this.setState({ imageUploadFailed: false })

    const imageUrlScaled = getFormatedImageUrl(
      imageUrl,
      'c_limit,f_auto,q_auto,w_1200'
    )

    this.props.onImageUploaded(imageUrlScaled)
  }

  private onFailure = (_: UploadImageForDraftFailureReason) => {
    this.setState({ imageWaitingForUpload: null, imageUploadFailed: true })
  }

  private onImageFileDropped = (
    uploadFile: (imageFile: GraphqlFile) => void
  ) => async (imageFile: IFileAsDropZone | null) => {
    if (imageFile == null) {
      this.setState({ imageWaitingForUpload: null, imageUploadFailed: false })
      this.props.onImagedRemoved()
    } else {
      this.setState({
        imageWaitingForUpload: imageFile,
        imageUploadFailed: false
      })
      const graphqFile = await toGraphqlFile(imageFile)
      uploadFile(graphqFile)
    }
  }
}

export default withStyles(styles)(ImageDropZoneTextEditor)
