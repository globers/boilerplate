import React from 'react'
import { createStyles, withStyles, WithStyles } from '@material-ui/core'
import Button from '@material-ui/core/Button/Button'
import DeleteForever from '@material-ui/icons/DeleteForever'
import { embedlyKey } from '../../services/environment'

const styles = () =>
  createStyles({
    deleteButton: {
      zIndex: 1,
      marginTop: 10,
      padding: 0,
      top: 0,
      right: 0,
      position: 'absolute'
    },
    deleteIcon: {
      backgroundColor: 'white',
      opacity: 0.8,
      borderRadius: 4
    },
    embedWrap: {
      position: 'relative' // so trash icon which is set with absolute position is set relative to this div
    }
  })

interface IProps extends WithStyles<typeof styles> {
  url: string
  onRemove: () => void
  editMode: boolean
}

class EmbedlyEmbed extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props)
  }

  public render() {
    const { url, classes, onRemove, editMode } = this.props
    return (
      <div className={classes.embedWrap}>
        {editMode && (
          <Button className={classes.deleteButton} onClick={onRemove}>
            <DeleteForever fontSize="large" className={classes.deleteIcon} />
          </Button>
        )}
        <a
          href={url}
          target="_blank"
          rel="noopener noreferrer"
          className="embedly-card"
          data-card-controls={0}
          data-card-width={'100%'}
          data-card-recommend={0}
          data-card-key={embedlyKey}
        >
          {url}
        </a>
      </div>
    )
  }
}

export default withStyles(styles)(EmbedlyEmbed)
