// @ts-ignore
import { Editor, findDOMNode, findDOMRange } from 'slate-react'
// @ts-ignore
import { Block, Change, Mark, Node, Operation, Value } from 'slate'

import React, { RefObject } from 'react'
import ReactDOM from 'react-dom'
// @ts-ignore
import { isKeyHotkey } from 'is-hotkey'
import { Theme, withStyles, WithStyles } from '@material-ui/core'
import { contentStyle } from '../article/ArticleGlobalStyles'
import EditorToolbar from './EditorToolbar'
import {
  getAbsolutePosition,
  getSelectedAnchorElt,
  getSelectionDOMElement
} from './domTools'
import { getEltFromMark, getEltFromNode } from './slateToHtml'
import HyperlinkInputPopper from './HyperlinkInputPopper'
import {
  HyperlinkDataFields,
  IHyperlinkData,
  MarkType,
  NodeType,
  schema
} from './slateSchema'

const DEFAULT_NODE = NodeType.Paragraph

interface IState {
  value: Value
  editorDOMElement:
    | any
    | null /* wee keep the editor DOM Element to position toolbar inside it*/
}

const styles = (theme: Theme) => ({
  root: {
    '@global': contentStyle(theme)
  },
  toolbarWrapper: {
    position: 'absolute' as 'absolute',
    userSelect: 'none' as 'none' // to prevent toolbar from being "selected" in window.selection because it causes toolar
    // to jump ouside as toolbar is above editor...But it doesn't seem to work...
  }
})

interface IProps extends WithStyles<typeof styles> {
  draftUri: string
  initialValue: Value
  onValueChanged: (slatejsValue: Value) => void
}

class TextEditor extends React.Component<IProps, IState> {
  private readonly editorRef: RefObject<Editor>

  constructor(props: IProps) {
    super(props)

    this.state = {
      value: props.initialValue,
      editorDOMElement: null // at the begining, editor is not created yet, we wait for DOM initialisation
    }
    this.editorRef = React.createRef()
  }

  public render() {
    let renderMenu = this.state.value.selection.isFocused // we render menu only if editor has focus

    let menuPosition
    if (renderMenu) {
      const editorPosition = getAbsolutePosition(this.state.editorDOMElement)
      const selectionElt = getSelectionDOMElement()
      if (selectionElt) {
        const selectionPosition = getAbsolutePosition(selectionElt)
        menuPosition = {
          top: selectionPosition.top - 110, // TO modify when menu is modified...
          left: editorPosition.left - 110 // TO modify when menu is modified...
        }
      } else {
        // prevent a bug on chrome:
        // https://stackoverflow.com/questions/22935320/uncaught-indexsizeerror-failed-to-execute-getrangeat-on-selection-0-is-not
        renderMenu = false
      }
    }

    let hoveringMenuAnchorElt = null
    let selectedLinkText: string | null = null
    if (
      this.state.value.selection.isExpanded &&
      this.state.value.blocks.size === 1
    ) {
      hoveringMenuAnchorElt = getSelectedAnchorElt()
      if (hoveringMenuAnchorElt) {
        selectedLinkText = this.getFirstHrefIfSelectedTextContainsLink()
      }
    }

    return (
      <div className={this.props.classes.root}>
        <Editor
          id="editor_text"
          spellCheck={true}
          autoFocus={false}
          placeholder="Write your article..."
          ref={this.editorRef}
          value={this.state.value}
          onChange={this.onChange}
          onKeyDown={this.onKeyDown}
          renderNode={this.renderNode}
          renderMark={this.renderMark}
          schema={schema}
        />
        {renderMenu &&
          ReactDOM.createPortal(
            <div
              className={this.props.classes.toolbarWrapper}
              style={menuPosition}
            >
              <EditorToolbar
                boldProps={this.markButtonProps(MarkType.bold)}
                header2Props={this.blockNotListButtonProps(
                  NodeType.headingThree
                )}
                underlinedProps={this.markButtonProps(MarkType.underlined)}
                italicProps={this.markButtonProps(MarkType.italic)}
                numberedListProps={this.blockListButtonProps(
                  NodeType.numberedList
                )}
                quoteProps={this.blockNotListButtonProps(NodeType.blockQuote)}
                bulletedListProps={this.blockListButtonProps(
                  NodeType.bulletedList
                )}
                headerProps={this.blockNotListButtonProps(NodeType.headingTwo)}
                imageProps={this.addImageButtonProps()}
                embedProps={this.embedButtonProps()}
              />
            </div>,
            this.state.editorDOMElement
          )}
        {hoveringMenuAnchorElt && (
          <HyperlinkInputPopper
            anchorElement={hoveringMenuAnchorElt}
            selectedLinkText={selectedLinkText}
            onLinkAdded={this.onLinkAdded}
            onLinkRemoved={this.onLinkRemoved}
          />
        )}
      </div>
    )
  }

  // after DOM is mounted, we can get DOM element of editor component
  public componentDidMount() {
    this.setState({
      editorDOMElement: ReactDOM.findDOMNode(this.editorRef.current)
    })
  }

  private markButtonProps = (type: MarkType) => ({
    onClick: (event: any) => {
      event.preventDefault()
      this.editorRef.current.toggleMark(type)
    },
    isActive: this.state.value.activeMarks.some(
      (mark: Mark) => mark.type === type
    )
  })

  private selectedBlocksContainsBlockOfType = (type: NodeType) => {
    return this.state.value.blocks.some((node: Node) => node.type === type)
  }

  private getFirstHrefIfSelectedTextContainsLink = (): string | null => {
    const value = this.state.value
    const inlineNode = value.inlines.find(
      (node: Node) => node.type === NodeType.hyperlink
    )
    if (inlineNode) {
      return inlineNode.data.get(HyperlinkDataFields.href)
    } else {
      return null
    }
  }

  private blockListButtonProps = (type: NodeType) => {
    // get the parent of the first selected block

    let parent = null
    if (this.state.value.blocks.size > 0) {
      parent = this.state.value.document.getParent(
        this.state.value.blocks.first().key
      )
    }

    return {
      isActive:
        this.selectedBlocksContainsBlockOfType(NodeType.listItem) &&
        parent &&
        parent.type === type,
      onClick: (event: any) => this.onBlockListButtonClick(event, type)
    }
  }

  private blockNotListButtonProps = (type: NodeType) => ({
    isActive: this.selectedBlocksContainsBlockOfType(type),
    onClick: (event: any) => this.onBlockNotListButtonClick(event, type)
  })

  private addImageButtonProps = () => ({
    isActive: this.selectedBlocksContainsBlockOfType(NodeType.image),
    onClick: this.onVoidBlockButtonCLick(NodeType.image, {
      url: null,
      captionText: null
    })
  })

  private embedButtonProps = () => ({
    isActive: this.selectedBlocksContainsBlockOfType(NodeType.embed),
    onClick: this.onVoidBlockButtonCLick(NodeType.embed, { url: null })
  })

  private onVoidBlockButtonCLick = (nodeType: NodeType, data: any) => (
    event: any
  ) => {
    event.preventDefault()
    const isActive = this.selectedBlocksContainsBlockOfType(nodeType)

    if (!isActive) {
      // if current line is empty, we setBlock instead of insertBlock
      const currentSelectionContainsText = this.state.value.blocks.some(
        (node: Node) => node.text
      )

      if (currentSelectionContainsText) {
        this.editorRef.current.insertBlock({
          type: nodeType,
          data
        })
      } else {
        // empty line
        this.editorRef.current.setBlocks({
          type: nodeType,
          data
        })
      }
    } else {
      this.editorRef.current.setBlocks(DEFAULT_NODE)
    }
  }

  private onLinkAdded = (url: string) => {
    const editor = this.editorRef.current

    const data: IHyperlinkData = {
      href: url
    }

    editor
      .wrapInline({
        type: NodeType.hyperlink,
        data
      })
      .moveToEnd()
      .focus()
  }

  private onLinkRemoved = () => {
    this.editorRef.current
      .unwrapInline(NodeType.hyperlink)
      .moveToEnd()
      .focus()
  }

  private renderNode = (props: any, _: Editor, next: () => any) => {
    const { attributes, children, node } = props

    const nodeType: NodeType = node.type
    return (
      getEltFromNode(node, nodeType, children, attributes, {
        editorRef: this.editorRef,
        draftUri: this.props.draftUri
      }) || next()
    )
  }

  private renderMark = (props: any, _: Editor, next: () => any) => {
    const { children, mark, attributes } = props

    const markType: MarkType = mark.type

    return getEltFromMark(markType, children, attributes) || next()
  }

  private onChange = (change: Change) => {
    this.setState({
      value: change.value
    })

    // We trigger on change only if some operations are not "set_selection"
    if (change.operations.find((o: Operation) => o.type !== 'set_selection')) {
      this.props.onValueChanged(change.value)
    }

    // On new block, remove all marks and make new blocks "paragraph" (except if we are building a list)
    if (
      change.operations.find(
        (o: Operation) => o.type === 'insert_node' || o.type === 'split_node'
      )
    ) {
      this.editorRef.current
        .removeMark(MarkType.italic)
        .removeMark(MarkType.bold)
        .removeMark(MarkType.underlined)

      // if we just inserted a new node but the current selected node is a list, we want to keep the new as a list
      // of the same type
      // Nb: ideally we would check for this directly on the above "find" command by I don't know why
      // the "node" field on operation is sometime undefined so we can't check it's type
      const isList = this.selectedBlocksContainsBlockOfType(NodeType.listItem)
      // if we just inserted a image or embed, we want to keep it !
      // TODO we should use or more clean way (because we need to add each node that can be inserted on purpose...)
      const isImage = this.selectedBlocksContainsBlockOfType(NodeType.image)
      const isEmbed = this.selectedBlocksContainsBlockOfType(NodeType.embed)

      if (!isList && !isImage && !isEmbed) {
        this.editorRef.current.setBlocks(DEFAULT_NODE)
      }
    }
  }

  /**
   * Manage shortcuts (ctrl+B...)
   */
  private onKeyDown = (event: any, editor: Editor, next: () => any) => {
    let mark

    if (isKeyHotkey('mod+b')(event)) {
      mark = MarkType.bold
    } else if (isKeyHotkey('mod+i')(event)) {
      mark = MarkType.italic
    } else if (isKeyHotkey('mod+u')(event)) {
      mark = MarkType.underlined
    } else {
      return next()
    }

    event.preventDefault()
    editor.toggleMark(mark)
  }

  /**
   * When a block button is clicked, toggle the block type.
   */
  private onBlockNotListButtonClick = (event: any, type: NodeType) => {
    event.preventDefault()

    const isActive = this.selectedBlocksContainsBlockOfType(type)
    const isList = this.selectedBlocksContainsBlockOfType(NodeType.listItem)

    if (isList) {
      this.editorRef.current
        .setBlocks(isActive ? DEFAULT_NODE : type)
        .unwrapBlock(NodeType.bulletedList)
        .unwrapBlock(NodeType.numberedList)
    } else {
      this.editorRef.current.setBlocks(isActive ? DEFAULT_NODE : type)
    }
  }

  private onBlockListButtonClick = (event: any, type: NodeType) => {
    event.preventDefault()

    const editor = this.editorRef.current
    const value = editor.value
    const { document } = value

    // list buttons
    // Handle the extra wrapping required for list buttons.
    const isList = this.selectedBlocksContainsBlockOfType(NodeType.listItem)
    const isType = value.blocks.some((block: Block) => {
      return !!document.getClosest(
        block.key,
        (parent: Node) => parent.type === type
      )
    })

    if (isList && isType) {
      editor
        .setBlocks(DEFAULT_NODE)
        .unwrapBlock(NodeType.bulletedList)
        .unwrapBlock(NodeType.numberedList)
    } else if (isList) {
      editor
        .unwrapBlock(
          type === NodeType.bulletedList
            ? NodeType.numberedList
            : NodeType.bulletedList
        )
        .wrapBlock(type)
    } else {
      editor.setBlocks(NodeType.listItem).wrapBlock(type)
    }
  }
}

export default withStyles(styles)(TextEditor)
