import React, { ReactNode } from 'react'
import { withStyles, WithStyles } from '@material-ui/core'
import IconButton from '@material-ui/core/IconButton/IconButton'

const styles = () => ({
  root: {
    padding: 4
  }
})

interface IProps extends WithStyles<typeof styles> {
  children: ReactNode
  isActive: boolean
  onClick: (event: any) => void
}

class EditorButton extends React.PureComponent<IProps> {
  constructor(props: IProps) {
    super(props)
  }

  public render() {
    const { classes, children, isActive, onClick } = this.props
    return (
      <IconButton
        onClick={onClick}
        color={isActive ? 'primary' : 'default'}
        className={classes.root}
      >
        {children}
      </IconButton>
    )
  }
}

export default withStyles(styles)(EditorButton)
