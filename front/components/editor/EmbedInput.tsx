import React from 'react'
import {
  createStyles,
  InputAdornment,
  Theme,
  withStyles,
  WithStyles
} from '@material-ui/core'
import TextField from '@material-ui/core/TextField/TextField'
import { Close, Done } from '@material-ui/icons'
import IconButton from '@material-ui/core/IconButton/IconButton'

const styles = (theme: Theme) =>
  createStyles({
    urlField: {
      width: '500px',
      borderStyle: 'solid',
      borderWidth: '1px',
      borderColor: '#eaeaea',
      borderRadius: 50,
      padding: 5,
      paddingLeft: 20,
      paddingRight: 20,
      backgroundColor: theme.palette.background.default
    }
  })

interface IState {
  url: string | null
}

interface IProps extends WithStyles<typeof styles> {
  onLinkAdded: (url: string) => void
  onRemove: () => void
}

class EmbedInput extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    this.state = {
      url: null
    }
  }

  // NB: we should take focus on add (don't found how to do it easily)
  public render() {
    const { classes } = this.props
    return (
      <div className={classes.urlField}>
        <TextField
          label="Paste a link to embed a video, tweet..."
          placeholder="https://www.youtube.com/mycoolvideo"
          fullWidth={true}
          onChange={this.onChange}
          autoFocus={true}
          onKeyPress={this.onKeyPress}
          margin="dense"
          onClick={this.onClick}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                {isValidUrl(this.state.url) && (
                  <IconButton
                    onClick={this.onValidateHyperlink}
                    aria-label="Add hyperlink"
                    color="primary"
                  >
                    <Done />
                  </IconButton>
                )}
                <IconButton
                  onClick={this.props.onRemove}
                  aria-label="Remove hyperlink"
                  color="secondary"
                >
                  <Close />
                </IconButton>
              </InputAdornment>
            )
          }}
        />
      </div>
    )
  }

  private onKeyPress = (event: any) => {
    if (event.key === 'Enter') {
      if (isValidUrl(this.state.url)) {
        this.onValidateHyperlink()
      }
    }
  }

  // to prevent bug with slateJS: focus is is lost if we don't do it
  // https://github.com/ianstormtaylor/slate/issues/2504
  private onClick = (event: any) => {
    event.stopPropagation()
  }

  private onChange = (event: any) => this.setState({ url: event.target.value })

  private onValidateHyperlink = () => {
    this.props.onLinkAdded(this.state.url as string)
  }
}

function isValidUrl(url: string | null): boolean {
  return !!url && isValidUrlRegex.test(url)
}

const isValidUrlRegex = /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.​\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[​6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1​,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00​a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u​00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?$/i

export default withStyles(styles)(EmbedInput)
