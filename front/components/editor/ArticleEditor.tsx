import React from 'react'
import TextEditor from './TextEditor'
import ImageDropZoneField, {
  IFileAsDropZone,
  ImageFile
} from '../utils/imageDropZoneField'
import { createStyles, Theme, withStyles, WithStyles } from '@material-ui/core'
// @ts-ignore
import { Value } from 'slate'
import {
  articleTitleAndContentLayout,
  rootLayout,
  titleStyle,
  subtitleStyle,
  articleHeaderImgStyle
} from '../article/ArticleGlobalStyles'
import Textarea from 'react-textarea-autosize'

const styles = (theme: Theme) =>
  createStyles({
    root: rootLayout,
    logoMediaImg: {
      maxWidth: '100%',
      maxHeight: '100%',
      margin: 'auto',
      display: 'block'
    },
    imgWrapperWithImage: articleHeaderImgStyle,
    imgWrapperNoImage: {
      width: '600px',
      maxWidth: '100%',
      height: '200px',
      margin: 'auto'
    },
    articleTitleAndContent: articleTitleAndContentLayout,
    titleTextArea: {
      resize: 'none', // Trick to remove manual resize button and let autosize function work
      border: 'none',
      outline: 'none',
      backgroundColor: 'transparent',
      overflow: 'hidden', // Trick to remove manual resize button and let autosize function work
      width: '100%',
      ...titleStyle(theme)
    },
    subtitleTextArea: {
      resize: 'none', // Trick to remove manual resize button and let autosize function work
      border: 'none',
      outline: 'none',
      backgroundColor: 'transparent',
      overflow: 'hidden', // Trick to remove manual resize button and let autosize function work
      width: '100%',
      ...subtitleStyle(theme)
    }
  })

interface IProps extends WithStyles<typeof styles> {
  draftUri: string
  title: string | null
  subtitle: string | null
  articleImage: ImageFile | null
  initialSlatejsValue: Value

  onTitleChanged: (title: string | null) => void
  onSubtitleChanged: (subtitle: string | null) => void
  onArticleImageChanged: (articleImage: IFileAsDropZone | null) => void
  onSlatejsValueChanged: (slatejsValue: Value | null) => void
}

class ArticleEditor extends React.Component<IProps> {
  public render() {
    const {
      classes,
      title,
      subtitle,
      articleImage,
      initialSlatejsValue
    } = this.props

    return (
      <div className={classes.root}>
        <div
          className={
            articleImage
              ? classes.imgWrapperWithImage
              : classes.imgWrapperNoImage
          }
        >
          <ImageDropZoneField
            onImageFileDropped={this.props.onArticleImageChanged}
            imageFile={articleImage}
            messageInDropZone="Article image. Should be in 3:1 format and at least 1200px width"
          />
        </div>
        <div className={classes.articleTitleAndContent}>
          <Textarea
            className={classes.titleTextArea}
            placeholder="Title"
            rows={1}
            onChange={this.onTitleChange}
            value={title || undefined}
          />

          <Textarea
            className={classes.subtitleTextArea}
            placeholder="Subtitle (short summary or intro)"
            rows={1}
            onChange={this.onSubtitleChange}
            value={subtitle || undefined}
          />

          <TextEditor
            draftUri={this.props.draftUri}
            initialValue={initialSlatejsValue}
            onValueChanged={this.props.onSlatejsValueChanged}
          />
        </div>
      </div>
    )
  }

  private onTitleChange = (e: any) => {
    this.props.onTitleChanged(e.target.value)
  }
  private onSubtitleChange = (e: any) => {
    this.props.onSubtitleChanged(e.target.value)
  }
}

export default withStyles(styles)(ArticleEditor)
