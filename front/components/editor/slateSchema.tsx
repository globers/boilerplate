// @ts-ignore
import { Value, Block, Node } from 'slate'
// @ts-ignore
import { Editor } from 'slate-react'

export const emptyValue = Value.fromJSON({
  document: {
    nodes: [
      {
        object: 'block',
        type: 'paragraph',
        nodes: [
          {
            object: 'text',
            leaves: [
              {
                text: ''
              }
            ]
          }
        ]
      }
    ]
  }
})

export enum NodeType {
  Paragraph = 'paragraph',
  headingTwo = 'heading-two',
  headingThree = 'heading-three',
  blockQuote = 'block-quote',
  numberedList = 'numbered-list',
  bulletedList = 'bulleted-list',
  listItem = 'list-item',
  hyperlink = 'hyperlink',
  image = 'image',
  embed = 'embed'
}

export enum MarkType {
  bold = 'bold',
  italic = 'italic',
  underlined = 'underlined'
}

export interface IHyperlinkData {
  href: string
}

export enum HyperlinkDataFields {
  href = 'href'
}

export enum ImageDataFields {
  url = 'url',
  captionText = 'captionText'
}

export enum EmbedDataFields {
  url = 'url'
}

// https://docs.slatejs.org/slate-core/schema#errors
interface ISlateError {
  code: string
  node: Node
  child: Node | undefined
  rule: any
}

export const schema = {
  document: {
    // last rule: last elt must be paragraph (so we can continue to write if image is added at end)
    last: { type: 'paragraph' },
    normalize: (editor: Editor, { code, node }: ISlateError) => {
      switch (code) {
        // last_child_type_invalid error can be thrown because we specified a "last" rule above
        case 'last_child_type_invalid': {
          const paragraph = Block.create(NodeType.Paragraph)
          return editor.insertNodeByKey(node.key, node.nodes.size, paragraph)
        }
      }
    }
  },
  blocks: {
    image: {
      isVoid: true // instruct slate that there is no node child (text child) to be managed by slate
    },
    embed: {
      isVoid: true
    }
  }
}
