// https://stackoverflow.com/questions/22935320/uncaught-indexsizeerror-failed-to-execute-getrangeat-on-selection-0-is-not
// https://github.com/ianstormtaylor/slate/issues/308
// https://gist.github.com/ianstormtaylor/7924e6abc71c1efa40411a4553afe4dc
function getSelectionDOMElement(): Element | Range | null {
  const native = window.getSelection()
  if (native && native.rangeCount > 0) {
    return native.getRangeAt(0)
  } else {
    return null
  }
}

function getSelectionBoundingClientRect() {
  const elt = getSelectionDOMElement()
  if (elt) {
    return elt.getBoundingClientRect()
  } else {
    return null
  }
}

function getSelectedAnchorElt() {
  return getAnchorElt(getSelectionBoundingClientRect)
}

// Return anchorElt as required by material-UI for popper and popover
// see Faked reference object at https://material-ui.com/utils/popper/
function getAnchorElt(
  getBoundingClientRect: () => ClientRect | DOMRect | null
) {
  const rect = getBoundingClientRect()
  if (rect) {
    return {
      clientWidth: rect.width,
      clientHeight: rect.height,
      getBoundingClientRect
    }
  } else {
    return null
  }
}

// https://plainjs.com/javascript/styles/get-the-position-of-an-element-relative-to-the-document-24/
function getAbsolutePosition(domElt: Element | Range) {
  const rect = domElt.getBoundingClientRect()
  const scrollLeft = window.pageXOffset || document.documentElement.scrollLeft
  const scrollTop = window.pageYOffset || document.documentElement.scrollTop

  return {
    top: rect.top + scrollTop,
    left: rect.left + scrollLeft
  }
}

export { getAbsolutePosition, getSelectionDOMElement, getSelectedAnchorElt }
