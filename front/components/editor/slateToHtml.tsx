import {
  EmbedDataFields,
  HyperlinkDataFields,
  ImageDataFields,
  MarkType,
  NodeType
} from './slateSchema'
import React, { ReactNode, RefObject } from 'react'
// @ts-ignore
import { Block, Node } from 'slate'
// @ts-ignore
import { Editor } from 'slate-react'
import ImageDropZoneTextEditor from './ImageDropZoneTextEditor'
import EmbedInput from './EmbedInput'
import EmbedlyEmbed from './EmbedlyEmbed'

interface IEditorContext {
  editorRef: RefObject<Editor>
  draftUri: string
}

export function getEltFromNode(
  node: Node,
  nodeType: NodeType,
  children: ReactNode,
  attributes: any | null,
  editorContext: IEditorContext | null
) {
  function onNodeRemoved() {
    const context = editorContext as IEditorContext
    context.editorRef.current.removeNodeByKey(node.key)
  }

  function onImageUploaded(uploadedImageUrl: string) {
    const context = editorContext as IEditorContext
    context.editorRef.current.replaceNodeByKey(
      node.key,
      Block.create({
        type: NodeType.image,
        data: {
          url: uploadedImageUrl
        }
      })
    )
  }

  function onImageCaptionChanged(captionText: string | null) {
    const context = editorContext as IEditorContext
    const url = node.data.get(ImageDataFields.url)
    context.editorRef.current.replaceNodeByKey(
      node.key,
      Block.create({
        type: NodeType.image,
        data: {
          url,
          captionText
        }
      })
    )
  }

  function onEmbedLinkAdded(url: string) {
    const context = editorContext as IEditorContext
    context.editorRef.current.replaceNodeByKey(
      node.key,
      Block.create({
        type: NodeType.embed,
        data: {
          url
        }
      })
    )
  }

  switch (nodeType) {
    case NodeType.blockQuote:
      return <blockquote {...attributes}>{children}</blockquote>
    case NodeType.bulletedList:
      return <ul {...attributes}>{children}</ul>
    case NodeType.headingTwo:
      return <h2 {...attributes}>{children}</h2>
    case NodeType.headingThree:
      return <h3 {...attributes}>{children}</h3>
    case NodeType.listItem:
      return <li {...attributes}>{children}</li>
    case NodeType.numberedList:
      return <ol {...attributes}>{children}</ol>
    case NodeType.Paragraph:
      return <p {...attributes}>{children}</p>
    case NodeType.hyperlink:
      const href: string = node.data.get(HyperlinkDataFields.href)
      return (
        <a href={href} {...attributes}>
          {children}
        </a>
      )
    case NodeType.image:
      const url = node.data.get(ImageDataFields.url)
      const imageCaptionText = node.data.get(ImageDataFields.captionText)
      if (!editorContext) {
        if (url) {
          return (
            <figure>
              <img key={node.key} {...attributes} src={url} />
              {imageCaptionText && <figcaption>{imageCaptionText}</figcaption>}
            </figure>
          )
        } else {
          return <></>
        }
      } else {
        return (
          <ImageDropZoneTextEditor
            key={node.key}
            {...attributes}
            draftUri={editorContext.draftUri}
            imageUrl={url}
            onImageUploaded={onImageUploaded}
            onImagedRemoved={onNodeRemoved}
            captionText={imageCaptionText}
            onCaptionTextChanged={onImageCaptionChanged}
          />
        )
      }
    case NodeType.embed:
      const embedUrl = node.data.get(EmbedDataFields.url)
      if (embedUrl) {
        return (
          <EmbedlyEmbed
            url={embedUrl}
            onRemove={onNodeRemoved}
            editMode={!!editorContext}
          />
        )
      } else {
        if (!editorContext) {
          return <></>
        } else {
          return (
            <EmbedInput
              key={node.key}
              {...attributes}
              onLinkAdded={onEmbedLinkAdded}
              onRemove={onNodeRemoved}
            />
          )
        }
      }
  }
}

export function getEltFromMark(
  markType: MarkType,
  children: ReactNode,
  attributes: any | null
) {
  switch (markType) {
    case MarkType.bold:
      return <strong {...attributes}>{children}</strong>
    case MarkType.italic:
      return <em {...attributes}>{children}</em>
    case MarkType.underlined:
      return <u {...attributes}>{children}</u>
  }
}
