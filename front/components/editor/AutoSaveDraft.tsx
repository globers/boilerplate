import React from 'react'
import { ImageFile } from '../utils/imageDropZoneField'
// @ts-ignore
import { Value } from 'slate'
// @ts-ignore
import { debounce } from 'debounce'
import {
  IArticleDraftRevisionInput,
  toRevisionInput
} from '../mutation/CreateArticleDraftRevision'

interface IProps {
  createOrUpdateRevision: (input: IArticleDraftRevisionInput) => void
  title: string | null
  subtitle: string | null
  articleImage: ImageFile | null
  slatejsValue: Value
}

class AutoSaveDraft extends React.Component<IProps> {
  private callMutation = debounce(async () => {
    const input = await toRevisionInput(
      this.props.title,
      this.props.subtitle,
      this.props.articleImage,
      this.props.slatejsValue
    )

    this.props.createOrUpdateRevision(input)
  }, 2000)

  public componentDidUpdate() {
    this.callMutation()
  }

  public componentWillUnmount() {
    this.callMutation.clear()
  }

  public shouldComponentUpdate(nextProps: IProps) {
    if (nextProps.title !== this.props.title) {
      return true
    }
    if (nextProps.subtitle !== this.props.subtitle) {
      return true
    }
    if (
      nextProps.articleImage !== this.props.articleImage &&
      (nextProps.articleImage == null ||
        (nextProps.articleImage != null &&
          nextProps.articleImage.type === 'IFileAsDropZone'))
    ) {
      return true
    }
    if (nextProps.slatejsValue !== this.props.slatejsValue) {
      return true
    }
    return false
  }

  public render() {
    return null
  }
}

export default AutoSaveDraft
