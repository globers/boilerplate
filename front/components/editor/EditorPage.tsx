import ArticleEditor from './ArticleEditor'
import * as React from 'react'
// @ts-ignore
import { Value } from 'slate'

import Layout from '../../components/MyLayout'
import AppBar from '../../components/AppBar'
import CreateArticleDraftRevision, {
  SaveArticleDraftRevisionFailureReason,
  IArticleDraftRevision,
  IArticleDraftRevisionInput,
  toRevisionInput
} from '../../components/mutation/CreateArticleDraftRevision'
import { IFileAsDropZone, ImageFile } from '../utils/imageDropZoneField'
import { emptyValue } from './slateSchema'
import PublishArticleFromDraft, {
  PublishArticleFailureReason
} from '../mutation/PublishArticleFromDraft'
import { getArticleUrl } from '../../services/urlService'
import Router from 'next/router'
import { StyleRules, WithStyles } from '@material-ui/core/es/styles'
import withStyles from '@material-ui/core/styles/withStyles'
import UpdateArticleDraftRevision from '../mutation/UpdateArticleDraftRevision'
import AutoSaveDraft from './AutoSaveDraft'
import Button from '@material-ui/core/Button/Button'
import PublishConfirmation from './PublishConfirmation'
import { Dialog } from '@material-ui/core'
import DialogTitle from '@material-ui/core/DialogTitle/DialogTitle'
import DialogActions from '@material-ui/core/DialogActions/DialogActions'
import Typography from '@material-ui/core/Typography/Typography'
import UnpublishArticle from '../mutation/UnpublishArticle'
import LoadingProgress from '../utils/loadingProgress'
import AppBarButton from '../utils/AppBarButton'

const styles: StyleRules = {}

interface IMedia {
  uri: string
  name: string
  logoUrl: string
  themePrimaryColor: string
}

interface IProps extends WithStyles<typeof styles> {
  initialTitle: string | null
  initialSubtitle: string | null
  initialArticleImageUrl: string | null
  initialSlatejsAsJsonStr: string | null
  initialLastSavedRevisionUri: string | null
  initialArticleUriIfPublished: string | null

  media: IMedia
  userFirstName: string
  articleDraftUri: string
}

interface IState {
  title: string | null
  subtitle: string | null
  articleImage: ImageFile | null
  slatejsValue: Value
  articleUriIfPublished: string | null

  errorMessage: string | null
  workingRevisionUri: string | null
  revisionUriToPublish: string | null
  justSaved: boolean
  waitForSaveToPublish: boolean
}

class EditorPage extends React.Component<IProps, IState> {
  private readonly initialSlatejsValue: Value

  constructor(props: IProps) {
    super(props)

    this.initialSlatejsValue = this.props.initialSlatejsAsJsonStr
      ? Value.fromJSON(JSON.parse(this.props.initialSlatejsAsJsonStr))
      : emptyValue

    this.state = {
      title: props.initialTitle,
      subtitle: props.initialSubtitle,
      articleImage: this.props.initialArticleImageUrl
        ? { type: 'IFileAsUrl', fileUrl: this.props.initialArticleImageUrl }
        : null,
      slatejsValue: this.initialSlatejsValue,
      articleUriIfPublished: props.initialArticleUriIfPublished,
      workingRevisionUri: null,
      errorMessage: null,
      revisionUriToPublish: null,
      justSaved: false,
      waitForSaveToPublish: false
    }
  }

  public render() {
    const { title, subtitle, articleImage } = this.state

    return (
      <Layout
        pageTitle={'Article editor - Globers'}
        appBar={
          <AppBar
            isHome={false}
            media={this.props.media}
            userFirstName={this.props.userFirstName}
          >
            {this.state.justSaved && (
              <Typography variant="body2" color="inherit">
                Saved
              </Typography>
            )}
            <UnpublishArticle
              mediaUri={this.props.media.uri}
              articleUri={this.state.articleUriIfPublished as string}
              onArticleUnpublished={this.onArticleUnpublished}
            >
              {this.renderUnpublishButton}
            </UnpublishArticle>
            <CreateArticleDraftRevision
              draftUri={this.props.articleDraftUri}
              onRevisionCreated={this.onRevisionCreatedPublish}
              onRevisionFailure={this.onRevisionFailure}
            >
              {this.renderPublishButton}
            </CreateArticleDraftRevision>
          </AppBar>
        }
      >
        <ArticleEditor
          draftUri={this.props.articleDraftUri}
          articleImage={articleImage}
          initialSlatejsValue={this.initialSlatejsValue}
          title={title}
          subtitle={subtitle}
          onArticleImageChanged={this.onArticleImageChanged}
          onSlatejsValueChanged={this.onSlatejsValueChanged}
          onSubtitleChanged={this.onSubtitleChanged}
          onTitleChanged={this.onTitleChanged}
        />

        {this.state.workingRevisionUri ? (
          <UpdateArticleDraftRevision
            draftRevisionUri={this.state.workingRevisionUri}
            onRevisionUpdated={this.onRevisionUpdated}
            onRevisionFailure={this.onRevisionFailure}
          >
            {this.registerCreateOrUpdateOnSave}
          </UpdateArticleDraftRevision>
        ) : (
          <CreateArticleDraftRevision
            draftUri={this.props.articleDraftUri}
            onRevisionCreated={this.onRevisionCreated}
            onRevisionFailure={this.onRevisionFailure}
          >
            {this.registerCreateOrUpdateOnSave}
          </CreateArticleDraftRevision>
        )}

        <Dialog
          open={this.state.errorMessage != null}
          aria-labelledby="alert-dialog-error"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-error">
            {this.state.errorMessage || ''}
          </DialogTitle>
          <DialogActions>
            <Button onClick={this.onOkMessageError} color="primary">
              Ok
            </Button>
          </DialogActions>
        </Dialog>

        <PublishArticleFromDraft
          onArticlePublished={this.onArticlePublished}
          onPublishArticleFailure={this.onPublishArticleFailure}
          mediaUri={this.props.media.uri}
        >
          {(publishArticle: (articleDraftRevisionUri: string) => void) => (
            <PublishConfirmation
              publishArticle={this.onConfirmPublish(publishArticle)}
              onClose={this.onCancelPublish}
              open={this.state.revisionUriToPublish != null}
            />
          )}
        </PublishArticleFromDraft>
      </Layout>
    )
  }

  private onOkMessageError = () => {
    this.setState({ errorMessage: null })
  }

  private onArticleImageChanged = (articleImage: IFileAsDropZone | null) =>
    this.setState({
      articleImage,
      errorMessage: null,
      justSaved: false
    })

  private onSlatejsValueChanged = (slatejsValue: Value | null) =>
    this.setState({
      slatejsValue,
      errorMessage: null,
      justSaved: false
    })
  private onSubtitleChanged = (subtitle: string | null) =>
    this.setState({
      subtitle,
      errorMessage: null,
      justSaved: false
    })
  private onTitleChanged = (title: string | null) =>
    this.setState({
      title,
      errorMessage: null,
      justSaved: false
    })

  private onCancelPublish = () => {
    this.setState({ revisionUriToPublish: null })
  }

  private onArticleUnpublished = (unpublished: boolean) => {
    if (unpublished) {
      this.setState({ articleUriIfPublished: null })
    }
  }

  private onConfirmPublish = (
    publishArticle: (articleRevisionUri: string) => void
  ) => () => {
    publishArticle(this.state.revisionUriToPublish as string)
  }

  private registerCreateOrUpdateOnSave = (
    createOrUpdateRevision: (input: IArticleDraftRevisionInput) => void
  ) => {
    return (
      <AutoSaveDraft
        createOrUpdateRevision={createOrUpdateRevision}
        title={this.state.title}
        subtitle={this.state.subtitle}
        articleImage={this.state.articleImage}
        slatejsValue={this.state.slatejsValue}
      />
    )
  }

  private renderPublishButton = (
    createRevision: (input: IArticleDraftRevisionInput) => void
  ) => {
    return (
      <div>
        <AppBarButton
          variant="outlined"
          onClick={this.onPublishButtonClick(createRevision)}
          size="small"
          disabled={this.state.waitForSaveToPublish}
          themePrimaryColor={this.props.media.themePrimaryColor}
        >
          {this.state.waitForSaveToPublish && <LoadingProgress />}
          Publish
        </AppBarButton>
      </div>
    )
  }

  private renderUnpublishButton = (unpublishArticle: (() => void)) => (
    <AppBarButton
      variant="outlined"
      onClick={unpublishArticle}
      disabled={this.state.articleUriIfPublished == null}
      size="small"
      themePrimaryColor={this.props.media.themePrimaryColor}
    >
      {this.state.articleUriIfPublished ? 'Unpublish' : 'Unpublished'}
    </AppBarButton>
  )

  private onPublishButtonClick = (
    createRevision: (input: IArticleDraftRevisionInput) => void
  ) => async () => {
    let errorMessage = null
    if (!this.state.title) {
      errorMessage = 'Add a title before publishing'
    } else if (!this.state.subtitle) {
      errorMessage = 'Add a subtitle before publishing'
    } else if (!this.state.articleImage) {
      errorMessage = 'Choose an image for article before publishing'
    }

    this.setState({
      errorMessage
    })
    if (errorMessage == null) {
      // 1) SAVE, 2) WAIT FOR CALLBACK 3) On CALLBACK => PUBLISH

      this.setState({
        waitForSaveToPublish: true
      })

      const input = await toRevisionInput(
        this.state.title,
        this.state.subtitle,
        this.state.articleImage,
        this.state.slatejsValue
      )
      createRevision(input)
    }
  }

  private onRevisionCreatedPublish = (revision: IArticleDraftRevision) => {
    this.setState({
      errorMessage: null,
      workingRevisionUri: null,
      revisionUriToPublish: revision.uri,
      justSaved: true,
      waitForSaveToPublish: false
    })
  }

  private onRevisionCreated = (revision: IArticleDraftRevision) => {
    this.setState({
      errorMessage: null,
      workingRevisionUri: revision.uri,
      justSaved: true
    })

    this.updateImageIfNeeded(revision.imageUrl)
  }

  private updateImageIfNeeded = (imageUrl: string | null) => {
    const currentImage = this.state.articleImage

    if (
      currentImage !== null &&
      currentImage.type === 'IFileAsDropZone' &&
      imageUrl !== null
    ) {
      this.setState({ articleImage: { type: 'IFileAsUrl', fileUrl: imageUrl } })
    }
  }

  private onRevisionUpdated = (revision: IArticleDraftRevision) => {
    this.setState({
      errorMessage: null,
      justSaved: true
    })

    this.updateImageIfNeeded(revision.imageUrl)
  }

  private onRevisionFailure = (
    reason: SaveArticleDraftRevisionFailureReason
  ) => {
    switch (reason) {
      case SaveArticleDraftRevisionFailureReason.INVALID_ARTICLE_IMAGE:
        this.setState({
          errorMessage: 'Invalid image file',
          waitForSaveToPublish: false
        })
    }
  }

  private onArticlePublished = (articleUri: string) => {
    this.setState({ revisionUriToPublish: null })

    const articleUrl = getArticleUrl(this.props.media.uri, articleUri)
    Router.push(articleUrl.href, articleUrl.as)
  }

  private onPublishArticleFailure = (reason: PublishArticleFailureReason) => {
    switch (reason) {
      case PublishArticleFailureReason.EMPTY_CONTENT:
        this.setState({
          revisionUriToPublish: null,
          errorMessage: 'Article content should not be empty'
        })
        break
      case PublishArticleFailureReason.EMPTY_IMAGE:
        this.setState({
          revisionUriToPublish: null,
          errorMessage: 'Article image should not be empty'
        })
        break
      case PublishArticleFailureReason.EMPTY_SUBTITLE:
        this.setState({
          revisionUriToPublish: null,
          errorMessage: 'Article subtitle should not be empty'
        })
        break
      case PublishArticleFailureReason.EMPTY_TITLE:
        this.setState({
          revisionUriToPublish: null,
          errorMessage: 'Article title should not be empty'
        })
        break
    }
  }
}

export default withStyles(styles)(EditorPage)
