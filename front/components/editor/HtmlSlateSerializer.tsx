// @ts-ignore
import { Mark, Node } from 'slate'
// @ts-ignore
import Html from 'slate-html-serializer'
import { getEltFromMark, getEltFromNode } from './slateToHtml'

const RULES = [
  {
    serialize(
      object: Node | Mark | string,
      children: string | JSX.Element | JSX.Element[]
    ) {
      if (object.object === 'block' || object.object === 'inline') {
        return getEltFromNode(object, object.type, children, true, null)
      } else if (object.object === 'mark') {
        return getEltFromMark(object.type, children, null)
      }
    }
  }
]

export const htmlSerializer = new Html({ rules: RULES })
