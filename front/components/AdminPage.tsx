import React from 'react'
import { createStyles, withStyles, WithStyles } from '@material-ui/core'
import Table from '@material-ui/core/Table/Table'
import TableHead from '@material-ui/core/TableHead/TableHead'
import TableRow from '@material-ui/core/TableRow/TableRow'
import TableCell from '@material-ui/core/TableCell/TableCell'
import TableBody from '@material-ui/core/TableBody/TableBody'

const styles = () =>
  createStyles({
    root: {
      width: 10
    }
  })

interface IState {
  myState: true
}

interface IMedia {
  uri: string
  name: string
  unpublished: boolean
  nbArticles: number
}

interface IContributedMedia {
  uri: string
  media: IMedia
  stripeAccountConnected: boolean
}

interface ICreator {
  uri: string
  contributedMedias: IContributedMedia[]
}

interface IUser {
  __typename: 'User'
  uri: string
  firstName: string
  lastName: string
  email: string
  creator: ICreator
}

export interface IAdmin {
  users: IUser[]
}

interface IProps extends WithStyles<typeof styles> {
  admin: IAdmin
}

class AdminPage extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    this.state = {
      myState: true
    }
  }

  public render() {
    const { admin } = this.props

    return (
      <div>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>First name</TableCell>
              <TableCell>Last name</TableCell>
              <TableCell>Email</TableCell>
              <TableCell>Media</TableCell>
              <TableCell>Url</TableCell>
              <TableCell>Nb articles</TableCell>
              <TableCell>Published</TableCell>
              <TableCell>Bank connected</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {admin.users.map((user: IUser) => {
              if (user.creator) {
                return user.creator.contributedMedias.map(
                  (contribMedia: IContributedMedia) => {
                    return (
                      <TableRow key={user.uri}>
                        <TableCell>{user.firstName}</TableCell>
                        <TableCell>{user.lastName}</TableCell>
                        <TableCell>{user.email}</TableCell>
                        <TableCell>{contribMedia.media.name}</TableCell>
                        <TableCell>
                          {'https://globers.co/m/' + contribMedia.media.uri}
                        </TableCell>
                        <TableCell>{contribMedia.media.nbArticles}</TableCell>
                        <TableCell>
                          {(!contribMedia.media.unpublished).toString()}
                        </TableCell>
                        <TableCell>
                          {contribMedia.stripeAccountConnected.toString()}
                        </TableCell>
                      </TableRow>
                    )
                  }
                )
              } else {
                return (
                  <TableRow key={user.uri}>
                    <TableCell>{user.firstName}</TableCell>
                    <TableCell>{user.lastName}</TableCell>
                    <TableCell>{user.email}</TableCell>
                  </TableRow>
                )
              }
            })}
          </TableBody>
        </Table>
      </div>
    )
  }
}

export default withStyles(styles)(AdminPage)
