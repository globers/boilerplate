import React, { RefObject } from 'react'
import { withStyles } from '@material-ui/core/styles'
import IconButton from '@material-ui/core/IconButton'
import { Menu, MenuItem, WithStyles } from '@material-ui/core'
import { logOffUser } from '../services/userService'
import { AccountCircle } from '@material-ui/icons'
import LoginOrCreateAccountModals, {
  ModalStatus
} from './account/loginOrCreateAccountModals'
import Avatar from '@material-ui/core/Avatar/Avatar'
import deepOrange from '@material-ui/core/colors/deepOrange'
import Router from 'next/router'

const styles = {
  avatar: {
    backgroundColor: deepOrange[500],
    marginRight: '5px'
  },
  avatarAppBar: {
    backgroundColor: deepOrange[500],
    width: 32,
    height: 32
  }
}

interface IProps extends WithStyles<typeof styles> {
  userFirstName: string | null
  isDarkBackground: boolean
}

interface IState {
  loginMenuAnchorEl: HTMLElement | undefined
}

class LoginMenu extends React.Component<IProps, IState> {
  private readonly loginRef: RefObject<LoginOrCreateAccountModals>

  constructor(props: IProps) {
    super(props)
    this.state = {
      loginMenuAnchorEl: undefined
    }

    this.loginRef = React.createRef()
  }

  public render() {
    const { userFirstName, classes } = this.props

    const contrastColor = this.props.isDarkBackground ? 'white' : 'black'

    const loginMenuOpen = Boolean(this.state.loginMenuAnchorEl)
    return (
      <React.Fragment>
        <IconButton
          aria-owns={loginMenuOpen ? 'menu-appbar' : undefined}
          aria-haspopup="true"
          onClick={this.openMenu}
          color="inherit"
          style={{ color: contrastColor }}
        >
          {userFirstName != null ? (
            <Avatar className={classes.avatarAppBar}>
              {userFirstName.charAt(0).toUpperCase()}
            </Avatar>
          ) : (
            <AccountCircle />
          )}
        </IconButton>
        <Menu
          id="menu-appbar"
          anchorEl={this.state.loginMenuAnchorEl}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right'
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'right'
          }}
          open={loginMenuOpen}
          onClose={this.closeMenu}
        >
          {userFirstName != null
            ? [
                <MenuItem key="avatar">
                  <Avatar className={classes.avatar}>
                    {userFirstName.charAt(0).toUpperCase()}
                  </Avatar>
                  {userFirstName}
                </MenuItem>,
                <MenuItem key="editProfile" onClick={this.editProfile}>
                  Edit Profile
                </MenuItem>,
                <MenuItem key="signout" onClick={this.logOff}>
                  Sign out
                </MenuItem>
              ]
            : [
                <MenuItem key="login'" onClick={this.openLogInModal}>
                  Log in
                </MenuItem>,
                <MenuItem key="createaccount" onClick={this.openSignUpModal}>
                  Create account
                </MenuItem>
              ]}
          <MenuItem key="newmedia" onClick={this.openNewMediaPage}>
            New Media
          </MenuItem>
        </Menu>
        <LoginOrCreateAccountModals
          closable={true}
          ref={this.loginRef}
          initialModalStatus={ModalStatus.Closed}
        />
      </React.Fragment>
    )
  }

  private editProfile = () => {
    Router.push('/editUser')
  }

  private openNewMediaPage = () => {
    Router.push('/newMedia')
  }

  private closeMenu = () => {
    this.setState({ loginMenuAnchorEl: undefined })
  }

  private openMenu = (event: any) => {
    this.setState({ loginMenuAnchorEl: event.currentTarget })
  }

  private openLogInModal = () => {
    this.closeMenu()
    if (this.loginRef.current) {
      this.loginRef.current.openLoginModal()
    }
  }

  private openSignUpModal = () => {
    this.closeMenu()
    if (this.loginRef.current) {
      this.loginRef.current.openCreateAccountModal()
    }
  }

  private logOff = () => {
    this.closeMenu()
    logOffUser()
  }
}

export default withStyles(styles)(LoginMenu)
