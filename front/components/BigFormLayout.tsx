import * as React from 'react'
import {
  createStyles,
  Divider,
  Typography,
  WithStyles
} from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import { globersLogoUrl } from '../services/imageService'
import { ReactNodeArray } from 'react'

const styles = () =>
  createStyles({
    root: {
      maxWidth: 800,
      margin: '0 auto', // center horizontally
      marginTop: 40
    },
    globersLogo: {
      width: 100,
      display: 'block',
      margin: '0 auto' // center horizontally
    },
    header: {
      width: '100%',
      display: 'flex'
    },
    title: {
      display: 'block',
      margin: '0 auto' // center horizontally
    }
  })

interface IProps extends WithStyles<typeof styles> {
  title: string
  children: ReactNodeArray
}

class EditMediaForm extends React.Component<IProps> {
  public render() {
    const { classes, children } = this.props
    return (
      <div className={classes.root}>
        <div className={classes.header}>
          <div className={classes.title}>
            <img src={globersLogoUrl} className={classes.globersLogo} />
            <Typography variant="h4" color="inherit" align="center">
              {this.props.title}
            </Typography>
          </div>
        </div>
        <Divider />
        {children}
      </div>
    )
  }
}

export default withStyles(styles)(EditMediaForm)
