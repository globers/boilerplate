import React from 'react'
import { createStyles, withStyles, WithStyles } from '@material-ui/core'
import Link from 'next/link'
import { IUrl } from '../../services/urlService'
import AppBarButton from './AppBarButton'
import { ButtonProps } from '@material-ui/core/Button'

const styles = () =>
  createStyles({
    link: {
      textDecoration: 'none'
    }
  })

interface IProps extends WithStyles<typeof styles> {
  url: IUrl
  themePrimaryColor: string
}

class LinkButton extends React.Component<IProps & ButtonProps> {
  constructor(props: IProps) {
    super(props)
  }

  public render() {
    const { classes, url, themePrimaryColor, ...buttonProps } = this.props

    return (
      <Link href={url.href} as={url.as}>
        <a className={classes.link}>
          <AppBarButton
            {...buttonProps}
            themePrimaryColor={themePrimaryColor}
          />
        </a>
      </Link>
    )
  }
}

export default withStyles(styles)(LinkButton)
