import * as React from 'react'
import Button from '@material-ui/core/Button/Button'
import DeleteForever from '@material-ui/icons/DeleteForever'
import ReactDropzone, { FileWithPreview } from 'react-dropzone'
import FormLabel from '@material-ui/core/FormLabel/FormLabel'
import { createStyles, WithStyles, withStyles } from '@material-ui/core'
import AddAPhoto from '@material-ui/icons/AddAPhoto'
import { FileFormat, GraphqlFile } from '../../graphql/schema/GraphqlFile'
import LoadingProgress from './loadingProgress'

const styles = () =>
  createStyles({
    formLabelDropZone: {
      padding: '10px'
    },
    dropZoneContentWhenEmpty: {
      display: 'table-cell',
      verticalAlign: 'middle'
    },
    dropZoneContentWhenEmptyInner: {
      margin: '0 auto'
    },
    dropZoneContentWhenEmptyOuter: {
      // http://www.tipue.com/blog/center-a-div/ last exemple
      display: 'table',
      position: 'absolute',
      height: '100%',
      width: '100%'
    },
    dropZoneContentWhenImage: {
      maxHeight: 'inherit',
      maxWidth: 'inherit',
      display: 'inline-block', // so div don't take full width (as for default 'block') but fit to content => trash icon is on image
      position: 'relative', // so trash icon which is set with absolute position is set relative to this div
      borderRadius: 'inherit'
    },
    dropZoneContentWhenImageCoverAndCenter: {
      height: 'inherit',
      width: 'inherit',
      maxHeight: 'inherit',
      maxWidth: 'inherit',
      position: 'relative', // so trash icon which is set with absolute position is set relative to this div
      borderRadius: 'inherit'
    },
    deleteButton: {
      zIndex: 1,
      marginTop: 10,
      padding: 0,
      top: 0,
      right: 0,
      position: 'absolute'
    },
    deleteIcon: {
      backgroundColor: 'white',
      opacity: 0.8,
      borderRadius: 4
    },
    img: {
      maxWidth: 'inherit',
      maxHeight: 'inherit',
      borderRadius: 'inherit'
    },
    imgCoverAndCenter: {
      objectFit: 'cover',
      maxWidth: 'inherit',
      maxHeight: 'inherit',
      width: 'inherit',
      height: 'inherit',
      borderRadius: 'inherit'
    }
  })

interface IProps extends WithStyles<typeof styles> {
  onImageFileDropped: (imageFile: IFileAsDropZone | null) => void
  imageFile: ImageFile | null
  messageInDropZone: string
  displayLoading: boolean
  coverAndTakeCenter?: boolean
  circle?: boolean
}

export interface IFileAsDropZone {
  type: 'IFileAsDropZone'
  fileWithPreview: FileWithPreview
}

export interface IFileAsUrl {
  type: 'IFileAsUrl'
  fileUrl: string
}

export type ImageFile = IFileAsDropZone | IFileAsUrl

class ImageDropZoneField extends React.PureComponent<IProps> {
  // I copy-pasted default styles from node_modules/react-dropzone/utils/styles then changed what I needed
  // because react-dropzone API is all default or nothing, no way to override some css properties
  private logoDropZoneCssStyles = {
    active: {
      borderStyle: 'solid',
      backgroundColor: '#eee'
    },
    accepted: {
      borderStyle: 'solid',
      borderColor: '#6c6',
      backgroundColor: '#eee'
    },
    rejected: {
      borderStyle: 'solid',
      borderColor: '#c66',
      backgroundColor: '#eee'
    },
    default: {
      maxHeight: 'inherit', // use properties of caller
      maxWidth: 'inherit',
      height: 'inherit',
      width: 'inherit',
      margin: 'auto',
      cursor: 'pointer', // display hand when cursor inside dropzone
      textAlign: 'center' as 'center', // so content that is "display: 'inline-block'" is centered inside
      borderWidth: 1,
      borderColor: '#c6c6c6',
      borderStyle: 'solid',
      backgroundColor: 'transparent',
      borderRadius: '20px'
    },
    disabled: {
      opacity: 0.5
    }
  }

  public render() {
    const { classes, coverAndTakeCenter, circle } = this.props

    const fileUrl: string | null = this.props.imageFile
      ? this.props.imageFile.type === 'IFileAsUrl'
        ? this.props.imageFile.fileUrl
        : (this.props.imageFile.fileWithPreview.preview as string)
      : null

    if (fileUrl) {
      this.logoDropZoneCssStyles.default.borderStyle = 'none'
      this.logoDropZoneCssStyles.default.borderRadius = circle ? '50%' : '0px'

      this.logoDropZoneCssStyles.default.height = coverAndTakeCenter
        ? 'inherit'
        : 'auto'
      this.logoDropZoneCssStyles.default.width = coverAndTakeCenter
        ? 'inherit'
        : 'auto'
    } else {
      this.logoDropZoneCssStyles.default.borderStyle = 'solid'
      this.logoDropZoneCssStyles.default.borderRadius = circle ? '50%' : '20px'

      this.logoDropZoneCssStyles.default.height = '100%' // take the whole space no image
      this.logoDropZoneCssStyles.default.width = '100%'
    }

    const imgClass = this.props.coverAndTakeCenter
      ? classes.imgCoverAndCenter
      : classes.img

    const imgDivWrapperClass = this.props.coverAndTakeCenter
      ? classes.dropZoneContentWhenImageCoverAndCenter
      : classes.dropZoneContentWhenImage

    return (
      <ReactDropzone
        accept="image/*"
        style={this.logoDropZoneCssStyles.default}
        acceptStyle={this.logoDropZoneCssStyles.accepted}
        activeStyle={this.logoDropZoneCssStyles.active}
        rejectStyle={this.logoDropZoneCssStyles.rejected}
        disabledStyle={this.logoDropZoneCssStyles.disabled}
        onDropAccepted={this.onImageDrop}
      >
        {fileUrl ? (
          <div className={imgDivWrapperClass}>
            {this.props.displayLoading && <LoadingProgress />}
            <Button
              className={classes.deleteButton}
              onClick={this.onDeleteLogoClick}
            >
              <DeleteForever fontSize="large" className={classes.deleteIcon} />
            </Button>
            <img className={imgClass} key={fileUrl} src={fileUrl} />
          </div>
        ) : (
          <div className={classes.dropZoneContentWhenEmptyOuter}>
            <div className={classes.dropZoneContentWhenEmpty}>
              <div className={classes.dropZoneContentWhenEmptyInner}>
                <FormLabel className={classes.formLabelDropZone}>
                  {this.props.messageInDropZone}
                </FormLabel>
                <br />
                <br />
                <AddAPhoto fontSize="large" />
              </div>
            </div>
          </div>
        )}
      </ReactDropzone>
    )
  }

  private onImageDrop = (acceptedFiles: FileWithPreview[]) => {
    if (acceptedFiles[0]) {
      const imageFile = acceptedFiles[0]

      this.props.onImageFileDropped({
        fileWithPreview: imageFile,
        type: 'IFileAsDropZone'
      })
    }
  }

  private onDeleteLogoClick = (event: any) => {
    event.preventDefault()
    this.setState({
      imageFile: null
    })
    this.props.onImageFileDropped(null)
  }
}

export async function toGraphqlFile(
  imageFile: ImageFile
): Promise<GraphqlFile> {
  switch (imageFile.type) {
    case 'IFileAsDropZone':
      return new Promise<GraphqlFile>(resolve => {
        const reader = new FileReader()
        reader.onload = () => {
          const file = new GraphqlFile(reader.result, FileFormat.BASE_64)
          resolve(file)
        }
        reader.readAsDataURL(imageFile.fileWithPreview)
      })
    case 'IFileAsUrl':
      return new GraphqlFile(imageFile.fileUrl, FileFormat.URL)
  }
}

export default withStyles(styles)(ImageDropZoneField)
