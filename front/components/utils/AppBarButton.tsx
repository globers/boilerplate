import React from 'react'
import { createStyles, withStyles, WithStyles } from '@material-ui/core'
import Button, { ButtonProps } from '@material-ui/core/Button/Button'
import { isDark } from '../../services/styleServices'

const styles = () =>
  createStyles({
    outlinedOnBlackRoot: {
      borderColor: 'white'
    },
    outlinedOnBlackLabel: {
      color: 'white'
    },
    button: {
      marginLeft: '8px',
      marginRight: '8px'
    }
  })

interface IProps extends WithStyles<typeof styles> {
  themePrimaryColor: string
}

class AppBarButton extends React.Component<IProps & ButtonProps> {
  constructor(props: IProps & ButtonProps) {
    super(props)
  }

  public render() {
    const { classes, themePrimaryColor, color, ...otherProps } = this.props

    const isDarkBackground = themePrimaryColor && isDark(themePrimaryColor)

    if (isDarkBackground) {
      if (this.props.variant === 'contained' && color === 'primary') {
        return (
          <Button
            className={classes.button}
            {...otherProps}
            size={'small'}
            color="default"
          />
        )
      } else if (this.props.variant === 'outlined') {
        return (
          <Button
            {...otherProps}
            className={classes.button}
            color={color}
            size={'small'}
            classes={{
              root: classes.outlinedOnBlackRoot,
              label: classes.outlinedOnBlackLabel
            }}
          />
        )
      } else {
        return (
          <Button
            className={classes.button}
            color={color}
            size={'small'}
            {...otherProps}
          />
        )
      }
    } else {
      return (
        <Button
          className={classes.button}
          color={color}
          size={'small'}
          {...otherProps}
        />
      )
    }
  }
}

export default withStyles(styles)(AppBarButton)
