import React from 'react'
import { CircularProgress, createStyles, WithStyles } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'

const styles = () =>
  createStyles({
    root: {
      position: 'absolute',
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)'
    }
  })

interface IProps extends WithStyles<typeof styles> {}

const LoadingProgress = (props: IProps) => (
  <div className={props.classes.root}>
    <CircularProgress />
  </div>
)

export default withStyles(styles)(LoadingProgress)
