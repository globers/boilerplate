import { WithStyles } from '@material-ui/core'
import React, { ReactNode } from 'react'
import withStyles from '@material-ui/core/styles/withStyles'

const styles = {
  articleImageDropZoneOutsideContainer: {
    width: '100%',
    paddingTop: '33.33%',
    position: 'relative' as 'relative'
  },
  articleImageDropZoneOutside: {
    position: 'absolute' as 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    display: 'flex'
  }
}

interface IProps extends WithStyles<typeof styles> {
  children: ReactNode
}

const HeaderImgResponsiveWrapper = (props: IProps) => (
  <div className={props.classes.articleImageDropZoneOutsideContainer}>
    <div className={props.classes.articleImageDropZoneOutside}>
      {props.children}
    </div>
  </div>
)

export default withStyles(styles)(HeaderImgResponsiveWrapper)
