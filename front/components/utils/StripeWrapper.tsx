import React, { ReactNode } from 'react'
// import Head from 'next/head'
import { Elements, StripeProvider } from 'react-stripe-elements'
import stripePublishableKey from '../../services/stripePublishableKey'

interface IProps {
  children: ReactNode
}

interface IState {
  stripe: any
}

class StripeWrapper extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    this.state = { stripe: null }
  }

  public componentDidMount() {
    // stripe script must be loaded only on browser, and componentDidMount is only called on browser
    // we use scriptjs to load it because we need to be notified when it's loaded so that we can
    // init stripe variable on state. it doesn't work with nextjs <Head> component, because
    // the code here: https://github.com/stripe/react-stripe-elements#loading-stripejs-asynchronously
    // suppose that the selector works, it's not the case with "Head" component on componentDidMount
    // (selector is null)
    // we also import scriptjs here because it calls "document" on import which fails on server

    const scriptjs = require('scriptjs')
    scriptjs('https://js.stripe.com/v3/', () => {
      // @ts-ignore
      this.setState({ stripe: window.Stripe(stripePublishableKey) })
    })
  }

  public render() {
    return (
      <StripeProvider stripe={this.state.stripe}>
        <Elements>{this.props.children}</Elements>
      </StripeProvider>
    )
  }
}

export default StripeWrapper
