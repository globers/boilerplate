export const isProductionEnv = process.env.IS_PRODUCTION_ENV === 'TRUE' // True if is running on appengine

export function isBrowser(): boolean {
  // @ts-ignore : process.browser nextjs field is not well defined
  return process.browser
}

export const embedlyKey = '82b7f3988ae0435eb59c86316f4027d4'
