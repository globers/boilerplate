// https://stackoverflow.com/questions/1349404/generate-random-string-characters-in-javascript/38622545
export function generateRandomString() {
  return (Math.random() + 1).toString(36)
}
