import Cookies from 'js-cookie'
import { IncomingMessage } from 'http'
import { apolloClient } from '../graphql/initApollo'
import { endOfMonth } from './dateServices'
import { isBrowser } from './environment'

const jwtTokenCookieKey = 'jwt_token'
const subscribedToNewsletterCookie = 'subscribed_to_newsletter'

export function setSubscribedToNewsletter() {
  Cookies.set(subscribedToNewsletterCookie, 'true', {
    expires: new Date(2030, 1, 1)
  })
}

export function isSubscribedToNewsletter() {
  return !!Cookies.get(subscribedToNewsletterCookie)
}

export function logInUser(jwtToken: string): void {
  Cookies.set(jwtTokenCookieKey, jwtToken, { expires: new Date(2030, 1, 1) })
  reloadApolloAndBrowser()
}

export function logInUserAndGoHome(jwtToken: string): void {
  Cookies.set(jwtTokenCookieKey, jwtToken, { expires: new Date(2030, 1, 1) })
  reloadApolloAndGoToHome()
}

export function canReadArticle(articleUri: string): boolean {
  if (!isBrowser()) {
    return true
  }
  if (Cookies.get(articleUri)) {
    return true
  }

  const endOfMonthDate = endOfMonth()
  const nbArticlesCookie = Cookies.get('nb_articles_this_month')
  const nbArticles = nbArticlesCookie
    ? parseInt(nbArticlesCookie, undefined)
    : 0

  if (nbArticles >= 3) {
    return false
  }

  Cookies.set('nb_articles_this_month', (nbArticles + 1).toString(), {
    expires: endOfMonthDate
  })

  Cookies.set(articleUri, 'true', {
    expires: endOfMonthDate
  })
  return true
}

export function logOffUser(): void {
  if (Cookies.get(jwtTokenCookieKey)) {
    Cookies.remove(jwtTokenCookieKey)
    reloadApolloAndGoToHome()
  }
}

function reloadApolloAndBrowser() {
  if (apolloClient != null) {
    apolloClient.cache
      .reset()
      .then(() => location.reload())
      .catch(() => location.reload())
  }
}

function reloadApolloAndGoToHome() {
  if (apolloClient != null) {
    apolloClient.cache
      .reset()
      .then(() => location.assign('/'))
      .catch(() => location.assign('/'))
  }
}

export function getUserJwtToken(
  requestIfServer: IncomingMessage | null | undefined
): string | null {
  let token = null
  if (requestIfServer) {
    token = getCookieValue(
      requestIfServer.headers.cookie as string,
      jwtTokenCookieKey
    )
  } else {
    token = Cookies.get(jwtTokenCookieKey)
  }

  if (token) {
    return token
  } else {
    return null
  }
}

// https://www.w3schools.com/js/js_cookies.asp
function getCookieValue(cookie: string, cname: string) {
  if (!cookie) {
    return null
  }

  const name = cname + '='
  const ca = cookie.split(';')

  // tslint:disable-next-line
  for (let i = 0; i < ca.length; i++) {
    let c = ca[i]
    while (c.charAt(0) === ' ') {
      c = c.substring(1)
    }
    if (c.indexOf(name) === 0) {
      return c.substring(name.length, c.length)
    }
  }
  return null
}
