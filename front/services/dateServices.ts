import { IGraphqlDate, toDate } from '../graphql/schema/GraphqlDate'

function isSameDay(d1: Date, d2: Date) {
  return (
    d1.getFullYear() === d2.getFullYear() &&
    d1.getMonth() === d2.getMonth() &&
    d1.getDate() === d2.getDate()
  )
}

export function formatDate(date: Date) {
  const now = new Date()

  const sameDay = isSameDay(now, date)
  if (sameDay) {
    // if today, display only hour
    const options = {
      hour: 'numeric',
      minute: 'numeric'
    }
    return date.toLocaleTimeString(undefined, options)
  } else {
    const sameYear = date.getFullYear() === now.getFullYear()
    const options = {
      year: sameYear ? undefined : 'numeric', // if this year,
      month: 'short',
      day: 'numeric'
    }
    return date.toLocaleDateString(undefined, options)
  }
}

export function formatGraphqlDate(date: IGraphqlDate) {
  return formatDate(toDate(date))
}

export function toGraphqlDate(date: Date): IGraphqlDate {
  return { nbSecFrom1970: Math.floor(date.getTime() / 1000) }
}

// https://stackoverflow.com/questions/222309/calculate-last-day-of-month-in-javascript
export function endOfMonth(): Date {
  const now = new Date()
  return new Date(now.getFullYear(), now.getMonth() + 1, 0)
}

export function TodayMinus(nbDays: number) {
  const dt = new Date()
  dt.setHours(0, 0, 0, 0) // set at midnight (or it generate new graphql request with a different parameter each call and cache is bypassed)
  dt.setDate(dt.getDate() - nbDays)
  return dt
}
