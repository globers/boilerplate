function getFormatedImageUrl(urlImage: string, transformation: string) {
  const cloudinaryPrefix = 'https://res.cloudinary.com/globers/image/upload/'

  if (urlImage.startsWith(cloudinaryPrefix)) {
    const logoUrlVersionAndPublicId = urlImage.slice(cloudinaryPrefix.length)
    urlImage =
      cloudinaryPrefix + transformation + '/' + logoUrlVersionAndPublicId
  }

  return urlImage
}

function getFormatedLogoUrl(urlLogo: string) {
  return getFormatedImageUrl(urlLogo, 'c_scale,f_auto,q_auto,w_560')
}

const globersLogoUrl = getFormatedImageUrl(
  'https://res.cloudinary.com/globers/image/upload/prod/app/logo/globers_logo_beta',
  'c_scale,f_auto,q_auto,w_560'
)
const globersIconUrl = getFormatedImageUrl(
  'https://res.cloudinary.com/globers/image/upload/prod/app/logo/globers_icon',
  'c_scale,f_auto,q_auto,w_180'
)

const noImageUrl =
  'https://res.cloudinary.com/globers/image/upload/prod/app/noimage.png'

const backgroundIndexImageUrl = getFormatedImageUrl(
  'https://res.cloudinary.com/globers/image/upload/prod/app/index/background.jpg',
  'c_scale,f_auto,q_auto,w_1920'
)

const quotemarkUrl =
  'https://res.cloudinary.com/globers/image/upload/prod/app/article/quotation_mark'

export {
  getFormatedImageUrl,
  getFormatedLogoUrl,
  globersLogoUrl,
  globersIconUrl,
  backgroundIndexImageUrl,
  quotemarkUrl,
  noImageUrl
}
