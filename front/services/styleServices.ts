// https://24ways.org/2010/calculating-color-contrast/
import memoizeOne from 'memoize-one'

function isDarkNoMemo(hexcolor: string): boolean {
  const r = parseInt(hexcolor.substr(1, 2), 16)
  const g = parseInt(hexcolor.substr(3, 2), 16)
  const b = parseInt(hexcolor.substr(5, 2), 16)
  const yiq = (r * 299 + g * 587 + b * 114) / 1000
  return yiq < 128
}

export const isDark = memoizeOne(isDarkNoMemo)
