interface IStripeAddress {
  city: string
  line1: string
  line2: string
  postal_code: string
}

interface IStripeDateOfBirth {
  day: number
  month: number
  year: number
}

interface IStripeLegalEntity {
  first_name: string
  last_name: string
  address: IStripeAddress
  dob: IStripeDateOfBirth
  type: 'individual' | 'company'
}

export interface IStripeAccountInputForToken {
  legal_entity: IStripeLegalEntity
  tos_shown_and_accepted: boolean
}

// Bank_account token returned directly from IBAN element

/*
external_account	"External accounts (bank accounts and/or cards) currently attached to this account.
"	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y
legal_entity.additional_owners	An array of any owners with more than 25% ownership of the company, excluding the individual responsible for the account	n	n	n	y	n	n	n	y	n	n	n	y	n	y	n	y	n	y	n	y	n	y	n	n	n	y	n	y	See Docs	See Docs	n	y	n	y	n	y	n	y	n	y	n	y	n	y
legal_entity.address.city	The primary address of the legal entity	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	n	y	y	y	y	y	y	y	Many	Many	y	y	y	y	y	y	y	y	y	y	y	y	y	y
legal_entity.address.line1	The primary address of the legal entity	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	n	y	y	y	y	y	y	y	Kanji	Kanji	y	y	y	y	y	y	y	y	y	y	y	y	y	y
legal_entity.address.postal_code	The primary address of the legal entity	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	n	y	y	y	y	y	y	y	and	and	y	y	y	y	y	y	y	y	y	y	y	y	y	y
legal_entity.address.state	The primary address of the legal entity	y	y	n	n	y	y	n	n	y	y	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	Kani	Kani	n	n	n	n	n	n	n	n	n	n	n	n	n	n
legal_entity.business_name	The legal name of the company	n	y	n	y	n	y	n	y	n	y	n	y	n	y	n	y	n	y	n	y	n	y	n	y	n	y	n	y	Fields	Fields	n	y	n	y	n	y	n	y	n	y	n	y	n	y
legal_entity.business_tax_id		n	y	n	y	n	y	n	y	n	y	n	y	n	y	n	y	n	y	n	y	n	y	n	y	n	y	n	y	n	y	n	y	n	y	n	y	n	y	n	y	n	y	n	y
legal_entity.dob.day	The date of birth of the individual responsible for the account	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y
legal_entity.dob.month	The date of birth of the individual responsible for the account	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y
legal_entity.dob.year	The date of birth of the individual responsible for the account	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y
legal_entity.first_name	The first name of the individual responsible for the account	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	See Docs	See Docs	y	y	y	y	y	y	y	y	y	y	y	y	y	y
legal_entity.last_name	The last name of the individual responsible for the account	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	See Docs	See Docs	y	y	y	y	y	y	y	y	y	y	y	y	y	y
legal_entity.personal_address.city	The primary address of the individual responsible for the account	n	n	n	n	n	n	n	y	n	n	n	y	n	y	n	y	n	y	n	y	n	y	n	y	n	y	n	y	See Docs	See Docs	n	y	n	y	n	y	n	y	n	y	n	n	n	y
legal_entity.personal_address.line1	The primary address of the individual responsible for the account	n	n	n	n	n	n	n	y	n	n	n	y	n	y	n	y	n	y	n	y	n	y	n	y	n	y	n	y	See Docs	See Docs	n	y	n	y	n	y	n	y	n	y	n	y	n	y
legal_entity.personal_address.postal_code	The primary address of the individual responsible for the account	n	n	n	n	n	n	n	y	n	n	n	y	n	y	n	y	n	y	n	y	n	y	n	y	n	y	n	y	See Docs	See Docs	n	y	n	y	n	y	n	y	n	y	n	y	n	y
legal_entity.personal_id_number	The ID number of the representative, as appropriate for the legal entity’s country. For example, a social security number in the US, social insurance number in Canada, etc. Instead of the number itself, you can also provide a PII token provided by Stripe.js.	n	n	n	n	n	n	n	n	y	y	n	n	n	n	n	n	n	n	n	n	n	n	y	y	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	y	y	n	n
legal_entity.ssn_last_4	The last 4 digits of the social security number of the representative of the legal entity. Only requested in the US.	y	y	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n
legal_entity.type	Either 'individual' or 'company', for what kind of legal entity the account owner is for	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y
tos_acceptance.date	Who accepted the Stripe terms of service, and when they accepted it. //  The timestamp when the account owner accepted Stripe’s terms.	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y
tos_acceptance.ip_	Who accepted the Stripe terms of service, and when they accepted it. // The IP address from which the account owner accepted Stripe’s terms.	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y	y


Additional Verification Requirements
legal_entity.personal_id_number	The government-issued ID number of the account representative.	maybe	maybe	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n	n
legal_entity.verification.document	(ID of a file upload) A photo (jpg or png) of an identifying document, either a passport or local ID card.	maybe	maybe	maybe	maybe	maybe	maybe	maybe	maybe	maybe	maybe	maybe	maybe	maybe	maybe	maybe	maybe	maybe	maybe	maybe	maybe	maybe	maybe	maybe	maybe	maybe	maybe	maybe	maybe	maybe	maybe	maybe	maybe	maybe	maybe	maybe	maybe	maybe	maybe	maybe	maybe	maybe	maybe	maybe	maybe
 */
