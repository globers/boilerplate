interface IUrl {
  as: string
  href: string
}

function getArticleUrl(mediaUri: string, articleUri: string): IUrl {
  return {
    as: `/m/${mediaUri}/a/${articleUri}`,
    href: `/article?mediaUri=${mediaUri}&articleUri=${articleUri}`
  }
}

function getEditArticleUrl(mediaUri: string, articleDraftUri: string): IUrl {
  const href = `/editArticle?mediaUri=${mediaUri}&articleDraftUri=${articleDraftUri}`

  return {
    as: href,
    href
  }
}

function getMediaUrl(mediaUri: string): IUrl {
  return {
    as: `/m/${mediaUri}`,
    href: `/media?mediaUri=${mediaUri}`
  }
}

function getMediaAdminUrl(mediaUri: string): IUrl {
  const href = `/mediaAdmin?mediaUri=${mediaUri}`

  return {
    as: href,
    href
  }
}

function getConnectBankAccountToMediaUrl(mediaUri: string): IUrl {
  const href = `/connectBankAccountToMedia?mediaUri=${mediaUri}`

  return {
    as: href,
    href
  }
}

function getSubscribeToMediaUrl(mediaUri: string): IUrl {
  const href = `/subscribe?mediaUri=${mediaUri}`

  return {
    as: href,
    href
  }
}

export {
  IUrl,
  getArticleUrl,
  getEditArticleUrl,
  getMediaUrl,
  getMediaAdminUrl,
  getConnectBankAccountToMediaUrl,
  getSubscribeToMediaUrl
}
