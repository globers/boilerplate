// @ts-ignore
import * as yes from 'yes-https' // node need this type of import to work (not import yes from yes-https)
import * as next from 'next'
import * as express from 'express'
import { Request, Response } from 'express-serve-static-core'
import { isProductionEnv } from '../services/environment'

const port = process.env.NODE_PORT || 3000
const nodeDev = process.env.NODE_ENV !== 'production' // production if server started with npm start
const app = next({ dev: nodeDev })
const handle = app.getRequestHandler()

app
  .prepare()
  .then(() => {
    const server = express()

    if (isProductionEnv) {
      // only redirect on appengine
      server.use(yes())
    }

    // redirect article/:id to article/id?:id
    server.get('/m/:mediaUri/a/:articleUri', (req: Request, res: Response) => {
      const actualPage = '/article'
      const queryParams = {
        mediaUri: req.params.mediaUri,
        articleUri: req.params.articleUri
      }
      app.render(req, res, actualPage, queryParams)
    })

    server.get('/m/:mediaUri', (req: Request, res: Response) => {
      const actualPage = '/media'
      const queryParams = {
        mediaUri: req.params.mediaUri
      }
      app.render(req, res, actualPage, queryParams)
    })

    server.get('*', (req: Request, res: Response) => {
      return handle(req, res)
    })

    server.listen(port, (err: Error) => {
      if (err) {
        throw err
      }
      // tslint:disable-next-line no-console
      console.log(`> Ready on http://localhost:${port}`)
    })
  })
  .catch((ex: any) => {
    // tslint:disable-next-line no-console
    console.error(ex.stack)
    process.exit(1)
  })
