import { ApolloClient, NormalizedCacheObject } from 'apollo-boost'
import Head from 'next/head'
import React, { ComponentType } from 'react'
import { ApolloProvider, getDataFromTree } from 'react-apollo'
import initApollo from './initApollo'
import { getUserJwtToken } from '../services/userService'
import { NextContext } from 'next'
import { isBrowser } from '../services/environment'

// Gets the display name of a JSX component for dev tools
function getComponentDisplayName<ComponentProps>(
  Component: ComponentType<ComponentProps>
) {
  // @ts-ignore, displayName is optional field on Component
  return Component.displayName || Component.name || 'Unknown'
}

// TODO : update withData with current example at nextjs/examples/apollo
// https://github.com/zeit/next.js/commit/728871b005d5b5b5a1730b02150f7d966f33d45a

export default <ComponentProps extends {}>(
  ComposedComponent: ComponentType<ComponentProps>
) => {
  return class WithData extends React.Component<ComponentProps, {}> {
    public static displayName = `WithData(${getComponentDisplayName(
      ComposedComponent
    )})`

    public static async getInitialProps(ctx: NextContext) {
      // Initial serverState with apollo (empty)
      let serverState

      // Evaluate the composed component's getInitialProps()
      let composedInitialProps = {}
      // @ts-ignore
      if (ComposedComponent.getInitialProps) {
        // @ts-ignore
        composedInitialProps = await ComposedComponent.getInitialProps(ctx)
      }

      // Run all GraphQL queries in the component tree
      // and extract the resulting data
      const apollo = initApollo(
        {},
        {
          getToken: () => getUserJwtToken(ctx.req)
        }
      )

      if (!isBrowser()) {
        // getDataFromTree does not call componentWillUnmount
        // head side effect therefore need to be cleared manually

        try {
          // create the url prop which is passed to every page
          const url = {
            asPath: ctx.asPath,
            query: ctx.query,
            pathname: ctx.pathname
          }

          // Run all GraphQL queries
          await getDataFromTree(
            // @ts-ignore
            <ComposedComponent ctx={ctx} url={url} {...composedInitialProps} />,
            {
              router: {
                asPath: ctx.asPath,
                pathname: ctx.pathname,
                query: ctx.query
              },
              client: apollo
            }
          )
        } catch (error) {
          // tslint:disable-next-line no-console
          console.error('Error in SSR:')
          // tslint:disable-next-line no-console
          console.error(error)
          // Prevent Apollo Client GraphQL errors from crashing SSR.
          // Handle them in components via the data.error prop:
          // http://dev.apollodata.com/react/api-queries.html#graphql-query-data-error
        }
        Head.rewind()
      }

      // Extract query data from the Apollo store
      serverState = {
        apollo: {
          data: apollo.cache.extract()
        }
      }

      return {
        serverState,
        ...composedInitialProps
      }
    }

    private readonly apollo: ApolloClient<NormalizedCacheObject>
    constructor(props: any) {
      super(props)
      // @ts-ignore
      this.apollo = initApollo(this.props.serverState.apollo.data, {
        getToken: () => getUserJwtToken(null)
      })
    }

    public render() {
      return (
        <ApolloProvider client={this.apollo}>
          <ComposedComponent {...this.props} />
        </ApolloProvider>
      )
    }
  }
}
