export interface IGraphqlDate {
  nbSecFrom1970: number
}

export type GraphqlDateInput = IGraphqlDate

export function toDate(graphqlDate: IGraphqlDate): Date {
  return new Date(1000 * graphqlDate.nbSecFrom1970)
}
