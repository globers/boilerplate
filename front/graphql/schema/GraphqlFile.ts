export enum FileFormat {
  BASE_64 = 'BASE_64',
  URL = 'URL'
}

export interface IGraphqlFile {
  fileFormat: FileFormat
  file: string
}

export class GraphqlFile implements IGraphqlFile {
  public readonly file: string
  public readonly fileFormat: FileFormat

  constructor(file: string, fileFormat: FileFormat) {
    this.file = file
    this.fileFormat = fileFormat
  }
}
