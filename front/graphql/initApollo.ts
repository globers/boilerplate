import {
  ApolloClient,
  NormalizedCacheObject,
  IntrospectionFragmentMatcher
} from 'apollo-boost'
import { HttpLink } from 'apollo-boost'
import { onError } from 'apollo-link-error'
import { setContext } from 'apollo-link-context'
import { InMemoryCache } from 'apollo-boost'
import apiServerUrl from './apiServerUrl'
import introspectionQueryResultData from './fragmentTypes'

// @ts-ignore
import fetch from 'isomorphic-unfetch'

let apolloClient: ApolloClient<NormalizedCacheObject> | null = null

function isClientBrowser(): boolean {
  // @ts-ignore : process.browser nextjs field is not well defined
  return process.browser
}

// Polyfill fetch() on the server (used by apollos-client)
if (!isClientBrowser()) {
  // @ts-ignore
  global.fetch = fetch
}
// tslint:disable-next-line no-console
console.log('api server url: ' + apiServerUrl)

function create(initialState: NormalizedCacheObject, { getToken }: any) {
  // authentication: https://www.apollographql.com/docs/react/recipes/authentication.html
  const authLink = setContext((_, { headers }) => {
    // get the authentication token from local storage if it exists
    const token = getToken()
    // return the headers to the context so httpLink can read them
    return {
      headers: {
        ...headers,
        authorization: token ? `Bearer ${token}` : ''
      }
    }
  })

  const httpLink = new HttpLink({
    uri: `${apiServerUrl}/graphql`, // Server URL (must be absolute)
    credentials: 'same-origin' // Additional fetch() options like `credentials` or `headers`
  })

  const errorLink = onError(({ graphQLErrors, networkError }) => {
    if (graphQLErrors) {
      graphQLErrors.map(({ message, locations, path }) => {
        // tslint:disable-next-line no-console
        console.log(
          `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
        )
      })
    }

    // TODO Display an error popup to say contact us...etc... Voir envoyer un mail ?

    if (networkError) {
      // tslint:disable-next-line no-console
      console.log(`[Network error]: ${networkError}`)
    }
  })

  const defaultOptions = {
    watchQuery: {
      errorPolicy: 'all' as 'all'
    },
    query: {
      errorPolicy: 'all' as 'all'
    },
    mutate: {
      errorPolicy: 'all' as 'all'
    }
  }

  return new ApolloClient({
    // @ts-ignore : process.browser nextjs field is not well defined
    connectToDevTools: process.browser,
    // @ts-ignore : process.browser nextjs field is not well defined
    ssrMode: !isClientBrowser(), // Disables forceFetch on the server (so queries are only run once)
    link: authLink.concat(errorLink).concat(httpLink),
    cache: new InMemoryCache({
      fragmentMatcher: new IntrospectionFragmentMatcher({
        introspectionQueryResultData
      }),
      dataIdFromObject: (object: any) => object.uri || null
    }).restore(initialState || {}),
    defaultOptions
  })
}

export default function initApollo(
  initialState: NormalizedCacheObject,
  option: any
) {
  // Make sure to create a new client for every server-side request so that data
  // isn't shared between connections (which would be bad)
  if (!isClientBrowser()) {
    return create(initialState, option)
  }

  // Reuse client on the client-side
  if (!apolloClient) {
    apolloClient = create(initialState, option)
  }

  return apolloClient
}

export { apolloClient }
