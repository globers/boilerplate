// TODO check: is it still useful with "_useless" query on graphql root Query that should solve ambiguity ?
export default {
  __schema: {
    types: [
      {
        kind: 'UNION',
        name: 'UserOrAnonymous',
        possibleTypes: [
          {
            name: 'User'
          },
          {
            name: 'Anonymous'
          }
        ]
      }
    ]
  }
}
