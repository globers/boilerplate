# Setup

* Install SDKs:
    * NodeJS 9.11 with and NPM 6.0
    * JDK 8
    * Docker
    * [Optional] Google cloud SDK (to deploy from local or for support)
* Build application:
    * `npm install` in `front` folder
    * `./gradlew build` in `back` folder
    * `docker build . -t globers-db` in `db-dev` folder
* Run tests:
    * `npm test` in `front` folder
    * `./gradlew test` in `back` folder
* Run application:
    * `docker run --publish 3306:3306 globers-db` in `db-dev` folder
    * `npm run dev` in `front` folder
    * `./gradlew bootRun` in `back` folder
* Run application inside docker (ISO production):
    * build back then run `docker-compose up` on root folder
    * check app on `http://localhost:3000/`, or api on `http://localhost:8080/graphiql/`

# Development environment

* Intellij ultimate
* MyQSL Workbench
* Google cloud SQL proxy
* Google SQL Proxy: ./cloud_sql_proxy -instances=insidoor-182517:us-east1:test-mysql-globers=tcp:3306